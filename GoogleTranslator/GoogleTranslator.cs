﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Translation.V2;
using System;
using System.Collections.Generic;

namespace Waliku.GoogleTranslator
{
    public class GoogleTranslator
    {
        //**
        //public static LanguageTranslator TranslateText(string Data, string toCulture)
        //{

        //    try
        //    {
        //      //  string Credential_Path = HttpContext.Current.Server.MapPath(@"~/ApiKey/GoogleTranslatorApiKey.json");
        //      //  System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", Credential_Path);
        //        TranslationClient client = TranslationClient.Create(model: TranslationModel.Base);
        //        TranslationResult result = client.TranslateText(Data, toCulture);

        //        LanguageTranslator Lang = new LanguageTranslator() { DetectedLanguage = result.DetectedSourceLanguage, TranslatedText = result.TranslatedText };

        //        return Lang;
        //    }
        //    catch (Exception e)
        //    {
        //        return new LanguageTranslator() { DetectedLanguage = string.Empty, TranslatedText = string.Empty};
        //    }

        //}

        public static Dictionary<string, string> TranslateText(string Data, string CurrentCulture, string ToCulture)
        {
            Dictionary<string, string> Dict = new Dictionary<string, string>();

            if (Data.Trim() == string.Empty)
            {
                Dict[CurrentCulture] = string.Empty;
                Dict[ToCulture] = string.Empty;
                return Dict;
            }

            try
            {
                Dict[CurrentCulture] = Data;
                using (TranslationClient client = TranslationClient.Create(model: TranslationModel.Base))
                {
                    TranslationResult result = client.TranslateText(Data, ToCulture);
                    Dict[ToCulture] = result.TranslatedText;
                }

                return Dict;
            }
            catch (Exception e)
            {
                Dict[ToCulture] = Data;
                return Dict;
            }

        }

       
    }
}
