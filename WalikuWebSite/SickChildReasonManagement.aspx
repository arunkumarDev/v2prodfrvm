﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SickChildReasonManagement.aspx.cs" Inherits="WalikuWebSite.SickChildReasonManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <style type="text/css">
        .table-responsive {
            overflow-x: inherit;
        }
        .modal-dialog {
        width: 100%;
        height: 100%;
        margin: 0;
        padding: 0;
        }

        .modal-content {
        height: auto;
        min-height: 100%;
        border-radius: 0;
        }
    </style>
    <!-- Modal -->
    <div id="SickChildReasonManagementModel" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>Add Reason</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal">
                        <input type="hidden" id="hdnReasonID" value="0" />
                        <div class="form-group">
                            <label for="txtTitle" class="control-label col-lg-2">Title</label>

                            <div class="col-lg-10">
                                <input type="text" id="txtTitle" placeholder="Title" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtContent" class="control-label col-lg-2">Content</label>

                            <div class="col-lg-10">
                                <textarea id="txtContent" placeholder="Title" class="form-control"></textarea>
                                <%--<input type="text" id="txtClassName" placeholder="Title" class="form-control">--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="txtContent" class="control-label col-lg-2">Content Indian</label>

                            <div class="col-lg-10">
                                <textarea id="txtContentIndian" placeholder="Title" class="form-control"></textarea>
                                <%--<input type="text" id="txtClassName" placeholder="Title" class="form-control">--%>
                            </div>
                        </div>
                         <!-- /.form-group -->
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveReason">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
    <div id="content">
        <div class="outer">
            <div class="inner bg-light lter">
                <!--Begin Datatables-->
                <div class="row">
                    <div class="col-lg-12 ui-sortable">
                        <div class="box ui-sortable-handle">
                            <header>
                                <div class="icons"><i class="fa fa-table"></i></div>
                                <h5>Sick Child Reasons</h5>
                                <div class="toolbar">
                                    <button type="button" class="btn btn-default btn-sm btn-metis-6" data-toggle="modal" data-target="#SickChildReasonManagementModel">Add Reason</button>
                                </div>
                            </header>
                            <div id="collapse4" class="body">
                                <div id="dataTable_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

                                    <div class="row">
                                        <div id="dvUsers" class="col-sm-12 table-responsive">
                                            <table id="dataTable" class="table table-bordered table-condensed table-hover table-striped dataTable no-footer" role="grid" aria-describedby="dataTable_info">
                                                <thead>
                                                    <tr role="row">
                                                        <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 50px;">ID</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 235px;">Title</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 235px;">Content</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 235px;">Content Indian</th>
                                                        <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="CSS grade: activate to sort column ascending" style="width: 108px;">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div style="display: none" class="row">
                                        <div class="col-sm-5">
                                            <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button previous disabled" id="dataTable_previous"><a href="#" aria-controls="dataTable" data-dt-idx="0" tabindex="0">Previous</a></li>
                                                    <li class="paginate_button active"><a href="#" aria-controls="dataTable" data-dt-idx="1" tabindex="0">1</a></li>
                                                    <li class="paginate_button "><a href="#" aria-controls="dataTable" data-dt-idx="2" tabindex="0">2</a></li>
                                                    <li class="paginate_button "><a href="#" aria-controls="dataTable" data-dt-idx="3" tabindex="0">3</a></li>
                                                    <li class="paginate_button "><a href="#" aria-controls="dataTable" data-dt-idx="4" tabindex="0">4</a></li>
                                                    <li class="paginate_button "><a href="#" aria-controls="dataTable" data-dt-idx="5" tabindex="0">5</a></li>
                                                    <li class="paginate_button "><a href="#" aria-controls="dataTable" data-dt-idx="6" tabindex="0">6</a></li>
                                                    <li class="paginate_button next" id="dataTable_next"><a href="#" aria-controls="dataTable" data-dt-idx="7" tabindex="0">Next</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!--End Datatables-->
            </div>
            <!-- /.inner -->
        </div>
        <!-- /.outer -->
    </div>

    <script src="Scripts/SickChildReasonManagement.js"></script>
</asp:Content>
