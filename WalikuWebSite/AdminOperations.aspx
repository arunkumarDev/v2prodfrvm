﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="AdminOperations.aspx.cs" Inherits="WalikuWebSite.AdminOperations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
   <%-- <div id="AssignTeacherModel" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md custom-material">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Lbl_AssignTeacher %></h4>
            </div>
            <div class="modal-body">
                <form class="custom-material">
                     <input type="hidden" id="hdnClassTeacherID" value="0" />
                    <div class="col-md-12 pr-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectClass  %> </label>
                                    <select id="ddlClassName" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectTeacher %> </label>
                                    <select id="ddlTeacherName" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal" > <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveClassTeacher"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>--%>

    <!-- Modal -->
    <main class="l-main">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
             <%--   <div class="col-md-12 segmented-progress"> 
                    <div class="segment-save"></div>
                </div>--%>
                    <div class="col-md-3"></div>
                     <div class="md-radiobox" style="width:10px;position: fixed;left:26%;" >
                                            <label>
                                                <input type="radio" name="rdoConfigurationData" id="rdoStudentConfig" value="0">
                                                <span class="md-radiobox-material">
                                                    <span class="check"></span>
                                                </span><%=Resources.Resource.Rdo_ConfigureStudent %>
                                            </label>
                                        </div>
                    <div class="col-md-3 studentConfigBG" style="height:25px;">
                   </div>
                    <div class="col-md-3 studentFormBG" style="height:25px;">
                        <div class="md-radiobox" style="width:10px;position: fixed;left:51%;">
                                            <label>
                                                <input type="radio" name="rdoConfigurationData" id="rdoStudentForm" value="1">
                                                <span class="md-radiobox-material">
                                                    <span class="check"></span>
                                               </span><%=Resources.Resource.Rdo_StudentForm %>
                                            </label>
                                        </div>
                        </div>
                    <div class="col-md-1 studentListBG" style="height:25px;width:10px;">
                        <div class="md-radiobox col-md-1 ">
                                            <label>
                                                <input type="radio" name="rdoConfigurationData" id="rdoStudentsList" value="2">
                                                <span class="md-radiobox-material">
                                                    <span class="check"></span>
                                                </span><%=Resources.Resource.Rdo_StudentsList %>
                                            </label>
                                        </div>
                        </div>
                      
                
            </div>
            <div class="page-content">
                        <div class="row">
                        <div class="col-sm-12">
                            <div class="configuration-details">
                                <div class="configuration_data">
                                    <div class="bmd-form-group">
                                        <div class="col-md-12">
                                            <div id= "configDataGroup">
                                         
                                            </div>
                                        </div>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="panel-footer">
                        <div class="col-md-9"></div>
                        <input id="btnSave" type="button" value="Save" class="btn btn-group btn-success" />
                        <input id="btnCancel" type="button" value="Delete" class="btn btn-group btn-default" />
                    </div>
            </div>
        </div>
    </main>
    <script src="Scripts/Common.js?0"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/AdminOperations.js?v=" + JsVerion + "'><\/script>");</script>
    <script>
    </script>
</asp:Content>

