﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ClassTeacherManagement.aspx.cs" Inherits="ClassTeacherManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            
        <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header" ><%=Resources.Resource.Menu_AssignMobileLogin %></h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 mb-5 text-right">
                    <button type="button" class="btn btn-primary " title="" data-toggle="modal" data-target="#AssignTeacherModel">
                        <%=Resources.Resource.Menu_AssignMobileLogin %></button>
                </div>
            </div>

            <div class="page-content">
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:60px;"><%=Resources.Resource.Lbl_SerialNo %></th>
                                            <th style="width:300px;"><%=Resources.Resource.Lbl_ClassName %></th>
                                            <th><%=Resources.Resource.Lbl_TeacherName %></th>
                                            <th style="width:100px;"><%=Resources.Resource.Lbl_Action %></th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


    </div>
        </main>

      <div id="AssignTeacherModel" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-md custom-material">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Menu_AssignMobileLogin %></h4>
            </div>
            <div class="modal-body">
                <form class="custom-material">
                     <input type="hidden" id="hdnClassTeacherID" value="0" />
                    <div class="col-md-12 pr-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectClass  %> </label>
                                    <select id="ddlClassName" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectTeacher %> </label>
                                    <select id="ddlTeacherName" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal" > <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveClassTeacher"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/ClassTeacherManagement.js?v=" + JsVerion + "'><\/script>");</script>

    <script>
        var Lbl_SelectTeacher = '<%=Resources.Resource.Lbl_SelectTeacher %>'
         var Lbl_SelectClass = '<%=Resources.Resource.Lbl_SelectClass %>'
    </script>

    </asp:Content>