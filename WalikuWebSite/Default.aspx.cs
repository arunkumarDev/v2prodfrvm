﻿
using Microsoft.ApplicationInsights.Web;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.App_Start;
using WalikuWebSite.DataAccess;
using WalikuWebSite.Entities.DataModels;


namespace WalikuWebSite
{
    public partial class Default : System.Web.UI.Page
    {

        public  int UserID = 0;
        public int uType = 0;
        public bool LoggedInMobile = false;
        string Lang = string.Empty;
        //public int uType1 = 0;
        //New

        public string cmpclass = "false";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                HttpContext.Current.Session["uType1"] = 0;
                HttpContext.Current.Session["divtest"] = "false";

                HttpContext.Current.Session["cmpclass"] = "false";
                if (Request.QueryString["auth_key"] != null && !string.IsNullOrEmpty(Request.QueryString["auth_key"].ToString()))
                {
                    UserID = Convert.ToInt32(Request.QueryString["auth_key"].ToString());
                    LoggedInMobile = true;
                }
                if (Request.QueryString["Lang"] != null && !string.IsNullOrEmpty(Request.QueryString["Lang"].ToString()))
                {
                    Lang = Request.QueryString["Lang"].ToString().Trim();
                }


                if (!Common.CommonFunction.IsUserLoggedIn() && UserID == 0)
                {
                    Response.Redirect("login");
                }

                //if (UserID == 0)
                //{
                //    UserID = Common.CommonFunction.GetUserIdFromCookieByID();
                //}

                //if (UserID > 0)
                //{
                //    //   UserID = Common.CommonFunction.GetUserIdFromCookieByID();
                //    var myUser = SqlHelper.GetUserByID(UserID);

                //    if (myUser.Tables[0].Rows.Count > 0)
                //    {
                //        uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
                //        if (LoggedInMobile)
                //        {
                //            Common.CommonFunction.CreateCookie(UserID, true, "en-us");
                //        }
                //    }
                //    else
                //    {
                //        uType = 0;
                //        Response.Write("Unauthorized Access");
                //        Response.End();
                //    }
                //}


            }
            if(UserID == 0)
            {
                UserID = Common.CommonFunction.GetUserIdFromCookieByID();
            }

        //    UserID = Common.CommonFunction.GetUserIdFromCookieByID();
            var myUser = SqlHelper.GetUserByID(UserID);
            if (myUser.Tables[0].Rows.Count > 0)
            {
                uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
                if (LoggedInMobile)
                {
                    Common.CommonFunction.CreateCookie(UserID, true, Lang);
                }
            }



        }

        [WebMethod]
        public static List<ChildrenMaster> GetAllChildrenForSchool(int SchoolID, int ClassID)
        {
            var UserID = Common.CommonFunction.GetUserIdFromCookieByID();
            return SqlHelper.GetAllChildrenForMeByFilter(UserID,1,SchoolID,ClassID,"");
        }

        [WebMethod]
        public static List<Notifications> LoadNotifications(DateTime SelectedDate)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var UserID = Common.CommonFunction.GetUserIdFromCookieByID();
            return NotificationsDataAccess.GetNotificationsForAdmin(UserID,dateTime);
        }
        
        
        [WebMethod]
        public static List<AbsentOrPresentMasterPacked> GetClassPresents(string ClassId, DateTime SelectedDate)
        {
            var allMonth = Reports.TeacherReport.GetClassPresentsOrAbsentsChildren(ClassId, SelectedDate);
            var mapped = new Dictionary<string, AbsentOrPresentMasterPacked>();
            foreach (var a in allMonth)
            {
                AbsentOrPresentMasterPacked packed;
                if (!mapped.TryGetValue("" + a.ID, out packed))
                {
                    packed = new AbsentOrPresentMasterPacked();
                    packed.ID = a.ID;
                    packed.Name = string.Join(" ", a.Name, a.Middlename, a.Surname);
                    packed.Presences = new List<AbsentOrPresentMaster>();
                    mapped.Add("" + a.ID, packed);
                }

                // Aggiungiamo questo giorno ad l
                packed.Presences.Add(a);
            }

            return mapped.Values.ToList();
        }


        [WebMethod]
        public static List<ClassMaster> GetClassRoomForMe()
        {
            //var udata = SqlHelper.GetUserByID(Common.CommonFunction.GetUserIdFromCookieByID());
            //if (int.Parse(udata.Tables[0].Rows[0]["UserType"].ToString()) == 1)
            //    return SqlHelper.GetAllClasses();
            return SqlHelper.GetAllTeacherClasses(Common.CommonFunction.GetUserIdFromCookieByID());
        }

        [WebMethod]
        public static List<ChildrenMaster> GetStudentsAddedorDeletedList(int ClassId, DateTime SelectedDate, int SelectedType, int MenuType)
        {
            HttpContext.Current.Session["uType1"] = MenuType;
            //uType1 = MenuType;
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID(); // UserID;
            return Reports.TeacherReport.GetStudentsAddedorDeletedList(ClassId, dateTime, SelectedType, TeacherId, MenuType);
            //return Reports.TeacherReport.GetTrackStudentsMonthly(dateTime);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReport(int ClassId, DateTime SelectedDate, int SelectedType, int MenuType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID(); // UserID;
            return Reports.TeacherReport.GetClassWisePresentAbsentReport(ClassId, dateTime, SelectedType, TeacherId, MenuType);
        }
        


        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdmin(int SchoolId, int ClassId, DateTime SelectedDate, int SelectedType, int MenuType)
        {


            HttpContext.Current.Session["SchoolID"] = SchoolId;
            HttpContext.Current.Session["uType1"] = MenuType;
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetClassWisePresentAbsentReportForAdmin(SchoolId, ClassId, dateTime, SelectedType, TeacherId, MenuType);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetDashboardForChild(int SchoolId, int ClassId, DateTime SelectedDate, int SelectedType, int MenuType = 0, int ChildID = 0)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetClassWisePresentAbsentReportForAdmin(SchoolId, ClassId, dateTime, SelectedType, TeacherId, MenuType, ChildID);
        }

        [WebMethod]
        public static List<VmChildrenReport> GetStudentWisePresentAbsentReport(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetStudentWisePresentAbsentReport(ClassId, dateTime, SelectedType,TeacherId);
        }
        
        [WebMethod]
        public static List<VmChildrenReport> GetAdminStaffSummaryMonthPrintout(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");

            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetAdminStaffSummaryMonthPrintout(ClassId, dateTime, SelectedType, TeacherId);
        }
        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportOnload(DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId =  Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetClassWisePresentAbsentReportOnload(dateTime, SelectedType, TeacherId);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummary(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyAttendanceRateSummary(ClassId, dateTime, SelectedType, TeacherId);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForAdmin(int SchoolId,int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId =  Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyAttendanceRateSummaryForAdmin(SchoolId, ClassId, dateTime, SelectedType, TeacherId);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForSA(int SchoolId, int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyAttendanceRateSummaryForSA(SchoolId,ClassId, dateTime, SelectedType, TeacherId);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyReasonReportRate(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId =  Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyReasonReportRate(ClassId, dateTime, SelectedType, TeacherId);
        }


        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetReasonSummary(int ClassId, DateTime SelectedDate, int SelectedType, int MenuType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetReasonSummary(ClassId, TeacherId, dateTime, SelectedType, MenuType);
        }
        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetReasonSummaryForGrid(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetReasonSummary(ClassId, TeacherId, dateTime, SelectedType, 0);
        }
        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetReasonSummaryForChild(int ClassId, DateTime SelectedDate, int SelectedType, int MenuType, int ChildID = 0)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetReasonSummary(ClassId, TeacherId, dateTime, SelectedType, MenuType, ChildID);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsent(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyStudentAbsent(ClassId, dateTime, SelectedType, TeacherId);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsentForSA(int SchoolId,int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyStudentAbsentForSA(SchoolId,ClassId, dateTime, SelectedType, TeacherId);
        }

        [WebMethod]
        public static List<ChartMaster.HolidayResponse> GetHolidayList(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            return Reports.TeacherReport.GetHolidayList(ClassId, dateTime, SelectedType);
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyReasonCount(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId =  Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyReasonCount(ClassId, dateTime, SelectedType, TeacherId, Common.CommonFunction.GetCurrentCulture());
        }

        [WebMethod]
        public static Dictionary<string, List<ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountNew(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyReasonCountNew(ClassId, dateTime, SelectedType, TeacherId, Common.CommonFunction.GetCurrentCulture());
        }

        [WebMethod]
        public static Dictionary<string, List<ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountForSA(int SchoolId,int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyReasonCountForSA(SchoolId,ClassId, dateTime, SelectedType, TeacherId, Common.CommonFunction.GetCurrentCulture());
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyReasonCount1ForAdmin(int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyReasonCount(ClassId, dateTime, SelectedType, TeacherId, Common.CommonFunction.GetCurrentCulture());
        }

        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetMonthlyReasonCount2(DateTime SelectedDate)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId =  Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyReasonCount2(dateTime);
        }

        [WebMethod]
        public static string GetYearlyHealthReason(int SchoolId, int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetYearlyHealthReason(SchoolId, ClassId, dateTime, SelectedType, TeacherId, Common.CommonFunction.GetCurrentCulture());
        }

        [WebMethod]
        public static string GetYearlyNonHealthReason(int SchoolId, int ClassId, DateTime SelectedDate, int SelectedType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetYearlyNonHealthReason(SchoolId, ClassId, dateTime, SelectedType, TeacherId, Common.CommonFunction.GetCurrentCulture());
        }
        [WebMethod]
        public static List<ChartMaster.AcademicYearResponse> GetCompareClassAcademicYear(int SchoolId,int MenuType)
        {
            HttpContext.Current.Session["uType1"] = MenuType;
            HttpContext.Current.Session["divtest"] = "true";
            return Reports.TeacherReport.GetCompareClassAcademicYear(SchoolId);
        }
        // Added by arun for yearly reports included academic year paramterts
        [WebMethod]
        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdminAcademicYear(int SchoolId, int ClassId, DateTime SelectedDate, int SelectedType, int MenuType,string AcademicYear)
        {
            HttpContext.Current.Session["uType1"] = MenuType;
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetClassWisePresentAbsentReportForAdminAcademicYear(SchoolId, ClassId, dateTime, SelectedType, TeacherId, AcademicYear, MenuType );
        }
        // Added by arun for Genderyearly reports 
        [WebMethod]
        public static List<ChartMaster.AcademicYearGenderResponse> GetClassWiseGenderReportForAdminAcademicYear(int SchoolId,string GenderSelectedType)
        {
            
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetClassWiseGenderReportForAdminAcademicYear(SchoolId, GenderSelectedType);
        }
        // Added by arun for ABSENTISM (MONTH) reports 
        [WebMethod]
        public static List<ChartMaster.AbsentismMonthResponse> GetMonthlyAbsentismForReports(int SchoolId, DateTime SelectedDate)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetMonthlyAbsentismForReports(SchoolId, dateTime);
        }
        [WebMethod]
        public static List<ChartMaster.LoadAbsentDetailsForChildResponse> LoadAbsentDetailsForChild(string SelMonth,string SelYear, int ChildID)
        {

            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.LoadAbsentDetailsForChild(Convert.ToInt32(SelMonth), Convert.ToInt32(SelYear),ChildID);
        }
        [WebMethod]
        public static List<ChartMaster.LoadYearlyTopReasonDetails> GetYearlyTopReasonDetails(int ClassId, DateTime SelectedDate, int SelectedType, int MenuType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetYearlyTopReasonDetails(ClassId, TeacherId, dateTime, SelectedType, MenuType);
        }

        [WebMethod]
        public static List<ChartMaster.LoadMonthlyStudentDetails> GetTrackStudentsMonthly(int SchoolId,int ClassId, DateTime SelectedDate, int SelectedType, int MenuType)
        {
            HttpContext.Current.Session["uType1"] = MenuType;
            //uType1 = MenuType;
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID(); // UserID;
            return Reports.TeacherReport.GetTrackStudentsMonthly(dateTime,SchoolId);
        }
        [WebMethod]
        public static List<VmChildrenReport> GetYearlychronicList(int ClassId, DateTime SelectedDate, int SelectedType,string ReasonType)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");

            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetYearlychronicList(ClassId, dateTime, SelectedType, TeacherId, ReasonType);
        }

        [WebMethod]
        public static List<ChartMaster.LoadConsecutiveresponse> LoadConsecutiveDaysAbsentMonthly(DateTime SelectedDate,int SchoolId,int ClassId)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.LoadConsecutiveDaysAbsentMonthly(dateTime,TeacherId,SchoolId,ClassId);
        }

        
         [WebMethod]
        public static List<ChartMaster.LoadStaffSummaryresponse> LoadMonthlyStaffAttendanceRateSummaryForLabel(DateTime SelectedDate, int SchoolId)
        {
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.LoadMonthlyStaffAttendanceRateSummaryForLabel(SchoolId, dateTime);
        }

        [WebMethod]
        public static List<ChartMaster.LoadAcademicMonthDetails> GetAcademicMonthDetails(int SchoolId)
        {
           
            var TeacherId = Common.CommonFunction.GetUserIdFromCookieByID();
            return Reports.TeacherReport.GetAcademicMonthDetails(SchoolId);
        }

        [WebMethod]
        public static VmResponseData ManageNotificationDetails(DateTime SelectedDate, int notificationCount,string NotifyText)
        {
            VmResponseData response = new VmResponseData();
            try
            {
                

                NotificationsDataAccess.ManageNotificationDetails(new NotificationDetails()
                {
                    Id = 0,
                    NotifyText = NotifyText,
                    NotifyCount = notificationCount,
                    UserId = Common.CommonFunction.GetUserIdFromCookieByID(),
                    CreatedOn = DateTime.Now,
                    CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID()
                });
                response.Message = "Success";
            }
           
            catch (Exception ex)
            {
                response.Message = "Failed";

            }
            return response;
        }

    }
}
