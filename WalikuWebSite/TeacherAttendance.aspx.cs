﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.Entities.DataModels;
using WalikuWebSite.DataAccess;
using static WalikuWebSite.App_Start.ChartMaster;

namespace WalikuWebSite
{
    public partial class TeacherAttendance : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
            }
        }

        [WebMethod]
        public static List<TeacherAttendanceMaster> GetTeacherAttendanceList()
        {
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            return TeacherAttendanceDataAccess.GetTeacherAttendanceList(uid);
        }
        [WebMethod]
        public static List<TeacherSubmittedDetailsResponse> GetTeacherAttendanceList1(DateTime SelectedDate, int SchoolId)
        {
            var uid = Common.CommonFunction.GetUserIdFromCookieByID();
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");

            return TeacherAttendanceDataAccess.GetTeacherAttendanceList1(dateTime, SchoolId);
        }

        [WebMethod]
        public static string SaveStaffAttendance(List<TeacherAttendanceMaster> StaffList)
        {
            try
            {
                var uid = Common.CommonFunction.GetUserIdFromCookieByID();

                if(StaffList[0].ID == 0)
                {
                    TeacherAttendanceDataAccess.SaveStaffAttendance(uid, StaffList);
                    return "success";
                }
                else
                {
                    TeacherAttendanceDataAccess.UpdateStaffAttendance(uid, StaffList);
                    return "success";
                }


               
            }
            catch(Exception e)
            {
                return "failure";
            }
        }

        [WebMethod]
        public static List<TeacherDateDetailsResponse> GetStaffAttendanceDateDetails(DateTime SelectedDate,int SchoolId)
        {
           
            string dateTime = SelectedDate.ToString("yyyy'-'MM'-'dd");
            return TeacherAttendanceDataAccess.GetStaffAttendanceDateDetails(dateTime, SchoolId);
        }


    }
}