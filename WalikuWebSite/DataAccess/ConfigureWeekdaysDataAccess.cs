﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WalikuWebSite.Entities.DataModels;

namespace WalikuWebSite.DataAccess
{
    public class ConfigureWeekdaysDataAccess
    {
        public static List<ConfigureWeekdays> LoadConfigureWeekdays()
        {
            SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
            DataSet ds = new DataSet();
            try
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.LoadConfigureWeekdays), sqlCnn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds, "ConfigureWeekdays");

                adapter.Dispose();
                sqlCmd.Dispose();
                sqlCnn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<ConfigureWeekdays> lst = ds.Tables[0].ToList<ConfigureWeekdays>();
                    return lst;
                }
                return new List<ConfigureWeekdays>();
            }
            catch (Exception ex)
            {
                return new List<ConfigureWeekdays>();
            }
        }
    }
}