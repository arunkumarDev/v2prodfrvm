﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WalikuWebSite.App_Start;
using WalikuWebSite.Entities.DataModels;

namespace WalikuWebSite.DataAccess
{
    public class NotificationsDataAccess
    {
        public static string ConnectionString = Common.SqlQueries.CONNECTION_STRING;

        public static List<Notifications> GetNotificationsForAdmin(int uid,string dateTime)
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {

                DataSet ds = new DataSet();
                sqlCnn.Open();
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetNotificationsForAdmin, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = uid;
                    sqlCmd.Parameters.Add(new SqlParameter("@AttendanceDate", SqlDbType.Date)).Value = dateTime;

                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "Academic Term");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<Notifications> lst = ds.Tables[0].ToList<Notifications>();
                        return lst;
                    }

                }

            }

            return new List<Notifications>();
            
        }

        
        public static void ManageNotificationDetails(NotificationDetails ObjDetails)
        {
            using (SqlConnection sqlCnn = new SqlConnection(ConnectionString))
            {
                //sqlCnn.Open();
                //using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.ManageNotificationDetails, sqlCnn))
                //{
                //    sqlCmd.CommandType = CommandType.StoredProcedure;
                //    sqlCmd.Parameters.Add(new SqlParameter("@ID", SqlDbType.Int)).Value = ObjDetails.Id ;
                //    sqlCmd.Parameters.Add(new SqlParameter("@NotifyText", SqlDbType.VarChar)).Value = ObjDetails.NotifyText;
                //    sqlCmd.Parameters.Add(new SqlParameter("@NotifyCount", SqlDbType.Int)).Value = ObjDetails.NotifyCount;
                //    sqlCmd.Parameters.Add(new SqlParameter("@UserId", SqlDbType.Int)).Value = ObjDetails.UserId;                   
                //    sqlCmd.Parameters.Add(new SqlParameter("@CreatedBy", SqlDbType.Int)).Value = ObjDetails.CreatedBy;
                //    sqlCmd.Parameters.Add(new SqlParameter("@CreatedOn", SqlDbType.DateTime)).Value = ObjDetails.CreatedOn;

                //    sqlCmd.Connection = sqlCnn;
                //    sqlCmd.ExecuteNonQuery();

                //}
            }
        }



    }
}