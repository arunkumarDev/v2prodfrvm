﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WalikuWebSite.Entities.DataModels;
using static WalikuWebSite.App_Start.ChartMaster;
using dac = WalikuWebSite.EntityFrameworkDAC;

namespace WalikuWebSite.DataAccess
{
    public class TeacherAttendanceDataAccess
    {
        static string ConnectionString = Common.SqlQueries.CONNECTION_STRING;

        public static List<TeacherAttendanceMaster> GetTeacherAttendanceList(int UserId)
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                sqlCnn.Open();

                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetTeacherAttendanceList, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = UserId;

                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "TeacherAttendanceList");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<TeacherAttendanceMaster> lst = ds.Tables[0].ToList<TeacherAttendanceMaster>();
                        return lst;
                    }

                }
            }
            return new List<TeacherAttendanceMaster>();

        }

        public static List<TeacherSubmittedDetailsResponse> GetTeacherAttendanceList1(string SelectedDate, int SchoolId)
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                sqlCnn.Open();

                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetTeacherAttendanceList1, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@Attendancedate", SqlDbType.VarChar)).Value = SelectedDate;
                    sqlCmd.Parameters.Add(new SqlParameter("@SchoolID", SqlDbType.Int)).Value = SchoolId;

                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "TeacherAttendanceList");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<TeacherSubmittedDetailsResponse> lst = ds.Tables[0].ToList<TeacherSubmittedDetailsResponse>();
                        return lst;
                    }

                }
            }
            return new List<TeacherSubmittedDetailsResponse>();

        }
        public static void SaveStaffAttendance(int UserId, List<TeacherAttendanceMaster> StaffList)
        {
            var StaffListEntity = new List<dac.TeacherAttendanceMaster>();
            foreach (var staff in StaffList)
            {
                StaffListEntity.Add(new dac.TeacherAttendanceMaster()
                {
                    ID = staff.ID,
                    AttendanceDate = staff.AttendanceDate,
                    CreatedBy = UserId,
                    CreatedOn = DateTime.Now,
                    IsPresent = staff.IsPresent,
                    IsSubmitted = staff.IsSubmitted,
                    UserID = staff.UserID,
                    SchoolID = staff.SchoolID,
                    ReasonTypeID = staff.ReasonTypeID
                });
            }
            try
            {
                dac.StaffAttendanceDAC.SaveStaffAttendance(UserId, StaffListEntity);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public static void UpdateStaffAttendance(int UserId, List<TeacherAttendanceMaster> StaffList)
        {
            var StaffListEntity = new List<dac.TeacherAttendanceMaster>();
            foreach (var staff in StaffList)
            {
                StaffListEntity.Add(new dac.TeacherAttendanceMaster()
                {
                    ID = staff.ID,
                    AttendanceDate = staff.AttendanceDate,
                    CreatedBy = UserId,
                    CreatedOn = DateTime.Now,
                    IsPresent = staff.IsPresent,
                    IsSubmitted = staff.IsSubmitted,
                    UserID = staff.UserID,
                    SchoolID = staff.SchoolID,
                    ReasonTypeID = staff.ReasonTypeID
                });
            }
            try
            {
                dac.StaffAttendanceDAC.UpdateStaffAttendance(UserId, StaffListEntity);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public static List<TeacherDateDetailsResponse> GetStaffAttendanceDateDetails(string SelectedDate,int SchoolId)
        {


            using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {

                DataSet ds = new DataSet();
                SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetStaffAttendanceDateDetails, SelectedDate,SchoolId), con);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds);

                adapter.Dispose();
                sqlCmd.Dispose();
                con.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<TeacherDateDetailsResponse> lst = ds.Tables[0].ToList<TeacherDateDetailsResponse>();
                    return lst;
                }
                return new List<TeacherDateDetailsResponse>();


            }



        }



    }
}