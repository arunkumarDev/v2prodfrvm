﻿using System.Data;
using System.Data.SqlClient;

namespace WalikuWebSite.DataAccess
{
    public class ChildrenDataAccess
    {
        public static VmChildren GetChildDetailsByID(int ChildID)
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetChildDetailsByID, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.AddWithValue("@ChildID", ChildID);                  
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = sqlCmd;
                    adapter.Fill(ds, "Child");
                    adapter.Dispose();
                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        return ds.Tables[0].ToList<VmChildren>()[0];
                    }

                }
                return new VmChildren();
            }
           
        }
    }
}