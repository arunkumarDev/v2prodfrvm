﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace WalikuWebSite.DataAccess
{
    public class UserDataAccess
    {
        public static List<UserMaster> GetMyUserList(int UserId,string LangID)
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                DataSet ds = new DataSet();
                sqlCnn.Open();

              
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetMyUserList, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;
                    sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = UserId;
                    sqlCmd.Parameters.Add(new SqlParameter("@LangID", SqlDbType.Char)).Value = LangID;


                    using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                    {
                        adapter.Fill(ds, "UserList");
                    }

                    sqlCmd.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<UserMaster> lst = ds.Tables[0].ToList<UserMaster>();
                       // foreach (var l in lst)
                         //   l.Password = "";
                        return lst;
                    }
                    
                }
            }
            return new List<UserMaster>();

        }
    }
}