﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;

namespace WalikuWebSite
{
    public partial class AddHolidays : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static VmResponseData InsertHolidays(string holidayname)
        {
            VmResponseData response = new VmResponseData();
            try
            {

                AcademicTermDataAccess.InsertHolidays(holidayname);
                response.Message = "Success";
            }
            catch (SqlException SqlEx)
            {
                if (SqlEx.Number == 2601)
                    response.Message = "Term Details Already Set For this school";
            }
            catch (Exception ex)
            {
                response.Message = "Failed";

            }
            return response;
        }

        //[WebMethod]
        //public static void InsertHolidays(string holidayname)
        //{
        //    SqlHelper.InsertHolidays(holidayname);
        //}
    }
}