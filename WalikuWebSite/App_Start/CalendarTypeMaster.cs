﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Calendar Event Type
/// </summary>
public class CalendarTypeMaster
{
    public int ID { get; set; }
    public string EventType { get; set; }
    public string EventType_Indonesian { get; set; }
    public int IsActive { get; set; }
}