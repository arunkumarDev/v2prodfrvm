﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for School Academic Calendar
/// </summary>
public class SchoolAcademicCalendarMaster
{
    public int ID { get; set; }
    public int AcademicYearId { get; set; }
    public int SchoolId { get; set; }
    public string Name { get; set; }
    public string Name_Indonesian { get; set; }
    public int CalendarTypeId { get; set; }
    public DateTime CalendarDate { get; set; }
    public int IsActive { get; set; }
    public int CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; } = DateTime.MaxValue;
    public string EventType { get; set; }
    public DateTime CalendarToDate { get; set; }
    public List<DateTime> calendarDate = new List<DateTime>();
    public List<DateTime> calendarToDate = new List<DateTime>();
    public List<string> calendarEventName = new List<string>();
   
}