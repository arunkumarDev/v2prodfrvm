﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.App_Start
{
    public class AbsentOrPresentMasterPacked
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public List<AbsentOrPresentMaster> Presences;
    }

    public class AbsentOrPresentMaster
    {  
        public bool IsPresent { get; set; }

        public int ID { get; set; }

        public string Name { get; set; }

        public string Middlename { get; set; }

        public string Surname { get; set; }

        public string ParentName { get; set; }

        public int CommunityWorkerID { get; set; } 

        public string Email { get; set; }

        public string Image { get; set; }

        public int SchoolID { get; set; }

        public int ClassroomID { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Note { get; set; }

        public string GeoLocation { get; set; }

        public int Day { get; set; }
    }
}