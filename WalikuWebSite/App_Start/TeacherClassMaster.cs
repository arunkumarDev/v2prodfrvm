﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class TeacherClassMapping
{
    public int ID { get; set; }

    public int TeacherID { get; set; }

    public int ClassRoomID { get; set; }

    public string Name { get; set; }

    public string ClassName { get; set; }

    public int IsActive { get; set; }

    public int CreatedBy { get; set; }

    public DateTime CreatedOn { get; set; } = DateTime.MinValue;
}
