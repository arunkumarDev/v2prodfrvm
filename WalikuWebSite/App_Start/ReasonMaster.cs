﻿public class ReasonMaster
{
    public int ID { get; set; }

    public int ReasonType { get; set; }

    public string Description { get; set; }

    // public string DescriptionIndian { get; set; }
    public string Description_Indonesian { get; set; }

    public string ReasonTypeName { get; set; }
}

public class ReasonType
{
    public int ID { get; set; }

    public string Code { get; set; }

    public string Description { get; set; }

}

public class TeacherAttendanceReasonType
{
    public int ID { get; set; }

    public string Code { get; set; }

    public string Description { get; set; }

    public string Description_Indonesian { get; set; }

}