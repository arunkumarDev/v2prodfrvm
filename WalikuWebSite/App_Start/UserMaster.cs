﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserMaster
/// </summary>
public class UserMaster
{

    public int ID { get; set; }

    public string UserName { get; set; } = "";

    public string Password { get; set; } = "";

    public string Name { get; set; } = "";

    public string Surname { get; set; } = "";

    public string Image { get; set; } = "";

    public int UserType { get; set; } = 0;

    public string Address { get; set; } = "";

    public string Phone { get; set; } = "";

    public string Note { get; set; } = "";


    public string UserTypeName { get; set; }

    public string GeoLocation { get; set; }

    public string Gender { get; set; }

    public int CreatedBy { get; set; }

    public DateTime CreatedOn { get; set; } = DateTime.MaxValue;

    public int IsActive { get; set; }

    public string Email { get; set; }

    public string Note_Indonesian { get; set; }
    public string NIK { get; set; }

    public long NUPTK { get; set; }
    public int DeleteReasonID { get; set; }
}