﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for Common
/// </summary>
public static class Common
{
    public class AppSettings
    {
        public static string WebApiUrl = ConfigurationManager.AppSettings["WebApiUrl"].ToString();
        public static string GoogleTranslateUrl = ConfigurationManager.AppSettings["GoogleTranslateUrl"].ToString();    
    }

    public class SqlQueries
    {
      //  public static string CONNECTION_STRING = "Data Source=SQL6001.site4now.net;Initial Catalog=DB_A39427_waliku;User Id=DB_A39427_waliku_admin;Password=waliku@123;"; //@"Server=.\SQLEXPRESS;Database=DB_A39427_waliku;Integrated Security=SSPI;";

        public static string CONNECTION_STRING = ConfigurationManager.ConnectionStrings["KiguzoConnectionString"].ConnectionString;

        public static string ValidateLoginUser = "select ID from UserMaster WHERE UserName='{0}' and Password='{1}'";

        public static string GetUserByID = "select * from UserMaster WHERE ID={0}";

        public static string GetAcademicYears = "Select * from AcademicYear WHERE IsActive =1";

        public static string DeleteUserByID = "UPDATE UserMaster SET IsActive=0 ,CREATEDON =GETDATE(),DELETEREASONID = {1}  WHERE ID={0}";    //"DELETE from UserMaster WHERE ID={0}";

        public static string GetAllUsers = "SELECT UserMaster.ID, UserName, Password, ISNULL(Name,'') as Name, ISNULL(Surname,'') as Surname,ISNULL(Image,'') Image, ISNULL(UserMaster.UserType,'') UserType,ISNULL(Address,'') Address,ISNULL(Phone,'') Phone,ISNULL(Note,'') Note, UTM.UserType as UserTypeName" +
            ",UserMaster.GeoLocation,UserMaster.Gender,UserMaster.Email,UserMaster.NIK  " +
            " from UserMaster Left Join UserTypeMaster UTM on UTM.ID = UserMaster.UserType WHERE UserMaster.IsActive=1 ";

        // public static string GetAllTeachers = GetAllUsers + " WHERE UserMaster.UserType=2";

        public static string GetAllTeachers = GetAllUsers + " AND UserMaster.UserType=2";

        //public static string GetClassWisePresentAbsentReport = "select ClassMaster.ID, ClassMaster.ClassName, " +
        //                                                    " (SELECT COUNT (ID) from AttendanceMaster WHERE AttendanceMaster.ClassRoomID = ClassMaster.ID and AttendanceMaster.IsPresent = 1 and AttendanceMaster.AttendanceDate = '{0}') PresentCount, " +
        //                                                    " (SELECT COUNT (ID) from AttendanceMaster WHERE AttendanceMaster.ClassRoomID = ClassMaster.ID and AttendanceMaster.IsPresent = 0 and AttendanceMaster.AttendanceDate = '{0}') AbsentCount " +
        //                                                    " from ClassMaster ";
  

        public static string GetAbsentsOrPresentsForClass = "SELECT ChildrenMaster.*, UTM.IsPresent as IsPresent FROM ChildrenMaster LEFT JOIN AttendanceMaster UTM on ChildrenMaster.ID = UTM.ChildID WHERE UTM.ClassRoomID = {0} AND UTM.IsPresent = {1} AND YEAR(UTM.AttendanceDate) = {2} AND MONTH(UTM.AttendanceDate) = {3} AND DAY(UTM.AttendanceDate) = {4}";


        public static string GetUsersByType = "GetUsersByType";

        public static string GetAllUserTypes = "select ID, UserType,UserType_Indonesian from UserTypeMaster ";
        public static string GetSuperAdminUserTypes = "select ID, UserType,UserType_Indonesian from UserTypeMaster where ID in (1,2,4)";
        public static string GetAdminUserTypes = "select ID, UserType,UserType_Indonesian from UserTypeMaster where ID in (1,2)";

        public static string GetAllClasses = "select cm.ID, cm.ClassName,cm.SchoolId,cm.CreatedBy,cm.CreatedOn, sm.SchoolName,cm.IsActive From ClassMaster cm " +
            "LEFT JOIN  SchoolMaster sm ON cm.SchoolId = sm.ID WHERE cm.IsActive =1";

        public static string GetAllStudentsForDeletion = " select CM.ID, CM.Name, ISNULL(CM.Middlename,'') as Middlename,CM.Surname,CM.ParentName,CM.CommunityWorkerID, CM.Email,CM.SchoolID,CM.ClassroomID,CM.Address, CM.Image, " +
                                                " CM.Phone, CM.Note , CM.Image as ImageBase, CM.GeoLocation,CM.Gender, UM.Name as CommunityWorkerName,SM.SchoolName,ClassMaster.ClassName as ClassroomName, CM.NISN,CM.GradeID,CM.DeleteReasonID from  ChildrenMaster CM " +
                                                " Left join UserMaster UM on UM.ID = CM.CommunityWorkerID " +
                                                " LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID " +
                                                " LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID WHERE " +
                                                " ClassMaster.IsActive = 0 " +
                                                "ORDER BY CM.ID";
        //  public static string GetAllReason = " select ID, Title, Content, ContentIndian from FirstAidMaster ";

        public static string GetAllFirstAidList = " select ID, Title, Content, ContentIndian from FirstAidMaster ";

        //public static string GetAllReasonMaster = " select ReasonMaster.ID, ReasonMaster.ReasonType, ReasonMaster.Description, ReasonMaster.Description_Indonesian,ReasonType.Description as ReasonTypeName from ReasonMaster" +
        //                                            " INNER JOIN ReasonType ON ReasonType.ID = ReasonMaster.ReasonType " +
        //                                            " WHERE ReasonMaster.IsActive=1 ORDER BY ReasonMaster.ReasonType, ReasonMaster.Description";

        public static string GetAllReasonMaster = "Usp_Get_ReasonMaster";

        public static string GetAllReasonType = " select ID, Code, {0} AS Description from ReasonType WHERE IsActive=1";

        public static string GetTeacherAttendanceReasonType = " select ID, Code, {0} AS Description from TeacherAttendanceReasonType WHERE IsActive=1";

        //   public static string GetAllTeacherClasses = "select TeacherClassMapping.*,(UserMaster.Name+' '+UserMaster.Surname) as Name,ClassMaster.ClassName from TeacherClassMapping LEFT JOIN  UserMaster on UserMaster.ID = TeacherClassMapping.TeacherID LEFT JOIN ClassMaster on ClassMaster.ID = TeacherClassMapping.ClassRoomID WHERE TeacherClassMapping.IsActive = 1";

        public static string GetAllTeacherClasses = " SELECT TeacherClassMapping.*, (UserMaster.Name+' '+UserMaster.Surname) as Name,ClassMaster.ClassName" +
                                                    " FROM ClassMaster" +
                                                    " LEFT JOIN TeacherClassMapping ON TeacherClassMapping.ClassRoomID = ClassMaster.ID" +
                                                    " LEFT JOIN UserMaster on UserMaster.ID = TeacherClassMapping.TeacherID" +
                                                    " WHERE ClassMaster.schoolId IN (SELECT SchoolID from SchoolAdminMapping where UserId = {0}) " +
                                                    "AND ClassMaster.isactive = 1 AND TeacherClassMapping.ISACTIVE = 1";

        public static string GetAllSchoolAdmin = "select SchoolAdminMapping.SchoolId,SchoolAdminMapping.UserId,SchoolAdminMapping.Id as ID," +
            "SchoolMaster.SchoolName as SchoolName,(UserMaster.Name+' '+UserMaster.Surname) as UserName from SchoolAdminMapping " +
            "LEFT JOIN  UserMaster on UserMaster.ID = SchoolAdminMapping.UserId LEFT JOIN SchoolMaster on SchoolMaster.ID = SchoolAdminMapping.SchoolId" +
            " WHERE SchoolAdminMapping.IsActive = 1 ORDER BY SchoolMaster.SchoolName";


          public static string GetAllHoliday = "select ID,HolidayDate,AcademicYear,CreatedBy,CreatedOn,SchoolId,IsActive,ISNULL({0},'') AS Reason from YearlyHolidayMaster where SchoolId={1} AND IsActive=1 AND AcademicYearId= (SELECT TOP 1 Id FROM AcademicYear WHERE IsActive =1 ) ORDER BY HolidayDate";
        

        public static string GetTeachersClasses = "select ClassMaster.* from TeacherClassMapping LEFT JOIN  UserMaster on UserMaster.ID = TeacherClassMapping.TeacherID LEFT JOIN ClassMaster on ClassMaster.ID = TeacherClassMapping.ClassRoomID WHERE TeacherID = {0}";

        public static string DeleteAcademicYear = "UPDATE AcademicYear SET IsActive =0, IsCurrentTerm=0 WHERE ID={0}";

        public static string UpdateAcademicYear  = "UPDATE AcademicYear SET IsCurrentTerm ={2}, Start_Date={3}, End_Date = {4}, Name = {1} WHERE ID={0}";
        public static string AddAcademicYear    = "INSERT INTO AcademicYear(Name,IsCurrentTerm ,Start_Date,End_Date,IsActive) VALUES({1}, {2}, {3}, {4}, 1)";

        public static string DeleteClassByID = "UPDATE ClassMaster SET IsActive =0 WHERE ID={0}";  //"DELETE from ClassMaster WHERE ID={0}";

        public static string DeleteSchoolAdminByID = "UPDATE SchoolAdminMapping SET IsActive =0 WHERE ID={0}";  //"DELETE from ClassMaster WHERE ID={0}";

        public static string DeleteHolidayByID = "UPDATE YearlyHolidayMaster SET IsActive =0 WHERE ID={0}";  //"DELETE from ClassMaster WHERE ID={0}";

        public static string DeleteReasonTypeByID = "UPDATE ReasonMaster SET IsActive = 0 WHERE ID={0}"; //"DELETE from ResonMaster WHERE ID={0}";

        public static string DeleteReasonByID = "UPDATE from FirstAidMaster SET IsActive = 0 WHERE ID={0}";  //"DELETE from FirstAidMaster WHERE ID={0}";

        public static string ManageUser = "ManageUser";

        public static string ManageSchool = "ManageSchool";

        public static string ManageClass = "ManageClass";

        public static string ManageAcademicYears = "ManageAcademicYears";

        public static string ManageAdminMapping = "ManageAdminMapping";

        public static string AcademicCalenderMapping = "AcademicCalender";

        public static string ManageReasonType = "ManageClassReasonType";

        public static string ManageReason = "ManageReason";

        public static string ManageClassTeacher = "ManageClassTeacher";

        public static string ManageChildren = "ManageChildren";

        public static string GetAllSchool = "SELECT * FROM SchoolMaster";

        public static string GetSchoolListForUser = "GetSchoolListForUser";

        public static string GetFirstAidCounterList = "GetFirstAidCounterList";

        public static string GetMyUserList = "Usp_Get_MyUserList";

        public static string GetMySchoolList = "Usp_Get_MySchoolList";

        public static string GetSchoolAcademicCalendarList = "Usp_Get_SchoolAcademicCalendarList";

        public static string GetTeacherAssignSubstituteList = "Usp_Get_TeacherAssignSubstituteList";

        public static string GetTeacherAttendanceList = "Usp_Get_TeacherAttendanceList";

        public static string GetTeacherAttendanceList1 = "Usp_Get_TeacherAttendanceList1";


        public static string GetFeedbackList = "GetFeedBackList";



        public static string GetAllUser = "SELECT ID, Name, SurName FROM UserMaster WHERE UserType = {0}";

        public static string GetAllChildren = " select   CM.ID, CM.Name, ISNULL(CM.Middlename,'') as Middlename,CM.Surname,CM.ParentName,CM.CommunityWorkerID, CM.Email,CM.SchoolID,CM.ClassroomID,CM.Address, CM.Image, " +
                                                " CM.Phone, CM.Note , '' as ImageBase, CM.GeoLocation,CM.Gender, UM.Name as CommunityWorkerName,SM.SchoolName,ClassMaster.ClassName as ClassroomName, CM.NISN,CM.GradeID,CM.DeleteReasonID from ChildrenMaster CM " +
                                                " Left join UserMaster UM on UM.ID = CM.CommunityWorkerID " +
                                                " LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID " +
                                                " LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID  " +
                                                " WHERE CM.IsActive = 1  " +
                                                " ORDER BY CM.ID";

        public static string GetAllChildrenForExport = "SELECT CM.Student_Registration_Number,CM.Birthplace,CM.Date_of_Birth,CM.ID_Number,CM.Religion,CM.RT,CM.RW,CM.Village,"
        +"CM.Village_Office,CM.Sub_District,CM.Postal_Code,CM.Living_Situation,CM.Transportation_Mode,CM.Certificate_of_National_Examination_Result,CM.Recipient_of_Social_Security_Card,"
        +"CM.Social_Security_No,CM.F_Name,CM.F_Birth_Year,CM.F_Education_Level,CM.F_Occupation,CM.F_Income,CM.F_ID_Number,CM.M_Name,CM.M_Birth_Year,CM.M_Education_Level,CM.M_Occupation,CM.M_Income,CM.M_ID_Number,"
        +"CM.G_Name,CM.G_Birth_Year,CM.G_Education_Level,CM.G_Occupation,CM.G_Income,CM.G_ID_Number,CM.Current_Class,CM.National_Examination_Participant_No,CM.Certificate_Serial_No,CM.Recipient_of_Indonesian_Smart_Card,"
        +"CM.Smart_Card_No,CM.Name_on_Smart_Card,CM.Family_Card_No,CM.Birth_Certificate_No,CM.Bank,CM.Bank_Account_Number,CM.Name_of_Account_Holder,CM.Suitable_for_Smart_Indonesia_Program,CM.Reason_for_Smart_Indonesia_Program,"
        +"CM.Special_Needs,CM.Previous_School,CM.Position_of_Child_among_Siblings,CM.Latitude,CM.Longitude,CM.Family_Register_No,CM.Weight,CM.Height,CM.Head_circumference,CM.No_of_Siblings_related_by_blood,CM.Mobile_No FROM ChildrenMaster CM WHERE IsActive = 1";

        public static string GetAllChildrenForMe = " select CM.ID, CM.Name, ISNULL(CM.Middlename,'') as Middlename,CM.Surname,CM.ParentName,CM.CommunityWorkerID, CM.Email,CM.SchoolID,CM.ClassroomID,CM.Address, CM.Image, " +
                                                " CM.Phone, CM.Note , '' as ImageBase, CM.GeoLocation,CM.Gender, UM.Name as CommunityWorkerName,SM.SchoolName,ClassMaster.ClassName as ClassroomName, CM.NISN,CM.GradeID,CM.DeleteReasonID,CM.Date_of_Birth from  ChildrenMaster CM " +
                                                " Left join UserMaster UM on UM.ID = CM.CommunityWorkerID " +
                                                " LEFT JOIN SchoolMaster SM on SM.ID = CM.SchoolID " +
                                                " LEFT JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID WHERE " +
                                                " CM.IsActive = 1  " +
                                                "ORDER BY CM.ID";

        public static string GetAllChildrenInSchool = " select CM.ID,CM.NISN, CM.ClassroomID from  ChildrenMaster CM " +
                                                " INNER JOIN SchoolMaster SM on SM.ID = CM.SchoolID " +
                                                " INNER JOIN ClassMaster on ClassMaster.ID = CM.ClassroomID WHERE " +
                                                " CM.IsActive = 1 AND SM.ID = {0} " +
                                                "ORDER BY CM.ID";

        public static string GetNotificationsForAdmin = "GetNotificationsForAdmin";
        public static string GetNotificationsMonthlyCount = "NotificationMonthlyCount";

        public static string GetAllChildrenForMeWithFilter = "GetAllChildrenByFilter";

        public static string GetSchoolAdminMapping = "SELECT * FROM SchoolAdminMapping WHERE schoolId={0}";

        public static string GetChildImageByID = "select Image from ChildrenMaster where ID = {0}";

        public static string GetAllClassesForMe = "Usp_GetClassListByUser";

        public static string GetTeacherMappingClass = "Usp_GetTeacherClassMappingDetails";

        public static string GetAcademicTermList = "Usp_GetAcademicTermDetails";

        public static string GetConfigurationData = "SELECT * FROM ConfigurationData WHERE ConfigType = '{0}'";

        public static string LoadConfigureWeekdays = "SELECT * FROM ConfigureWeekdays";

        public static string DeleteAcademicTermByID = "UPDATE AcademicTermDetails SET IsActive = 0 WHERE ID={0}";

        public static string ManageAcademTermDetails = "Usp_ManageAcademicTermDetails";

        public static string GetChildDetailsByID = "Usp_Get_ChildDetailsByID";



        public static string DeleteSchoolByID = "UPDATE SchoolMaster SET IsActive = 0 WHERE ID={0}";    //"DELETE from SchoolMaster WHERE ID={0}";
        public static string DeleteChildByID = "UPDATE ChildrenMaster SET IsActive = 0 WHERE ID={0}";
        public static string DeleteChildByStudentID = "UPDATE ChildrenMaster SET IsActive = 0,CREATEDON =GETDATE(),DELETEREASONID = {1}  WHERE ID={0}"; //"DELETE from ChildrenMaster WHERE ID={0}";
        public static string ArchiveChildByID = "Usp_ArchiveChildByID"; //"DELETE from ChildrenMaster WHERE ID={0}";

        public static string DeleteClassTeacherByID = "UPDATE TeacherClassMapping SET IsActive = 0 WHERE ID={0}";   //"DELETE from TeacherClassMapping WHERE ID={0}";

        public static string GetAllGrade = "SELECT * FROM GradeMaster where IsActive = 1";

        public static string GetAllReasonForDelete = "SELECT Id, {0} AS Reason FROM DeleteReasonMaster where IsActive = 1 and ReasonType='Student'";

        public static string GetAllReasonForDeleteTeacher = "SELECT Id, {0} AS Reason FROM DeleteReasonMaster where IsActive = 1 and ReasonType='Teacher' order by {0} desc";

        public static string GetCalendarEventType = "SELECT ID, {0} AS EventType  FROM CalendarTypeMaster where IsActive = 1";

        public static string SaveSchoolAcademicCalendar = "SaveSchoolAcademicCalendar";

        public static string SaveTeacherAssignSubstitute = "SaveTeacherAssignSubstitute";

        public static string DeleteTeacherAssignSubstituteByID = "UPDATE TeacherAssignSubstitute SET IsActive = 0 WHERE ID={0}";

        public static string DeleteAcademicCalendarByID = "UPDATE AcademicCalendar SET IsActive = 0 WHERE ID={0}";

        public static string InsertHolidays = "SaveHolidays";

        public static string GetTrackStudents = "Usp_GetMonthlyStudentsAddedFrReports";

        public static string ManageBulkChildren = "ManageBulkChildren";

        public static string ManageStaffAttendance = "ManageStaffAttendance";

        public static string UpdateStaffAttendance = "UpdateStaffAttendance";

        public static string UpdateAcademicTerm = "UpdateAcademicTermDetails";

        public static string ValidateUserType = "select UserType from UserMaster WHERE id='{0}'";

        public static string GetExistingNISN = "SELECT COUNT(ID) IsExistingNisn,ID as ChildId FROM CHILDRENMASTER WHERE schoolid = '{0}' AND nisn = '{1}' group by ID";


        public static string GetStaffAttendanceDateDetails = " SELECT TAM.Userid,TAM.IsPresent,TAR.Code FROM[dbo].[TeacherAttendanceMaster] TAM " +
                                                             " LEFT JOIN[dbo].[TeacherAttendanceReasonType] TAR ON TAR.ID = TAM.ReasonTypeId " +
                                                             " WHERE TAM.AttendanceDate = '{0}' AND TAM.SchoolID = '{1}' ";
            

        public static string GetAcademicMonthDetails = "SELECT Name,DATENAME(month,Start_Date) StartMonth,DATENAME(month,End_Date) EndMonth FROM[dbo].[AcademicTermDetails] " +
            "  where schoolid = '{0}' AND ISCURRENTTERM =1 ORDER BY CREATEDON DESC ";


        public static string GetUserByType = "SELECT Id, Name FROM[dbo].USERMASTER where usertype = '{0}' and isactive = 1";


        public static string GetUserExistscount = "select count(*) UserCount from UserMaster where UserName='{0}' and IsActive=1 ";
        // "SELECT COUNT(*) as count FROM USERMASTER WHERE NAME = '{0}'";
        public static string GetChildrenExistscount = "select Count(*) UserCount from childrenMaster where [Name]='{0}' and IsActive = 1";


    }



    public class Constant
    {
        public static string CookieName = "UserInfo";

        public static string CookieUserNameKey = "UserName";

        public static string CookieIDKey = "ID";

        public static string CookieNameKey = "Name";

        public static string CookieImageKey = "Image";

        public static string Language = "en-us";

        public static string RememberMe = "Remember";

    }
    public class CommonFunction
    {
        public static string GetXmlIncludingNull(DataSet dsDataset)
        {


            DataSet dsDatasetAux = dsDataset.Copy();


            List<DataColumn> aColumnsToReplace = new List<DataColumn>();


            foreach (DataTable dtTable in dsDatasetAux.Tables)
            {
                if (dtTable.Rows.Count > 0)
                {
                    foreach (DataColumn oColumn in dtTable.Columns)
                    {
                        //check if none of the the rows has a value for the column
                        if (dtTable.Select(string.Format("{0} is not null", oColumn.ColumnName)).Length == 0)
                        {
                            if ((!object.ReferenceEquals(oColumn.DataType, typeof(string))))
                            {
                                aColumnsToReplace.Add(oColumn);
                            }
                            else
                            {
                                dtTable.Rows[0][oColumn] = string.Empty;
                            }
                        }
                    }
                    foreach (DataColumn oColumn in aColumnsToReplace)
                    {
                        dtTable.Columns.Remove(oColumn);
                        dtTable.Columns.Add(oColumn.ColumnName, typeof(string)).DefaultValue = string.Empty;
                        //setting the value for the column in at least one row is enough for GetXML to include it
                        dtTable.Rows[0][oColumn.ColumnName] = string.Empty;
                    }
                }
            }
            dsDatasetAux.AcceptChanges();

            return dsDatasetAux.GetXml();
        }

        public static bool CreateCookie(int userID, bool RememberMe,string Language)
        {
            try { 
            DataSet ds = SqlHelper.GetUserByID(userID);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    HttpCookie userInfo = new HttpCookie(Common.Constant.CookieName);
                    userInfo[Common.Constant.CookieUserNameKey] = Convert.ToString(ds.Tables[0].Rows[0]["UserName"]);
                    userInfo[Common.Constant.CookieIDKey] = Convert.ToString(ds.Tables[0].Rows[0]["ID"]);
                    userInfo[Common.Constant.CookieNameKey] = Convert.ToString(ds.Tables[0].Rows[0]["Name"]);
                    userInfo[Common.Constant.CookieImageKey] = Convert.ToString(ds.Tables[0].Rows[0]["Image"]);

                    userInfo[Common.Constant.Language] = Language;  // HttpContext.Current.Session["Language"].ToString();
                    userInfo[Common.Constant.RememberMe] = RememberMe.ToString();

                    // userInfo.Expires.Add(new TimeSpan(0, 1, 0));

                    if (RememberMe)
                        userInfo.Expires = DateTime.Now.AddDays(1);
                    else
                        userInfo.Expires = DateTime.Now.AddMinutes(10);

                    HttpContext.Current.Response.Cookies.Add(userInfo);
                    return true;
                }
                return false;
            }
            catch(Exception)
            {
                return false;
            }
           
        }
        public static string GetUserNameFromCookieByID()
        {
            HttpCookie aCookie = HttpContext.Current.Request.Cookies[Common.Constant.CookieName];
            if (aCookie != null)
            {
                return aCookie[Common.Constant.CookieUserNameKey];
            }
            return string.Empty;
        }
        public static int GetUserIdFromCookieByID()
        {
            HttpCookie aCookie = HttpContext.Current.Request.Cookies[Common.Constant.CookieName];
            if (aCookie != null)
            {
                return int.Parse(aCookie[Common.Constant.CookieIDKey]);
            }
            return 0;
        }
        public static string GetUserImageFromCookieByID()
        {
            HttpCookie aCookie = HttpContext.Current.Request.Cookies[Common.Constant.CookieName];
            if (aCookie != null)
            {
                return aCookie[Common.Constant.CookieImageKey];
            }
            return "emptyuiser.jpg";
        }
        public static bool IsUserLoggedIn()
        {
            HttpCookie aCookie = HttpContext.Current.Request.Cookies[Common.Constant.CookieName];
            if (aCookie != null)
            {
                return true;
            }
            return false;
        }

        public static string GetUserLanguageFromCookie()
        {
            HttpCookie aCookie = HttpContext.Current.Request.Cookies[Common.Constant.CookieName];
            if (aCookie != null)
            {
                return aCookie[Common.Constant.Language];
            }
            return "en-us";
        }

        public static string GetCurrentCulture()
        {
            try
            {
                HttpContext context = HttpContext.Current;

                string Language = CommonFunction.GetUserLanguageFromCookie();
               // if (context.Session["Language"] != null)
               if(Language != null)
                {
                    // return context.Session["Language"].ToString().Split('-')[0].ToLower();
                    return Language.Split('-')[0].ToLower();
                }
                return "en";
            }
            catch (Exception)
            {
                return "en";
            }
           
        }

        public static string GetToCulture()
        {       
            string CurrentCulture = CommonFunction.GetCurrentCulture();
            if (CurrentCulture != null && !string.IsNullOrEmpty(CurrentCulture))
            {
                return (CurrentCulture == "en" ? "id" : "en");
            }
            else
                return "en";
        }


       

      


        //public static string GetToCulture()
        //{
        //    HttpContext context = HttpContext.Current;
        //    if (context.Session["CurrentCulture"] != null && !string.IsNullOrEmpty(context.Session["CurrentCulture"].ToString()))
        //    {
        //        return (context.Session["CurrentCulture"].ToString().ToLower() == "en" ? "id" : "en");               
        //    }
        //    else
        //        return "en";

        //}


    }

}