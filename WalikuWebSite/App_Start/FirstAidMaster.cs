﻿using System;


public class FirstAidMaster
{
    public int ID { get; set; }

    public string Title { get; set; }

    public string Content { get; set; }

    public string ContentIndian { get; set; }

    public int CreatedBy { get; set; } = 0;

    public DateTime CreatedOn { get; set; } = DateTime.MinValue;

    public bool IsActive { get; set; }
}
