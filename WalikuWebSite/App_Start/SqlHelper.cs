﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using WalikuWebSite.App_Start;
using WalikuWebSite.Entities.DataModels;
using WalikuWebSite.Model;
using static WalikuWebSite.App_Start.ChartMaster;

/// <summary>
/// Summary description for SqlHelper
/// </summary>
public static class SqlHelper
{
    public static int ValidateUser(string UserName, string Password)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        try
        {
            sqlCnn.Open();
            DataSet ds = new DataSet();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.ValidateLoginUser, UserName, Password), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }

        }
        catch (Exception ex)
        {
            return 0;
        }

    }


    public static void DeleteAcademicYearByID(int ID)
    {
        DataSet ds = new DataSet();
        try
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteAcademicYear, ID), sqlCnn);
                sqlCmd.ExecuteNonQuery();
                sqlCnn.Close();
            }

        }
        catch (Exception ex)
        {
        }

    }

    public static void ManageAcademicYear(AcademicYear yearDetails)
    {
        DataSet ds = new DataSet();
        try
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageAcademicYears, sqlCnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ID", yearDetails.ID);
                    cmd.Parameters.AddWithValue("@Name", yearDetails.Name);
                    cmd.Parameters.AddWithValue("@IsCurrentTerm", yearDetails.IsCurrentTerm);
                    cmd.Parameters.AddWithValue("@StartDate", yearDetails.Start_Date);
                    cmd.Parameters.AddWithValue("@StartDate", yearDetails.End_Date);

                    sqlCnn.Open();
                    cmd.ExecuteNonQuery();
                    sqlCnn.Close();
                }
            }

        }
        catch (Exception ex)
        {
        }

    }

    public static DataSet GetAcademicYearsDetails()
    {
        DataSet ds = new DataSet();
        try
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAcademicYears, sqlCnn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds);

                adapter.Dispose();
                sqlCmd.Dispose();
                sqlCnn.Close();

                return ds;
            }

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet GetUserByID(int UserID)
    {

        DataSet ds = new DataSet();
        try
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetUserByID, UserID), sqlCnn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds);

                adapter.Dispose();
                sqlCmd.Dispose();
                sqlCnn.Close();
                return ds;
            }

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static List<UserMaster> GetAllUsers()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllUsers, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Users");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<UserMaster> lst = ds.Tables[0].ToList<UserMaster>();

                //foreach (var l in lst)
                //    l.Password = "";
                return lst;
            }
            return new List<UserMaster>();
        }
        catch (Exception ex)
        {
            return new List<UserMaster>();
        }

    }

    public static List<SchoolAdminMappingMaster> GetAllSchoolAdmin()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllSchoolAdmin, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "SchoolAdmin");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<SchoolAdminMappingMaster> lst = new List<SchoolAdminMappingMaster>();
                lst = DatatableConvertion.ConvertDataTable<SchoolAdminMappingMaster>(ds.Tables[0]);
                return lst;
            }
            return new List<SchoolAdminMappingMaster>();
        }
        catch (Exception ex)
        {
            return new List<SchoolAdminMappingMaster>();
        }

    }


    public static List<AcademicCalenderMaster> GetAllHoliday(int SchoolID, string LangID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAllHoliday, (LangID == "id" ? "Reason_Indonesian" : "Reason"), SchoolID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "HolidayList");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<AcademicCalenderMaster> lst = new List<AcademicCalenderMaster>();
                lst = DatatableConvertion.ConvertDataTable<AcademicCalenderMaster>(ds.Tables[0]);
                // lst = ds.Tables[0].ToList<AcademicCalenderMaster>();
                return lst;
            }
            return new List<AcademicCalenderMaster>();
        }
        catch (Exception ex)
        {
            return new List<AcademicCalenderMaster>();
        }

    }



    public static List<VmFirstAidAnalytics> GetFirstAidCounterList()
    {

        DataSet ds = new DataSet();
        try
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                sqlCnn.Open();
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetFirstAidCounterList, sqlCnn))
                {
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = sqlCmd;
                    adapter.Fill(ds, "FirstAidCounter");

                    adapter.Dispose();
                    sqlCmd.Dispose();
                    sqlCnn.Close();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<VmFirstAidAnalytics> lst = new List<VmFirstAidAnalytics>();
                        lst = DatatableConvertion.ConvertDataTable<VmFirstAidAnalytics>(ds.Tables[0]);
                        return lst;
                    }
                }
            }
            return new List<VmFirstAidAnalytics>();
        }
        catch (Exception ex)
        {
            return new List<VmFirstAidAnalytics>();
        }

    }

    public static List<UserTypeMaster> GetAllUserTypes(int uType)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            string query = "";
            if (uType == 0)
            {
                query = Common.SqlQueries.GetAllUserTypes;
            }
            else if (uType == 4)
            {
                query = Common.SqlQueries.GetSuperAdminUserTypes;
            }
            else if (uType == 1)
            {
                query = Common.SqlQueries.GetAdminUserTypes;
            }
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(query, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "UserTypes");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<UserTypeMaster> lst = ds.Tables[0].ToList<UserTypeMaster>();


                return lst;
            }
            return new List<UserTypeMaster>();
        }
        catch (Exception ex)
        {
            return new List<UserTypeMaster>();
        }

    }

    public static void ManageUser(UserMaster userMaster)
    {
        // E' un update?
        if (userMaster.ID > 0)
        {
            if (string.IsNullOrEmpty(userMaster.Password))
            {
                var ds = GetUserByID(userMaster.ID);
                if (ds != null)
                {
                    userMaster.Password = ds.Tables[0].Rows[0]["Password"].ToString();
                }
            }
        }

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageUser, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", userMaster.ID);
                cmd.Parameters.AddWithValue("@UserName", userMaster.UserName);
                cmd.Parameters.AddWithValue("@Password", userMaster.Password);
                cmd.Parameters.AddWithValue("@Name", userMaster.Name);
                cmd.Parameters.AddWithValue("@Surname", userMaster.Surname);
                cmd.Parameters.AddWithValue("@Image", userMaster.Image);
                cmd.Parameters.AddWithValue("@UserType", userMaster.UserType);
                cmd.Parameters.AddWithValue("@Address", userMaster.Address);
                cmd.Parameters.AddWithValue("@Phone", userMaster.Phone);
                cmd.Parameters.AddWithValue("@Note", userMaster.Note);
                cmd.Parameters.AddWithValue("@GeoLocation", userMaster.GeoLocation);
                cmd.Parameters.AddWithValue("@Gender", userMaster.Gender);

                cmd.Parameters.AddWithValue("@CreatedBy", userMaster.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", userMaster.CreatedOn);
                cmd.Parameters.AddWithValue("@IsActive", userMaster.IsActive);

                cmd.Parameters.AddWithValue("@Email", userMaster.Email);
                cmd.Parameters.AddWithValue("@Note_Indonesian", userMaster.Note_Indonesian);
                cmd.Parameters.AddWithValue("@NIK", userMaster.NIK);
                cmd.Parameters.AddWithValue("@DeleteReasonID", userMaster.DeleteReasonID);

                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }

    public static DataSet DeleteUserByID(int ID, int DelID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteUserByID, ID, DelID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static List<ChildrenMaster> GetAllStudentsForDeletion()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllStudentsForDeletion, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "StudentsForDeletion");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<ChildrenMaster> lst = ds.Tables[0].ToList<ChildrenMaster>();

                return lst;
            }
            return new List<ChildrenMaster>();
        }
        catch (Exception ex)
        {
            return new List<ChildrenMaster>();
        }

    }


    public static List<ClassMaster> GetAllClasses()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllClasses, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Classes");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<ClassMaster> lst = ds.Tables[0].ToList<ClassMaster>();

                return lst;
            }
            return new List<ClassMaster>();
        }
        catch (Exception ex)
        {
            return new List<ClassMaster>();
        }

    }

    //public static List<ReasonMaster> GetAllReason()
    //{
    //    SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
    //    DataSet ds = new DataSet();
    //    try
    //    {
    //        sqlCnn.Open();
    //        SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllReason, sqlCnn);
    //        SqlDataAdapter adapter = new SqlDataAdapter();
    //        adapter.SelectCommand = sqlCmd;
    //        adapter.Fill(ds, "Reasons");

    //        adapter.Dispose();
    //        sqlCmd.Dispose();
    //        sqlCnn.Close();

    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            List<ReasonMaster> lst = ds.Tables[0].ToList<ReasonMaster>();

    //            return lst;
    //        }
    //        return new List<ReasonMaster>();
    //    }
    //    catch (Exception ex)
    //    {
    //        return new List<ReasonMaster>();
    //    }

    //}


    public static List<FirstAidMaster> GetAllFirstAidList()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllFirstAidList, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Reasons");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<FirstAidMaster> lst = ds.Tables[0].ToList<FirstAidMaster>();

                return lst;
            }
            return new List<FirstAidMaster>();
        }
        catch (Exception ex)
        {
            return new List<FirstAidMaster>();
        }

    }

    public static List<TeacherClassMapping> GetAllTeacherClasses(int UserID, bool Falg)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAllTeacherClasses, UserID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "TeacherClasses");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<TeacherClassMapping> lst = ds.Tables[0].ToList<TeacherClassMapping>();
                // List<TeacherClassMapping> lst = DatatableConvertion.ConvertDataTable<TeacherClassMapping>(ds.Tables[0]); //Sumathi
                return lst;
            }
            return new List<TeacherClassMapping>();
        }
        catch (Exception ex)
        {
            return new List<TeacherClassMapping>();
        }

    }

    public static List<ClassMaster> GetAllTeacherClasses(int TeacherId)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetTeachersClasses, TeacherId), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "ClassMaster");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].ToList<ClassMaster>();


            }
            return new List<ClassMaster>();
        }
        catch (Exception ex)
        {
            return new List<ClassMaster>();
        }

    }


    public static List<ReasonMaster> GetAllReasonMaster(string LangID)
    {

        DataSet ds = new DataSet();
        try
        {
            using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
            {
                sqlCnn.Open();
                using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllReasonMaster, sqlCnn))
                {
                    sqlCmd.CommandType = CommandType.StoredProcedure;

                    sqlCmd.Parameters.Add(new SqlParameter("@LangID", SqlDbType.Char)).Value = LangID;

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = sqlCmd;
                    adapter.Fill(ds, "Reasons");

                    adapter.Dispose();
                    sqlCmd.Dispose();
                    sqlCnn.Close();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ReasonMaster> lst = ds.Tables[0].ToList<ReasonMaster>();
                        //  List<ReasonTypeMaster> lst = DatatableConvertion.ConvertDataTable<ReasonTypeMaster>(ds.Tables[0]); //Sumathi
                        return lst;
                    }
                }
                return new List<ReasonMaster>();
            }
        }
        catch (Exception ex)
        {
            return new List<ReasonMaster>();
        }

    }
    public static int ManageClass(ClassMaster classMaster)
    {
        if (IsClassNameExists(classMaster.SchoolId, classMaster.ClassName, classMaster.ID))
        {
            return -1;
        }
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageClass, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", classMaster.ID);
                cmd.Parameters.AddWithValue("@ClassName", classMaster.ClassName);
                cmd.Parameters.AddWithValue("@SchoolId", classMaster.SchoolId);
                cmd.Parameters.AddWithValue("@CreatedBy", classMaster.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", classMaster.CreatedOn);
                cmd.Parameters.AddWithValue("@IsActive", classMaster.IsActive);
                con.Open();
                cmd.ExecuteNonQuery();
                return 1;
            }
        }

    }


    public static void ManageAdmin(SchoolAdminMappingMaster schoolAdmin)
    {
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageAdminMapping, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IsActive", schoolAdmin.IsActive);
                cmd.Parameters.AddWithValue("@SchoolId", schoolAdmin.SchoolId);
                cmd.Parameters.AddWithValue("@UserId", schoolAdmin.UserId);
                cmd.Parameters.AddWithValue("@CreatedBy", schoolAdmin.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", schoolAdmin.CreatedOn);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }

    public static void ManageHoliday(AcademicCalenderMaster academicCalender)
    {
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.AcademicCalenderMapping, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", academicCalender.ID);
                cmd.Parameters.AddWithValue("@Reason", academicCalender.Reason);
                cmd.Parameters.AddWithValue("@SchoolId", academicCalender.SchoolID);
                cmd.Parameters.AddWithValue("@HolidayDate", academicCalender.HolidayDate);
                cmd.Parameters.AddWithValue("@AcademicYear", academicCalender.HolidayDate.Year);
                cmd.Parameters.AddWithValue("@IsActive", academicCalender.IsActive);
                cmd.Parameters.AddWithValue("@CreatedBy", academicCalender.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", academicCalender.CreatedOn);
                cmd.Parameters.AddWithValue("@Reason_Indonesian", academicCalender.Reason_Indonesian);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }

    public static void ManageReasonType(ReasonMaster reasonTypeMaster)
    {
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageReasonType, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", reasonTypeMaster.ID);
                cmd.Parameters.AddWithValue("@ReasonType", reasonTypeMaster.ReasonType);
                cmd.Parameters.AddWithValue("@Description", reasonTypeMaster.Description);
                cmd.Parameters.AddWithValue("@DescriptionIndian", reasonTypeMaster.Description_Indonesian);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }

    public static void ManageReason(FirstAidMaster reasonMaster)
    {
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageReason, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", reasonMaster.ID);
                cmd.Parameters.AddWithValue("@Title", reasonMaster.Title);
                cmd.Parameters.AddWithValue("@Content", reasonMaster.Content);
                cmd.Parameters.AddWithValue("@ContentIndian", reasonMaster.ContentIndian);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }


    public static void ManageClassTeacher(TeacherClassMapping classMaster)
    {

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {

            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageClassTeacher, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", classMaster.ID);
                cmd.Parameters.AddWithValue("@ClassRoomID", classMaster.ClassRoomID);
                cmd.Parameters.AddWithValue("@TeacherID", classMaster.TeacherID);
                cmd.Parameters.AddWithValue("@CreatedBy", classMaster.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", classMaster.CreatedOn);
                cmd.Parameters.AddWithValue("@IsActive", classMaster.IsActive);
                con.Open();
                cmd.ExecuteNonQuery();

            }
        }
    }

    public static DataSet DeleteClassByID(int ClassID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteClassByID, ClassID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet DeleteSchoolAdminByID(int ID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteSchoolAdminByID, ID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet DeleteHolidayByID(int ID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteHolidayByID, ID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet DeleteReasonTypeByID(int ReasonTypeID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteReasonTypeByID, ReasonTypeID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet DeleteReasonByID(int ReasonID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteReasonByID, ReasonID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }
    public static List<UserMaster> GetAllTeachers()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllTeachers, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Classes");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<UserMaster> lst = ds.Tables[0].ToList<UserMaster>();
                return lst;
            }
            return new List<UserMaster>();
        }
        catch (Exception ex)
        {
            return new List<UserMaster>();
        }

    }


    //public static List<AbsentOrPresentMaster> GetClassListPresentAbsentReport(string ClassName, string SelectedDate)
    //{
    //    return GetClassPresentsOrAbsentsChildren(ClassName, DateTime.Parse(SelectedDate));
    //}


    /// <summary>
    /// Ottiene la lista di tutti i bambini presenti o assenti nella data classe e alla data scelta
    /// </summary>
    /// <param name="ClassRoomID"></param>
    /// <param name="IsPresent"></param>
    /// <param name="SelectedDate"></param>
    /// <returns></returns>
    //public static List<AbsentOrPresentMaster> GetClassPresentsOrAbsentsChildren(string ClassRoomID, DateTime SelectedDate)
    //{
    //    SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
    //    DataSet ds = new DataSet();
    //    try
    //    {
    //        sqlCnn.Open();
    //        SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAllAbsentsOrPresentsForClass, ClassRoomID, SelectedDate.Year, SelectedDate.Month), sqlCnn);
    //        SqlDataAdapter adapter = new SqlDataAdapter
    //        {
    //            SelectCommand = sqlCmd
    //        };
    //        adapter.Fill(ds);

    //        adapter.Dispose();
    //        sqlCmd.Dispose();
    //        sqlCnn.Close();

    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            List<AbsentOrPresentMaster> lst = ds.Tables[0].ToList<AbsentOrPresentMaster>();
    //            return lst;
    //        }
    //        else
    //            return new List<AbsentOrPresentMaster>();
    //    }
    //    catch (Exception ex)
    //    {
    //        return new List<AbsentOrPresentMaster>();
    //    }

    //}

    /// <summary>
    /// Ottiene la lista di tutti i bambini presenti o assenti nella data classe e alla data scelta
    /// </summary>
    /// <param name="ClassRoomID"></param>
    /// <param name="IsPresent"></param>
    /// <param name="SelectedDate"></param>
    /// <returns></returns>



    public static List<SchoolMaster> GetAllSchools()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllSchool, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Schools");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<SchoolMaster> lst = ds.Tables[0].ToList<SchoolMaster>();
                return lst;
            }
            return new List<SchoolMaster>();
        }
        catch (Exception ex)
        {
            return new List<SchoolMaster>();
        }

    }

    public static List<SchoolMaster> GetSchoolListForUser(int UserID, string LangID)
    {
        DataSet ds = new DataSet();

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.GetSchoolListForUser, con))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", UserID);
                cmd.Parameters.AddWithValue("@LangID", LangID);
                con.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "Users");
                adapter.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<SchoolMaster> lst = ds.Tables[0].ToList<SchoolMaster>();
                    return lst;
                }
                return new List<SchoolMaster>();

            }
        }

    }

    public static List<UserMaster> GetUsersByType(int UserType)
    {
        DataSet ds = new DataSet();

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.GetUsersByType, con))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserType", UserType);
                con.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "UserMaster");
                adapter.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<UserMaster> lst = new List<UserMaster>();  //ds.Tables[0].ToList<UserMaster>();
                    lst = DatatableConvertion.ConvertDataTable<UserMaster>(ds.Tables[0]);
                    return lst;
                }
                return new List<UserMaster>();

            }
        }

    }

    public static int ManageSchool(SchoolMaster schoolMaster)
    {
        int InsertedID = 0;

        if (IsSchoolNameExists(schoolMaster.ID, schoolMaster.SchoolName))
        {
            return -1;
        }

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageSchool, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", schoolMaster.ID);
                cmd.Parameters.AddWithValue("@SchoolName", schoolMaster.SchoolName);
                cmd.Parameters.AddWithValue("@Email", schoolMaster.Email);
                cmd.Parameters.AddWithValue("@Image", schoolMaster.Image);
                cmd.Parameters.AddWithValue("@City", schoolMaster.City);
                cmd.Parameters.AddWithValue("@Address", schoolMaster.Address);
                cmd.Parameters.AddWithValue("@Country", schoolMaster.Country);
                cmd.Parameters.AddWithValue("@Phone1", schoolMaster.Phone1);
                cmd.Parameters.AddWithValue("@Phone2", schoolMaster.Phone2);
                cmd.Parameters.AddWithValue("@Note", schoolMaster.Note);
                cmd.Parameters.AddWithValue("@GeoLocation", schoolMaster.GeoLocation);
                cmd.Parameters.AddWithValue("@IsActive", schoolMaster.IsActive);
                cmd.Parameters.AddWithValue("@CreatedBy", schoolMaster.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", schoolMaster.CreatedOn);
                cmd.Parameters.AddWithValue("@Note_Indonesian", schoolMaster.Note_Indonesian);



                cmd.Parameters.Add("@InsertedID", SqlDbType.Int);
                cmd.Parameters["@InsertedID"].Direction = ParameterDirection.Output;
                con.Open();
                cmd.ExecuteNonQuery();
                InsertedID = cmd.Parameters["@InsertedID"].Value != DBNull.Value ? Convert.ToInt32(cmd.Parameters["@InsertedID"].Value) : 0;

            }
        }
        return InsertedID;
    }

    public static DataSet DeleteSchoolByID(int SchoolID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteSchoolByID, SchoolID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static string GetChildImageByID(int ChildID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetChildImageByID, ChildID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Children");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToString(ds.Tables[0].Rows[0][0]);

            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }

    }

    public static DataTable GetAllChildrenForExport()
    {
        // public static string GetAllChildrenForExport = "SELECT * FROM ChildrenMaster";
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllChildrenForExport, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "ChildrenExport");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                var lst = ds.Tables[0];
                return lst;
            }
            return new DataTable();
        }
        catch (Exception ex)
        {
            return new DataTable();
        }

    }

    public static List<ChildrenMaster> GetAllChildren()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllChildren, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Children");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<ChildrenMaster> lst = ds.Tables[0].ToList<ChildrenMaster>();
                return lst;
            }
            return new List<ChildrenMaster>();
        }
        catch (Exception ex)
        {
            return new List<ChildrenMaster>();
        }

    }

    public static List<ChildrenMaster> GetAllChildrenInSchool(int SchoolID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAllChildrenInSchool, SchoolID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Children");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<ChildrenMaster> lst = ds.Tables[0].ToList<ChildrenMaster>();
                return lst;
            }
            return new List<ChildrenMaster>();
        }
        catch (Exception ex)
        {
            return new List<ChildrenMaster>();
        }

    }
    public static List<ChildrenMaster> GetAllChildrenForMe(int UserId, int UserType, string LangID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(UserType == 1 ? Common.SqlQueries.GetAllChildren : string.Format(Common.SqlQueries.GetAllChildrenForMe, UserId), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "Children");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<ChildrenMaster> lst = ds.Tables[0].ToList<ChildrenMaster>();
                return lst;
            }
            return new List<ChildrenMaster>();
        }
        catch (Exception ex)
        {
            return new List<ChildrenMaster>();
        }

    }

    public static void ManageChildren(ChildrenMaster childrenMaster)
    {
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageChildren, con))
            {

                //  List<ChildrenMaster> existing = SqlHelper.GetAllChildrenInSchool(childrenMaster.SchoolID);
                //var isExisting = false;
                //existing.ForEach(eStudent =>
                //{

                //    if (childrenMaster.NISN == eStudent.NISN)
                //    {
                //        isExisting = true;

                //    }
                //});

                //if (isExisting == true)
                //{
                //    return ;
                //}
                //else
                //{
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", childrenMaster.ID);
                cmd.Parameters.AddWithValue("@Name", childrenMaster.Name);
                cmd.Parameters.AddWithValue("@Surname", childrenMaster.Surname);
                cmd.Parameters.AddWithValue("@ParentName", childrenMaster.ParentName);
                cmd.Parameters.AddWithValue("@CommunityWorkerID", childrenMaster.CommunityWorkerID);
                cmd.Parameters.AddWithValue("@Email", childrenMaster.Email);
                cmd.Parameters.AddWithValue("@Image", childrenMaster.Image);
                cmd.Parameters.AddWithValue("@SchoolID", childrenMaster.SchoolID);
                cmd.Parameters.AddWithValue("@ClassroomID", childrenMaster.ClassroomID);
                cmd.Parameters.AddWithValue("@Address", childrenMaster.Address);
                cmd.Parameters.AddWithValue("@Phone", childrenMaster.Phone);
                cmd.Parameters.AddWithValue("@Note", childrenMaster.Note);
                cmd.Parameters.AddWithValue("@GeoLocation", childrenMaster.GeoLocation == null ? string.Empty : childrenMaster.GeoLocation);
                cmd.Parameters.AddWithValue("@Gender", childrenMaster.Gender);
                cmd.Parameters.AddWithValue("@IsActive", childrenMaster.IsActive);
                cmd.Parameters.AddWithValue("@CreatedBy", childrenMaster.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", childrenMaster.CreatedOn);
                cmd.Parameters.AddWithValue("@Note_Indonesian", childrenMaster.Note_Indonesian);
                cmd.Parameters.AddWithValue("@NISN", childrenMaster.NISN);
                cmd.Parameters.AddWithValue("@GradeID", childrenMaster.GradeID);
                cmd.Parameters.AddWithValue("@DeleteReasonID", childrenMaster.DeleteReasonID);
                cmd.Parameters.AddWithValue("@Dob", childrenMaster.Date_of_Birth);

                con.Open();
                cmd.ExecuteNonQuery();
                //}

            }
        }

    }



    public static DataSet ArchiveChildByID(int ChildID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.ArchiveChildByID, sqlCnn);
            sqlCmd.CommandType = CommandType.StoredProcedure;
            sqlCmd.Parameters.AddWithValue("@ChildID", ChildID);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet DeleteChildByID(int ChildID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteChildByID, ChildID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet DeleteChildByStudentID(int ID, int DelID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteChildByStudentID, ID, DelID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }




    public static DataSet DeleteClassTeacherByID(int ChildID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteClassTeacherByID, ChildID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static void ManageAdminMapping(SchoolAdminMapping schoolAdminMapping)
    {
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.ManageAdminMapping, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@SchoolId", schoolAdminMapping.SchoolId);
                cmd.Parameters.AddWithValue("@UserId", schoolAdminMapping.UserId);
                cmd.Parameters.AddWithValue("@IsActive", schoolAdminMapping.IsActive);
                cmd.Parameters.AddWithValue("@CreatedBy", schoolAdminMapping.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", schoolAdminMapping.CreatedOn);
                con.Open();
                cmd.ExecuteNonQuery();

            }
        }
    }



    public static List<ChildrenMaster> GetAllChildrenForMeByFilter(int UserId, int UserType, int SchoolID, int ClassRoomID, string @LangID)
    {
        DataSet ds = new DataSet();
        List<ChildrenMaster> lst = new List<ChildrenMaster>();
        using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            sqlCnn.Open();
            using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllChildrenForMeWithFilter, sqlCnn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;

                sqlCmd.Parameters.AddWithValue("@UserType", UserType);
                sqlCmd.Parameters.AddWithValue("@SchoolID", SchoolID);
                sqlCmd.Parameters.AddWithValue("@ClassRoomID", ClassRoomID);
                sqlCmd.Parameters.AddWithValue("@UserID", UserId);
                sqlCmd.Parameters.AddWithValue("@LangID", LangID);

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds, "Users");

                adapter.Dispose();
                sqlCmd.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //  lst = DatatableConvertion.ConvertDataTable<ChildrenMaster>(ds.Tables[0]);
                    lst = ds.Tables[0].ToList<ChildrenMaster>();
                }

            }
        }
        return lst;


    }

    public static List<SchoolAdminMapping> GetSchoolAdminMapping(int SchoolId)
    {
        DataSet ds = new DataSet();

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(string.Format(Common.SqlQueries.GetSchoolAdminMapping, SchoolId), con))
            {

                cmd.CommandType = CommandType.Text;
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "Admin");
                adapter.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<SchoolAdminMapping> lst = ds.Tables[0].ToList<SchoolAdminMapping>();
                    return lst;
                }
                return new List<SchoolAdminMapping>();

            }
        }

    }


    public static List<SchoolAdminMappingDetails> GetUserByType(int usertype)
    {
        DataSet ds = new DataSet();

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(string.Format(Common.SqlQueries.GetUserByType, usertype), con))
            {

                cmd.CommandType = CommandType.Text;
                con.Open();
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "Admin");
                adapter.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<SchoolAdminMappingDetails> lst = ds.Tables[0].ToList<SchoolAdminMappingDetails>();
                    return lst;
                }
                return new List<SchoolAdminMappingDetails>();

            }
        }

    }

    public static List<ClassMaster> GetAllClassesForMe(int SchoolId, int UserId)
    {
        using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            DataSet ds = new DataSet();
            sqlCnn.Open();

            string Query = string.Format(Common.SqlQueries.GetAllClassesForMe);
            using (SqlCommand sqlCmd = new SqlCommand(Query, sqlCnn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = UserId;
                sqlCmd.Parameters.Add(new SqlParameter("@schoolID", SqlDbType.Int)).Value = SchoolId;


                using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                {
                    adapter.Fill(ds, "ClassList");
                }

                sqlCmd.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<ClassMaster> lst = ds.Tables[0].ToList<ClassMaster>();
                    lst.Sort((f1, f2) => f1.ClassName.CompareTo(f2.ClassName));
                    return lst;
                }
                return new List<ClassMaster>();
            }
        }

    }

    public static List<UserMaster> GetTeacherClassMappingDetails(int ClassRoomID)
    {
        using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            DataSet ds = new DataSet();
            sqlCnn.Open();

            string Query = string.Format(Common.SqlQueries.GetTeacherMappingClass);
            using (SqlCommand sqlCmd = new SqlCommand(Query, sqlCnn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add(new SqlParameter("@ClassRoomID", SqlDbType.Int)).Value = ClassRoomID;

                using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                {
                    adapter.Fill(ds, "UserList");
                }

                sqlCmd.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<UserMaster> lst = ds.Tables[0].ToList<UserMaster>();
                    return lst;
                }
                return new List<UserMaster>();
            }
        }

    }

    public static List<ReasonType> GetAllReasonTypes(string LangID)
    {
        using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            DataSet ds = new DataSet();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAllReasonType, (LangID == "id" ? "Description_Indonesian" : "Description")), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "ReasonType");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<ReasonType> lst = ds.Tables[0].ToList<ReasonType>();

                return lst;
            }
            return new List<ReasonType>();
        }

    }

    public static List<TeacherAttendanceReasonType> GetTeacherAttendanceReasonTypes(string LangID)
    {
        using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            DataSet ds = new DataSet();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetTeacherAttendanceReasonType, (LangID == "id" ? "Description_Indonesian" : "Description")), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds, "TeacherAttendanceReasonType");

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<TeacherAttendanceReasonType> lst = ds.Tables[0].ToList<TeacherAttendanceReasonType>();

                return lst;
            }
            return new List<TeacherAttendanceReasonType>();
        }

    }

    public static bool IsClassNameExists(int schoolId, string className, int ClassId)
    {
        int result = 0;
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand("SELECT COUNT(1) cnt FROM ClassMaster where SchoolId = @SchoolId AND ClassName = @ClassName AND IsActive = 1", con))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@SchoolId", schoolId);
                cmd.Parameters.AddWithValue("@ClassName", className.Trim());
                // cmd.Parameters.AddWithValue("@ClassId", ClassId);
                con.Open();
                result = Convert.ToInt32(cmd.ExecuteScalar());
            }
        }
        return result > 0 ? true : false;
    }

    public static bool IsSchoolNameExists(int schoolId, string schoolName)
    {
        int result = 0;
        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand("SELECT COUNT(1) cnt FROM SchoolMaster where SchoolName = @SchoolName AND Id <> @SchoolId", con))
            {
                cmd.CommandType = CommandType.Text;
                cmd.Parameters.AddWithValue("@SchoolId", schoolId);
                cmd.Parameters.AddWithValue("@SchoolName", schoolName.Trim());
                con.Open();
                result = Convert.ToInt32(cmd.ExecuteScalar());
            }
        }
        return result > 0 ? true : false;
    }

    public static List<GradeMaster> GetAllGrade()
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetAllGrade, sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<GradeMaster> lst = ds.Tables[0].ToList<GradeMaster>();
                return lst;
            }
            return new List<GradeMaster>();
        }
        catch (Exception ex)
        {
            return new List<GradeMaster>();
        }

    }

    public static List<DelteReasonMaster> GetAllReasonForDelete(string LangID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAllReasonForDelete, (LangID == "id" ? "Reason_Indonesian" : "Reason")), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<DelteReasonMaster> lst = ds.Tables[0].ToList<DelteReasonMaster>();
                return lst;
            }
            return new List<DelteReasonMaster>();
        }
        catch (Exception ex)
        {
            return new List<DelteReasonMaster>();
        }

    }

    public static List<DelteReasonMaster> GetAllReasonForDeleteTeacher(string LangID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAllReasonForDeleteTeacher, (LangID == "id" ? "Reason_Indonesian" : "Reason")), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<DelteReasonMaster> lst = ds.Tables[0].ToList<DelteReasonMaster>();
                return lst;
            }
            return new List<DelteReasonMaster>();
        }
        catch (Exception ex)
        {
            return new List<DelteReasonMaster>();
        }

    }


    public static List<UserExistcount> GetUserExistscount(string Name)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetUserExistscount, Name), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<UserExistcount> lst = ds.Tables[0].ToList<UserExistcount>();
                return lst;
            }
            return new List<UserExistcount>();
        }
        catch (Exception ex)
        {
            return new List<UserExistcount>();
        }

    }


    public static List<UserExistcount> GetChildrenExistscount(string Name)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetChildrenExistscount, Name), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<UserExistcount> lst = ds.Tables[0].ToList<UserExistcount>();
                return lst;
            }
            return new List<UserExistcount>();
        }
        catch (Exception ex)
        {
            return new List<UserExistcount>();
        }

    }

    public static List<CalendarTypeMaster> GetCalendarEventType()
    {
        var LangID = Common.CommonFunction.GetCurrentCulture();
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetCalendarEventType, (LangID == "id" ? "Eventtype_Indonesian" : "EventType")), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<CalendarTypeMaster> lst = ds.Tables[0].ToList<CalendarTypeMaster>();
                return lst;
            }
            return new List<CalendarTypeMaster>();
        }
        catch (Exception ex)
        {
            return new List<CalendarTypeMaster>();
        }

    }

    public static void SaveSchoolAcademicCalendar(SchoolAcademicCalendarMaster schoolAcademicCalendar)
    {

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.SaveSchoolAcademicCalendar, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", schoolAcademicCalendar.ID);
                cmd.Parameters.AddWithValue("@Name", schoolAcademicCalendar.Name);
                cmd.Parameters.AddWithValue("@Name_Indonesian", schoolAcademicCalendar.Name_Indonesian);
                cmd.Parameters.AddWithValue("@CalendarTypeId", schoolAcademicCalendar.CalendarTypeId);
                cmd.Parameters.AddWithValue("@SchoolId", schoolAcademicCalendar.SchoolId);
                cmd.Parameters.AddWithValue("@CalendarDate", schoolAcademicCalendar.CalendarDate);
                cmd.Parameters.AddWithValue("@CalendarToDate", schoolAcademicCalendar.CalendarToDate);
                cmd.Parameters.AddWithValue("@CreatedBy", schoolAcademicCalendar.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", schoolAcademicCalendar.CreatedOn);
                cmd.Parameters.AddWithValue("@IsActive", schoolAcademicCalendar.IsActive);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }

    public static void SaveTeacherAssignSubstitute(AssignSubstitute assignSubstitute)
    {

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.SaveTeacherAssignSubstitute, con))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ID", assignSubstitute.ID);
                cmd.Parameters.AddWithValue("@AssignFromDate", assignSubstitute.AssignFromDate);
                cmd.Parameters.AddWithValue("@AssignToDate", assignSubstitute.AssignToDate);
                cmd.Parameters.AddWithValue("@SchoolID", assignSubstitute.SchoolID);
                cmd.Parameters.AddWithValue("@ClassRoomID", assignSubstitute.ClassRoomID);
                cmd.Parameters.AddWithValue("@SubstituteTeacherID", assignSubstitute.SubstituteTeacherID);
                cmd.Parameters.AddWithValue("@CreatedBy", assignSubstitute.CreatedBy);
                cmd.Parameters.AddWithValue("@CreatedOn", assignSubstitute.CreatedOn);
                cmd.Parameters.AddWithValue("@IsActive", assignSubstitute.IsActive);
                con.Open();
                cmd.ExecuteNonQuery();
            }
        }

    }

    public static DataSet DeleteTeacherAssignSubstituteByID(int ChildID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteTeacherAssignSubstituteByID, ChildID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static DataSet InsertHolidays(string hname)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.InsertHolidays, hname), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }


    public static DataSet DeleteAcademicCalendarByID(int ChildID)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        DataSet ds = new DataSet();
        try
        {
            sqlCnn.Open();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.DeleteAcademicCalendarByID, ChildID), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            return ds;

        }
        catch (Exception ex)
        {
            return ds;
        }

    }

    public static List<TrackStudents> GetTrackStudents(string Date)
    {
        DataSet ds = new DataSet();

        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            using (SqlCommand cmd = new SqlCommand(Common.SqlQueries.GetTrackStudents, con))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Date", Date);
                con.Open();

                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = cmd;
                adapter.Fill(ds, "Students");
                adapter.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<TrackStudents> lst = ds.Tables[0].ToList<TrackStudents>();
                    return lst;
                }
                return new List<TrackStudents>();

            }
        }

    }



    public static int GetUserType(int uid)
    {
        SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
        try
        {
            sqlCnn.Open();
            DataSet ds = new DataSet();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.ValidateUserType, uid), sqlCnn);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            sqlCnn.Close();
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }
            else
            {
                return 0;
            }

        }
        catch (Exception ex)
        {
            return 0;
        }

    }

    public static List<ExistingNisn> GetExistingNISN(int TeacherId, int SchoolId, int ClassId, string Nisn)
    {


        using (SqlConnection con = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {

            DataSet ds = new DataSet();
            SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetExistingNISN, SchoolId, Nisn), con);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = sqlCmd;
            adapter.Fill(ds);

            adapter.Dispose();
            sqlCmd.Dispose();
            con.Close();

            if (ds.Tables[0].Rows.Count > 0)
            {
                List<ExistingNisn> lst = ds.Tables[0].ToList<ExistingNisn>();
                return lst;
            }
            return new List<ExistingNisn>();


        }



    }



}



