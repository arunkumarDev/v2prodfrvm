﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite
{
    public class ApiConfig
    {
        public static void Register()
        {
            string Credential_Path = HttpContext.Current.Server.MapPath(@"~/ApiKey/GoogleTranslatorApiKey.json");
            System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", Credential_Path);
        }
    }
}