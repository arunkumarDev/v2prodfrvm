﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Reason for Delete
/// </summary>
public class DelteReasonMaster
{
    public int ID { get; set; }
    public string Reason { get; set; }
    public string Reason_Indonesian { get; set; }
    public int IsActive { get; set; }
    public string ReasonType { get; set; }
}


public class UserExistcount
{
    public int UserCount { get; set; }
}