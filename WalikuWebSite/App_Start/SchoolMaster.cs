﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SchoolMaster
/// </summary>
public class SchoolMaster
{
    public int ID { get; set; }

    public string SchoolName { get; set; }

    public string Email { get; set; }

    public string Image { get; set; }

    public string City { get; set; }

    public string Address { get; set; }

    public string Country { get; set; }

    public string Phone1 { get; set; }

    public string Phone2 { get; set; }

    public string Note { get; set; }

    public string GeoLocation { get; set; }

    public int IsActive { get; set; }

    public int CreatedBy { get; set; }

    public DateTime CreatedOn { get; set; } = DateTime.MinValue;

    public string Note_Indonesian { get; set; }

}