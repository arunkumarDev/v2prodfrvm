﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SchoolMaster
/// </summary>
public class SchoolAdminMapping
{
    public int ID { get; set; }
    public int SchoolId { get; set; }
    public int UserId { get; set; }
    public int IsActive { get; set; }
    public int CreatedBy { get; set; }
    public DateTime CreatedOn { get; set; } = DateTime.MinValue;
}

public class SchoolAdminMappingDetails
{
    public int Id { get; set; }
    public string Name { get; set; }

}

