﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="ChildrenManagement.aspx.cs" Inherits="ChildrenManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <div id="viewprofile" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><%=Resources.Resource.Title_StudentProfile %></h4>
            </div>
            <div class="modal-body custom-scroll mh-500">
                <form class="custom-material">
                    <div class="col-md-2 w-120">
                        <div class="image-view">
                            <img src="images/user.png" id="viewProfilePhoto" class="img-responsive img-border" alt="preview">
                        </div>
                    </div>
                    <div class="col-md-10 right-content no-pad">
                        <div class="col-md-8">
                            <div class="row mt-20">
                                <div class="col-md-12">
                                    <strong class="pr-15 mb-10" style="display:block;"><span id="viewStudName"></span></strong>
                                    <%--<span class="badge">UID123</span>--%>
                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-5">
                                    <img src="images/icons/boy.png" class="mr-10"><span class="info-content" id="viewGender"></span>
                                    <!-- <img src="images/icons/girl.png" class="mr-10"><span class="info-content">Female</span> -->
                                </div>
                                <div class="col-md-7">
                                    <img src="images/icons/school.png" class="mr-10"><span class="info-content" id="viewSchoolName">&nbsp;</span>
                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-5">
                                    <img src="images/icons/Class.png" class="mr-10"><span class="info-content" id="viewClassName"></span>
                                </div>
                                <div class="col-md-7">
                                    <img src="images/icons/Teacher.png" class="mr-10"><span class="info-content" id="viewTeacherName"></span>
                                </div>
                            </div>

                            <div class="row mt-10">
                                <div class="col-md-5">
                                    <i class="fa fa-user site-color pr-10" aria-hidden="true"></i><span class="info-content" id="viewParentName">
                                    </span>
                                </div>
                                <div class="col-md-7">
                                    <img src="images/icons/parent.png" class="mr-10"><span></span>

                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-5">
                                    <i class="fa fa-phone site-color pr-10" aria-hidden="true"></i><span class="info-content" id="viewPhone"></span>
                                </div>
                                <div class="col-md-7">
                                    <i class="fa fa-map-marker site-color pr-15" aria-hidden="true"></i><span class="info-content" id="viewAddress"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6" id="viewAttendancePercent">
                                   <%-- <div class="progress att-percent">
                                        <div class="progress-bar green" role="progressbar" style="width: 90%;"
                                            aria-valuenow="10" aria-valuemin="0" aria-valuemax="10"><span id="viewAttendancePercent"></span></div>
                                    </div>--%>
                                </div>
                            </div>

                            <div class="row mt-10">
                                <div class="col-md-5">
                                    <i class="fa fa-asterisk   site-color pr-10" aria-hidden="true"></i><span class="info-content" id="viewNisn"></span>
                                </div>           
                                
                                 <div class="col-md-4">
                              <div class="bmd-form-group">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Dateofbirth %></label>
                               <input type="text" class="form-control date-icon hasDatepicker viewDob" id="datepicker1">

                                <i class="fa fa-calendar fa-icons"></i>
                              </div>
                                 </div>

                            </div>

                        </div>
                        <div class="col-md-4">                  
                            <div id="viewprofileMap" style="width: 100%; height:280px"></div>
                        </div>
                        <div class="mt-10 mb-20">
                            <div class="col-md-12">
                                <p><%=Resources.Resource.Lbl_Note %></p>
                                <div class="notes-box" id="viewNotes"></div>
                                
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


    
<div id="DelReasonPopup" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><%=Resources.Resource.Title_DeleteStudent %></h4>
            </div>
            <div class="modal-body custom-scroll mh-500 custom-material">
                <p class="mt-5 col-md-12"><%=Resources.Resource.Lbl_Delmsg %></p>
                <div class="col-md-12">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required mb-10">
                                        <%=Resources.Resource.Lbl_ReasonForDelete %></label>
                                    <select id="ddlPopupDeleteReason" class="form-control select-validate">
                                     </select>
                                     <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
            </div>
             <div class="modal-footer" style="padding:0px;">
                <div class="col-md-12 text-right">
                    <button type="button"  style="min-width: 97px;" class="btn btn-default mr-5" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button"  style="min-width: 97px;" class="btn btn-primary" title="" id="btndelChild"> OK </button>
                </div>
            </div>
        </div>
    </div>
</div>

    <div id="ChildrenManagementModel" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg custom-material ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"></h4>
            </div>
            <div class="modal-body custom-scroll mh-500">
                <form class="custom-material">
                    <div class="col-md-3 w-190">
                        <div class="image-view">
                            <img id="imgPhoto" src="images/user.png" class="img-responsive img-border" alt="preview">
                            <input type="file" id="txtImage" name="txtImage" class="custom-file-input preview-icon">
                             <input type="text" id="hdnImage" name="hdnImage" style="display: none" />
                           <%-- <a href="javascript:;" class="reset-photo">Upload Photo</a>--%>
                            <span class="reset-photo"><%=Resources.Resource.Lbl_UploadPhoto %></span>
                        </div>
                    </div>
                    <div class="col-md-9 right-content">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bmd-form-group">
                                      <input type="hidden" id="hdnChildID" value="0" />
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_StudentName %></label>
                                   <input type="text" id="txtChildName" class="form-control input-validate">
                                </div>
                            </div>
                           <%-- <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Surname %></label>
                                     <input type="text" id="txtSurName" class="form-control">
                                </div>
                            </div>--%>
                            <div class="col-md-6">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_ParentName %></label>
                                      <input type="text" id="txtParentName" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Email %></label>
                                    <input type="email" id="txtEmail" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Phone %></label>
                                      <input type="text" id="txtPhone" onkeypress="return isPhoneNumber(event,this)"  class="form-control input-validate">
                                </div>
                            </div>
                          <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SelectGender %></label>
                                    <select id="ddlGender"  class="form-control select-validate">
                                        <option value="M"><%=Resources.Resource.Lbl_Boy %></option>
                                        <option value="F"><%=Resources.Resource.Lbl_Girl %></option>
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>

                            
                            <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SchoolName %></label>
                                   <select id="ddlSchool" class="form-control select-validate">
                                     </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_ClassName %></label>
                                    <select id="ddlClassRoom" class="form-control select-validate">
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>                 

                            <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectTeacher %></label>
                                    <select id="ddlCommunityWorker" class="form-control">
                                     </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                           <%-- <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_GradeType %></label>
                                   <select id="ddlGrade" class="form-control select-validate">
                                     </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>--%>
                            <%--<div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ReasonForDelete %></label>
                                   <select id="ddlDeleteReason" class="form-control select-validate">
                                     </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>--%>
                            <div class="col-md-4">                                    
                                      <div class="bmd-form-group">
                                    <label class="bmd-label-floating">NISN</label>
                                      <input type="text" id="txtNisn" class="form-control input-validate" 
                                         maxlength="10" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  >
                                </div>

                            </div>
                             <div class="col-md-4">
                              <div class="bmd-form-group">
                                <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Dateofbirth %></label>
                               <input type="text"  class="form-control date-icon hasDatepicker txtDob input-validate" id="datepicker">

                                <i class="fa fa-calendar fa-icons"></i>
                              </div>
                                 </div>

                            <div class="col-md-12">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Address %></label>
                                    <input type="text" id="txtAddress" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Note %></label>
                                    <textarea id="txtNote" class="form-control mt-10"></textarea>
                                </div>
                            </div>
                                                         
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group mcus-search">
                                            <input type="text" class="form-control" placeholder="Search" name="srch-term"
                                                id="txtSearch">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary" type="button" id="btnLocationSearch"><i class="glyphicon glyphicon-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="bmd-form-group">
                                            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SearchResult %></label>
                                            <input type="text" id="txtSearchResult" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-8">
                              <%--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26360909.888257876!2d-113.74875964478716!3d36.242299409623534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1545830068358"
                                    width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>--%>
                                  <div id="dvMap" style="width: 100%; height:150px"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button" class="btn btn-primary" title="" id="btnSaveChild"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>


<main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_StudentManagement %></h1>
                </div>
            </div>
            <div class="row mb-10">
                <div class="col-sm-3" id="schoolsearch1">
                    <div class="bmd-form-group is-filled" id="schoolsearch">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchoolSearch" class="form-control">
                           
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ClassName %></label>
                        <select id="ddlClassSearch" class="form-control">
                            
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>

                

                <div class="col-sm-2 mt-15">
                    <button type="button" id="btnSearchChildren" class="btn btn-primary" title=""> <%=Resources.Resource.Lbl_Submit %> </button>
                </div>
                <!-- <div class="col-md-3 mt-15 pull-right">
                    <div class="search-form"><input type="text" placeholder="Search..." class="form-control">
                        <i class="fa fa-search fa-icons"></i>
                    </div>
                </div> -->
            </div>
            <div class="row mb-10">
                <div class="col-md-12">
                    <button type="button" class="btn btn-primary pull-right" title="" data-toggle="modal" id="btnAddStudent"><%=Resources.Resource.Lbl_AddStudent %></button>
                    <p class="student-count"><%=Resources.Resource.Lbl_TotalStudents %> : <span id="total">0</span></p>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper">
                    <table id="dataTable" class="table table-hover mb-0 student-table" style="width:100%">
                        <thead>
                            <tr>
                               
                                <th><span><%=Resources.Resource.Lbl_SerialNo %></span></th>
                                 <th>&nbsp;</th>
                                <th><span><%=Resources.Resource.Lbl_StudentName %></span></th>
                                <th><%=Resources.Resource.Lbl_ContactNumber %></th>
                                <th><%=Resources.Resource.Lbl_Address %></th>
                                <th><span><%=Resources.Resource.Lbl_Attendance %> %</span></th>
                                <th><%=Resources.Resource.Lbl_Action %></th>
                            </tr>
                        </thead>
                       <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>

    <script>
           var Lbl_AddStudent = '<%=Resources.Resource.Lbl_AddStudent %>'
          var Lbl_EditStudent = '<%=Resources.Resource.Lbl_EditStudent %>'
    </script>
      
      <script>document.write("<script type='text/javascript' src='Scripts/ChildrenManagement.js?v=" + JsVerion + "'><\/script>");</script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV6jv36xrNX23a9RWk-9ZsCSCOmSU-iys"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
     <script src="Scripts/bootstrap-datepicker.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.id.min.js"></script>

    
    <script>
        
         function noSunday(date) {
        var day = date.getDay();
        return [(day > 0), ''];
    };


        $('#datepicker').datepicker({
            autoclose: true,
            beforeShowDay: noSunday,
            format: 'mm/dd/yyyy',
            language: localStorage.getItem('loglang')


        });
      
        //var date = new Date();
        //$('#datepicker').datepicker('setDate', date);

         $('#datepicker1').datepicker({
            autoclose: true,
             beforeShowDay: noSunday
        });
      
        var date = new Date();
        $('#datepicker1').datepicker('setDate', date);

     
    </script>
    
    <script>
       

          $(document).ready(function () {
             LoadMap();
         });

    var map;
    var marker;
    var geocoder;
          var markers = [];

    function LoadMap() {
	  geocoder = new google.maps.Geocoder();
	   var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {         
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: latlng
        };
        map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
           google.maps.event.addListener(map, 'click', function (e) {
            $("#txtSearchResult").val(e.latLng.lat().toFixed(3) + "/" + e.latLng.lng().toFixed(3));              
            });
       // SetMarker(0);
        setDefaultMapLocation();
    };

          function setDefaultMapLocation() {
               searchMapLocation('Waikabubak, Indonesia');
               FnGooglMaps.clearMarkers(markers);
              //setLocation("-9.740/119.493");
          }

          function setLocation(latlng) {
              if (latlng != "") {
                  var myLatlng = latlng.split("/");
                   var data =  {                     	
                        "lat": myLatlng[0],	
                        "lng": myLatlng[1]	                      
                  }
					SetMapMarker(data);
              }
              else
                    setDefaultMapLocation();
          }
          function SetMapMarker(data) {
           FnGooglMaps.clearMarkers(markers);
        var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        marker = new google.maps.Marker({
            position: myLatlng,
            map: map
          //  title: data.title
        });
           
           markers.push(marker);   
        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow({
							//content: data.title 
        });   

        google.maps.event.trigger(map, "resize");
         map.panTo(marker.getPosition());
         map.setZoom(14);
	    // map.setCenter(marker.getPosition());   
    };
          	
	$("#btnLocationSearch").click(function(){
		//alert("The paragraph was clicked.");
        var address = $("#txtSearch").val();
         var latlng = address.split("/");
        if (latlng.length == 2) {
            try {          
                setLocation(address);
                e.preventDefault();                
            }
            catch (e) {}
        }
        else {
            searchMapLocation(address);
        }
	});
	
	function searchMapLocation(address)
	{	
		  getMapLatLang(address);
	}
        function getMapLatLang(address) {
            //alert('sd1');
            // Define address to center map to
            if (address == "") {
                address = 'Waikabubak, Indonesia';
            }
            geocoder.geocode({
                'address': address
            }, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    $("#txtSearchResult").val(results[0].geometry.location.lat().toFixed(3) + "/" + results[0].geometry.location.lng().toFixed(3));
                   
				  var data =  {
                        "title": address,	
                        "lat": results[0].geometry.location.lat().toFixed(3),	
                        "lng": results[0].geometry.location.lng().toFixed(3),	
                        "description": ''
                    }
					SetMapMarker(data);

                } else {

                    alert("Geocode was not successful for the following reason: " + status);
                }
            });
          }



    </script>

</asp:Content>

