﻿using System;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.UI;
using Waliku.GoogleTranslator;

namespace WalikuWebSite
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
            }
        }

        [WebMethod]
        public static List<ReasonMaster> GetAllReason(int PageIndex)
        {
            return SqlHelper.GetAllReasonMaster(Common.CommonFunction.GetCurrentCulture());

        }

        [WebMethod]
        public static List<ReasonType> GetAllReasonTypes()
        {
            return SqlHelper.GetAllReasonTypes(Common.CommonFunction.GetCurrentCulture());

        }

        [WebMethod]
        public static List<TeacherAttendanceReasonType> GetTeacherAttendanceReasonTypes()
        {
            return SqlHelper.GetTeacherAttendanceReasonTypes(Common.CommonFunction.GetCurrentCulture());

        }

        [WebMethod]
        public static VmResponseData ManageReasonType(int ID, int ReasonType,string Description)
        {
            VmResponseData VMResponseData = new VmResponseData();
            try
            {
                string CurrentCulture = Common.CommonFunction.GetCurrentCulture(), ToCulture = Common.CommonFunction.GetToCulture();
                Dictionary<string, string> Dict = GoogleTranslator.TranslateText(Description, CurrentCulture,
                        ToCulture);


                SqlHelper.ManageReasonType(new ReasonMaster()
                {
                    ID = ID,
                    ReasonType = ReasonType,
                    Description = Dict["en"],  //Description,
                                                         //DescriptionIndian = DescriptionIndian,
                    Description_Indonesian = Dict["id"]
                });
                VMResponseData.Message = "Success";
            }
            catch(Exception ex)
            {
                VMResponseData.Message = "Falied";
            }
            return VMResponseData;
        }

        [WebMethod]
        public static void DeleteReasonTypeByID(int ID)
        {
            SqlHelper.DeleteReasonTypeByID(ID);
        }
    }
}