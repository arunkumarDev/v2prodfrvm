﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using Aspose.Cells;
using dac = WalikuWebSite.EntityFrameworkDAC;


namespace WalikuWebSite
{
    /// <summary>
    /// Summary description for ImportDataHandler
    /// </summary>
    public class ExportDataHandler : IHttpHandler
    {

        private bool isError = false;

        public void ProcessRequest(HttpContext context)
        {
            string strImageName = string.Empty;
            //if (context.Request.Files.Count > 0)
            //{
                
                    var exportDac = new dac.ExportData();

            if (context.Request.Params["ExportType"] == "0")
            {
                //Student Export
                var students = exportDac.ExportStudentData();

                var templatePath = context.Server.MapPath("~/ImportExport/Template/ChildrenExport.xlsx"); 
                
                Workbook wb = new Workbook(templatePath);
                Worksheet ws = wb.Worksheets[0];

                ImportTableOptions imp = new ImportTableOptions();
                imp.InsertRows = true;

                imp.DateFormat = "dd/mm/yyyy";
                imp.ColumnIndexes = null;

                List<ChildrenMaster> studentOrd = new List<ChildrenMaster>();
                students.ForEach(s =>
                {
                    var CM = new ChildrenMaster()
                    {
                        Name = s.Name,
                        Surname = s.Surname,
                        ParentName = s.ParentName,
                        CommunityWorkerID = s.CommunityWorkerID == null ? 0 : (int)s.CommunityWorkerID,
                        Email = s.Email,
                        Image = s.Image,
                        Address = s.Address,
                        Phone = s.Phone,
                        Note = s.Note,
                        GeoLocation = s.GeoLocation,
                        Gender = s.Gender,
                        Middlename = s.Middlename,
                        Note_Indonesian = s.Note_Indonesian,
                        NISN = s.NISN,
                        GradeID = s.GradeID == null ? 0 : (int)s.GradeID,
                        DeleteReasonID = s.DeleteReasonID == null ? 0 : (int)s.DeleteReasonID,
                        Student_Registration_Number = s.Student_Registration_Number,
                        Birthplace = s.Birthplace,
                        Date_of_Birth = s.Date_of_Birth,
                        ID_Number = s.ID_Number,
                        Religion = s.Religion,
                        RT = s.RT,
                        RW = s.RW,
                        Village = s.Village,
                        Village_Office = s.Village_Office,
                        Sub_District = s.Sub_District,
                        Postal_Code = s.Postal_Code,
                        Living_Situation = s.Living_Situation,
                        Transportation_Mode = s.Transportation_Mode,
                        Certificate_of_National_Examination_Result = s.Certificate_of_National_Examination_Result,
                        Recipient_of_Social_Security_Card = s.Recipient_of_Social_Security_Card,
                        Social_Security_No = s.Social_Security_No,
                        F_Name = s.F_Name,
                        F_Birth_Year = s.F_Birth_Year,
                        F_Education_Level = s.F_Education_Level,
                        F_Occupation = s.F_Occupation,
                        F_Income = s.F_Income,
                        F_ID_Number = s.F_ID_Number,
                        M_Name = s.M_Name,
                        M_Birth_Year = s.M_Birth_Year,
                        M_Education_Level = s.M_Education_Level,
                        M_Occupation = s.M_Occupation,
                        M_Income = s.M_Income,
                        M_ID_Number = s.M_ID_Number,
                        G_Name = s.G_Name,
                        G_Birth_Year = s.G_Birth_Year,
                        G_Education_Level = s.G_Education_Level,
                        G_Occupation = s.G_Occupation,
                        G_Income = s.G_Income,
                        G_ID_Number = s.G_ID_Number,
                        Current_Class = s.Current_Class,
                        National_Examination_Participant_No = s.National_Examination_Participant_No,
                        Certificate_Serial_No = s.Certificate_Serial_No,
                        Recipient_of_Indonesian_Smart_Card = s.Recipient_of_Indonesian_Smart_Card,
                        Smart_Card_No = s.Smart_Card_No,
                        Name_on_Smart_Card = s.Name_on_Smart_Card,
                        Family_Card_No = s.Family_Card_No,
                        Birth_Certificate_No = s.Birth_Certificate_No,
                        Bank = s.Bank,
                        Bank_Account_Number = s.Bank_Account_Number,
                        Name_of_Account_Holder = s.Name_of_Account_Holder,
                        Suitable_for_Smart_Indonesia_Program = s.Suitable_for_Smart_Indonesia_Program,
                        Reason_for_Smart_Indonesia_Program = s.Reason_for_Smart_Indonesia_Program,
                        Special_Needs = s.Special_Needs,
                        Previous_School = s.Previous_School,
                        Position_of_Child_among_Siblings = s.Position_of_Child_among_Siblings,
                        Latitude = s.Latitude,
                        Longitude = s.Longitude,
                        Family_Register_No = s.Family_Register_No,
                        Weight = s.Weight,
                        Height = s.Height,
                        Head_circumference = s.Head_circumference,
                        No_of_Siblings_related_by_blood = s.No_of_Siblings_related_by_blood,
                        Mobile_No = s.Mobile_No
                    };


                    studentOrd.Add(CM);
                });

                try
                {
                    

                     ws.Cells.ImportCustomObjects((ICollection)studentOrd, 1, 0,imp);

                    //var cell = ws.Cells["A2"];
                    //cell.PutValue("Fantastic");

                    // Auto-fit all the columns
                    wb.Worksheets[0].AutoFitColumns();
                    var currentPath = @"~/ImportExport/ExportedFiles/ChildrenExport.xlsx";
                    var exportPath = context.Server.MapPath(currentPath);
                    // Save the Excel file
                    //  wb.Save("ChildrenExport.xlsx",SaveFormat.Xlsx);
                    wb.Save(exportPath);
                }
                catch(Exception e)
                {
                    throw e;
                }

            }
            else
            {
                //Teacher Export

            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}