﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="SchoolAcademicCalendarNew.aspx.cs" Inherits="SchoolAcademicCalendarNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link id="bs-css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">
  <%--  <link rel="Stylesheet" href=" " "text="" javascript""="" src=""https://code.jquery.com/jquery-1.12.3.js""> " target="_blank">https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />  --%>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/fullcalendar@5.1.0/main.min.css" />
    <script src="https://unpkg.com/fullcalendar@5.1.0/main.min.js"></script>
    <!-- Add User -->
   
<div id="SchoolAcademicCalendarModel" class="modal fade custom-modal" role="dialog"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg custom-material" >
        <div class="modal-content" >
           
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Lbl_AddCalendarEvent %></h4>
            </div>
            <div class="modal-body custom-scroll mh-400">
                <form class="custom-material">
                   
                    <div class="col-md-9 pr-0">
                        <div class="row"  > <%--style="display:none;"--%>
                            <div class="col-sm-5" >
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchoolSearch" class="form-control">
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                            
                       
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_CalendarName %></label>
                                    <input type="text" id="txtName" class="form-control input-validate">
                                     <input type="hidden" id="hdnUserID" value="0" />
                                </div>
                            </div>
                             <div class="col-md-3">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarEventType %></label>
                                    <select id="ddlCalendarEventType" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="bmd-form-group">
                                 <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarFromDate %></label>
                                 <input type="text" class="form-control date-icon hasDatepicker" id="dtCalendarDate">
                                 <i class="fa fa-calendar fa-icons"></i>
                                 </div>
                           </div>
                            <div class="col-md-3">
                                <div class="bmd-form-group">
                                 <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarToDate %></label>
                                 <input type="text" class="form-control date-icon hasDatepicker" id="dtCalendarToDate">
                                 <i class="fa fa-calendar fa-icons"></i>
                                 </div>
                           </div>
                           
                        </div>
                        <div class="row">
                            
                            <div class="col-md-12 ">
                                <div class="bmd-form-group">
                                 <div class="col-md-12 text-right">
                     <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveCalendar"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
                                 </div>
                           </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>

       <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
                     
            <div class="row mb-10 mt-10">
                <div class="col-sm-12">
            <ul class="nav nav-pills" >
              <li class="active"><a href="SchoolAcademicCalendarNew.aspx"><%=Resources.Resource.Lbl_CalendarView%></a></li>
              <li ><a href="SchoolAcademicCalendar.aspx" ><%=Resources.Resource.Lbl_CalendarList%></a></li>
              
            </ul>
        </div>
                  <div class="col-sm-4 mt-20" >
                      <div id="schoolsearch">
                  <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchoolSearch1"  class="form-control">
                        </select>
                          </div>
                      </div>

                        <i class="fa fa-angle-down fa-icons text-18"></i>
                <div class="col-md-8 mt-20">
                    <button type="button" class="btn btn-primary pull-right" title="" data-toggle="modal" id="btnAddSchoolAcademicCalendar"><%=Resources.Resource.Lbl_AddCalendarEvent %></button>
                    <button type="button" class="btn btn-primary pull-right" style="display:none"  title="" data-toggle="modal" id="btnDownloadSchoolAcademicCalendar"><%=Resources.Resource.Lbl_DownloadCalendarEvent %></button> 
                </div>
            </div>
            <div class="page-content">
                <div class="bmd-form-group">
                 <%--<asp:Calendar ID="Calendar1" runat="server" Height="430px" Width="1064px" ForeColor="#663399" 
            BorderWidth="1px" DayNameFormat="Full" Font-Names="Verdana" Font-Size="8pt" NextPrevFormat="FullMonth" 
            ShowGridLines="True" OnDayRender="Calendar1_DayRender" OnSelectionChanged="Calendar1_SelectionChanged"  
            OnVisibleMonthChanged="Calendar1_VisibleMonthChanged"> 
            <SelectedDayStyle BackColor="#CCCCFF" Font-Bold="True" />  
            <SelectorStyle BackColor="#FFCC66" />  
            <TodayDayStyle BackColor="#FFCC66" ForeColor="White" />  
            <OtherMonthDayStyle ForeColor="#CC9966" />  
            <NextPrevStyle Font-Size="9pt" ForeColor="#FFFFCC" />  
            <DayHeaderStyle BackColor="#FFCC66" Font-Bold="True" HorizontalAlign="Center" Height="30px" />  
            <TitleStyle BackColor="#990000" Font-Bold="True" Height="30px" Font-Size="9pt" ForeColor="#FFFFCC" />  
        </asp:Calendar>--%>
                   
                    <div id='calendar'></div>
    
           <%-- <asp:Calendar ID="Calendar1" runat="server" Height="430px" Width="1064px" NextPrevFormat="FullMonth" ForeColor="WhiteSmoke" SelectionMode="Day" 
            DayNameFormat="Full" Font-Names="Book Antiqua" Font-Size="Medium" OnDayRender="Calendar1_DayRender" 
            OnSelectionChanged="Calendar1_SelectionChanged"  OnVisibleMonthChanged="Calendar1_VisibleMonthChanged"   >
            <DayHeaderStyle
                 BackColor="White"
                 BorderColor="SkyBlue"
                 BorderWidth="1"
                 ForeColor="black"
                 HorizontalAlign="Center"
                 VerticalAlign="Middle"
                 Height="35"
                 />
            <DayStyle
                 BackColor="white"
                 ForeColor="black"
                 BorderColor="SkyBlue"
                 HorizontalAlign="Center"
                 VerticalAlign="Middle"
                 BorderWidth="1"
                 Font-Bold="true"
                 Font-Italic="true"
                 />
            <NextPrevStyle
                 Font-Italic="true"
                 Font-Names="Arial CE"
                 />
            <SelectedDayStyle
                 BackColor="white"
                 BorderColor="SpringGreen"
                 ForeColor="black"
                 />
            <OtherMonthDayStyle BackColor="white" ForeColor="Gray" />
            <TitleStyle
                 BackColor="SeaGreen"
                 Height="46"
                 Font-Size="Large"
                 Font-Names="Courier New Baltic"
                 />
                <TodayDayStyle BackColor="#337ab7" ForeColor="White" />  
        </asp:Calendar>--%>
                    
                                 </div>
            </div>
        </div>
    </main>
    <script src="Scripts/bootstrap-datepicker.js"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/SchoolAcademicCalendarView.js?v=" + JsVerion + "'><\/script>");</script>
        
    <script>
        var Lbl_AddCalendarEvent = '<%=Resources.Resource.Lbl_AddCalendarEvent %>'
        var Lbl_EditCalendarEvent = '<%=Resources.Resource.Lbl_EditCalendarEvent %>'

        $('#dtCalendarDate').datepicker({
            autoclose: true,
            format: 'mm/dd/yyyy'
        });
        var date = new Date(); //'2018-01-01'
        $('#dtCalendarDate').datepicker('setDate', date);

        $('#dtCalendarToDate').datepicker({
            autoclose: true,
            format: 'mm/dd/yyyy'
        });
        var date = new Date(); //'2018-01-01'
        $('#dtCalendarToDate').datepicker('setDate', date);


    </script>

    <style>
        .fc td.fc-today {
    border-style: dashed;
    background: aqua;
}


.fc .fc-daygrid-day.fc-day-today
{
    background: aqua;
    
}
.fc .fc-daygrid-day-number 
{
    color : black;
}
.fc-event-time
{
    display : none;
}
.fc .fc-button-primary:disabled
{
    display : none;
}


    </style>

</asp:Content>


