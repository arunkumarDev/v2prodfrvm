.mt-5 {
    margin-top: 5px;
}

.pt-5 {
    padding-top: 5px;
}

.mb-5 {
    margin-bottom: 5px;
}

.pb-5 {
    padding-bottom: 5px;
}

.ml-5 {
    margin-left: 5px;
}

.pl-5 {
    padding-left: 5px;
}

.mr-5 {
    margin-right: 5px;
}

.pr-5 {
    padding-right: 5px;
}

.mt-10 {
    margin-top: 10px;
}

.pt-10 {
    padding-top: 10px;
}

.mb-10 {
    margin-bottom: 10px;
}

.pb-10 {
    padding-bottom: 10px;
}

.ml-10 {
    margin-left: 10px;
}

.pl-10 {
    padding-left: 10px;
}

.mr-10 {
    margin-right: 10px;
}

.pr-10 {
    padding-right: 10px;
}

.mt-15 {
    margin-top: 15px;
}

.pt-15 {
    padding-top: 15px;
}

.mb-15 {
    margin-bottom: 15px;
}

.pb-15 {
    padding-bottom: 15px;
}

.ml-15 {
    margin-left: 15px;
}

.pl-15 {
    padding-left: 15px;
}

.mr-15 {
    margin-right: 15px;
}

.pr-15 {
    padding-right: 15px;
}

.mt-20 {
    margin-top: 20px;
}

.pt-20 {
    padding-top: 20px;
}

.mb-20 {
    margin-bottom: 20px;
}

.pb-20 {
    padding-bottom: 20px;
}

.ml-20 {
    margin-left: 20px;
}

.pl-20 {
    padding-left: 20px;
}

.mr-20 {
    margin-right: 20px;
}

.pr-20 {
    padding-right: 20px;
}

.mt-40 {
    margin-top: 40px;
}

.pt-40 {
    padding-top: 40px;
}

.mb-40 {
    margin-bottom: 40px;
}

.pb-40 {
    padding-bottom: 40px;
}

.ml-40 {
    margin-left: 40px;
}

.pl-40 {
    padding-left: 40px;
}

.mr-40 {
    margin-right: 40px;
}

.pr-40 {
    padding-right: 40px;
}

.ptb-5 {
    padding-top: 5px;
    padding-bottom: 5px;
}

.plr-5 {
    padding-left: 5px;
    padding-right: 5px;
}

.ptb-10 {
    padding-top: 10px;
    padding-bottom: 10px;
}

.plr-10 {
    padding-left: 10px;
    padding-right: 10px;
}

.plr-20 {
    padding-left: 20px;
    padding-right: 20px;
}

.mtb-5 {
    margin-top: 5px;
    margin-bottom: 5px;
}

.mlr-5 {
    margin-left: 5px;
    margin-right: 5px;
}

.mtb-10 {
    margin-top: 10px;
    margin-bottom: 10px;
}

.mlr-10 {
    margin-left: 10px;
    margin-right: 10px;
}

.p-5 {
    padding: 5px;
}

.p-10 {
    padding: 10px;
}

.p-15 {
    padding: 15px;
}

.p-20 {
    padding: 20px;
}

.p-25 {
    padding: 25px;
}

.p-30 {
    padding: 30px;
}

.p-40 {
    padding: 40px;
}

.m-5 {
    margin: 5px;
}

.m-10 {
    margin: 10px;
}

.m-15 {
    margin: 15px;
}

.m-20 {
    margin: 20px;
}

.m-25 {
    margin: 25px;
}

.m-30 {
    margin: 30px;
}

.m-40 {
    margin: 40px;
}

.text-12 {
    font-size: 12px;
    line-height: 18px;
}

.text-13 {
    font-size: 13px;
    line-height: 19px;
}

.text-14 {
    font-size: 14px;
    line-height: 20px;
}

.text-16 {
    font-size: 16px;
    line-height: 22px;
}

.text-18 {
    font-size: 18px;
    line-height: 24px;
}

.text-20 {
    font-size: 20px;
    line-height: 26px;
}

.text-22 {
    font-size: 22px;
    line-height: 28px;
}

.text-24 {
    font-size: 24px;
    line-height: 30px;
}

.text-26 {
    font-size: 26px;
    line-height: 32px;
}

.text-28 {
    font-size: 28px;
    line-height: 34px;
}

.text-30 {
    font-size: 30px;
    line-height: 36px;
}

body::after {
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    position: absolute;
}

body {
    font-family: 'Roboto', sans-serif;
    font-size: 14px;
    color: #4d4d4d;
    font-weight: normal;
    line-height: inherit;
    background: #f5f6fa;
}

input.placeholder,
textarea.placeholder {
    color: #ff0000;
}

input:-moz-placeholder,
textarea:-moz-placeholder {
    color: #ff0000;
}

input::-moz-placeholder,
textarea::-moz-placeholder {
    color: #ff0000;
}

input:-ms-input-placeholder,
textarea:-ms-input-placeholder {
    color: #ff0000;
}

input::-webkit-input-placeholder,
textarea::-webkit-input-placeholder {
    color: #ff0000;
}

.htruncate-ml4 {
    vertical-align: top;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    text-overflow: -o-ellipsis-lastline;
    height: 80px;
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
}

.htruncate-ml2 {
    vertical-align: top;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    text-overflow: -o-ellipsis-lastline;
    height: 40px;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
}

.htruncate-sl2 {
    vertical-align: top;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    text-overflow: -o-ellipsis-lastline;
    height: 30px;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
}

.square {
    width: 100px;
    height: 100px;
}

.size {
    width: auto;
    height: auto;
}

.gradient {
    background: #07c;
    background: -webkit-linear-gradient(top, #07c, #06f);
    background: linear-gradient(to bottom, #07c, #06f);
}

.vc-box {
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
}

body .sfont {
    font-size: 80%;
}

.cur {
    cursor: pointer;
}

.over-hide {
    overflow: hidden;
}

.no-bg {
    background: none;
}

.no-bor {
    border: 0 !important;
}

.no-pad {
    padding: 0 !important;
}

.no-mar {
    margin: 0 !important;
}

.no-round {
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}

.no-under,
.no-under:hover {
    text-decoration: none;
}

.underline {
    text-decoration: underline;
}

.z-top {
    z-index: 100;
}

.over-hide {
    overflow: hidden;
}

.no-shad {
    text-shadow: none;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
}

.btn {
    font-weight: 500;
    padding: 7px 12px;
    font-size: 15px;
    border-radius: 20px;
    transition: all 0.5s;
    min-width: 130px;
}

.btn-primary {
     background: #00ca9d; 
    border: 1px solid #00ca9d;
    color: #fff;

/* background: -moz-linear-gradient(left, #0f7bc0 0%, #26a264 100%);
background: -webkit-linear-gradient(left, #0f7bc0 0%,#26a264 100%); 
background: linear-gradient(to right, #0f7bc0 0%,#26a264 100%); 
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0f7bc0', endColorstr='#26a264',GradientType=1 );  */
}

.btn-primary:active,
.btn-primary:hover,
.btn-primary:active:focus,
.btn-primary:focus {
    background: #0bcca1;
    color: #fff;
    border-color: #0bcca1;
    box-shadow: none;
    outline: 0;
}

.btn-default {
    color: #0a1832;
    border: 1px solid #dce0e6;
    background: #dce0e6;
}

.btn-default:active,
.btn-default:hover,
.btn-default:focus {
    background: #ccd4e0;
    color: #0a1832;
    border-color: #ccd4e0;
    box-shadow: none;
    outline: 0;
}

.btn-info {
    background: #0173ba;
    border: 1px solid #0173ba;
    color: #fff;
}

.btn-info:active,
.btn-info:hover,
.btn-info:focus {
    background: #00629e;
    color: #fff;
    border-color: #00629e;
    box-shadow: none;
    outline: 0;
}

.btn-warning {
    background: transparent;
    border: 2px solid #df9a16;
    color: #df9a16;
}

.btn-warning:active,
.btn-warning:hover,
.btn-warning:focus {
    background: #df9a16;
    color: #fff;
    border-color: #df9a16;
    box-shadow: none;
    outline: 0;
}

.btn-success {
    background: #e3ffe3;
    border: 1px solid #e3ffe3;
    color: #3a3c3a;
}

.btn-success:active,
.btn-success:hover,
.btn-success:focus {
    background: #4fd3c49c;
    color: #3a3c3a;
    border-color: #4fd3c4;
    box-shadow: none;
    outline: 0;
}

.btn-orange {
    background: #ffa500;
    border: 2px solid #ffa500;
    color: #fff;
    padding: 0px 8px;
    font-size: 22px;
    border-radius: 5px;
}

.btn-orange:active,
.btn-orange:hover,
.btn-orange:focus {
    background: #fff;
    color: #ffa500;
    border-color: #ffa500;
    box-shadow: none;
    outline: 0;
}

.btn-sm {
    padding: 4px 12px 3px;
}

.btn-lg {
    padding: 8px 50px;
    font-size: 15px;
}

.text-uppercase {
    text-transform: uppercase;
}

.text-capitalize {
    text-transform: capitalize;
}

.text-lowercase {
    text-transform: lowercase;
}

a {
    color: #0073bb;
}

a:hover,
a:focus {
    text-decoration: none;
    outline: none;
}

.form-control {
    border-radius: 6px;
    border-color: #949cb8;
    box-shadow: none;
    height: 36px;
    padding: 4px 10px;
}

.form-control:focus {
    box-shadow: none;
}

select::-ms-expand {
    display: none;
}

select {
    -webkit-appearance: none;
    appearance: none;
    -moz-appearance: none;
}

.sep {
    border: 1px solid #318bff;
}

.sep-top {
    border-top: 1px solid #318bff;
}

.sep-bot {
    border-bottom: 1px solid #318bff;
}

.sep-left {
    border-left: 1px solid #318bff;
}

.sep-right {
    border-right: 1px solid #318bff;
}

.unstyled {
    padding: 0;
    margin: 0;
}

.unstyled li {
    list-style: none;
}

.blackc {
    color: #333;
}

.whitec {
    color: #fff;
}

.orangec {
    color: #d97310;
}

.grayc {
    color: #b0b8c4;
}

.bluec {
    color: #4774a5;
}

.bluec-1 {
    color: #921de3;
}

.site-color {
  color: #1d9e74;
}

.redc {
    color: #f47180;
}

.redc-d {
    color: #f42a2a;
}

.greenc {
    color: #1bac6d;
}

.yellowc{
    color: #ffc018;
}
.orangec-d {
    color: #d97412;
}

.bg-white {
    background: #fff;
}

.bg-color-1 {
    background: #fde4d3;
}

.bg-color-2 {
    background: #ddf4ff;
}

.bg-color-3 {
    background: #f8e6e7;
}

.bg-color-4 {
    background: #f9f9f9;
}

.bg-color-5 {
    background: #fee9d2;
}

.bg-color-6 {
    background: #cbf0f7;
}

.bg-color-7 {
    background: #e8dcb5;
}

.bg-color-8 {
    background: #cff2ce;
}

.bg-color-9 {
    background: #d3e8f6;
}

.bg-color-10 {
    background: #ffbece;
}

.bg-color-11 {
    background: #f1f1f1;
}

.textb {
    font-weight: 700;
}

.textb-s {
    font-weight: 500;
}

.custom-table .table{
  box-shadow: 0px 0px 5px #d4d8e9;
  border-radius: 10px;
}

.custom-table .table thead>tr>th {
    background: #e9ebf5;
    color: #505b62;
    border-bottom: 0;
    text-transform: uppercase;
    font-weight: 900;
    font-size: 13px;
}

.custom-table .table thead>tr>th[colspan] {
    background: #d4d8e9;
    text-align: center;
}
.custom-table .table thead>tr>th[rowspan] {
  vertical-align: middle;
}
.custom-table .table thead>tr:first-child>th:first-child {
    border-radius: 8px 0 0 0;
}

.custom-table .table thead>tr:first-child>th:last-child {
    border-radius: 0 8px 0 0;
}

.custom-table .table tbody>tr:last-child>td:first-child {
    border-radius: 0 0 0 8px;
}

.custom-table .table tbody>tr:last-child>td:last-child {
    border-radius: 0 0 8px 0;
}

.custom-table .table tbody>tr>td {
    padding: 10px 10px;
}

.custom-table .table thead>tr>th{
    padding: 5px 10px; 
}

.custom-table .table thead>tr + tr th{
    padding: 5px 10px;
}

.custom-table .table tbody>tr>td {
    color: #515254;
    border-top: 0;
}

.custom-table .table tbody>tr:nth-child(odd)>td {
    background: #fff;
}

.custom-table .table tbody>tr:nth-child(even)>td {
    background: #f9f9f9;
}

.inline-show {
    display: inline-block;
}

.custom-table .table>tbody+tbody {
    border-top: 1px solid #ddd;
}

.custom-table .table .table thead>tr>th,
.custom-table .table .table tfoot>tr>td {
    background: #d8eaf6;
    text-transform: capitalize;
}

.custom-table .table .table thead>tr>th,
.custom-table .table .table tbody>tr>td,
.custom-table .table .table tfoot>tr>td {
    padding: 6px 15px;
}

.custom-table-1 .table {
    border: 1px solid #ccc;
}

.custom-table-1 .table thead>tr>th {
    background: #28465a;
    color: #fff;
    border-bottom: 0px;
}

.custom-table-1 .table thead>tr>th,
.custom-table-1 .table tbody>tr>td {
    text-transform: capitalize;
    padding: 5px 10px;
    border-top: 0px;
    font-weight: 500;
}

.custom-table-1 .table-striped>tbody>tr:nth-child(odd)>td,
.custom-table-1 .table-striped>tbody>tr:nth-child(odd)>th {
    background: #fff;
}

.custom-table-1 .table-striped>tbody>tr:nth-child(even)>td,
.custom-table-1 .table-striped>tbody>tr:nth-child(even)>th {
    background: #f8f8f8;
}

/* Material design for Bootstrap CheckBox */

.md-checkbox {
    margin: 0;
}

.md-checkbox {
    display: inline-block;
    padding: 0px 0 0 20px;
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
}

.md-checkbox label {
    cursor: pointer;
    padding-left: 0;
    color: #636363;
}

.md-checkbox input[type="checkbox"] {
    opacity: 0;
    position: absolute;
    margin: 0;
    z-index: -1;
    width: 0;
    height: 0;
    overflow: hidden;
    left: 0;
    pointer-events: none;
}

.md-checkbox .md-checkbox-material {
    vertical-align: middle;
    position: relative;
}

.md-checkbox .md-checkbox-material:before {
    position: absolute;
    left: 8px;
    top: 2px;
    content: "";
    background-color: rgb(47, 174, 248);
    height: 4px;
    width: 4px;
    border-radius: 100%;
    z-index: 1;
    opacity: 0;
    margin: 0;
}

.md-checkbox .md-checkbox-material .check {
    position: relative;
    display: inline-block;
    width: 20px;
    height: 20px;
    border: 2px solid;
    border-radius: 2px;
    overflow: hidden;
    z-index: 1;
    vertical-align: middle;
    top: -3px;
    margin-right: 6px;
}

.md-checkbox .md-checkbox-material .check:before {
    position: absolute;
    content: "";
    -webkit-transform: rotate(45deg);
    transform: rotate(45deg);
    display: block;
    margin-top: -4px;
    margin-left: 6px;
    width: 0;
    height: 0;
    box-shadow: 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0 inset;
    -webkit-animation: md-checkbox-off 0.3s forwards ease-out;
    animation: md-checkbox-off 0.3s forwards ease-out;
}

.md-checkbox input[type="checkbox"]:focus+.md-checkbox-material .check:after {
    opacity: 0.2;
}

.md-checkbox input[type="checkbox"]:checked+.md-checkbox-material .check:before {
    box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px -12px 0 11px;
    -webkit-animation: md-checkbox-on 0.3s forwards ease-out;
    animation: md-checkbox-on 0.3s forwards ease-out;
}

.md-checkbox input[type="checkbox"]:not(:checked)+.md-checkbox-material:before {
    -webkit-animation: rippleOff 700ms forwards ease-out;
    animation: rippleOff 700ms forwards ease-out;
}

.md-checkbox input[type="checkbox"]:checked+.md-checkbox-material:before {
    -webkit-animation: rippleOn 700ms forwards ease-out;
    animation: rippleOn 700ms forwards ease-out;
}

.md-checkbox input[type="checkbox"]:not(:checked)+.md-checkbox-material .check:after {
    -webkit-animation: rippleOff 700ms forwards ease-out;
    animation: rippleOff 700ms forwards ease-out;
}

.md-checkbox input[type="checkbox"]:checked+.md-checkbox-material .check:after {
    -webkit-animation: rippleOn 700ms forwards ease-out;
    animation: rippleOn 700ms forwards ease-out;
}

.md-checkbox input[type="checkbox"][disabled]:not(:checked)~.md-checkbox-material .check:before,
.md-checkbox input[type="checkbox"][disabled]+.circle {
    opacity: 0.5;
}

.md-checkbox input[type="checkbox"][disabled]+.md-checkbox-material .check:after {
    background-color: rgba(0, 0, 0, 0.84);
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);
}

.md-checkbox input[type="checkbox"]:checked+.md-checkbox-material .check {
    color: #2faef8;
}

@-webkit-keyframes md-checkbox-on {
    0% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 15px 2px 0 11px;
    }

    50% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px 2px 0 11px;
    }

    100% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px -12px 0 11px;
    }
}

@keyframes md-checkbox-on {
    0% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 15px 2px 0 11px;
    }

    50% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px 2px 0 11px;
    }

    100% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px -12px 0 11px;
    }
}

@-webkit-keyframes md-checkbox-off {
    0% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px -12px 0 11px, 0 0 0 0 inset;
    }

    25% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px -12px 0 11px, 0 0 0 0 inset;
    }

    50% {
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        margin-top: -4px;
        margin-left: 6px;
        width: 0px;
        height: 0px;
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 15px 2px 0 11px, 0 0 0 0 inset;
    }

    51% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
        margin-top: -2px;
        margin-left: -2px;
        width: 20px;
        height: 20px;
        box-shadow: 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0px 0px 0 10px inset;
    }

    100% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
        margin-top: -2px;
        margin-left: -2px;
        width: 20px;
        height: 20px;
        box-shadow: 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0px 0px 0 0px inset;
    }
}

@keyframes md-checkbox-off {
    0% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px -12px 0 11px, 0 0 0 0 inset;
    }

    25% {
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 20px -12px 0 11px, 0 0 0 0 inset;
    }

    50% {
        -webkit-transform: rotate(45deg);
        transform: rotate(45deg);
        margin-top: -4px;
        margin-left: 6px;
        width: 0px;
        height: 0px;
        box-shadow: 0 0 0 10px, 10px -10px 0 10px, 32px 0px 0 20px, 0px 32px 0 20px, -5px 5px 0 10px, 15px 2px 0 11px, 0 0 0 0 inset;
    }

    51% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
        margin-top: -2px;
        margin-left: -2px;
        width: 20px;
        height: 20px;
        box-shadow: 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0px 0px 0 10px inset;
    }

    100% {
        -webkit-transform: rotate(0deg);
        transform: rotate(0deg);
        margin-top: -2px;
        margin-left: -2px;
        width: 20px;
        height: 20px;
        box-shadow: 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0px 0px 0 0px inset;
    }
}

@-webkit-keyframes rippleOn {
    0% {
        opacity: 0.5;
    }

    100% {
        opacity: 0;
        -webkit-transform: scale(13, 13);
        transform: scale(13, 13);
    }
}

@keyframes rippleOn {
    0% {
        opacity: 0.5;
    }

    100% {
        opacity: 0;
        -webkit-transform: scale(13, 13);
        transform: scale(13, 13);
    }
}

@-webkit-keyframes rippleOff {
    0% {
        opacity: 0.5;
    }

    100% {
        opacity: 0;
        -webkit-transform: scale(13, 13);
        transform: scale(13, 13);
    }
}

@keyframes rippleOff {
    0% {
        opacity: 0.5;
    }

    100% {
        opacity: 0;
        -webkit-transform: scale(13, 13);
        transform: scale(13, 13);
    }
}

@keyframes ripple {
    0% {
        box-shadow: 0px 0px 0px 1px rgba(0, 0, 0, 0);
    }

    50% {
        box-shadow: 0px 0px 0px 15px rgba(0, 0, 0, 0.1);
    }

    100% {
        box-shadow: 0px 0px 0px 15px rgba(0, 0, 0, 0);
    }
}

/* Material design for Bootstrap CheckBox */

/* Material design for Bootstrap RadioBox */
.md-radiobox {
    margin: 0;
}

.md-radiobox {
    display: inline-block;
    padding: 0px 0 0 20px;
    -webkit-transform: translateZ(0);
    transform: translateZ(0);
    margin-bottom: 10px;
}

.md-radiobox label {
    cursor: pointer;
    padding-left: 0;
    color: #636363;
}

.md-radiobox input[type="radio"] {
    opacity: 0;
    position: absolute;
    margin: 0;
    z-index: -1;
    width: 0;
    height: 0;
    overflow: hidden;
    left: 0;
    pointer-events: none;
}

.md-radiobox .md-radiobox-material {
    vertical-align: middle;
    position: relative;
    top: 3px;
    padding-right: 5px;
}

.md-radiobox .md-radiobox-material:before {
    position: absolute;
    left: 8px;
    top: 2px;
    content: "";
    background-color: rgb(47, 174, 248);
    height: 4px;
    width: 4px;
    border-radius: 100%;
    z-index: 1;
    opacity: 0;
    margin: 0;
}

.md-radiobox .md-radiobox-material .check {
    position: relative;
    display: inline-block;
    width: 20px;
    height: 20px;
    border: 2px solid;
    border-radius: 100%;
    overflow: hidden;
    z-index: 1;
}

.md-radiobox .md-radiobox-material .check:before {
    position: absolute;
    content: "";
    display: block;
    margin-top: 4px;
    margin-left: 4px;
    width: 8px;
    height: 8px;
    border-radius: 100%;
    background: transparent;
    /*box-shadow: 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0, 0 0 0 0 inset;*/
    -webkit-animation: md-radiobox-off 0.3s forwards ease-out;
    animation: md-radiobox-off 0.3s forwards ease-out;
}

.md-radiobox input[type="radio"]:focus+.md-radiobox-material .check:after {
    opacity: 0.2;
}

.md-radiobox input[type="radio"]:checked+.md-radiobox-material .check:before {
    background: #2faef8;
}

.md-radiobox input[type="radio"]:not(:checked)+.md-radiobox-material:before {
    -webkit-animation: rippleOff 700ms forwards ease-out;
    animation: rippleOff 700ms forwards ease-out;
}

.md-radiobox input[type="radio"]:checked+.md-radiobox-material:before {
    -webkit-animation: rippleOn 700ms forwards ease-out;
    animation: rippleOn 700ms forwards ease-out;
}

.md-radiobox input[type="radio"]:not(:checked)+.md-radiobox-material .check:after {
    -webkit-animation: rippleOff 700ms forwards ease-out;
    animation: rippleOff 700ms forwards ease-out;
}

.md-radiobox input[type="radio"]:checked+.md-radiobox-material .check:after {
    -webkit-animation: rippleOn 700ms forwards ease-out;
    animation: rippleOn 700ms forwards ease-out;
}

.md-radiobox input[type="radio"][disabled]:not(:checked)~.md-radiobox-material .check:before,
.md-radiobox input[type="radio"][disabled]+.circle {
    opacity: 0.5;
}

.md-radiobox input[type="radio"][disabled]+.md-radiobox-material .check:after {
    background-color: rgba(0, 0, 0, 0.84);
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);
}

.md-radiobox input[type="radio"]:checked+.md-radiobox-material .check {
    color: #fff;
    border-color: #2faef8;
}

/* Material design for Bootstrap RadioBox */

label {
    font-size: 13px;
    color: #222222;
    font-weight: 500;
}

/*input checkbox switch model*/

.switch {
    display: inline-block;
    height: 26px;
    position: relative;
    width: 60px;
}

.switch input {
    display: none;
}

.slider {
    background-color: #e97286;
    bottom: 0;
    cursor: pointer;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    transition: .4s;
    box-shadow: 0px 0px 5px 0px #6d6d6d;
}

.slider:before {
    background-color: #ecedef;
    bottom: 3px;
    content: "";
    height: 20px;
    left: 4px;
    position: absolute;
    transition: .4s;
    width: 20px;
}

.slider:after {
    bottom: 4px;
    content: "\2715";
    display: block;
    position: absolute;
    transition: .4s;
    right: 6px;
    color: #8e0018;
    font-size: 15px;
}

input:checked+.slider {
    background-color: #66bb6a;
}

input:checked+.slider:before {
    transform: translateX(34px);
}

input:checked+.slider:after {
    content: "\2713";
    right: auto;
    left: 8px;
    color: #0e7d13;
}

.slider.round {
    border-radius: 34px;
}

.slider.round:before {
    border-radius: 50%;
}

.fa-icons{
position: absolute;
top: 26px;
right: 5px;
}
