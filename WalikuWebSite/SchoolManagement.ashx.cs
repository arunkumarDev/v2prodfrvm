﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Waliku.GoogleTranslator;

namespace WalikuWebSite
{
    /// <summary>
    /// Summary description for SchoolManagement
    /// </summary>
    public class SchoolManagement : IHttpHandler
    {       

        public void ProcessRequest(HttpContext context)
        {
            VmResponseData VMResponseData = new VmResponseData();

            string strImageName = string.Empty;
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {


                        HttpPostedFile file = files[i];
                        strImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                        string filePath = context.Server.MapPath("~/Photos/" + strImageName);
                       
                        file.SaveAs(filePath);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

            }
            else
            {
                if (Convert.ToInt32(context.Request.Form["ID"]) != 0)
                {
                    strImageName = context.Request.Form["hdnImage"].Trim();
                }
            }
            try
            {
                int AdminId = 0;
                if (context.Request.Form["AdminId"]!=null )
                    AdminId = Convert.ToInt32(context.Request.Form["AdminId"]);

                int ID = Convert.ToInt32(context.Request.Form["ID"]);

                string Note = context.Request.Form["Note"].Trim();

                string CurrentCulture = Common.CommonFunction.GetCurrentCulture(), ToCulture = Common.CommonFunction.GetToCulture();
                Dictionary<string, string> Dict = GoogleTranslator.TranslateText(Note, CurrentCulture, ToCulture);


                int InsertedID = SqlHelper.ManageSchool(new SchoolMaster()
                {
                    ID = Convert.ToInt32(context.Request.Form["ID"]),
                    SchoolName = context.Request.Form["SchoolName"].Trim(),                   
                    Email = context.Request.Form["Email"].Trim(),
                    Image = strImageName,
                    City = context.Request.Form["City"].Trim(),
                    Address = context.Request.Form["Address"].Trim(),
                    Country = context.Request.Form["Country"].Trim(),
                    Phone1 = context.Request.Form["Phone1"].Trim(),
                    Phone2 = context.Request.Form["Phone2"].Trim(),
                    Note = Dict["en"],   // Note,
                    GeoLocation = context.Request.Form["GeoLocation"].Trim(),
                    CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                    CreatedOn = DateTime.Now,
                    IsActive = 1,
                    Note_Indonesian = Dict["id"]
                });

                if (InsertedID > 0)
                {

                    if (AdminId > 0)
                    {
                        SqlHelper.ManageAdminMapping(new SchoolAdminMapping()
                        {
                            SchoolId = (ID == 0) ? InsertedID : ID,
                            UserId = AdminId,
                            CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                            CreatedOn = DateTime.Now,
                            IsActive = 1
                        });
                    }
                }

               
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}