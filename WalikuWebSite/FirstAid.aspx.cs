﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.App_Start;

public partial class FirstAid : System.Web.UI.Page
{
    public string LangID = "";
    
    protected void Page_Load(object sender, EventArgs e)
    {
     
        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
        LangID = Common.CommonFunction.GetCurrentCulture();
    }

    [WebMethod]
    public static List<VmFirstAidAnalytics> FirstAidCounterList()
    {
        return SqlHelper.GetFirstAidCounterList();
    }
    

}