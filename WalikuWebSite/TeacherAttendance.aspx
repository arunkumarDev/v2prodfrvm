﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="TeacherAttendance.aspx.cs" Inherits="WalikuWebSite.TeacherAttendance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
     <link id="bs-css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
       
    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div id="divLoader" class="loading" align="center"> 
             <div class="loading_inner">
        <img src="Content/loading.gif" alt="Loader" /> 
              </div>
      </div>
             <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_StaffAttendance %></h1>
                </div>
            </div>
            <div class="page-content">

                 <div class="row">
                    <div class="col-sm-12">
                        <div class="user-details">
                            <div class="user_data">
                                <div class="row">

                                  <%--  <div class="col-md-1">
                                         <div class="bmd-form-group">
                                         <label class="bmd-label-floating"  ><%=Resources.Resource.Lbl_Date %> :</label>
                                         </div>
                                    </div>  --%>
                                     <div class="col-md-2">
                                         <div class="bmd-form-group">
                                              <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectDate %></label>
                                         <input type="text" class="form-control date-icon hasDatepicker" id="dtCalendarDate">
                                         <i class="fa fa-calendar fa-icons"></i>
                                         </div>
                                    </div>  

            

                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 
                <div class="row">
                    <br />
                </div>

                <div class="table-responsive">
                    <table id="dataTableTeacherAttendance" class="table table-striped table-bordered data-table mb-0" style="width:100%" role="grid">
                        <thead>
                            <tr>
                                <th style="width:70px;"><span><%=Resources.Resource.Lbl_SerialNo %></span></th>   
                                <th style="width:200px;"><span><%=Resources.Resource.Lbl_StaffID %></span></th>
                                <th style="width:250px;"><span><%=Resources.Resource.Lbl_StaffName %></span></th>
                                <th style="width:250px;"><span><%=Resources.Resource.Lbl_StaffType %></span></th>
                                <th style="width:130px;"><span><%=Resources.Resource.Lbl_StaffPresent %></span></th>
                                <th style="width:230px;"><span><%=Resources.Resource.Lbl_ReasonType %></span></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

                <div class="row">
                   <div class="bmd-form-group">
                        <div class="col-md-12 text-right">
                           <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveStaffAttendance"> <%=Resources.Resource.Lbl_Save %> </button>
                         </div>
                    </div>
                 </div>

            </div>
        </div>
    </main>
   <script src="Scripts/bootstrap-datepicker.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.id.min.js"></script>

    <script>document.write("<script type='text/javascript' src='Scripts/TeacherAttendance.js?v=" + JsVerion + "'><\/script>");</script>
     <script>

         $('#dtCalendarDate').datepicker({
             autoclose: true,
            language: localStorage.getItem('loglang')
            
         });
         var date = new Date(); //'2018-01-01'
         $('#dtCalendarDate').datepicker('setDate', date);

    </script>
</asp:Content>


