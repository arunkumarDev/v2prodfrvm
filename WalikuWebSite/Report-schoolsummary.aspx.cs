﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalikuWebSite
{
    public partial class Report_schoolsummary : System.Web.UI.Page
    {
        public int uType = 0;
        public int UserID = 0;
        public bool LoggedInMobile = false;
        string Lang = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                HttpContext.Current.Session["uType1"] = 0;
                HttpContext.Current.Session["divtest"] = "false";
                if (Request.QueryString["auth_key"] != null && !string.IsNullOrEmpty(Request.QueryString["auth_key"].ToString()))
                {
                    UserID = Convert.ToInt32(Request.QueryString["auth_key"].ToString());
                    LoggedInMobile = true;
                }
                if (Request.QueryString["Lang"] != null && !string.IsNullOrEmpty(Request.QueryString["Lang"].ToString()))
                {
                    Lang = Request.QueryString["Lang"].ToString().Trim();
                }


                if (!Common.CommonFunction.IsUserLoggedIn() && UserID == 0)
                {
                    Response.Redirect("login");
                }
                
            }
            if (UserID == 0)
            {
                UserID = Common.CommonFunction.GetUserIdFromCookieByID();
            }

            //    UserID = Common.CommonFunction.GetUserIdFromCookieByID();
            var myUser = SqlHelper.GetUserByID(UserID);
            if (myUser.Tables[0].Rows.Count > 0)
            {
                uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
                if (LoggedInMobile)
                {
                    Common.CommonFunction.CreateCookie(UserID, true, Lang);
                }
            }
        }


    }
}