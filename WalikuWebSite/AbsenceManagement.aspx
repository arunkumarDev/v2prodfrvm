﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AbsenceManagement.aspx.cs" Inherits="WalikuWebSite.AbsenceManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link id="bs-css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">
    <main class="l-main custom-material">
       
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_Absences %></h1>
                </div>
            </div>
            <div class="row mb-10">
                <div class="col-sm-2">
                    <div class="bmd-form-group">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_StartDate %></label>
                        <input type="text" class="form-control date-icon hasDatepicker" id="dtStartDate">
                        <i class="fa fa-calendar fa-icons"></i>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="bmd-form-group">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_EndDate %></label>
                        <input type="text" class="form-control date-icon hasDatepicker" id="dtEndDate">
                        <i class="fa fa-calendar fa-icons"></i>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchool" class="form-control">
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ClassName %></label>
                        <select id="ddlClassRoom" class="form-control">
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                <div class="col-sm-2 mt-15">
                    <button type="button" class="btn btn-primary" title="" id="btnSearch"><%=Resources.Resource.Lbl_Submit %></button>
                </div>
            </div>
            <div class="row mb-10">
                <div class="col-md-9">
                    <p class="student-count"><%=Resources.Resource.Lbl_TotalAbsence %> : <span id="totalAbsence">0</span></p>
                </div>
              
            </div>

               <div class="">
                    <div class="page-content">
                        <div class="table-responsive ">
                            <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                    <tbody>
                                    </tbody>
                            </table>
                        </div>
                    </div>
                </div>

        </div>
    </main>

    <div id="editabsence" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Lbl_EditAbsence %></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <form class="custom-material">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="text-14"><%=Resources.Resource.Lbl_AttendanceConfirmation %></label>
                                    <div class="radio-wrapper mt-5">
                                        <div class="md-radiobox">
                                            <label>
                                                <input type="radio" name="rdoAttendance" value="1" id="rdoPresent">
                                                <span class="md-radiobox-material">
                                                    <span class="check"></span>
                                                </span><%=Resources.Resource.Lbl_Present %>
                                            </label>
                                        </div>
                                        <div class="md-radiobox">
                                            <label>
                                                <input type="radio" name="rdoAttendance" id="rdoAbsent" value="0">
                                                <span class="md-radiobox-material">
                                                    <span class="check"></span>
                                                </span><%=Resources.Resource.Lbl_Abscent %>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6" id="dvAbsenceReason">
                                    <div class="bmd-form-group is-filled">
                                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ReasonAbsence %></label>
                                        <select id="ddlAbsenceReason" class="form-control">
                                        </select>
                                        <i class="fa fa-angle-down fa-icons text-18"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="dvInformer">
                                <div class="col-md-6">
                                    <div class="bmd-form-group is-filled">
                                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_AbsenceInformer %></label>
                                        <select id="ddlInformer" class="form-control">
                                            <%--<option value="0">By self</option>
                                    <option value="1">Other Student</option>
                                    <option value="2">Other Adults</option>
                                    <option value="3">Parent</option>--%>
                                        </select>
                                        <i class="fa fa-angle-down fa-icons text-18"></i>
                                    </div>
                                </div>
                                <div class="col-md-6" id="dvHealthReason">
                                    <div class="bmd-form-group is-filled">
                                        <label class="bmd-label-floating"><span id="lblReason"><%=Resources.Resource.Lbl_HealthReason %></span></label>
                                        <select id="ddlReason" class="form-control">
                                        </select>
                                        <i class="fa fa-angle-down fa-icons text-18"></i>
                                    </div>
                                    <div class="bmd-form-group" id="dvOtherReason" style="display: none">
                                        <label class="bmd-label-floating"><span id="lblOtherReason"><%=Resources.Resource.Lbl_OtherHealthIssues %></span></label>
                                        <input id="txtOtherReason" type="text" class="form-control">
                                    </div>
                                    <%--  <div class="bmd-form-group is-filled" >
                                <label class="bmd-label-floating">Absence Reason Non Health</label>
                                <select id="ddlNonHealth" class="form-control">
                                    <option value="0">Fever</option>
                                    <option value="1">Cough</option>
                                    <option value="2">Headache</option>
                                    <option value="3">Broken</option>
                                </select>
                                <i class="fa fa-angle-down fa-icons text-18"></i>
                            </div>
                           <div class="bmd-form-group" >
                                <label class="bmd-label-floating">Add other non health related issues</label>
                                <input  id="txtotherNonHealthIssue" type="text" class="form-control">
                            </div>--%>
                                </div>
                            </div>
                            <div class="row" id="dvPermission" style="display:none;">
                                <div class="col-md-12 mt-20">
                                    <label class="text-14"><input type="checkbox" id="chkPermission"> <%=Resources.Resource.Lbl_PriorPermissionNonHealth %>?</label>
                                    <%--<label class="switch toggle-check">
                                        <input id="IsPermission" type="checkbox" checked>
                                        <span class="slider round"></span>
                                    </label>--%>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12 text-right">
                        <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal"><%=Resources.Resource.Lbl_Cancel %> </button>
                        <button type="button" class="btn btn-primary" title="" id="btnSave"><%=Resources.Resource.Lbl_Save %> </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="Scripts/bootstrap-datepicker.js"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/AbsenceManagement.js?v=" + JsVerion + "'><\/script>");</script>

    <script>

        var LocalResources = {
            "Lbl_AbsenceReasonNonHealth" :'<%= Resources.Resource.Lbl_AbsenceReasonNonHealth %>',
           "Lbl_HealthReason": '<%= Resources.Resource.Lbl_HealthReason %>',
           "Lbl_OtherNonHealthIssues": '<%= Resources.Resource.Lbl_OtherNonHealthIssues %>',
            "Lbl_OtherHealthIssues": '<%= Resources.Resource.Lbl_OtherHealthIssues %>',
            "Lbl_AbsenceInformer": '<%= Resources.Resource.Lbl_AbsenceInformer %>',
             "Lbl_ReasonForAbsense": '<%= Resources.Resource.Lbl_ReasonForAbsense %>'            
        }

        $('#dtStartDate').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
        var date = new Date(); //'2018-01-01'
        $('#dtStartDate').datepicker('setDate', date);

       $('#dtEndDate').datepicker({
           autoclose: true,
           format: 'dd/mm/yyyy'
        });
         var date = new Date()
        $('#dtEndDate').datepicker('setDate', date);

    </script>

</asp:Content>
