﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;
using WalikuWebSite.Entities.DataModels;

namespace WalikuWebSite
{
    public partial class InactiveStudents : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<ChildrenMaster> LoadInactiveStudents()
        {
            try
            {
                List<ChildrenMaster> childrenList = SqlHelper.GetAllStudentsForDeletion();
                
                //foreach(var child in childrenList)
                //{
                //    var bytes = Convert.FromBase64String(child.Image);
                //    using (var imageFile = new FileStream(@"~\WalikuWebSite\images\StudentProfile\"+child.ID+".png", FileMode.Create))
                //    {
                //        imageFile.Write(bytes, 0, bytes.Length);
                //        imageFile.Flush();
                //    }

                //    child.ImageBase = 

                //    return image;
                //}

                return childrenList;
            }
           catch(Exception e)
            {
                return null;
            }

        }

        [WebMethod]
        public static void DeleteChildren(int ID)
        {
            SqlHelper.ArchiveChildByID(ID);
        }

    }
}