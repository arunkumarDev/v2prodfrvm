﻿using DocumentFormat.OpenXml.Office.CustomUI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using WalikuWebSite.App_Start;

namespace WalikuWebSite.Reports
{
    public static class TeacherReport
    {
        static string ConnectionString = Common.SqlQueries.CONNECTION_STRING;


        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportOnload(string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.TeacherSchoolSummary, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "LoadReports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }


        public static List<AbsentOrPresentMaster> GetClassPresentsOrAbsentsChildren(string ClassRoomID, DateTime SelectedDate)
        {
            SqlConnection sqlCnn = new SqlConnection(ConnectionString);
            DataSet ds = new DataSet();
            try
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAbsentsOrPresentsForClass, ClassRoomID, SelectedDate.Year, SelectedDate.Month, SelectedDate.Day), sqlCnn);
                SqlDataAdapter adapter = new SqlDataAdapter
                {
                    SelectCommand = sqlCmd
                };
                adapter.Fill(ds);

                adapter.Dispose();
                sqlCmd.Dispose();
                sqlCnn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<AbsentOrPresentMaster> lst = ds.Tables[0].ToList<AbsentOrPresentMaster>();
                    return lst;
                }
                else
                    return new List<AbsentOrPresentMaster>();
            }
            catch (Exception ex)
            {
                return new List<AbsentOrPresentMaster>();
            }

        }

        //public static List<ChildrenMaster> GetStudentWisePresentAbsentReport(int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        public static List<VmChildrenReport> GetStudentWisePresentAbsentReport(int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetStudentWisePresentAbsentReport, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();
                    List<VmChildrenReport> lst = new List<VmChildrenReport>();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        // List<VmChildrenReport> lst = ds.Tables[0].ToList<VmChildrenReport>();

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            VmChildrenReport obj = new VmChildrenReport();

                            obj.ID = (row.Table.Columns.Contains("ID") && row["ID"] != DBNull.Value) ? Convert.ToInt32(row["ID"].ToString()) : 0;
                            obj.Name = (row.Table.Columns.Contains("Name") && row["Name"] != DBNull.Value) ? row["Name"].ToString() : "";
                            obj.Attendance = (row.Table.Columns.Contains("Attendance") && row["Attendance"] != DBNull.Value) ? row["Attendance"].ToString() : "";
                            obj.AttendanceDate = (row.Table.Columns.Contains("AttendanceDate") && row["AttendanceDate"] != DBNull.Value) ? Convert.ToInt32(row["AttendanceDate"].ToString()) : 0;
                            obj.AttendanceType = (row.Table.Columns.Contains("AttendanceType") && row["AttendanceType"] != DBNull.Value) ? row["AttendanceType"].ToString() : "";
                            obj.Gender = (row.Table.Columns.Contains("Gender") && row["Gender"] != DBNull.Value) ? row["Gender"].ToString() : "";
                            obj.AbsentDays = (row.Table.Columns.Contains("AbsentDays") && row["AbsentDays"] != DBNull.Value) ? Convert.ToInt32(row["AbsentDays"].ToString()) : 0;
                            obj.ReasonType = (row.Table.Columns.Contains("ReasonType") && row["ReasonType"] != DBNull.Value) ? row["ReasonType"].ToString() : "";
                            lst.Add(obj);
                        }

                        return lst;
                    }
                    return new List<VmChildrenReport>();

                }
            }

        }



        public static List<VmChildrenReport> GetAdminStaffSummaryMonthPrintout(int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {

                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetAdminStaffSummaryMonthPrintout, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@SchoolId", ClassId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();
                    List<VmChildrenReport> lst = new List<VmChildrenReport>();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        // List<VmChildrenReport> lst = ds.Tables[0].ToList<VmChildrenReport>();

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            VmChildrenReport obj = new VmChildrenReport();

                            obj.ID = (row.Table.Columns.Contains("ID") && row["ID"] != DBNull.Value) ? Convert.ToInt32(row["ID"].ToString()) : 0;
                            obj.Name = (row.Table.Columns.Contains("Name") && row["Name"] != DBNull.Value) ? row["Name"].ToString() : "";
                            obj.Attendance = (row.Table.Columns.Contains("Attendance") && row["Attendance"] != DBNull.Value) ? row["Attendance"].ToString() : "";
                            obj.AttendanceDate = (row.Table.Columns.Contains("AttendanceDate") && row["AttendanceDate"] != DBNull.Value) ? Convert.ToInt32(row["AttendanceDate"].ToString()) : 0;
                            obj.AttendanceType = (row.Table.Columns.Contains("AttendanceType") && row["AttendanceType"] != DBNull.Value) ? row["AttendanceType"].ToString() : "";
                            obj.Gender = (row.Table.Columns.Contains("Gender") && row["Gender"] != DBNull.Value) ? row["Gender"].ToString() : "";
                            obj.AbsentDays = (row.Table.Columns.Contains("AbsentDays") && row["AbsentDays"] != DBNull.Value) ? Convert.ToInt32(row["AbsentDays"].ToString()) : 0;
                            obj.ReasonType = (row.Table.Columns.Contains("ReasonType") && row["ReasonType"] != DBNull.Value) ? row["ReasonType"].ToString() : "";
                            obj.TotalDays = (row.Table.Columns.Contains("TotalDays") && row["TotalDays"] != DBNull.Value) ? row["TotalDays"].ToString() : "";

                            lst.Add(obj);
                        }

                        return lst;
                    }
                    return new List<VmChildrenReport>();

                }
            }

        }

        public static List<ChartMaster.PresentAbsetReport> GetTeacherPresentAbsentReport(int ClassId, string SelectedDate, int SelectedType, int TeacherId, int MenuType)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = MenuType == 1 ? ReportQueries.TeacherSchoolSummary : ReportQueries.GetClassWisePresentAbsentReport;
                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChildrenMaster> GetStudentsAddedorDeletedList(int ClassId, string SelectedDate, int SelectedType, int TeacherId, int MenuType)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = ReportQueries.GetStudentsAddedorDeletedList;

                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@Type", MenuType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@ClassRoomId", ClassId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "StudentsAddedorDeletedList");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChildrenMaster> lst = ds.Tables[0].ToList<ChildrenMaster>();
                        return lst;
                    }
                    return new List<ChildrenMaster>();

                }
            }

        }


        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReport(int ClassId, string SelectedDate, int SelectedType, int TeacherId, int MenuType)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = "";
                switch (MenuType)
                {
                    case 1:
                        commandText = ReportQueries.TeacherSchoolSummary;
                        break;
                    case 2:
                        commandText = ReportQueries.TeacherClassSummary;
                        break;
                    case 3:
                        commandText = ReportQueries.TeacherIndividualClassSummary;
                        break;
                    case 4:
                        commandText = ReportQueries.TeacherIndividualChildSummary;
                        break;
                    case 5:
                        commandText = ReportQueries.TeacherStaffSummary;
                        break;
                    default:
                        commandText = ReportQueries.GetClassWisePresentAbsentReport;
                        break;
                }
                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    if (MenuType == 3)
                        cmd.Parameters.AddWithValue("@ClassRoomId", ClassId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdmin(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId, int MenuType = 0, int ChildID = 0)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = "";
                switch (MenuType)
                {
                    case 1:
                        commandText = ReportQueries.AdminSchoolSummary;
                        break;
                    case 2:
                        commandText = ReportQueries.AdminCompareClasses;
                        break;
                    case 3:
                        commandText = ReportQueries.AdminIndividualClassSummary;
                        break;
                    case 4:
                        commandText = ReportQueries.AdminChildSummary;
                        break;
                    case 5:
                        commandText = ReportQueries.AdminStaffSummary;
                        break;
                    default:
                        commandText = ReportQueries.GetClassWisePresentAbsentReport;
                        break;
                }
                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    if (MenuType != 4)
                    {
                        if (MenuType == 3)
                            cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                        if (MenuType == 3 || MenuType == 1)
                            cmd.Parameters.AddWithValue("@LangId", Common.CommonFunction.GetCurrentCulture());
                        if (MenuType != 5)
                        {
                            if (MenuType != 3)
                                cmd.Parameters.AddWithValue("@SchoolID", SchoolId);

                            cmd.Parameters.AddWithValue("@Type", SelectedType);
                            cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                        }
                        else
                        {

                            cmd.Parameters.AddWithValue("@UserId", TeacherId);
                            cmd.Parameters.AddWithValue("@SchoolID", SchoolId);
                        }
                    }
                    else
                        cmd.Parameters.AddWithValue("@ChildID", ChildID);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                     if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        if (lst[0].ClassName != null)
                            lst.Sort((f1, f2) => f1.ClassName.CompareTo(f2.ClassName));

                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummary(int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyAttendanceRateSummary, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForSA(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyAttendanceRateSummaryForSA, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChartMaster.PresentAbsetReport> GetMonthlyAttendanceRateSummaryForAdmin(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyAttendanceRateSummaryForAdmin, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@SchoolID", SchoolId);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChartMaster.PresentAbsetReport> GetMonthlyReasonReportRate(int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyReasonReportRate, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChartMaster.PresentAbsetReport> GetReasonSummary(int ClassId, int TeacherId, string SelectedDate, int SelectedType, int MenuType, int ChildID = 0)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var cmdText = ReportQueries.ReportReasonRateSummary;
                if (MenuType == 1 || MenuType == 2)
                    cmdText = ReportQueries.ReasonCountSchoolSummary;
                if (MenuType == 3)
                    cmdText = ReportQueries.ReasonCountIndividualClass;
                if (MenuType == 4)
                    cmdText = ReportQueries.MobileChildReasonSummary;
                using (SqlCommand cmd = new SqlCommand(cmdText, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    if (MenuType != 4)
                    {
                        cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                        cmd.Parameters.AddWithValue("@TeacherId", TeacherId);

                        cmd.Parameters.AddWithValue("@Type", SelectedType);
                    }
                    else
                        cmd.Parameters.AddWithValue("@ChildID", ChildID);
                        
                    if (MenuType == 1 || MenuType == 2 || MenuType == 3 || MenuType == 4)
                        cmd.Parameters.AddWithValue("@LangId", Common.CommonFunction.GetCurrentCulture());


                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }


        public static List<ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsent(int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyStudentAbsent, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }


        public static List<ChartMaster.PresentAbsetReport> GetMonthlyStudentAbsentForSA(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyStudentAbsentForSA
, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }


        public static List<ChartMaster.HolidayResponse> GetHolidayList(int ClassId, string SelectedDate, int SelectedType)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetHolidayListForReport, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.HolidayResponse> lst = ds.Tables[0].ToList<ChartMaster.HolidayResponse>();
                        return lst;
                    }
                    return new List<ChartMaster.HolidayResponse>();
                }
            }
        }


        public static Dictionary<string, List<ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountNew(int ClassId, string SelectedDate, int SelectedType, int TeacherId, string LangID)
        {
            DataSet ds = new DataSet();
            Dictionary<string, List<ChartMaster.PresentAbsetReport>> resultSet = new Dictionary<string, List<ChartMaster.PresentAbsetReport>>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyReasonCount, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@LangID", LangID);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    List<ChartMaster.PresentAbsetReport> lst = new List<ChartMaster.PresentAbsetReport>();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ChartMaster.PresentAbsetReport obj = new ChartMaster.PresentAbsetReport();

                            obj.AttendanceMonth = (row.Table.Columns.Contains("AttendanceMon") && row["AttendanceMon"] != DBNull.Value) ? Convert.ToInt32(row["AttendanceMon"].ToString()) : 0;
                            obj.AttendanceMonthName = (row.Table.Columns.Contains("AttendanceMonthName") && row["AttendanceMonthName"] != DBNull.Value) ? row["AttendanceMonthName"].ToString() : "";
                            obj.Description = (row.Table.Columns.Contains("Description") && row["Description"] != DBNull.Value) ? row["Description"].ToString() : "";
                            obj.ReasonType = (row.Table.Columns.Contains("ReasonType") && row["ReasonType"] != DBNull.Value) ? Convert.ToInt32(row["ReasonType"].ToString()) : 0;
                            obj.ReasonCode = (row.Table.Columns.Contains("ReasonCode") && row["ReasonCode"] != DBNull.Value) ? row["ReasonCode"].ToString() : "";
                            obj.ReasonCount = (row.Table.Columns.Contains("ReasonCount") && row["ReasonCount"] != DBNull.Value) ? Convert.ToInt32(row["ReasonCount"].ToString()) : 0;

                            lst.Add(obj);
                        }

                        resultSet.Add("Top3Reason", lst);
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        lst = new List<ChartMaster.PresentAbsetReport>();

                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            ChartMaster.PresentAbsetReport obj = new ChartMaster.PresentAbsetReport();

                            obj.AttendanceMonth = (row.Table.Columns.Contains("AttendanceMon") && row["AttendanceMon"] != DBNull.Value) ? Convert.ToInt32(row["AttendanceMon"].ToString()) : 0;
                            obj.AttendanceMonthName = (row.Table.Columns.Contains("AttendanceMonthName") && row["AttendanceMonthName"] != DBNull.Value) ? row["AttendanceMonthName"].ToString() : "";
                            obj.Description = (row.Table.Columns.Contains("Description") && row["Description"] != DBNull.Value) ? row["Description"].ToString() : "";
                            obj.ReasonType = (row.Table.Columns.Contains("ReasonType") && row["ReasonType"] != DBNull.Value) ? Convert.ToInt32(row["ReasonType"].ToString()) : 0;
                            obj.ReasonCode = (row.Table.Columns.Contains("ReasonCode") && row["ReasonCode"] != DBNull.Value) ? row["ReasonCode"].ToString() : "";
                            obj.ReasonCount = (row.Table.Columns.Contains("ReasonCount") && row["ReasonCount"] != DBNull.Value) ? Convert.ToInt32(row["ReasonCount"].ToString()) : 0;

                            lst.Add(obj);
                        }

                        resultSet.Add("PieChart", lst);
                    }

                    return resultSet;

                }
            }

        }


        public static List<ChartMaster.PresentAbsetReport> GetMonthlyReasonCount(int ClassId, string SelectedDate, int SelectedType, int TeacherId, string LangID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyReasonCount, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@LangID", LangID);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }


        public static Dictionary<string, List<ChartMaster.PresentAbsetReport>> GetMonthlyReasonCountForSA(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId, string LangID)
        {
            DataSet ds = new DataSet();
            Dictionary<string, List<ChartMaster.PresentAbsetReport>> resultSet = new Dictionary<string, List<ChartMaster.PresentAbsetReport>>();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyReasonCountForSA, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@LangID", LangID);
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        resultSet.Add("Top3Reason", lst);
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[1].ToList<ChartMaster.PresentAbsetReport>();
                        resultSet.Add("PieChart", lst);
                    }

                    return resultSet;

                }
            }

        }


        public static string GetYearlyHealthReason(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId, string LangID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetYearlyHealthReason, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    cmd.Parameters.AddWithValue("@LangID", LangID);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in ds.Tables[0].Columns)
                            {
                                row.Add(col.ColumnName, dr[col]);
                            }
                            rows.Add(row);
                        }
                        return serializer.Serialize(rows);
                    }
                    return "";

                }
            }

        }

        public static string GetYearlyNonHealthReason(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId, string LangID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetYearlyNonHealthReason, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    cmd.Parameters.AddWithValue("@LangID", LangID);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                        List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                        Dictionary<string, object> row;
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            row = new Dictionary<string, object>();
                            foreach (DataColumn col in ds.Tables[0].Columns)
                            {
                                row.Add(col.ColumnName, dr[col]);
                            }
                            rows.Add(row);
                        }
                        return serializer.Serialize(rows);
                    }
                    return "";

                }
            }

        }


        public static List<ChartMaster.PresentAbsetReport> GetMonthlyReasonCount2(string SelectedDate)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyReasonCount2, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }



        public static List<ChartMaster.AcademicYearResponse> GetCompareClassAcademicYear(int SchoolId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetCompareClassAcademicYear, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.AddWithValue("@SelectedYear", SelectedYear);
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);


                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.AcademicYearResponse> lst = ds.Tables[0].ToList<ChartMaster.AcademicYearResponse>();
                        return lst;
                    }
                    return new List<ChartMaster.AcademicYearResponse>();
                }
            }
        }


        // Added by arun for academic year 
        public static List<ChartMaster.PresentAbsetReport> GetClassWisePresentAbsentReportForAdminAcademicYear(int SchoolId, int ClassId, string SelectedDate, int SelectedType, int TeacherId, string AcademicYear, int MenuType = 0, int ChildID = 0)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = "";
                switch (MenuType)
                {
                    case 1:
                        commandText = ReportQueries.AdminSchoolSummary;
                        break;
                    case 2:
                        commandText = ReportQueries.AdminCompareClassesAcademicYear;
                        break;
                    case 3:
                        commandText = ReportQueries.AdminIndividualClassSummary;
                        break;
                    case 4:
                        commandText = ReportQueries.AdminChildSummary;
                        break;
                    case 5:
                        commandText = ReportQueries.AdminStaffSummary;
                        break;
                    default:
                        commandText = ReportQueries.GetClassWisePresentAbsentReport;
                        break;
                }
                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {


                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    if (MenuType != 4)
                    {
                        if (MenuType == 3)
                            cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                        if (MenuType != 5)
                        {
                            cmd.Parameters.AddWithValue("@SchoolID", SchoolId);
                            cmd.Parameters.AddWithValue("@Type", SelectedType);
                            cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                            cmd.Parameters.AddWithValue("@AcademicYear", AcademicYear);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@UserId", TeacherId);
                           
                        }
                    }
                    else
                        cmd.Parameters.AddWithValue("@ChildID", ChildID);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.PresentAbsetReport> lst = ds.Tables[0].ToList<ChartMaster.PresentAbsetReport>();
                        if (lst[0].ClassName != null)
                            lst.Sort((f1, f2) => f1.ClassName.CompareTo(f2.ClassName));

                        return lst;
                    }
                    return new List<ChartMaster.PresentAbsetReport>();

                }
            }

        }

        public static List<ChartMaster.AcademicYearGenderResponse> GetClassWiseGenderReportForAdminAcademicYear(int SchoolId, string GenderSelectedType)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetClassWiseGenderReportForAdminAcademicYear, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    cmd.Parameters.AddWithValue("@GenderSelectedType", GenderSelectedType);


                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.AcademicYearGenderResponse> lst = ds.Tables[0].ToList<ChartMaster.AcademicYearGenderResponse>();
                        if (lst[0].ClassRoomName != null)
                            lst.Sort((f1, f2) => f1.ClassRoomName.CompareTo(f2.ClassRoomName));
                        return lst;
                    }
                    return new List<ChartMaster.AcademicYearGenderResponse>();
                }
            }
        }

        public static List<ChartMaster.AbsentismMonthResponse> GetMonthlyAbsentismForReports(int SchoolId, String SelectedDate)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetMonthlyAbsentismForReports, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);


                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.AbsentismMonthResponse> lst = ds.Tables[0].ToList<ChartMaster.AbsentismMonthResponse>();
                        return lst;
                    }
                    return new List<ChartMaster.AbsentismMonthResponse>();
                }
            }
        }

        public static List<ChartMaster.LoadAbsentDetailsForChildResponse> LoadAbsentDetailsForChild(int SelMonth, int SelYear, int ChildID)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.LoadAbsentDetailsForChild, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@ChildID", ChildID);
                    cmd.Parameters.AddWithValue("@SelYear", SelYear);
                    cmd.Parameters.AddWithValue("@SelMonth", SelMonth);
                    cmd.Parameters.AddWithValue("@LangId", Common.CommonFunction.GetCurrentCulture());

                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.LoadAbsentDetailsForChildResponse> lst = ds.Tables[0].ToList<ChartMaster.LoadAbsentDetailsForChildResponse>();
                        return lst;
                    }
                    return new List<ChartMaster.LoadAbsentDetailsForChildResponse>();
                }
            }
        }


        public static List<ChartMaster.LoadYearlyTopReasonDetails> GetYearlyTopReasonDetails(int ClassId, int TeacherId, string SelectedDate, int SelectedType, int MenuType, int ChildID = 0)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.LoadYearlyTopReasonDetails, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@TeacherID", TeacherId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@LangId", Common.CommonFunction.GetCurrentCulture());
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.LoadYearlyTopReasonDetails> lst = ds.Tables[0].ToList<ChartMaster.LoadYearlyTopReasonDetails>();
                        return lst;
                    }
                    return new List<ChartMaster.LoadYearlyTopReasonDetails>();

                }
            }

        }

        public static List<ChartMaster.LoadMonthlyStudentDetails> GetTrackStudentsMonthly(string SelectedDate,int SchoolId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = Common.SqlQueries.GetTrackStudents;

                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Date", SelectedDate);
                    cmd.Parameters.AddWithValue("@SchoolId", SchoolId);
                    cmd.Parameters.AddWithValue("@LangId", Common.CommonFunction.GetCurrentCulture());
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "StudentsAddedorDeletedList");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.LoadMonthlyStudentDetails> lst = ds.Tables[0].ToList<ChartMaster.LoadMonthlyStudentDetails>();
                        return lst;
                    }
                    return new List<ChartMaster.LoadMonthlyStudentDetails>();

                }
            }

        }


        //public static List<ChildrenMaster> GetStudentWisePresentAbsentReport(int ClassId, string SelectedDate, int SelectedType, int TeacherId)
        public static List<VmChildrenReport> GetYearlychronicList(int ClassId, string SelectedDate, int SelectedType, int TeacherId, string ReasonType)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                using (SqlCommand cmd = new SqlCommand(ReportQueries.GetYearlychronicList, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@ClassRoomID", ClassId);
                    cmd.Parameters.AddWithValue("@Type", SelectedType);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@ReasonType", ReasonType);

                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "Reports");
                    adapter.Dispose();
                    List<VmChildrenReport> lst = new List<VmChildrenReport>();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        // List<VmChildrenReport> lst = ds.Tables[0].ToList<VmChildrenReport>();

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            VmChildrenReport obj = new VmChildrenReport();

                            obj.ID = (row.Table.Columns.Contains("ID") && row["ID"] != DBNull.Value) ? Convert.ToInt32(row["ID"].ToString()) : 0;
                            obj.Name = (row.Table.Columns.Contains("Name") && row["Name"] != DBNull.Value) ? row["Name"].ToString() : "";
                            obj.AttendanceDate = (row.Table.Columns.Contains("AttendanceDate") && row["AttendanceDate"] != DBNull.Value) ? Convert.ToInt32(row["AttendanceDate"].ToString()) : 0;
                            obj.AbsentDays = (row.Table.Columns.Contains("AbsentDays") && row["AbsentDays"] != DBNull.Value) ? Convert.ToInt32(row["AbsentDays"].ToString()) : 0;
                            obj.ReasonType = (row.Table.Columns.Contains("ReasonType") && row["ReasonType"] != DBNull.Value) ? row["ReasonType"].ToString() : "";
                            obj.NISN = (row.Table.Columns.Contains("NISN") && row["NISN"] != DBNull.Value) ? row["NISN"].ToString() : "";
                            obj.ClassName = (row.Table.Columns.Contains("ClassName") && row["ClassName"] != DBNull.Value) ? row["ClassName"].ToString() : "";

                            lst.Add(obj);
                        }

                        return lst;
                    }
                    return new List<VmChildrenReport>();

                }
            }

        }


        public static List<ChartMaster.LoadConsecutiveresponse> LoadConsecutiveDaysAbsentMonthly(string SelectedDate,int TeacherId,int SchoolId,int ClassId)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = ReportQueries.LoadConsecutiveDaysAbsentMonthly;

                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@TeacherId", TeacherId);
                    cmd.Parameters.AddWithValue("@SchoolID", SchoolId);
                    cmd.Parameters.AddWithValue("@ClassId", ClassId);
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "StudentsAddedorDeletedList");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.LoadConsecutiveresponse> lst = ds.Tables[0].ToList<ChartMaster.LoadConsecutiveresponse>();
                        return lst;
                    }
                    return new List<ChartMaster.LoadConsecutiveresponse>();

                }
            }

        }

        

            public static List<ChartMaster.LoadStaffSummaryresponse> LoadMonthlyStaffAttendanceRateSummaryForLabel(int SchoolId, string SelectedDate)
        {
            DataSet ds = new DataSet();

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                var commandText = ReportQueries.LoadMonthlyStaffAttendanceRateSummaryForLabel;

                using (SqlCommand cmd = new SqlCommand(commandText, con))
                {

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@AttendanceDate", SelectedDate);
                    cmd.Parameters.AddWithValue("@SchoolID", SchoolId);
                   
                    con.Open();

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = cmd;
                    adapter.Fill(ds, "StudentsAddedorDeletedList");
                    adapter.Dispose();

                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        List<ChartMaster.LoadStaffSummaryresponse> lst = ds.Tables[0].ToList<ChartMaster.LoadStaffSummaryresponse>();
                        return lst;
                    }
                    return new List<ChartMaster.LoadStaffSummaryresponse>();

                }
            }

        }


        public static List<ChartMaster.LoadAcademicMonthDetails> GetAcademicMonthDetails(int SchoolId)
        {

            SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING);
            DataSet ds = new DataSet();
            try
            {
                sqlCnn.Open();
                SqlCommand sqlCmd = new SqlCommand(string.Format(Common.SqlQueries.GetAcademicMonthDetails, SchoolId), sqlCnn);
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = sqlCmd;
                adapter.Fill(ds, "ClassMaster");

                adapter.Dispose();
                sqlCmd.Dispose();
                sqlCnn.Close();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return ds.Tables[0].ToList<ChartMaster.LoadAcademicMonthDetails>();


                }
                return new List<ChartMaster.LoadAcademicMonthDetails>();
            }
            catch (Exception ex)
            {
                return new List<ChartMaster.LoadAcademicMonthDetails>();
            }

        }


    }

}