﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WalikuWebSite.Reports
{
    public class ReportQueries
    {
        public static string GetClassWisePresentAbsentReport = "TeacherDailyReport";
        public static string GetStudentsAddedorDeletedList = "StudentsAddedorDeletedList";
        public static string ReportReasonRateSummary = "ReportReasonRateSummary";
        public static string TeacherSchoolSummary = "TeacherSchoolSummary";
        public static string TeacherClassSummary = "TeacherClassSummary";
        public static string TeacherIndividualChildSummary = "TeacherIndividualChildSummary";
        public static string TeacherIndividualClassSummary = "TeacherIndividualClassSummary";
        public static string TeacherStaffSummary = "TeacherStaffSummary";
        public static string AdminSchoolSummary = "AdminSchoolSummary";
        public static string AdminCompareClasses = "AdminCompareClasses";
        public static string AdminClassSummary = "AdminClassSummary";
        public static string AdminChildSummary = "AdminChildSummary";
        public static string AdminIndividualClassSummary = "AdminIndividualClassSummary";
        public static string AdminStaffSummary = "AdminStaffSummary";

        public static string GetClassWisePresentAbsentReportForAdmin = "AdminDailyReport";

        public static string GetStudentWisePresentAbsentReport = "StudentAttendaceReport";
        public static string GetAdminStaffSummaryMonthPrintout = "AdminStaffSummaryMonthPrintout";

        public static string GetClassWisePresentAbsentReportOnload = "TeacherInitialReport";

        public static string GetMonthlyAttendanceRateSummary = "StudentAttendaceSummary";

        public static string GetMonthlyAttendanceRateSummaryForAdmin = "StudentAttendaceSummaryForAdmin";

        public static string GetMonthlyReasonReportRate = "MonthlyReasonReportRate";

        public static string GetMonthlyStudentAbsent = "StudentMonthlyAttendanceSummary";

        public static string GetMonthlyReasonCount = "MonthlyAbsentReasonSummary";

        public static string GetMonthlyReasonCount1 = "ReasonCountHealth";

        public static string ReasonCountSchoolSummary = "ReasonCountSchoolSummary";

        public static string ReasonCountIndividualClass = "ReasonCountIndividualClass";

        public static string MobileChildReasonSummary = "MobileChildReasonSummary";

        public static string GetMonthlyReasonCount2 = "ReasonCountNonHealth";

        public static string GetHolidayListForReport = "GetHolidayList";

        public static string GetYearlyHealthReason = "GetYearlyHealthReason";

        public static string GetYearlyNonHealthReason = "GetYearlyNonHealthReason";

        public static string GetMonthlyAttendanceRateSummaryForSA = "StudentAttendaceSummaryForSA";

        public static string GetMonthlyReasonCountForSA = "MonthlyAbsentReasonSummaryForSA";

        public static string GetMonthlyStudentAbsentForSA = "StudentMonthlyAttendanceSummaryForSA";

        public static string GetCompareClassAcademicYear = "GetCompareClassAcademicYear";

        public static string AdminCompareClassesAcademicYear = "AdminCompareClassesAcademicYear";

        public static string GetClassWiseGenderReportForAdminAcademicYear = "GetClassWiseGenderReportForAdminAcademicYear";

        public static string GetMonthlyAbsentismForReports = "GetMonthlyAbsentismForReports";

        public static string LoadAbsentDetailsForChild = "GetMonthlyAbsentDetailsForChild";

        public static string LoadYearlyTopReasonDetails = "GETTOPREASONSCOUNTYEARLY";

        public static string GetYearlychronicList = "YearlychronicList";

        public static string LoadConsecutiveDaysAbsentMonthly = "Usp_consecutivedaysAbsent_Web";

        public static string LoadMonthlyStaffAttendanceRateSummaryForLabel = "AdminStaffSummaryPrintRates";

    }
}