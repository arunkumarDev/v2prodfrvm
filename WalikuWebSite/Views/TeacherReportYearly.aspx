﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeacherReportYearly.aspx.cs" Inherits="WalikuWebSite.TeacherReportYearly" %>

      <div class="clearfix"></div>
      <h4 id="YearlyLeftSummary"><%=Resources.Resource.Title_YearlyAttendanceSummary%></h4>
      
      <div class="row report-leftsec">
    
        <div class="col-sm-3" id="leftsumarry">          
 
            <ol class="list-group mt-20">
                <li class="list-group-item" id="Yearlyleftsec"><span><%=Resources.Resource.Lbl_YearlyAttendanceRate%> </span><span class="badge badge-success" id="YearlyAttendance">%</span></li>
              </ol>  
          <p class="mt-15" id="YearlySummary" ><span><%=Resources.Resource.Lbl_YearlyAbsenteeismRate%> </span> </p>      
          <div class="stats" style="border-top:0;" id ="YearlySummary1">
            <div>
              <span><img src="images/icons/Class1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Class.ToUpper() %></span> </strong><span id="AbsenteesimRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/boy1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Boys.ToUpper() %></span></strong><span id="BoysAbsenteeRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/girl1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Girls.ToUpper() %></span></strong><span id="GirlsAbsenteeRate">%</span>
            </div>
          </div>
          <div class="stats" id ="YearlySummary2">
            <div>
              <span><img src="images/icons/health.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_Health.ToUpper() %></span></strong><span id="HealthAbsenteeRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/absent.png" alt="image"></span>
              <strong class="mb-5 mt-5"><span><%=Resources.Resource.Lbl_NonHealth.ToUpper() %></span></strong><span id="NonHealthAbsenteeRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/unknown.png" alt="image"></span>
              <strong class="mb-5 mt-5" ><span><%=Resources.Resource.Lbl_Unknown.ToUpper() %></span></strong><span id="UnKnownAbsenteeRate">%</span>
            </div>
          </div>
          <ol class="list-group mt-20" id ="YearlySummary3">            
              <li class="list-group-item"><span> <%=Resources.Resource.Lbl_ChronicallyAbsent %></span> <span class="badge  badge-error" id="StudentCount1"></span></li>
            <li class="list-group-item"><span> <%=Resources.Resource.Lbl_SeverelyChronicallyAbsent %> </span><span class="badge badge-warning"  id="StudentCount2"></span></li>
          </ol>
          <p id ="YearlySummary4"><%=Resources.Resource.Lbl_NumberOfStudentsAbsent %> </p>
          <div class="Health-Reasons" id ="YearlySummary5">
            <div class="panel panel-default">
              <div class="panel-heading">
                <%=Resources.Resource.Lbl_CommonHealthReasons %> 
              </div>
              <div class="panel-body" id="dvHealthReasonCount">
                
              </div>
              <div class="panel-heading">
               <%=Resources.Resource.Lbl_CommonNonHealthReasons %> 
              </div>
                <div class="panel-body" id="dvNonHealthReasonCount">
              
              </div>
            </div>
          </div>
      
   
        </div>
       
        <div class="col-sm-9">
          <div class="page-content">
            <div class="portlet">
              <div class="portlet-title">
                <span> <%=Resources.Resource.Title_ChronicAndSevereChronic %> </span>
                <ul class="nav nav-tabs">
                  <li>
                    <a href="#portlet_tab2" data-toggle="tab">
                      <i class="fa fa-list-ul" aria-hidden="true"></i> </a>
                  </li>
                  <li class="active">
                    <a href="#portlet_tab1" data-toggle="tab">
                      <i class="fa fa-th" aria-hidden="true"></i> </a>
                  </li>
                </ul>
              </div>
              <div class="portlet-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="portlet_tab1">
                    <div class="filter-box mt-0">
                      <div id="attendance-chart" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                    </div>
                  </div>
                  <div class="tab-pane" id="portlet_tab2">
                    <div class="data-table-wrapper">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="table-responsive">
                            <table id="dataTableAttendance" class="table table-striped table-hover" style="width:100%">
                              <thead id="tblhead">
                                
                              </thead>
                              <tbody id="tblBody">
                                                       
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
            <br/><br/>
          <div class="page-content" id="Yearlygraph">
          <div class="col-md-6">
              <div id="burden-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          <div class="col-md-6">
              <div id="burden-non-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          </div>

        </div>
      </div>

    <script src="../Scripts/Reports/TeacherReportYearly.js?6"></script>

