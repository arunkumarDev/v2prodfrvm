﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StudentForm.aspx.cs" Inherits="WalikuWebSite.StudentForm" %>

      <div class="clearfix"></div>
       
      
      <div class="row">
     
        <form class="custom-material">
            <h4 id="DailySummary"><%=Resources.Resource.Rdo_StudentForm%></h4>    
          <div class="page-content">
         <div class="col-md-2">
                        <div class="image-view is-filled">
                            <img src="images/user.png" id="uploadProfilePhoto" class="img-responsive img-border" alt="preview">
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div id ="studentFormGroup">
                            
                        </div>
                    </div>
          </div>
           

        </form>
      </div>

    <script type="text/javascript" src="Scripts/AdminOperations/StudentForm.js"></script>