﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SuperAdminReportYearly.aspx.cs" Inherits="WalikuWebSite.SuperAdminReportYearly" %>

     <%-- <div class="clearfix"></div>
      
      <div class="row mt-20">        
        <div class="col-sm-6">
          <div class="page-content"  style="min-height: 490px; max-height: 490px;overflow: auto;">           
            <div class="portlet">
                <div class="portlet-title">
                    <span><%=Resources.Resource.Lbl_YearlyAbsenteeismRate.ToUpper()%> </span>
                  <ul class="nav nav-tabs">
                    <li>
                      <a href="#portlet_tab2" data-toggle="tab">
                        <i class="fa fa-list-ul" aria-hidden="true"></i> </a>
                    </li>
                    <li class="active">
                      <a href="#portlet_tab1" data-toggle="tab">
                        <i class="fa fa-th" aria-hidden="true"></i> </a>
                    </li>
                  </ul>
                </div>
                <div class="portlet-body">
                  <div class="tab-content">                     
                    <div class="tab-pane active" id="portlet_tab1">
                      <div class="filter-box mt-0">
                        <div id="absenteeism-rate" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                      </div>
                    </div>
                    <div class="tab-pane" id="portlet_tab2">
                        <div class="gridview-tbl table-responsive">                        
                         <table id="dataTableAttendance" class="stripe row-border order-column display" style="width:100%">
                            <thead id="tblhead">
                                
                            </thead>
                            <tbody id="tblBody">
                                                           
                            </tbody>
                          </table> 
                        </div>
                      
                      </div>
                    
                  </div>
                </div>
              </div>
          </div>
        </div>
        <div class="col-sm-6">
            <div class="page-content">
              <p><%=Resources.Resource.Lbl_AbsenteeismRateByReason.ToUpper() %></p>
              <div class="filter-box mt-0">
                <div id="absenteeism-rate-reason" class="custom-charts" style="min-width: 310px; height: 429px; margin: 0 auto"></div>
              </div>
            </div>
          </div>
      </div>

<div class="row mt-20">        
        <div class="col-sm-6">
          <div class="page-content">
            <p><%=Resources.Resource.Lbl_NumStudentsNonHealthSymptoms %></p>
            <div class="filter-box mt-0">
              <div id="absence-nonhealth-reason" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
            </div>
          </div>
        </div>
        <div class="col-sm-6">
            <div class="page-content">
              <p><%=Resources.Resource.Lbl_NumStudentsHealthSymptoms %></p>
              <div class="filter-box mt-0">
                <div id="absence-prevalence-reason" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
              </div>
            </div>
          </div>
      </div>    --%>


    <div class="clearfix"></div>
 <%--<% if (HttpContext.Current.Session["divtest"] == "true")  //(uType != 1)
        { %>--%>
    <div id="testdiv" runat="server">
      <%-- <% if (HttpContext.Current.Session["uType1"].ToString() == "2")  //(uType != 1)
        { %>--%>
           <div class="col-sm-12" >  
        <select id="ddlyear" class="form-control">
            
                        </select>
               </div>
        <%--<div class="col-sm-6 mt-10 mb-20" >  
        <select id="ddlGender" class="form-control">
             <option value="All"><%=Resources.Resource.Lbl_AllGender%></option>            
                <option value="M"><%=Resources.Resource.Lbl_Boys%></option>  
                <option value="F"><%=Resources.Resource.Lbl_Girls%></option> 
                        </select>
               </div>--%>
          <%--  <% } %>  --%>
        </div>
<%-- <% } %> --%> 
      <div class="row mt-20">        
        <div class="col-sm-12">
          <div class="page-content"  style="">           
            <div class="portlet">
                <div class="portlet-title">
                  
                    <span><%=Resources.Resource.Lbl_YearlyAbsenteeismRate.ToUpper()%> </span>
                  <ul class="nav nav-tabs">
                    <li class="active">
                      <a href="#portlet_tab1" data-toggle="tab">
                        <i class="fa fa-th" aria-hidden="true"></i> </a>
                    </li>
                  </ul>
                </div>
                <div class="portlet-body">
                  <div class="tab-content">                     
                    <div class="tab-pane active" id="portlet_tab1">
                      <div class="filter-box mt-0">
                           <div class="row mt-20">        
                        <div class="col-sm-12">
                        <div id="absenteeism-rate" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                      </div>
                               </div>
                        </div>
                      <%--  <div class="filter-box mt-0">
                        <div id="gender-charts" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                      </div>--%>
                    </div>
                    <div class="tab-pane" id="portlet_tab2">
                        <div class="gridview-tbl table-responsive">                        
                       <%--  <table id="dataTableAttendance" class="stripe row-border order-column display" style="width:100%">
                            <thead id="tblhead">
                                
                            </thead>
                            <tbody id="tblBody">
                                                           
                            </tbody>
                          </table> --%>
                        </div>
                      
                      </div>
                    
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
     <div class="row mt-20" id="hidecmpclass">
         <div class="col-md-8">
             <div class="col-md-6">
          <div class="stats">
            <div class="i_presentcount"></div>
              <span id="AttendanceRate"></span>
              <strong ><%=Resources.Resource.Lbl_Rpt_AttendanceRate%> (YTD)</strong>
            </div>
          </div>
              <div class="col-md-6">
            <div class="stats">
            <div class="i_absencecount"></div>
                <span id="AbsenteesimRate"></span>
              <strong  ><%=Resources.Resource.Lbl_Rpt_AbsenteeismRate%> (YTD)</strong>
            </div>
          </div>
           <div class="col-md-6 mt-10">
             <div class="stats">
            <div class="i_studentsaddedordeleted"></div>
                 <span id="count-chronic"></span>
              <strong ><%=Resources.Resource.Lbl_Rptchronic%></strong>
                 <a id="chronicPopup"><%=Resources.Resource.Lbl_Year_ChList%></a>
            </div>
          </div>
              <div class="col-md-6 mt-10">
             <div class="stats">
            <div class="i_studentsaddedordeleted"></div>
                 <span id="count-sevchronic"></span>
              <strong ><%=Resources.Resource.Lbl_Rptsev_chronic%></strong>
                 <a id="sev-chronicPopup"><%=Resources.Resource.Lbl_Year_ChList%></a>
            </div>
          </div>
             </div>
                 <div class="col-md-4">
          <div class="stats">
            <div class="i_topreason" style="width: 25%;"></div>
               <span id="Health" style="font-size:15px"> </span> 
               <span class="numberofstd" style="top: 0px !important;"> <%=Resources.Resource.Lbl_Noofstud%>  </span>
              <ul class="studentlist">
                  <li id="Health1"> </li>
                  <li id="Health2"> </li>
                  <li id="Health3"> </li>
                  <li id="Health4"> </li>
                  <li id="Health5"> </li>
              </ul> 

              <%--<strong ><%=Resources.Resource.Lbl_Health.ToUpper()%></strong>--%>
            </div>
          </div>

        </div>

    <script src="../Scripts/Reports/SuperAdminReportYearly.js?9"></script>



<div id="Viewsev-chronicPopup" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog" style="width:70%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="closepopup">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Lbl_Rptsev_chronic%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="data-table-wrapper">
                    <table id="dataTableAttendance" class="stripe row-border order-column display" style="width:100%">
                            <thead id="tblhead">
                                
                            </thead>
                            <tbody id="tblBody">
                                                           
                            </tbody>
                          </table> 
  </div>
                    </div>
                </div>
            </div>
         </div>


<div id="ViewchronicPopup" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog" style="width:70%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="closepopup1">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Lbl_Rptchronic%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="data-table-wrapper">
                    <table id="dataTableAttendance1" class="stripe row-border order-column display" style="width:100%">
                            <thead id="tblhead1">
                                
                            </thead>
                            <tbody id="tblBody1">
                                                           
                            </tbody>
                          </table> 
  </div>
                    </div>
                </div>
            </div>
         </div>
