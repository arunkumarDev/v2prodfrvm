﻿
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TeacherReportDaily.aspx.cs" Inherits="WalikuWebSite.TeacherReportDaily" %>

      <div class="clearfix"></div>
      <h4 id="DailySummary"><%=Resources.Resource.Title_DailyAttendanceSummary%></h4>     
      
      <div class="row report-leftsec">
        <div class="col-sm-3" id="leftsumarry">          
          <ol class="list-group" id="Dailyleftsec">
            <li class="list-group-item"><span><%=Resources.Resource.Lbl_PresentStudents %> </span><span class="badge badge-success" id="presentCount"></span></li>
            <li class="list-group-item"><span><%=Resources.Resource.Lbl_AbsentStudents %></span><span class="badge  badge-error" id="absentCount"></span></li>
            <li class="list-group-item"><span><%=Resources.Resource.Lbl_AbsentBoys %></span><span class="badge badge-error" id="absentBoysCount"></span></li>
            <li class="list-group-item"><span><%=Resources.Resource.Lbl_AbsentGirls %></span><span class="badge badge-error" id="absentGirlsCount"></span></li>
          </ol>
           
        </div>
       
        <div class="col-sm-9">
          <div class="page-content">
            <div class="portlet">
              <div class="portlet-title" id="grid">   
                <div class="caption caption-red">
                   <p id="monthName"> </p>
               </div>
                <ul class="nav nav-tabs">
                  <li>
                    <a href="#portlet_tab2" data-toggle="tab">
                      <i class="fa fa-list-ul" aria-hidden="true"></i> </a>
                  </li>
                  <li class="active">
                    <a href="#portlet_tab1" data-toggle="tab">
                      <i class="fa fa-th" aria-hidden="true"></i> </a>
                  </li>
                </ul>
              </div>
              <div class="portlet-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="portlet_tab1">
                      <p id="MonthlyGraphHeading"> <%=Resources.Resource.Lbl_Attendance %>  </p>
                      <p id="YearlyGraphHeading"> <%=Resources.Resource.Title_ChronicAndSevereChronic %></p>
                    <div class="filter-box mt-0">
                      <div id="attendance-chart" class="custom-charts" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                         <div id="attendance-chart2" class="custom-charts" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                        <div id="attendance-chart3" class="custom-charts" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                        <div id="attendance-chart4" class="custom-charts" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                    </div>
                  </div>
                  <div class="tab-pane" id="portlet_tab2">
                    <div class="custom-scroll" style="padding-right: 5px;">
                           <div class="gridview-tbl table-responsive" style="min-height:380px;">     
                             <table id="dataTableAttendance" class="stripe row-border order-column display" style="width:100%">
                              <thead id="tblhead">
                 
                              </thead>
                                 <tbody id="tblBody">

                                 </tbody>
                            </table>
                          </div>
                        </div>
                       <div class="clearfix"></div>
                     </div>
                   
                    <ul class="list-inline mt-20 gridview-tbl-info">
                       <li><span class="legend"><%=Resources.Resource.Holiday %></span>- <%=Resources.Resource.Lbl_Holiday %></li>
                      <li><span class="legend"><%=Resources.Resource.Present %></span>- <%=Resources.Resource.Lbl_Present %></li>
                      <li><span class="legend"><%=Resources.Resource.Health %></span>- <%=Resources.Resource.Lbl_Health %> </li>
                      <li><span class="legend"><%=Resources.Resource.NonHealth %></span>- <%=Resources.Resource.Lbl_NonHealth %></li>
                      <li><span class="legend"><%=Resources.Resource.UnknownLeave %></span>- <%=Resources.Resource.Lbl_UnknownLeave %></li>
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>
           

        </div>

    <script src="../Scripts/Reports/TeacherReportDaily.js?9"></script>

