﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminReportWeekly.aspx.cs" Inherits="WalikuWebSite.AdminReportWeekly" %>

      <div class="clearfix"></div>
      <%--<h4 id="DailySummary"><%=Resources.Resource.Title_WeeklyAttendanceSummary%></h4>--%>     
      
      <div class="row report-leftsec">
      
       
        <div class="col-sm-12">
          <div class="page-content">
            <div class="portlet">
              <div class="portlet-title" id="grid"> 
                <div class="caption caption-red">
                 <%--  <p id="monthName">&nbsp;</p>--%>
               </div>
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a href="#portlet_tab1" data-toggle="tab">
                      <i class="fa fa-th" aria-hidden="true"></i> </a>
                  </li>
                </ul>
              </div>
              <div class="portlet-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="portlet_tab1">
                      <%--<p id="MonthlyGraphHeading"> <%=Resources.Resource.Lbl_Attendance %> </p>--%>
                      <p id="YearlyGraphHeading"><%=Resources.Resource.Title_ChronicAndSevereChronic %></p>
                    <div class="filter-box mt-0">
                      <div id="attendance-chart" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                        <%-- <div id="attendance-chart2" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                        <div id="attendance-chart3" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                        <div id="attendance-chart4" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>--%>
                    </div>
                  </div>
                  <div class="tab-pane" id="portlet_tab2">
                    <div class="custom-scroll" >
                      <div class="table-responsive gridview-tbl" style="height:400px; padding-right: 5px;">
                        <table id="dataTableAttendance" class="table table-striped table-bordered data-table mb-0" role="grid">
                          <thead id="tblhead">                            
                          </thead>
                             <tbody id="tblBody">

                             </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <ul class="list-inline mt-20 gridview-tbl-info">
                      <li><span class="legend"><%=Resources.Resource.Holiday %></span>- <%=Resources.Resource.Lbl_Holiday %></li>
                      <li><span class="legend"><%=Resources.Resource.Present %></span>- <%=Resources.Resource.Lbl_Present %></li>
                      <li><span class="legend"><%=Resources.Resource.Health %></span>- <%=Resources.Resource.Lbl_Health %> </li>
                      <li><span class="legend"><%=Resources.Resource.NonHealth %></span>- <%=Resources.Resource.Lbl_NonHealth %></li>
                      <li><span class="legend"><%=Resources.Resource.UnknownLeave %></span>- <%=Resources.Resource.Lbl_UnknownLeave %></li>
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>
           

        </div>

        <div class="col-sm-12 mt-20" id="hidecmpclass">
             <div class="col-md-3">
          <div class="stats">
            <div class="i_healthreasoncount"></div>
              <span id="HealthRate"></span>%
              <strong ><%=Resources.Resource.Lbl_Rpt_SchoolAbsentStudents%>
                   <br />
                  <%=Resources.Resource.Lbl_RptHealth%>
              </strong>
            </div>
          </div>
              <div class="col-md-3">
            <div class="stats">
            <div class="i_nonhealthreasoncount"></div>
                <span id="NonHealthRate"></span>%
              <strong  ><%=Resources.Resource.Lbl_Rpt_SchoolAbsentStudents%>
                  <br />
                  <%=Resources.Resource.Lbl_RptNonHealth%>
              </strong>
            </div>
          </div>
           <div class="col-md-3">
             <div class="stats">
            <div class="i_unknownreasoncount"></div>
                 <span id="UnknownRate"></span> %
              <strong ><%=Resources.Resource.Lbl_Rpt_SchoolAbsentStudents%>
                   <br />
                  <%=Resources.Resource.Lbl_Rptunknown%>
              </strong>
            </div>
          </div>
                 <%--<div class="col-md-3">
          <div class="stats">
              
            <div class="i_topreason"></div>
               <span class="numberofstd"><%=Resources.Resource.Lbl_Noofstud%>  </span>
               <span class="mr-abs" id="TopReason"></span>
              <strong class="mr-abs" ><%=Resources.Resource.Lbl_TopReason%>
                
              </strong>
            </div>
          </div>--%>

              <div class="col-md-3" id="Equals">
          <div class="stats">              
            <div class="i_topreason"> </div>
              <span class="numberofstd"> <%=Resources.Resource.Lbl_Noofstud%>  </span>
              
               <strong class="mr-abs" style="top:-5px;" >
                   <%=Resources.Resource.Lbl_topabsunknown%>
               </strong>
             <span class="mr-abs"  id="TopReason" style="font-weight:400;top:-5px;font-size:16px"></span>
             
            </div>
          </div>

             <div class="col-md-3" id="NotEquals">
          <div class="stats">              
            <div class="i_topreason"> </div>
              <span class="numberofstd"> <%=Resources.Resource.Lbl_Noofstud%>  </span>
             <span class="mr-abs"  id="TopReason1" style="font-weight:700"></span>
               <strong class="mr-abs" >
                   <%=Resources.Resource.Lbl_topabsunknown%>
               </strong>
             
            </div>
          </div>



        </div>
      </div>

    <script src="../Scripts/Reports/AdminReportWeekly.js?8"></script>

<script>  
  
   
  </script>
<style>
    th, td{white-space:nowrap;}
   div.dataTables_wrapper{
       width:100%;
       
   }
</style>