﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WalikuWebSite.Views
{
    public partial class SuperAdminReportWeekly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetLanguage(Common.CommonFunction.GetUserLanguageFromCookie());
            }
        }
        private void SetLanguage(string Language)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Language);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Language);
                Page.Culture = Language;
            }
            catch (Exception) { }
        }
    }
}