﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SuperAdminReportMonthly.aspx.cs" Inherits="WalikuWebSite.SuperAdminReportMonthly" %>

      <div class="clearfix"></div>
      <%--<h4 id="MonthlyLeftSummary"><%=Resources.Resource.Title_MonthlyAttendanceSummary %> </h4>--%>
      
      <div class="row report-leftsec">
    
        <%--<div class="col-sm-3" id="leftsumarry">          
 
            <ol class="list-group mt-20">
                <li class="list-group-item" id="Monthlyleftsec"><span><%=Resources.Resource.Lbl_MonthlyAttendanceRate %> </span><span class="badge badge-success" id="MonthlyAttendance">%</span></li>
              </ol>  
          <p class="mt-15" id="MonthlySummary" ><%=Resources.Resource.Lbl_MonthlyAbsenteeismRate %> </p>      
          <div class="stats" style="border-top:0;" id ="MonthlySummary1">
            <div>
              <span><img src="images/icons/Class1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><%=Resources.Resource.Lbl_School.ToUpper() %> </strong><span id="AbsenteesimRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/boy1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><%=Resources.Resource.Lbl_Boys.ToUpper() %></strong><span id="BoysAbsenteeRate">%</span>
            </div>
            <div>
              <span><img src="images/icons/girl1.png" alt="image"></span>
              <strong class="mb-5 mt-5"><%=Resources.Resource.Lbl_Girls.ToUpper() %></strong><span id="GirlsAbsenteeRate">%</span>
            </div>
          </div>
          <div class="stats" id ="MonthlySummary2">
            <div>
              <span><img src="images/icons/health.png" alt="image"></span>
              <strong class="mb-5 mt-5"><%=Resources.Resource.Lbl_Health.ToUpper() %></strong><span id="Health">%</span>
            </div>
            <div>
              <span><img src="images/icons/absent.png" alt="image"></span>
              <strong class="mb-5 mt-5"><%=Resources.Resource.Lbl_NonHealth.ToUpper() %><</strong><span id="NonHealth">%</span>
            </div>
            <div>
              <span><img src="images/icons/unknown.png" alt="image"></span>
              <strong class="mb-5 mt-5" ><%=Resources.Resource.Lbl_Unknown.ToUpper() %></strong><span id="Unknown">%</span>
            </div>
          </div>
          <ol class="list-group mt-20" id ="MonthlySummary3">
            <!-- <li class="list-group-item">Monthly Attendance Rate <span class="badge badge-success">14</span></li> -->
            <li class="list-group-item"><span><%=Resources.Resource.Lbl_StudentAbsent %> &gt;= 3 <%=Resources.Resource.Lbl_Days.ToUpper() %> </span><span class="badge  badge-error" id="StudentCount1"></span></li>
            <li class="list-group-item"><span><%=Resources.Resource.Lbl_StudentAbsent %> &lt; 3 <%=Resources.Resource.Lbl_Days.ToUpper() %></span><span class="badge badge-warning"  id="StudentCount2"></span></li>
          </ol>
          <p id ="MonthlySummary4"><%=Resources.Resource.Lbl_NumberOfStudentsAbsent %></p>
          <div class="Health-Reasons" id ="MonthlySummary5">
            <div class="panel panel-default">
              <div class="panel-heading">
                <%=Resources.Resource.Lbl_CommonHealthReasons %>
              </div>
              <div class="panel-body" id="dvHealthReasonCount">
                
              </div>
              <div class="panel-heading">
                <%=Resources.Resource.Lbl_CommonNonHealthReasons %>
              </div>
                <div class="panel-body" id="dvNonHealthReasonCount">
              
              </div>
            </div>
          </div>
      
   
        </div>
       
        <div class="col-sm-9">
          <div class="page-content">
            <div class="portlet">
              <div class="portlet-title" id="grid">      
                  <div class="caption caption-red">
                   <p id="monthName"> </p>
               </div>
                <ul class="nav nav-tabs">
                  <li>
                    <a href="#portlet_tab2" data-toggle="tab">
                      <i class="fa fa-list-ul" aria-hidden="true"></i> </a>
                  </li>
                  <li class="active">
                    <a href="#portlet_tab1" data-toggle="tab">
                      <i class="fa fa-th" aria-hidden="true"></i> </a>
                  </li>
                </ul>
              </div>
              <div class="portlet-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="portlet_tab1">
                      <p id="MonthlyGraphHeading"> <%=Resources.Resource.Lbl_Attendance %> </p>
                      <p id="YearlyGraphHeading"> <%=Resources.Resource.Title_ChronicAndSevereChronic %></p>
                    <div class="filter-box mt-0">
                      <div id="attendance-chart" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                         <div id="attendance-chart2" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                        <div id="attendance-chart3" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                        <div id="attendance-chart4" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                    </div>
                  </div>
                  <div class="tab-pane" id="portlet_tab2">
                    <div class="custom-scroll" style="max-height:400px; padding-right: 5px;">
                      <div class="table-responsive gridview-tbl">
                        <table id="dataTableAttendance" class="table table-striped table-bordered data-table mb-0" role="grid">
                          <thead id="tblhead">
                 
                          </thead>
                             <tbody id="tblBody">

                             </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <ul class="list-inline mt-20 gridview-tbl-info">
                       <li><span class="legend"><%=Resources.Resource.Holiday %></span>- <%=Resources.Resource.Lbl_Holiday %></li>
                      <li><span class="legend"><%=Resources.Resource.Present %></span>- <%=Resources.Resource.Lbl_Present %></li>
                      <li><span class="legend"><%=Resources.Resource.Health %></span>- <%=Resources.Resource.Lbl_Health %> </li>
                      <li><span class="legend"><%=Resources.Resource.NonHealth %></span>- <%=Resources.Resource.Lbl_NonHealth %></li>
                      <li><span class="legend"><%=Resources.Resource.UnknownLeave %></span>- <%=Resources.Resource.Lbl_UnknownLeave %></li>
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>
            <br/><br/>
          <div class="page-content" id="Monthlygraph">
          <div class="col-md-6">
              <div id="burden-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          <div class="col-md-6">
              <div id="burden-non-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          </div>

        </div>
      </div>--%>


           <div class="col-sm-12">
          <div class="page-content">
            <div class="portlet">
              <div class="portlet-title" id="grid">      
                  <div class="caption caption-red">
                   <p id="monthName"> </p>
               </div>
                <ul class="nav nav-tabs">
                  <li>
                    <a id="listview_printout" href="#portlet_tab2" data-toggle="tab">
                      <i class="fa fa-list-ul" aria-hidden="true"></i> </a>
                  </li>
                  <li class="active">
                    <a href="#portlet_tab1" data-toggle="tab">
                      <i class="fa fa-th" aria-hidden="true"></i> </a>
                  </li>
                </ul>
              </div>
              <div class="portlet-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="portlet_tab1">
                      <%--<p id="MonthlyGraphHeading"> <%=Resources.Resource.Lbl_Attendance %> </p>--%>
                      <p id="YearlyGraphHeading"> <%=Resources.Resource.Title_ChronicAndSevereChronic %></p>
                    <div class="filter-box mt-0">
                        <div class="row mt-20">        
                        <div class="col-sm-12">
                      <div id="attendance-chart" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                         <div id="attendance-chart2" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                        <div id="attendance-chart3" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                        <div id="attendance-chart4" class="custom-charts" style="min-width: 310px;height: 300px; margin: 0 auto"></div>
                           <div id="hidecmpclass3">
                                <a id="clickPopup" class="pull-right">
                                   
                                    <%=Resources.Resource.Title_ViewMore %>
                                </a>
                               </div>
                        </div>
                           

              
                            </div>
                        </div>
                  </div>
                  <div class="tab-pane" id="portlet_tab2">
                    <div class="custom-scroll" style="padding-right: 5px;">
                      <div class="table-responsive gridview-tbl">
                        <table id="dataTableAttendance" style="width:100%"
                            class="table table-striped table-bordered data-table mb-0" role="grid">
                          <thead id="tblhead">
                 
                          </thead>
                             <tbody id="tblBody">

                             </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <ul class="list-inline mt-20 gridview-tbl-info">
                         <label id="lgdlbl"> Legends</label>
                      <li><span class="legend"><%=Resources.Resource.Holiday %></span>- <%=Resources.Resource.Lbl_Holiday %></li>
                      <li><span class="legend"><%=Resources.Resource.Present %></span>- <%=Resources.Resource.Lbl_Present %></li>
                      <li><span class="legend"><%=Resources.Resource.lgd_keylblHealth %> </span>- <%=Resources.Resource.lgd_valuelblHealth %></li>
                      <li><span class="legend"><%=Resources.Resource.lgd_keylblSocial %></span>- <%=Resources.Resource.lgd_valuelblSocial %></li>
                      <li id="hdnunknownfrstaff"><span class="legend"><%=Resources.Resource.lgd_keylblUnknown1 %></span>- <%=Resources.Resource.lgd_valuelblUnknown1 %></li>
                      <li id="hdnunknownfrchild"><span class="legend"><%=Resources.Resource.lgd_keylblUnknown %></span>- <%=Resources.Resource.lgd_valuelblUnknown %></li>
                      
                        <li id="hdnassgmnts"><span class="legend"><%=Resources.Resource.lgd_keylblAssgmnt %></span>- <%=Resources.Resource.lgd_valuelblAssgmnt %></li>
    </ul>

                      <div>
                           <ul class="list-inline mt-20 gridview-tbl-info lg-sum">
                                <label id="sumlbl"> Summary</label>
                              <li><span id="legend1"> </span> </li>
                              <li><span id="legend5"></span></li>
                              <li><span id="legend2"></span></li>
                              <li><span id="legend6"></span></li>
                              <li><span id="legend3"></span></li>
                              <li><span id="legend7"></span></li>
                              <li><span id="legend4"></span></li>
                              <li><span id="legend8"></span></li>

                    </ul>
                      </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
            <br/><br/>
        <%--  <div style="display:none;" class="page-content" id="Monthlygraph">
          <div class="col-md-6">
              <div style="display:none;" id="burden-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          <div class="col-md-6">
              <div style="display:none;" id="burden-non-healthabsence" class="custom-charts" style="min-width: 310px;height: 250px; max-width: 600px; margin: 0 auto"></div>
          </div> 
          </div>--%>
 
        </div>
      </div>
<%--<div class="row mt-20">  --%>  
<div class="row" id="hidecmpclass2">
      <div class="mt-20 reason-summary" >
        <div class="col-md-4">
            <div class="row">
             <div class="col-md-12 threedays">
          <div class="stats">
            <div class="i_absencecount"></div>
              <span id="AttendanceRate"></span>
              <strong ><%=Resources.Resource.Lbl_StudentsAbsentForTwoDays%></strong>
            </div>
          </div>
               <div class="col-md-12 oneortwodays mt-10">
            <div class="stats">
            <div class="i_absencecount"></div>
                <span id="AbsenteesimRate"></span>
              <strong  ><%=Resources.Resource.Lbl_StudentsAbsentForThreeDays%></strong>
            </div>
          </div>
        </div>
      </div>
           <div class="col-md-4 mostcommon">
          <div class="stats" style="min-height: 218px;">
               
            <div class="i_mostcommonreason" style="width: 25%;"></div>
              <span class="numberofstd"> <%=Resources.Resource.Lbl_Noofstud%>  </span>
              <strong ><%=Resources.Resource.Lbl_MostCommonReaons.ToUpper()%></strong>
               <ul class="studentlist">
                  <li id="HealthReason1"> </li>
                  <li id="HealthReason2"> </li>
                  <li id="HealthReason3"> </li>
                
              </ul> 
         <%--     <span id="HealthReason1" style="font-size:15px;"> </span> 
              <span id="HealthReason2" style="font-size:15px;"> </span> 
              <span id="HealthReason3" style="font-size:15px;"> </span> --%>
              
            </div>
          </div> 
            <div class="col-md-4 viewlistlink" >
             <div class="stats" id="AddedDeleted">
            <div class="i_studentsaddedordeleted"></div>
                 <span id="Lbl_Students_count"> </span>
                 <strong ><%=Resources.Resource.Lbl_StudentsAddedOrDeleted.ToUpper()%></strong>
                      <a href="#" id="StudentsAddedorDeleted"><%=Resources.Resource.Lbl_ViewList.ToUpper()%></a>
              
            </div>
          </div>
         
                     
        </div>
       </div>   


  <div id="ViewOtherCharts" class="modal fade custom-modal" role="dialog">
        <div class="modal-dialog modal-lg custom-material " style="width:90%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="closepopup">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Title_monthSummary%></h4>
                </div>
                <div class="modal-body custom-scroll mh-500">
                    <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <p><%=Resources.Resource.Lbl_AbsenteeismRateByReason %></p>
                         <div id="absenteeism-rate-reason" class="custom-charts page-content" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
                            
                        </div>
                    </div>

                         
<div class="row mt-20" id="hidecmpclass1">        
        <div class="col-sm-12">
             <p><%=Resources.Resource.Lbl_NumStudentsNonHealthSymptoms %></p>
          <div class="page-content">
            <div class="filter-box mt-0">
              <div id="absence-nonhealth-reason" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
            </div>
          </div>
        </div>
    </div>
         <div class="row mt-20">
        <div class="col-sm-12">
             <p><%=Resources.Resource.Lbl_NumStudentsHealthSymptoms %></p>
            <div class="page-content">
              <div class="filter-box mt-0">
                <div id="absence-prevalence-reason" class="custom-charts" style="min-width: 310px;height: 250px; margin: 0 auto"></div>
              </div>
            </div>
          </div>
      </div> 

                </div>
                    </div>
                </div>
            </div>
         </div>


    <script src="../Scripts/Reports/SuperAdminReportMonthly.js?9"></script>
