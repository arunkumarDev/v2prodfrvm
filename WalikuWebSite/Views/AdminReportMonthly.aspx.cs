﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.UI;

namespace WalikuWebSite
{
    public partial class AdminReportMonthly : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                SetLanguage(Common.CommonFunction.GetUserLanguageFromCookie());
            }
        }
        private void SetLanguage(string Language)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Language);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Language);
                Page.Culture = Language;
            }
            catch (Exception) { }
        }
    }
}


