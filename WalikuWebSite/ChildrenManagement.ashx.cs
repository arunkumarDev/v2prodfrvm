﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using Waliku.GoogleTranslator;

namespace WalikuWebSite
{
    /// <summary>
    /// Summary description for ChildrenManagement
    /// </summary>
    public class ChildrenManagement : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string strImageName = string.Empty;
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {


                        HttpPostedFile file = files[i];
                        strImageName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                        string filePath = context.Server.MapPath("~/Photos/" + strImageName);
                       // filePath = "h:\\root\\home\\walikuwaliku-001\\www\\site2\\Photos\\" + strImageName;
                        file.SaveAs(filePath);

                      //  string destiPath = @"h:\root\home\walikuwaliku-001\www\Photos\" + strImageName;
                      //  string sourceFile = filePath;
                        
                      //  System.IO.File.Copy(filePath, destiPath, true);

                        //String ftpurl = "ftp://walikuwaliku-001@ftp-eu.site4now.net/Photos/" + strImageName; // e.g. ftp://serverip/foldername/foldername
                        //String ftpusername = "walikuwaliku-001"; // e.g. username
                        //String ftppassword = "BbABy0%o$!WhpN9x"; // e.g. password

                        //string filename = Path.GetFileName(filePath);
                        //string ftpfullpath = ftpurl;
                        //FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpfullpath);
                        //ftp.Credentials = new NetworkCredential(ftpusername, ftppassword);

                        //ftp.KeepAlive = true;
                        //ftp.UseBinary = true;
                        //ftp.Method = WebRequestMethods.Ftp.UploadFile;

                        //FileStream fs = File.OpenRead(filePath);
                        //byte[] buffer = new byte[fs.Length];
                        //fs.Read(buffer, 0, buffer.Length);
                        //fs.Close();

                        //Stream ftpstream = ftp.GetRequestStream();
                        //ftpstream.Write(buffer, 0, buffer.Length);
                        //ftpstream.Close();

                    }
                    catch (Exception ex)
                    {

                        throw ex;
                    }
                }

            }
            else
            {
                if (Convert.ToInt32(context.Request.Form["ID"]) != 0)
                {
                    strImageName = context.Request.Form["hdnImage"].Trim();
                }
            }
            try
            {
                int CommunityWorkerID = 0;
                Int32.TryParse(context.Request.Form["CommunityWorkerID"], out CommunityWorkerID);

                string Note = context.Request.Form["Note"].Trim();

                string CurrentCulture = Common.CommonFunction.GetCurrentCulture(), ToCulture = Common.CommonFunction.GetToCulture();
                Dictionary<string, string> Dict = GoogleTranslator.TranslateText(Note, CurrentCulture, ToCulture);


                SqlHelper.ManageChildren(new ChildrenMaster()
                {


                    ID = Convert.ToInt32(context.Request.Form["ID"]),
                    Name = context.Request.Form["Name"].Trim(),
                    Middlename = context.Request.Form["Middlename"] != "undefined" ? context.Request.Form["Middlename"].Trim() : "",
                    Surname = "",
                    ParentName = context.Request.Form["ParentName"].Trim(),
                    //    CommunityWorkerID = context.Request.Form["CommunityWorkerID"] != null  && context.Request.Form["CommunityWorkerID"] != "null" ? Convert.ToInt32(context.Request.Form["CommunityWorkerID"]) : 0,
                    CommunityWorkerID = CommunityWorkerID,
                    Email = context.Request.Form["Email"].Trim(),
                    Image = strImageName,
                    SchoolID = Convert.ToInt32(context.Request.Form["SchoolID"]),
                    ClassroomID = context.Request.Form["ClassroomID"] != null ? Convert.ToInt32(context.Request.Form["ClassroomID"]) : 0,
                    Address = context.Request.Form["Address"].Trim(),
                    Phone = context.Request.Form["Phone"].Trim(),
                    Note = Dict["en"],   // Note,
                    GeoLocation = context.Request.Form["GeoLocation"].Trim(),
                    Gender = context.Request.Form["Gender"].Trim(),
                    CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                    CreatedOn = DateTime.Now,
                    IsActive = 1,
                    Note_Indonesian = Dict["id"],
                    NISN = context.Request.Form["Nisn"].Trim(),
                    //GradeID = context.Request.Form["GradeID"] != "null" ?  Convert.ToInt32(context.Request.Form["GradeID"]) : 0,
                    DeleteReasonID = 0,
                    Date_of_Birth = Convert.ToDateTime(context.Request.Form["Date_of_Birth"])
                });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}