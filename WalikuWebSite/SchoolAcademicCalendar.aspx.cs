﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;


public partial class SchoolAcademicCalendar : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!Common.CommonFunction.IsUserLoggedIn())
            {
                Response.Redirect("login");
            }
        }
      //  Thread.CurrentThread.CurrentCulture = new CultureInfo("id-ID");
    }

    [WebMethod]
    public static List<CalendarTypeMaster> GetCalendarEventType()
    {
        return SqlHelper.GetCalendarEventType();
    }
    [WebMethod]
    public static List<SchoolMaster> GetMySchoolList(int PageIndex)
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        return AcademicCalendarDataAccess.GetMySchoolList(uid);
    }

    [WebMethod]
    public static List<SchoolMaster> GetMySchools()
    {
        return SqlHelper.GetSchoolListForUser(Common.CommonFunction.GetUserIdFromCookieByID(), Common.CommonFunction.GetCurrentCulture());
    }


    [WebMethod]
    public static List<SchoolAcademicCalendarMaster> GetSchoolAcademicCalendarList(int ID)
    {
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        //HttpContext.Current.Session["SchoolID"] = ID;
        return AcademicCalendarDataAccess.GetSchoolAcademicCalendarList(uid,ID);
    }
    
    [WebMethod]
    public static void DeleteAcademicCalendarByID(int ID)
    {
        SqlHelper.DeleteAcademicCalendarByID(ID);
    }

}