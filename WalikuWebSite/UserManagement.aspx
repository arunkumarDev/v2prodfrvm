﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="UserManagement.aspx.cs" Inherits="UserManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
  <%--  <link rel="Stylesheet" href=" " "text="" javascript""="" src=""https://code.jquery.com/jquery-1.12.3.js""> " target="_blank">https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />  --%>

    <!-- Add User -->
   <div id="viewprofile" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><%=Resources.Resource.Title_UserProfile %></h4>
            </div>
            <div class="modal-body custom-scroll mh-500">
                <form class="custom-material">
                    <div class="col-md-2 w-120">
                        <div class="image-view">
                            <img src="images/user.png" id="viewProfilePhoto" class="img-responsive img-border" alt="preview">
                        </div>
                    </div>
                    <div class="col-md-10 right-content no-pad">
                        <div class="col-md-8">
                            <div class="row mt-20">
                                <div class="col-md-12">
                                    <strong class="pr-15 mb-10" style="display:block;" id="viewName"></strong>
                                   
                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-6">
                                    <i class="fa fa-envelope site-color pr-10" aria-hidden="true"></i><span id="viewEmail"></span>
                                </div>
                                <div class="col-md-6">
                                    <i class="fa fa-phone site-color pr-10" aria-hidden="true"></i><span id="viewPhone"></span>
                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-12">
                                    <i class="fa fa-map-marker site-color pr-15" aria-hidden="true"></i><span class="info-content" id="viewAddress"></span>
                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-12">
                                    <i class="fa fa-comments site-color pr-15" aria-hidden="true"></i><span class="info-content" id="viewNotes"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="viewprofileMap" style="width: 100%; height:280px"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<div id="UserManagementModel" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg custom-material">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Lbl_AddUser %></h4>
            </div>
            <div class="modal-body custom-scroll mh-500">
                <form class="custom-material">
                    <div class="col-md-3 w-190">
                        <div class="image-view">
                            <img src="images/user.png" id="imgUser" class="img-responsive img-border" alt="preview">
                             <input type="file"  id="inImage" name="inImage" class="custom-file-input preview-icon">
                             <input type="text" id="hdnImage" name="hdnImage" style="display:none" />
                            <a href="javascript:;" class="reset-photo"><%=Resources.Resource.Lbl_PhotoUpload %></a>
                              <input type="hidden" id="hdnUserID" value="0" />
                        </div>
                    </div>
                    <div class="col-md-9 pr-0">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_FirstName %></label>
                                    <input type="text" id="txtName" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Surname %></label>
                                     <input type="text" id="txtSurname" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_UserRole %></label>
                                    <select id="ddlUserType" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_UerName %></label>
                                     <input type="text" id="txtLoginUserName" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Password %></label>
                                     <input type="password" id="txtLoginPassword" class="form-control input-validate">
                                    <input type='checkbox' id='toggle' value='0' onchange='togglePassword(this);'>&nbsp; <span id='toggleText'>
                                        <%=Resources.Resource.Lbl_pwdshow %>
                                         </span>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Phone %></label>
                                      <input type="text" id="txtPhone" onkeypress="return isPhoneNumber(event,this)" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Email %></label>
                                   <input type="email"  id="txtEmail"  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating">NIK</label>
                                     <input type="text" id="txtNik" class="form-control" maxlength="16"
                                         onkeypress="return event.charCode >= 48 && event.charCode <= 57"  >
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Gender %></label>
                                    <select id="ddlGender" class="form-control select-validate">
                                        <option value="M"><%=Resources.Resource.Lbl_Male %></option>
                                        <option value="F"><%=Resources.Resource.Lbl_Female %></option>
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bmd-form-group is-filled" style="visibility:hidden">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ReasonForDelete %></label>
                                    <select id="ddlDeleteReason1" class="form-control select-validate">
                                       
                                    </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Address%></label>
                                    <textarea id="txtAddress" class="form-control mt-10 input-validate"></textarea>
                                </div>
                            </div>                          
                            
                          <div class="col-md-6">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Note %></label>
                                    <textarea id="txtNote"  class="form-control mt-10"></textarea>
                                </div>
                            </div>
                          <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group mcus-search">
                                            <input type="text" id="txtLocation" class="form-control" placeholder="<%=Resources.Resource.Lbl_Search %>" name="srch-term">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary" id="btnLocationSearch" type="button"><i class="glyphicon glyphicon-search"></i>  

                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="bmd-form-group">
                                            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SearchResult %></label>
                                            <input type="text" id= "txtGeoLocation" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="col-lg-8">
                                <div id="dvMap" style="width: 100%; height: 200px"></div>
                            </div>

                           <%-- <div class="col-md-8">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26360909.888257876!2d-113.74875964478716!3d36.242299409623534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1545830068358"
                                    width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>--%>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal" > <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button" class="btn btn-primary mr-15" title="" id="btnSaveUser"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>


      
<div id="DelReasonPopup" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><%=Resources.Resource.Title_DeleteUser %></h4>
            </div>
            <div class="modal-body custom-scroll mh-500 custom-material">
                <p class="mt-5 col-md-12"><%=Resources.Resource.Lbl_Delmsg %></p>
                <div class="col-md-12">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating required mb-10">
                                        <%=Resources.Resource.Lbl_ReasonForDelete %></label>
                                    <select id="ddlDeleteReason" class="form-control select-validate">
                                     </select>
                                     <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
            </div>
             <div class="modal-footer" style="padding:0px;">
                <div class="col-md-12 text-right">
                    <button type="button"  style="min-width: 97px;" class="btn btn-default mr-5" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button"  style="min-width: 97px;" class="btn btn-primary" title="" id="btndelChild"> OK </button>
                </div>
            </div>
        </div>
    </div>
</div>

       <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_UserManagement %></h1>
                </div>
            </div>

            <div class="row mb-10">
                <div class="col-md-12">
                    <button type="button" class="btn btn-primary pull-right" title="" data-toggle="modal" id="btnAddUser"><%=Resources.Resource.Lbl_AddUser %></button>
                    <p class="student-count"><%=Resources.Resource.Lbl_TotalUsers %> : <span id="total">0</span></p>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper">
                    <table id="dataTableUser" class="table table-hover mb-0 user-tbl" style="width:100%">
                        <thead>
                            <tr>
                                <th><span><%=Resources.Resource.Lbl_SerialNo %></span></th>
                                <th>&nbsp;</th>                               
                                <th><span><%=Resources.Resource.Lbl_UerName %></span></th>
                                <th><span><%=Resources.Resource.Lbl_UserRole %></span></th>
                                <th><%=Resources.Resource.Lbl_Phone %></th>
                                <th><%=Resources.Resource.Lbl_Address %></th>
                                <th><%=Resources.Resource.Lbl_Action%></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>

    <script>document.write("<script type='text/javascript' src='Scripts/UserManagement.js?v=" +JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV6jv36xrNX23a9RWk-9ZsCSCOmSU-iys"></script>
      <script>
         var Lbl_AddUser = '<%=Resources.Resource.Lbl_AddUser %>'
          var Lbl_EditUser = '<%=Resources.Resource.Lbl_EditUser %>'
          
          $(document).ready(function () {
        //  $('#dataTable').DataTable();
             LoadMap();
        });

     var map;
     var marker;
    var geocoder;
    var markers = [];

    function LoadMap() {
	  geocoder = new google.maps.Geocoder();
	   var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
           // center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: latlng
        };
        map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

        google.maps.event.addListener(map, 'click', function (e) {
            $("#txtGeoLocation").val(e.latLng.lat().toFixed(3) + "/" + e.latLng.lng().toFixed(3));              
            });

       // SetMarker(0);
        setDefaultMapLocation();
    };

          function setDefaultMapLocation() {
              //searchMapLocation('Paris, France');
              searchMapLocation('Waikabubak, Indonesia');
              FnGooglMaps.clearMarkers(markers);
          }

          function setLocation(latlng) {
              if (latlng != "") {
                  var myLatlng = latlng.split("/");
                   var data =  {                     	
                        "lat": myLatlng[0],	
                        "lng": myLatlng[1]	                      
                    }
					SetMapMarker(data);
              }
               else
                    setDefaultMapLocation();
            
          }
    function SetMapMarker(data) {
        FnGooglMaps.clearMarkers(markers);
        var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        marker = new google.maps.Marker({
            position: myLatlng,
            map: map
          //  title: data.title
        });
         markers.push(marker);  
        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow({
							//content: data.title 
        });   

        		google.maps.event.trigger(map, "resize");
                map.panTo(marker.getPosition());
                map.setZoom(14);
	    // map.setCenter(marker.getPosition());   
    };
	
	
	$("#btnLocationSearch").click(function(e){
        var address = $("#txtLocation").val();
        var latlng = address.split("/");
        if (latlng.length == 2) {
            try {          
                setLocation(address);
                e.preventDefault();                
            }
            catch (e) {}
        }
        else {
            searchMapLocation(address);
        }
		 
	});
	
	function searchMapLocation(address)
	{	
		  getMapLatLang(address);
	}

        function getMapLatLang(address) {
            //alert('sd1');
            // Define address to center map to
            if (address == "") {
                address = 'Waikabubak, Indonesia';
            }
            geocoder.geocode({
                'address': address
            }, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    $("#txtGeoLocation").val(results[0].geometry.location.lat().toFixed(3) + "/" + results[0].geometry.location.lng().toFixed(3));
                   
				  var data =  {
                        "title": address,	
                        "lat": results[0].geometry.location.lat().toFixed(3),	
                        "lng": results[0].geometry.location.lng().toFixed(3),	
                        "description": ''
                    }
					SetMapMarker(data);			   

                } else {

                   // alert("Geocode was not successful for the following reason: " + status);
                }
            });
          }
         
          
    </script>

</asp:Content>

