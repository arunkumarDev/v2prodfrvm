﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="CalendarConfigureWeekdays.aspx.cs" Inherits="WalikuWebSite.CalendarConfigureWeekdays" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
   
    <!-- Modal -->
    <main class="l-main">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_ConfigureWeekdays %></h1>
                </div>
            </div>
            <div class="page-content">
                        <div class="row">
                        <div class="col-sm-12">
                            <div class="configuration-details">
                                <div class="configuration_data">
                                    <div class="bmd-form-group">
                                         <div class="col-sm-2">
                                          <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                                                <select id="ddlSchool" class="form-control">
                                                </select>
                                          </div>
                                        <div class="col-md-12">
                                            <div id= "configureWeekdaysGroup">
                                         
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                   <div class="panel-footer">
                        <div class="col-md-10 text-right"></div>
                        <input id="btnSave" type="button" value="Save" class="btn btn-group btn-success" />
                        
                    </div>
            </div>
        </div>
    </main>
    <script src="Scripts/Common.js?0"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/ConfigureWeekdays.js?v=" + JsVerion + "'><\/script>");</script>
    <script>
    </script>
</asp:Content>
