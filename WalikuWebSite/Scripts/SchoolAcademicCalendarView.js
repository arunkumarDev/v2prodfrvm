﻿$(document).ready(function () {

    let school = localStorage.getItem('Usertypefrimport');
    if (school == 1) {
        document.getElementById("schoolsearch").style.display = "none";

    }
    else {
        document.getElementById("schoolsearch").style.display = "block";
    }

    initEvents();
    ClearControl();
    LoadNotifications();

});

function initEvents() {
    // LoadSchool();

    LoadSchoolFrDD();
    $("#ddlSchoolSearch1").on("change", function () {
       
        var SelectedVal = $(this).val();
        LoadSchoolAcademicCalendar();
      //  document.location.reload();
       // window.location.href = "SchoolAcademicCalendarNew.aspx";
    });
    $("#btnAddSchoolAcademicCalendar").click(function () {
        ClearControl();
        $('#txtHeader').text(Lbl_AddCalendarEvent);
        $('#SchoolAcademicCalendarModel').modal('show');
        var calendarDate = FormatSQLDate($("#dtCalendarDate").val());
        
    });
    
    LoadCalendarEventType(); 
   
    $("#btnSaveCalendar").click(function () {
        SubmitForm();
    });

    

}
var getSchool = '';
function LoadSchoolAcademicCalendar() {
    debugger;
    
    let lang = localStorage.getItem('loglang');

    academicCalendarList = [];
    getSchool = $("#ddlSchoolSearch1").val();
    $('#dataTableUser').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
    $('#dataTableUser tbody').empty();
    $.ajax({
        type: "POST",
        data: '{SchoolID: "' + $("#ddlSchoolSearch1").val() + '"}',
        url: "SchoolAcademicCalendarNew.aspx/GetSchoolAcademicCalendarList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            console.log(data);
            var arr = [];
            
                for (var i = 0; i < data.d.length; i++) {
                    var obj = {
                        title: data.d[i].Name,
                        start: moment(data.d[i].CalendarDate).format('YYYY-MM-DD'),
                        end: moment(data.d[i].CalendarToDate).format('YYYY-MM-DD')
                    }
                    var aa = obj.end.split('-');
                    var aa1 = parseInt(aa[2]) + 1;
                    if (aa1 < 10) {
                        aa1 = "0" + aa1;
                    }
                    obj.end = aa[0] + '-' + aa[1] + '-' + aa1
                    arr.push(obj);
                }

                //arr = [
                //        {
                //            title: 'All Day Event',
                //            start: '2020-06-21'
                //        },
                //    {
                //        title: 'Long Event',
                //        start: '2020-06-27',
                //        end: '2020-06-28'
                //    }
                //    ]

                console.log(arr);


                var calendarEl = document.getElementById('calendar');
            var events1 = arr;
            $("#exttitle").text('');

            
            //document.getElementById("exttitle").innerHTML = "Johnny Bravo"; 
                var calendar = new FullCalendar.Calendar(calendarEl, {
                    initialView: 'dayGridMonth',
                    locale: lang,
                    eventColor: 'green',
                    events: events1,
                });

                calendar.render();

            var aa = document.getElementsByClassName('fc-toolbar-title')[0].innerText;
            var b = aa.split(" ");
            thisMonth = b[0];
            var Eng_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var Ind_months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
            var dummymonths = [];
            if (lang == "en-us") {
            }
            else {
                for (var j = 0; j < Eng_months.length; j++) {
                    if (thisMonth == Eng_months[j]) {
                        thisMonth = Ind_months[j];
                    }
                }
            }
            //document.getElementsByClassName('fc-toolbar-title')[0].innerText = thisMonth + ' ' + b[1];

                //$('#calendar').fullCalendar({
                //    header: {
                //        left: 'today,prev,next',
                //        center: 'title',
                //        right: 'month'
                //    },

                //    defaultView: 'month',
                //    editable: true,
                //   // events: arr,

                //    events: [
                //        {
                //        title: 'covid',
                //        start: '2020-06-27'
                //    }
                //    ],

                //});

                //});


            
        },
        failure: function (response) {
            alert(response.d);
        }
    });

} 

function ClearControl() {
    $("#txtName").val('');
    FormatSQLDate($("#dtCalendarDate").val());
    FormatSQLDate($("#dtCalendarToDate").val());
    $("#hdnUserID").val('0');
}

function EditCalendarEvent(ID) {
    $('#txtHeader').text(Lbl_EditCalendarEvent);
    OpenModalPopup(ID);
    return false;
}

function OpenModalPopup(ctrl) {
    var academicCalendarData = academicCalendarList[ctrl];
    ClearControl();

    if (!IsNullOrEmpty(academicCalendarData)) {
        $("#hdnUserID").val($.trim(academicCalendarData.ID));
        $("#txtName").val($.trim(academicCalendarData.Name));
        $("#dtCalendarDate").val($.trim(FormatDate(academicCalendarData.CalendarDate)));
        $("#dtCalendarToDate").val($.trim(FormatDate(academicCalendarData.CalendarToDate)));
        $("#ddlSchoolSearch").val($.trim(academicCalendarData.SchoolId));
        $("#ddlCalendarEventType").val($.trim(academicCalendarData.CalendarTypeId));
  
        $('#SchoolAcademicCalendarModel').modal('show');

    }

}

function LoadCalendarEventTypeSuccess(data) {
    $('#ddlCalendarEventType').find('option').remove().end();
    debugger;
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.EventType + " </option>";
    });
    $('#ddlCalendarEventType').html(appenddata);

}

function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

    });
    $('#ddlSchoolSearch').html(appenddata);
    $('#ddlSchoolSearch1').html(appenddata);
    LoadSchoolAcademicCalendar();
}

function LoadCalendarEventType() {
    AjaxCall("SchoolAcademicCalendar.aspx/GetCalendarEventType", '{PageIndex: "' + 1 + '"  }', "POST", LoadCalendarEventTypeSuccess, FailureCallBack);
}

//function LoadSchool() {
//    AjaxCall("SchoolAcademicCalendar.aspx/GetMySchoolList", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
//}

function LoadSchoolFrDD() {
    AjaxCall("SchoolAcademicCalendar.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function FailureCallBack(data) {
    alert(data.d);
}

var academicCalendarList = [];

function ValidateUser() {
    var Eventname = $.trim($("#txtName").val());

    if (Eventname === '') {

        IsInputValid("#txtName", false);
        return false;
    }
    if ($.trim($("#ddlCalendarEventType").val()) === "") {
        IsInputValid("#ddlCalendarEventType", false);
        return false;
    }
    
    return true;
}

function SubmitForm() {
     if (!ValidateUser()) {
        return false;
    }



    var formData = new FormData();
    formData.append('ID', $("#hdnUserID").val());
    formData.append('Name', $("#txtName").val());
    formData.append('CalendarTypeId', $("#ddlCalendarEventType").val());
    formData.append('SchoolId', $("#ddlSchoolSearch").val());
    formData.append('CalendarDate', moment($("#dtCalendarDate").val()).format('YYYY-MM-DD HH:mm:ss'));
    formData.append('CalendarToDate', moment($("#dtCalendarToDate").val()).format('YYYY-MM-DD HH:mm:ss'));
        //FormatSQLDate($("#dtCalendarToDate").val()));
    
    $.ajax({
        url: "SchoolAcademicCalendar.ashx",
        data: formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            if (data == 'Success')
                window.location.href = "SchoolAcademicCalendarNew.aspx";
            else
                alert(data);
        },
        failure: function (response) {
            alert(response.d);
        }

    }); 


   
}







