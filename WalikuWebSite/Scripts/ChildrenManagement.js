﻿$(window).load(function () {   

    let school = localStorage.getItem('Usertypefrimport');
    if (school == 1) {
        document.getElementById("schoolsearch1").style.display = "none";
        document.getElementById("schoolsearch").style.display = "none";

    }
    else {
        document.getElementById("schoolsearch").style.display = "block";
        document.getElementById("schoolsearch1").style.display = "block";
    }

  //  LoadChildren(0,0);
   // LoadCommunityWorker();
    LoadSchool();
  //  LoadClassRoom(); 
    InitEvents();
    LoadNotifications();

});
var studentList = [];
var FormLoad = true;
var MySchoolList = [];
var MyClassList = [];
var childid = 0;

function InitEvents() {
    $("#ddlSchool").change(function () {
        LoadClassRoom(parseInt($(this.val())));
    });
    $("#ddlSchoolSearch").change(function () {
        LoadClassRoom(parseInt($(this).val()));
    });

    $("#btnSearchChildren").bind("click", function () {
        var table = $('#dataTable').DataTable();
        table.destroy();// table.clear().draw();
        LoadChildren($("#ddlSchoolSearch").val(), $("#ddlClassSearch").val());   // GetChildrenData(2,0);       
    });

    LoadGrade();
    LoadReasonForDelete();

    $("#btnAddStudent").bind("click", function () {
        ClearControl();
        AddChildren();
        
    });

    $("#ddlClassRoom").change(function () {
        GetTeacherMapping(parseInt($(this).val()));
    });


}


function ValidateUser() {
    debugger;
    var Studentname = $.trim($("#txtChildName").val());

    if (Studentname === '') {
        // $('#txtChildName').parent().addClass("invalid");  
        IsInputValid("#txtChildName", false);
        return false;
    }
    if ($.trim($("#txtParentName").val()) === "") {
        IsInputValid("#txtParentName", false);
        return false;
    }
    var emailId = $.trim($("#txtEmail").val());

    if (emailId !== "" && !validEmail(emailId)) {
        IsInputValid("#txtEmail", false);
        alert("Invalid Email");
        return false;
    }
    if ($.trim($("#ddlSchool").val()) === "") {
        IsInputValid("#ddlSchool", false);
        return false;
    }


    if ($.trim($("#ddlGender").val()) === "") {
        IsInputValid("#ddlGender", false);
        return false;
    }
    var PhoneNo = $.trim($("#txtPhone").val());

    if (PhoneNo != '' && !validPhoneNumber(PhoneNo)) {
        IsInputValid("#txtPhone", false);
        return false;
    }

    if ($.trim($("#datepicker").val()) === "") {
        IsInputValid("#datepicker", false);
        return false;
    }

    //if ($.trim($("#txtAddress").val()) === "") {
    //    IsInputValid("#txtAddress", false);
    //    return false;
    //}
    if ($.trim($("#ddlGrade").val()) !== "1") {
        //if ($.trim($("#txtNisn").val()) === "") {
        //    IsInputValid("#txtNisn", false);
        //    return false;
        //}
    }
    return true;
}

$("#ddlGrade").change(function () {
    if ($.trim($("#ddlGrade").val()) === "1") {
        if ($.trim($("#txtNisn").val()) === "") {
            IsInputValid("#txtNisn", true);
            return false;
        }
    }
    else {
        if ($.trim($("#txtNisn").val()) === "") {
            IsInputValid("#txtNisn", false);
            return false;
        }
    }
});


$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });
});


$("#btnSaveChild").bind("click", function () {
    if (!ValidateUser()) {
        return;
    }
    debugger;

    if ($("#txtNisn").val() != "" && $("#txtNisn").val() != "undefined") {
        if ($("#txtNisn").val().length != 10 ) {
            alert('Enter 10 Digits Nisn');
            return false;
        }
        $.ajax({
            type: "POST",
            url: "ChildrenManagement.aspx/GetExistingNISN",
            //  data: '{PageIndex: "' + 1 + '"  }',
            data: '{SchoolID: "' + $("#ddlSchool").val() + '", Nisn:"' + $("#txtNisn").val() + '"   ,ClassRoomID: "' + $("#ddlClassRoom").val() + '",PageIndex: "1"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                if (data.d.length > 0) {
                    if (data.d[0].IsExistingNisn > 0 && data.d[0].ChildId != $("#hdnChildID").val()) {
                        alert('Nisn No. already there');
                        return false;
                    }
                    else {
                        var formData = new FormData();
                        var file = $('#txtImage')[0].files[0];
                        formData.append('fileData', $('#txtImage')[0].files[0]);

                        formData.append('hdnImage', $("#hdnImage").val());
                        formData.append('ID', $("#hdnChildID").val());
                        formData.append('Name', $("#txtChildName").val());
                        formData.append('Middlename', $("#txtChildMiddlename").val());
                        //formData.append('Surname', $("#txtSurName").val());
                        formData.append('ParentName', $('#txtParentName').val());
                        formData.append('CommunityWorkerID', $("#ddlCommunityWorker").val());
                        formData.append('Email', $("#txtEmail").val());
                        formData.append('Image', 'NA');
                        formData.append('SchoolID', $("#ddlSchool").val());
                        formData.append('ClassroomID', $("#ddlClassRoom").val());
                        formData.append('Address', $("#txtAddress").val());
                        formData.append('Phone', $("#txtPhone").val());
                        formData.append('Note', $("#txtNote").val());
                        formData.append('GeoLocation', $("#txtSearchResult").val());
                        formData.append('Gender', $("#ddlGender").val());
                        formData.append('Nisn', $("#txtNisn").val());
                        formData.append('GradeID', $("#ddlGrade").val());
                        formData.append('DeleteReasonID', $("#ddlDeleteReason").val());
                        formData.append('Date_of_Birth', moment($("#datepicker").val()).format('YYYY-MM-DD'));


                        $.ajax({
                            url: "ChildrenManagement.ashx",
                            data: formData,
                            enctype: 'multipart/form-data',
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function (data) {
                                //LoadChildren();
                                //ClearControl();
                                //   window.location.href = "ChildrenManagement.aspx";
                                alert('Success');
                                $('#ChildrenManagementModel').modal('hide');
                                GetChildrenData(parseInt($("#ddlSchool").val()), parseInt($("#ddlClassSearch").val())); //  ddlClassRoom
                                window.location.href = "ChildrenManagement.aspx";
                            }
                        });
                    }


                }
                else {
                    var formData = new FormData();
                    var file = $('#txtImage')[0].files[0];
                    formData.append('fileData', $('#txtImage')[0].files[0]);

                    formData.append('hdnImage', $("#hdnImage").val());
                    formData.append('ID', $("#hdnChildID").val());
                    formData.append('Name', $("#txtChildName").val());
                    formData.append('Middlename', $("#txtChildMiddlename").val());
                    //formData.append('Surname', $("#txtSurName").val());
                    formData.append('ParentName', $('#txtParentName').val());
                    formData.append('CommunityWorkerID', $("#ddlCommunityWorker").val());
                    formData.append('Email', $("#txtEmail").val());
                    formData.append('Image', 'NA');
                    formData.append('SchoolID', $("#ddlSchool").val());
                    formData.append('ClassroomID', $("#ddlClassRoom").val());
                    formData.append('Address', $("#txtAddress").val());
                    formData.append('Phone', $("#txtPhone").val());
                    formData.append('Note', $("#txtNote").val());
                    formData.append('GeoLocation', $("#txtSearchResult").val());
                    formData.append('Gender', $("#ddlGender").val());
                    formData.append('Nisn', $("#txtNisn").val());
                    formData.append('GradeID', $("#ddlGrade").val());
                    formData.append('DeleteReasonID', $("#ddlDeleteReason").val());
                    formData.append('Date_of_Birth', moment($("#datepicker").val()).format('YYYY-MM-DD'));


                    $.ajax({
                        url: "ChildrenManagement.ashx",
                        data: formData,
                        enctype: 'multipart/form-data',
                        processData: false,
                        contentType: false,
                        type: 'POST',
                        success: function (data) {
                            //LoadChildren();
                            //ClearControl();
                            //   window.location.href = "ChildrenManagement.aspx";
                            alert('Success');
                            $('#ChildrenManagementModel').modal('hide');
                            GetChildrenData(parseInt($("#ddlSchool").val()), parseInt($("#ddlClassSearch").val())); //  ddlClassRoom
                            window.location.href = "ChildrenManagement.aspx";
                        }
                    });
                }
            }

        });
    }
    else {
        var formData = new FormData();
        var file = $('#txtImage')[0].files[0];
        formData.append('fileData', $('#txtImage')[0].files[0]);

        formData.append('hdnImage', $("#hdnImage").val());
        formData.append('ID', $("#hdnChildID").val());
        formData.append('Name', $("#txtChildName").val());
        formData.append('Middlename', $("#txtChildMiddlename").val());
        //formData.append('Surname', $("#txtSurName").val());
        formData.append('ParentName', $('#txtParentName').val());
        formData.append('CommunityWorkerID', $("#ddlCommunityWorker").val());
        formData.append('Email', $("#txtEmail").val());
        formData.append('Image', 'NA');
        formData.append('SchoolID', $("#ddlSchool").val());
        formData.append('ClassroomID', $("#ddlClassRoom").val());
        formData.append('Address', $("#txtAddress").val());
        formData.append('Phone', $("#txtPhone").val());
        formData.append('Note', $("#txtNote").val());
        formData.append('GeoLocation', $("#txtSearchResult").val());
        formData.append('Gender', $("#ddlGender").val());
        formData.append('Nisn', $("#txtNisn").val());
        formData.append('GradeID', $("#ddlGrade").val());
        formData.append('DeleteReasonID', $("#ddlDeleteReason").val());
        formData.append('Date_of_Birth', moment($("#datepicker").val()).format('YYYY-MM-DD'));


        $.ajax({
            url: "ChildrenManagement.ashx",
            data: formData,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                //LoadChildren();
                //ClearControl();
                //   window.location.href = "ChildrenManagement.aspx";
                alert('Success');
                $('#ChildrenManagementModel').modal('hide');
                GetChildrenData(parseInt($("#ddlSchool").val()), parseInt($("#ddlClassSearch").val())); //  ddlClassRoom
                window.location.href = "ChildrenManagement.aspx";
            }
        });

    }
});
$("#txtImage").change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //alert(e.target.result);
            $('#imgPhoto').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function LoadChildren(SchoolId,ClassRoomID) {  
    $('#dataTable').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();

    SchoolId = IsNullOrEmpty(SchoolId) ? 0 : SchoolId;
    ClassRoomID = IsNullOrEmpty(ClassRoomID) ? 0 : ClassRoomID;

    GetChildrenData(SchoolId, ClassRoomID);


    InitializeDataTable('dataTable');
}

function GetChildrenData(SchoolId, ClassRoomID) {
    $('#dataTable tbody').empty();
    studentList = [];
    $.ajax({
        type: "POST",
        url: "ChildrenManagement.aspx/GetAllChildrenForMe",
      //  data: '{PageIndex: "' + 1 + '"  }',
        data: '{SchoolID: "' + SchoolId + '" ,ClassRoomID: "' + ClassRoomID + '",PageIndex: "1"}',      
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var SerialNo = 0;
            $('#total').text(data.d.length);
            $.each(data.d, function (i, item) {
                studentList[item.ID] = item;
                var UserImage = $.trim(item.Image) !== "" ? ('/Photos/' + item.Image) : 'images/user.png';
               
                if (item.AttendancePercent < 100) {
                    $('<tr id="tr' + item.ID + '" >').html(
                        // "<td>" + item.ID + "</td>" +
                        //"<td>" +
                        //"<div class=\"text-center\">" +
                        //" <figure class=\"list-pic\">" +
                        //"<img src=" + UserImage + " alt='user image' class='img-responsive img-border'>" +
                        //"</figure>" +
                        //" </div></td>" +

                        "<td> " + (++SerialNo) + "</td >" +
                        "<td>" +
                        "<div class=\"text-center\">" +
                        "<figure class=\"list-pic\">" +
                        "<img src=" + UserImage + " alt=\"user\" class=\"img-responsive img-border\">" +

                        "</figure>" +
                        "</div>" +
                        "</td> " +
                        "<td> " + item.Name + "</td >" +
                        "<td>" + item.Phone + "</td>" +
                        "<td data-GeoLocation =" + item.GeoLocation + ">" + item.Address + "</td>" +
                        //  "<td>&nbsp;</td>" +
                        "<td><div class='progress att-percent'><div class='progress-bar red' role='progressbar' style='width: " + item.AttendancePercent + "%;' aria-valuenow=" + item.AttendancePercent + " aria-valuemin='0' aria-valuemax='100'>" + item.AttendancePercent.toFixed(2) + "%</div> </div></td>" +

                        "<td><a href='javascript:;' onclick='return EditChildren(" + item.ID + ");'><i class= 'fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                        "<a href='javascript:;'  onclick='return DeleteChild(" + item.ID + ");' >	<i class= 'fa fa-trash site-color mr-10' aria-hidden='true'></i></a> " +
                        "<a href='javascript:;' onclick='return ViewProfile(" + item.ID + ");' data-toggle='modal'>	<i class= 'fa fa-eye site-color' aria-hidden='true'></i></a> " +
                        "</td > ").appendTo('#dataTable tbody');
                }
                else {
                    $('<tr id="tr' + item.ID + '" >').html(
                        // "<td>" + item.ID + "</td>" +
                        //"<td>" +
                        //"<div class=\"text-center\">" +
                        //" <figure class=\"list-pic\">" +
                        //"<img src=" + UserImage + " alt='user image' class='img-responsive img-border'>" +
                        //"</figure>" +
                        //" </div></td>" +

                        "<td> " + (++SerialNo) + "</td >" +
                        "<td>" +
                        "<div class=\"text-center\">" +
                        "<figure class=\"list-pic\">" +
                        "<img src=" + UserImage + " alt=\"user\" class=\"img-responsive img-border\">" +

                        "</figure>" +
                        "</div>" +
                        "</td> " +
                        "<td> " + item.Name + "</td >" +
                        "<td>" + item.Phone + "</td>" +
                        "<td data-GeoLocation =" + item.GeoLocation + ">" + item.Address + "</td>" +
                        //  "<td>&nbsp;</td>" +
                        "<td><div class='progress att-percent'><div class='progress-bar red' role='progressbar' style='width: " + item.AttendancePercent + "%;' aria-valuenow=" + item.AttendancePercent + " aria-valuemin='0' aria-valuemax='100'>100.00%</div> </div></td>" +

                        "<td><a href='javascript:;' onclick='return EditChildren(" + item.ID + ");'><i class= 'fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                        "<a href='javascript:;'  onclick='return DeleteChild(" + item.ID + ");' >	<i class= 'fa fa-trash site-color mr-10' aria-hidden='true'></i></a> " +
                        "<a href='javascript:;' onclick='return ViewProfile(" + item.ID + ");' data-toggle='modal'>	<i class= 'fa fa-eye site-color' aria-hidden='true'></i></a> " +
                        "</td > ").appendTo('#dataTable tbody');
                }
            });
        },
        
        failure: function (response) {

            alert(response.d);
        }
    });
}


function OpenChildrenImageModel(childID) {
    $('#ChildrenImageModel').modal('show');
    $.ajax({
        type: "POST",
        url: "ChildrenManagement.aspx/GetChildImageByID",
        data: '{ChildID: "' + childID + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#srcImage").attr('src', data.d);
        },
        failure: function (response) {

            alert(response.d);
        }
    });
}
//function LoadCommunityWorker() {

//    $.ajax({
//        type: "POST",
//        url: "ChildrenManagement.aspx/GetAllCommunityWorker",
//        data: '{PageIndex: "' + 1 + '"  }',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            var appenddata;
//            $.each(data.d, function (i, item) {
//                appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";

//            });
//            $('#ddlCommunityWorker').html(appenddata);
//        },
//        failure: function (response) {
//            alert(response.d);
//        }
//    });

//}


function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

    });
    $('#ddlSchoolSearch').html(appenddata);
    LoadClassRoom(parseInt($('#ddlSchoolSearch').val()));

}

function LoadGradeSuccess(data) {
    $('#ddlGrade').find('option').remove().end();
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";
    });
    $('#ddlGrade').html(appenddata);
    
}

function LoadReasonForDeleteSuccess(data) {
    $('#ddlDeleteReason').find('option').remove().end();
    $('#ddlPopupDeleteReason').find('option').remove().end();


    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.Reason + " </option>";
    });
    $('#ddlDeleteReason').html(appenddata);
    $('#ddlPopupDeleteReason').html(appenddata);
    

}

function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadClassRoom(SchoolId ) {
    AjaxCall("ChildrenManagement.aspx/GetAllClassRoomForMe", '{SchoolID: "' + SchoolId + '"  }', "POST", LoadClassRoomSuccess, FailureCallBack);
}

function GetTeacherMapping(ClassRoomID) {
    AjaxCall("ChildrenManagement.aspx/GetTeacherClassMapping", '{ClassRoomID: "' + ClassRoomID + '"  }', "POST", TeacherMappingSuccess, FailureCallBack);
}

function LoadGrade() {
    AjaxCall("ChildrenManagement.aspx/GetAllGrade", '{PageIndex: "' + 1 + '"  }', "POST", LoadGradeSuccess, FailureCallBack);
}

function LoadReasonForDelete() {
    AjaxCall("ChildrenManagement.aspx/GetAllReasonForDelete", '{PageIndex: "' + 1 + '"  }', "POST", LoadReasonForDeleteSuccess, FailureCallBack);
}

function LoadClassRoomSuccess(data) {
    $('#ddlClassSearch')
        .find('option')
        .remove()
        .end();

    MyClassList = data.d;
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";

    });
    $('#ddlClassSearch').html(appenddata);

    if (FormLoad) {
        FormLoad = false;
        setTimeout(function () {
            LoadChildren($("#ddlSchoolSearch").val(), $("#ddlClassSearch").val());
        }, 500)

    }
}

function ClearControl() {
    $("#hdnChildID").val('0');
    $("#txtChildName").val('');
    $("#txtChildMiddlename").val('');
    $("#txtSurName").val('');
    $("#txtParentName").val('');
    $("#ddlCommunityWorker").val(0);
    $("#txtEmail").val('');
    $("#txtImage").val('');
    $("#ddlSchool").val(0);
    $("#ddlClassRoom").val(0);
    $("#txtPhone").val('');
    $("#txtAddress").val('');
    $("#txtNote").val('');
    $("#imgPhoto").attr('src', "images/user.png");
    $("#txtNisn").val('');
    $("#ddlGrade").val(0);
    $("#ddlDeleteReason").val(0);
   
}


function TeacherMappingSuccess(data) {
    $.each(data.d, function (i, item) {
        $('#ddlCommunityWorker').html("<option value = '" + item.ID + "'>" + item.Name + " </option>");
    });

    $("#ddlCommunityWorker").val($("#ddlCommunityWorker option:first").val());
}


function AddChildren() {
    ClearControl();
    $('#txtHeader').text(Lbl_AddStudent);
    SetSelectedSchool(parseInt($('#ddlSchoolSearch').val()));
    SetSelectedClass(parseInt($('#ddlClassSearch').val()));
    GetTeacherMapping(parseInt($('#ddlClassSearch').val()));
    $("#ddlGrade").val("1");
    $("#ddlDeleteReason").val("7");
    $('#ChildrenManagementModel').modal('show');  
    setDefaultMapLocation();
}
function EditChildren(ID) {
    ClearControl();
    $('#txtHeader').text(Lbl_EditStudent);
    var studData = studentList[ID];
    SetSelectedSchool(parseInt(studData.SchoolID));
    SetSelectedClass(parseInt(studData.ClassroomID));
    GetTeacherMapping(parseInt(studData.ClassroomID));
    OpenModalPopup(studData);
}

function SetSelectedSchool(val) {
    $('#ddlSchool').find('option').remove().end();
    var appenddata;
    $.each(MySchoolList, function (i, item) {
        if (val == item.ID) {          
            $('#ddlSchool').html("<option value = '" + item.ID + "'>" + item.SchoolName + " </option>");
            return;
        }
    });
}

function SetSelectedClass(val) {
    $('#ddlClassRoom').find('option').remove().end();
    var appenddata;
    $.each(MyClassList, function (i, item) {
        //if (val == item.ID) {
        //    $('#ddlClassRoom').html("<option value = '" + item.ID + "'>" + item.ClassName + " </option>");
        //    return;
        //}
        $('#ddlClassRoom').append("<option value = '" + item.ID + "'>" + item.ClassName + " </option>");     
    });
    $('#ddlClassRoom').val(val);
}

function OpenModalPopup(studData) {

    if (studData !== undefined) {
        debugger;

        //  $("#hdnChildID").val($('#tr' + ctrl + ' td').eq(0).text());
        $("#hdnChildID").val(studData.ID);
        $("#txtChildName").val(studData.Name);
        $("#txtChildMiddlename").val($.trim(studData.Middlename));
        //$("#txtSurName").val($.trim(studData.Surname));
        $("#txtParentName").val($.trim(studData.ParentName));
        $("#ddlCommunityWorker").val(studData.CommunityWorkerID);
        $("#txtEmail").val($.trim(studData.Email));
        if (studData.Date_of_Birth != null)
            //$("#txtDob").val(moment(studData.Date_of_Birth).format('YYYY-MM-DD'));
           $(".txtDob").val(moment(studData.Date_of_Birth).format('MM/DD/YYYY'));
        else
            $("#txtDob").val(new Date().toISOString().slice(0, 10));
        
        if ($.trim(studData.Image) !== "")
            $("#imgPhoto").attr("src", $.trim("Photos/" + studData.Image));
        else
            $("#imgPhoto").attr("src", $.trim("images/user.png"));

        $("#hdnImage").val($.trim(studData.Image));
        $("#ddlSchool").val(studData.SchoolID);

        $("#ddlClassRoom").val(studData.ClassroomID);
        let lang = localStorage.getItem('loglang');
        if (lang == "en-us") {
            $("#ddlGender").val(studData.Gender);
        }
        else {
            if (studData.Gender == "P")
                studData.Gender = "F";
            if (studData.Gender == "L")
                studData.Gender = "M";
             $("#ddlGender").val(studData.Gender);

        }
       
        $("#txtPhone").val($.trim(studData.Phone));
        $("#txtAddress").val($.trim(studData.Address));
        $("#txtNote").val($.trim(studData.Note));

        $("#txtSearchResult").val($.trim(studData.GeoLocation));

        $('#txtHeader').text(Lbl_EditStudent);

        $("#txtNote").parent().addClass('is-filled');

        $("#txtNisn").val($.trim(studData.NISN));
        studData.GradeID === 0 ? $("#ddlGrade").val("1") : $("#ddlGrade").val(studData.GradeID);
        studData.DeleteReasonID === 0 ? $("#ddlDeleteReason").val("7") : $("#ddlDeleteReason").val(studData.DeleteReasonID);
                        
        $('#ChildrenManagementModel').modal('show');   

       setLocation($("#txtSearchResult").val());
    }
}

function DeleteChild(ID) {
    $('#DelReasonPopup').modal('show');
    console.log($("#ddlDeleteReason").val());
    childid = ID;
    //if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
    //    return;
    //}
    //$.ajax({
    //    type: "POST",
    //    url: "ChildrenManagement.aspx/DeleteChildByID",
    //    data: '{ID: "' + ID + '"}',
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (data) {
    //        ////LoadChildren();
    //        window.location.href = "ChildrenManagement.aspx";
    //    },
    //    failure: function (response) {
    //        alert(response.d);
    //    }
    //});
}


function checkEmptyImage(v) {
    if (("" + v).length === 0)
        return "emptyuser.jpg";
    else
        return v;
}

function FailureCallBack(data) {
    alert(data.d);
}

function ViewProfile(ID) {
    GetChildDetails(ID);
    $('#viewprofile').modal('show');
}

function GetChildDetails(ID) {
   //alert(ID)
    AjaxCall("ChildrenManagement.aspx/GetChildDetailsByID", '{ID: "' + ID + '"  }', "POST", GetChildDetailsSuccess, FailureCallBack);
}

function GetChildDetailsSuccess(data) {
    data = data.d;
    debugger;
    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        if (data.Gender == "Girl")
            data.Gender = "Female";
        if (data.Gender == "Boy")
            data.Gender = "Male";
        $("#viewGender").val(data.Gender);
    }
    else {
        if (data.Gender == "Girl")
            data.Gender = "Perempuan";
        if (data.Gender == "Boy")
            data.Gender = "Laki-Laki";
    }
    $('#viewStudName').html(data.Name);
    $('#viewGender').html(data.Gender);
    $('#viewSchoolName').html(data.SchoolName);
    $('#viewClassName').html(data.ClassroomName);
    $('#viewTeacherName').html(data.TeacherName);
    $('#viewParentName').html(data.ParentName);
    $('#viewPhone').html(data.Phone);
    $('#viewAddress').html(data.Address);
    $('#viewNotes').html(data.Note);
    $('#viewNisn').html(data.NISN);
    var studData = studentList[data.ID];

    if (studData.Date_of_Birth != null)
        //$("#txtDob").val(moment(studData.Date_of_Birth).format('YYYY-MM-DD'));
        $(".viewDob").val(moment(data.Date_of_Birth).format('MM/DD/YYYY'));
    else
        $(".viewDob").val(new Date().toISOString().slice(0, 10));


  //  $('#AttendancePercent').html(studentList[data.ID].AttendancePercent);

    $('#viewAttendancePercent').html("<div class='progress att-percent'><div class='progress-bar red' role='progressbar' style='width: " + studData.AttendancePercent + "%;' aria-valuenow=" + studData.AttendancePercent + " aria-valuemin='0' aria-valuemax='100'>" + studData.AttendancePercent.toFixed(2) + "%</div> </div>");

    if ($.trim(data.Image) !== "")
        $("#viewProfilePhoto").attr("src", $.trim("Photos/" + data.Image));
    else
        $("#viewProfilePhoto").attr("src", $.trim("images/user.png"));

    setViewProfileMap(data.GeoLocation,'viewprofileMap');

}


$("#btndelChild").bind("click", function () {
    debugger;
     $.ajax({
        type: "POST",
        url: "ChildrenManagement.aspx/DeleteChildByStudentID",
         data: '{DelID: "' + $("#ddlPopupDeleteReason").val() + '",ID: "' + childid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadChildren();
            window.location.href = "ChildrenManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
})



function FailureCallBack() {

}


