﻿$(document).ready(function () {
    InitEvents();
    LoadReasons();
    LoadReasonType();
    LoadNotifications();
});
window.onload = function () {
    $('input').parent().addClass('is-filled');
   // $('select').parent().addClass('is-filled');

};

function InitEvents() {
    $('#btnAddReason').click(function () {
        ClearControl();
        $('#txtHeader').text(Lbl_AddReason);
        $('#ReasonModel').modal('show');
    });

    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {        
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

}


$("#btnSaveReason").bind("click", function () {
    if (!ValidateReason()) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "ReasonManagement.aspx/ManageReasonType",
        data: '{ID: "' + $("#hdnReasonID").val() + '",ReasonType: "' + $("#ddlReasonType").val() + '",Description: "' + $("#txtDescription").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //LoadReasons();
            //ClearControl();
            window.location.href = "ReasonManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
});

function ValidateReason() {

    if ($.trim($("#ddlReasonType").val()) === "") {
     
        IsInputValid("#ddlReasonType", false);
        return false;
    }

    if ($.trim($("#txtDescription").val()) === "") {
        IsInputValid("#txtDescription", false);
        return false;
    }

    return true;
}


function LoadReasons() {
    ReasonList = [];
    $('#dataTable').dataTable({
        "bDestroy": true
    }).fnDestroy();
    $('#dataTable tbody').empty();
    $.ajax({
        type: "POST",
        url: "ReasonManagement.aspx/GetAllReason",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;

            $.each(data.d, function (i, item) {
                ReasonList[item.ID] = item;

                //var reasonTypeText = 'Health Reason';
                //if (item.ReasonType === 2)
                //{
                //    reasonTypeText = 'Non-Health Reason';
                //}
                $('<tr id="tr' + item.ID + '">').html("<td>" + (++slNo) + "</td>" +
                    "<td data-reasonType =" + item.ReasonType + "> " + item.ReasonTypeName + "</td >" +
                    "<td>" + item.Description + "</td>" +
                   // "<td>" + item.DescriptionIndian + "</td>" +
                    "<td><a style='cursor: pointer;' onclick='return EditReason(" + item.ID + ");' data-toggle='modal' class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></a><a style='cursor: pointer;' onclick='return DeleteReasonType(" + item.ID + ");' data-toggle='modal' class='fa fa-trash site-color' aria-hidden='true'></a></td>").appendTo('#dataTable tbody');
            });
           InitializeDataTable('dataTable');
         
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function DeleteReasonType(ID) {
    if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "ReasonManagement.aspx/DeleteReasonTypeByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadReasons();
            window.location.href = "ReasonManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function EditReason(ID) {
    ClearControl();
    $('#txtHeader').text(Lbl_EditReason);
    OpenModalPopup(ID);
}

function OpenModalPopup(ctrl) {
    var ReasonData = ReasonList[ctrl];
    {
        $("#hdnReasonID").val(ReasonData.ID);
        $("#ddlReasonType").val(ReasonData.ReasonType);
        $("#txtDescription").val(ReasonData.Description);
        // $("#txtDescriptionIndian").val($('#tr' + ctrl + ' td').eq(3).text());     
        $('#ReasonModel').modal('show');
    }
   
}
function ClearControl() {
    $("#hdnReasonID").val(0);
    $("#ddlReasonType").val('');
    $("#txtDescription").val('');
   // $("#txtDescriptionIndian").val('');
}


function LoadReasonType() {

    $.ajax({
        type: "POST",
       url: "ReasonManagement.aspx/GetAllReasonTypes",
      //  data: '{PageIndex: "' + 1 + '"  }',
    //    url: AppUrls.WalikuWebApiUri + "api/Reason/GetAllReasonType",  
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--Select Reason--</option>";
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.Description + " </option>";

            });
            $('#ddlReasonType').html(appenddata);         

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
