﻿
////https://canvasjs.com/javascript-charts/multiple-axis-column-chart/
var thisMonth = '';

LoadReport();
LoadStudentAttendanceReport();
//LoadHolidays();
LoadWeeklyAttendanceRateSummary();


var classData = [];

var tdClass = { "H": "present", "K": "absent", "NK": "sick_leave", "C": "unknown_leave", "L": "holyday" }


//function LoadHolidays() {
//    var inputDate = $("#datepicker").val();
//    var SelectedType = SelectedType;

//    $.ajax({
//        type: "POST",        
//        url: "../Default.aspx/GetHolidayList",
//        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            GetHolidayListForGrid(data.d);
//        },
//        failure: function (response) {
//            alert(response.d);
//        }
//    });
//}

function LoadReport() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 2;
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];
    $("#monthName").text('');

    GetHolidayList(inputDate, $("#ddlClassRoom").val(), 1);   

    thisMonth = getMonthName(inputDate);

    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetClassWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + $('#ddlMenuType').val() + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {        
            $("#monthName").text(thisMonth);
            drawWeeklyReportChart(data);
            //$.each(data.d, function (i, item) {

            //    //dataPointsPresent.push(item.PresentCount);
            //    dataPointsPresent.push(item.Present);
            //    dataPointsAbsent.push(item.Absent);
            //    dataPointsClassName.push(item.ClassName);
            //    dataTotalCount.push(item.Present);
            //    dataTotalCount.push(item.Absent);

            //});
            dataTotalCount.sort(function (a, b) { return a - b });
            var maxYvalue1 = dataTotalCount[dataTotalCount.length - 1];
            maxYvalue1 = maxYvalue1 + 10;
            maxYvalue1 = maxYvalue1.toString().substr(0, maxYvalue1.toString().length - 1);
            var maxYvalue = parseInt(maxYvalue1 * 10);

            var barChartData = {
                labels: dataPointsClassName,
                datasets: [{
                    label: LocalResources.Lbl_Present, //'Present',
                    backgroundColor: "#83A7D0",
                    yAxisID: "y-axis-1",
                    data: dataPointsPresent
                }, {
                        label: LocalResources.Lbl_Absent,  //'Absent',
                    backgroundColor: "#D38483",
                    yAxisID: "y-axis-2",
                    data: dataPointsAbsent
                }]

            };

            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    onClick: function (cc) {
                      //  console.log(cc);
                    },
                    responsive: true,
                    title: {
                        display: true,
                        text: "Class wise attendance report for " + $("#txtDatePicker").val()
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true
                    },
                    scales: {
                        yAxes: [{
                            type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: "left",
                            id: "y-axis-1",

                            ticks: {
                                beginAtZero: true,
                                steps: 10,
                                stepValue: 5,
                                max: maxYvalue
                            }
                        }, {
                            type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: false,
                            position: "right",
                            id: "y-axis-2",
                            gridLines: {
                                drawOnChartArea: false
                            },
                            ticks: {
                                beginAtZero: true,
                                steps: 10,
                                stepValue: 5,
                                max: maxYvalue
                            }
                        }],
                    }
                }
            });

            $("#trigo").show();
            $("#fullReport").hide();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function LoadWeeklyAttendanceRateSummary() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 2;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyAttendanceRateSummary",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.d.length; i++) {
                $("#MonthlyAttendance").text(data.d[i].AttendanceRate + ' %');
                $("#AbsenteesimRate").text(data.d[i].AbsenteesimRate + ' %');
                $("#BoysAbsenteeRate").text(data.d[i].BoysAbsenteeRate + ' %');
                $("#GirlsAbsenteeRate").text(data.d[i].GirlsAbsenteeRate + ' %');
                $("#HealthAbsenteeRate").text(data.d[i].HealthAbsenteeRate + ' %');
                $("#NonHealthAbsenteeRate").text(data.d[i].NonHealthAbsenteeRate + ' %');
                $("#UnKnownAbsenteeRate").text(data.d[i].UnKnownAbsenteeRate + ' %');
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function LoadStudentAttendanceReport() {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    var SelectedType = 2;

    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
        url: "../Default.aspx/GetStudentWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        timeout: 15000, 
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
            DrawTeacherReportGrid(data.d);
            InitDataTableReport('dataTableAttendance', $("#ddlClassRoom option:selected").text() + ' - ' + thisMonth + ' ' + $('#datepicker').datepicker('getDate').getFullYear());

            //$.each(data.d, function (i, item) {

            //    AttendanceList[item.ID] = item;
            //    $('<tr id="tr' + item.ID + '" geoLoc=' + item.GeoLocation + '>').html(

            //        "<td>" + (++slNo) + "</td>" +
            //        "<td>" + item.Name + "</td>" +
            //        "<td>" + item.Gender + "</td>" +
            //        "<td>" + + item.AttendanceType + + "</td>").appendTo('#dataTableAttendance tbody');


            //});
            //$('#dataTableAttendance').DataTable({
            //    initComplete: function () {
            //        this.api().columns().every(function () {
            //            var column = this;
            //            var select = $('<select><option value=""></option></select>')
            //                .appendTo($(column.footer()).empty())
            //                .on('change', function () {
            //                    var val = $.fn.dataTable.util.escapeRegex(
            //                        $(this).val()
            //                    );
            //                    //to select and search from grid  
            //                    column
            //                        .search(val ? '^' + val + '$' : '', true, false)
            //                        .draw();
            //                });

            //            column.data().unique().sort().each(function (d, j) {
            //                select.append('<option value="' + d + '">' + d + '</option>')
            //            });
            //        });
            //    }
            //});


            //  });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}



function drawWeeklyReportChart(data) {   
    
    $('#WeeklyLeftSummary').show(); 
    $('#YearlyGraphHeading').hide();
    $('#burden-healthabsence').hide();
    $('#burden-non-healthabsence').hide();
    $('#Monthlygraph').hide();
    $('#attendance-chart3').show();   
    $('#attendance-chart').hide();
    $('#attendance-chart2').hide();
    $('#attendance-chart4').hide();
    $('#WeeklySummary').show();
    $('#WeeklySummary1').show();
    $('#WeeklySummary2').show();
    $('#WeeklySummary3').show();
    $('#leftsumarry').show();
    $('#grid').show();

    var presentData = [];
    var absentData = [];
    var categoryData = [];



    for (var i = 0; i < data.d.length; i++) {
        presentData.push(data.d[i].Present);
        absentData.push(data.d[i].Absent);
        switch ($('#ddlMenuType').val()) {
            case "1":
                break;
            case "2":
                categoryData.push(data.d[i].ClassName);
                break;
            default:
                categoryData.push(data.d[i].AttendanceDate);
                break;
        }

    }

    generateHighChart(categoryData, absentData, presentData);

    
}

function generateHighChart(categoryData, absentData, presentData) {
    if ($('#ddlReportType').val() == 1) {
        Highcharts.chart('attendance-chart3', {
            chart: {
                type: 'column',
                marginBottom: 80
            },
            title: {
                text: ''
            },
            xAxis: {
                categories: categoryData
            },
            yAxis: {
                min: 0,
                title: {
                    text: LocalResources.Lbl_TotalStudents  //'Total Students'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    }
                }
            },
            legend: {
                align: 'center',
                x: -30,
                verticalAlign: 'bottom',
                y: 15,
                floating: true,
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            tooltip: {
                headerFormat: '<b>{point.x}</b><br/>',
                // pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                pointFormat: 'Percentage {series.name}: {point.percentage:.1f}% <br/> {series.name}: {point.y}'
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
            series: [{
                name: LocalResources.Lbl_Absent,   //'Absent',
                data: absentData,
                pointWidth: 50,
                color: '#a00101'
            }, {
                name: LocalResources.Lbl_Present,  //Present',
                data: presentData,
                pointWidth: 50,
                color: '#01a051'
            }],
            destroy: true
        });
    }
    else {
        Highcharts.chart('attendance-chart3', {
            title: {
                text: ''
            },
            xAxis: {
                categories: categoryData
            },
            yAxis: {
                title: {
                    text: LocalResources.Lbl_TotalStudents  //'Total Students'
                }
            },
            tooltip: {
                formatter: function () {
                    var s = '<b>' + this.x + '</b>';
                    $.each(this.points, function () {
                        s += '<br/>' + this.series.name + ': ' +
                            this.y + '';
                    });
                    return s;
                },
                shared: true
            },
            series: [{
                name: LocalResources.Lbl_Present,   //'Present',
                color: '#01a051',
                data: presentData,
            }, {
                name: LocalResources.Lbl_Absent,   //'Absent ',
                color: '#ff0716',
                data: absentData,
            }]
        });
    }
}
