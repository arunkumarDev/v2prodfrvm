﻿
////https://canvasjs.com/javascript-charts/multiple-axis-column-chart/
var thisMonth = '';
if (FormLoad) {
    LoadDefaultReport();
    LoadStudentAttendanceReport();
    menuType = $('#ddlMenuType').val();
    $("#monthName").text('');
    var inputDate = $("#datepicker").val();
    var now = parseInt(inputDate.toString().substring(0, 2)) - 1;
    thisMonth = getMonthName(inputDate);
    $("#monthName").text(thisMonth);

    FormLoad = false;
}
else {
    LoadReport();
    LoadStudentAttendanceReport();
}

var menuType=1;
//$(window).load(function () {
//  //  console.log(getCookie());
//    LoadDefaultReport();
//    // drawMonthlyReportChart();
//});
//$("#btnSearchReport").bind("click", function () {
//    LoadReport();
//    LoadStudentAttendanceReport();
//});
//$("#btnSearch2").bind("click", function () {
//    LoadReportForClass();
//});

var categoryData = [];

var tdClass = { "H": "present", "K": "absent", "NK": "sick_leave", "C": "unknown_leave", "L": "holyday" }


function LoadReport() {
    
    var inputDate = $("#datepicker").val();
    var SelectedType = 1;
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];

    $("#monthName").text('');

    var now = parseInt(inputDate.toString().substring(0, 2)) - 1;
    var thisMonth = months[now];
    
    GetHolidayList(inputDate, $("#ddlClassRoom").val(), 1);

    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetClassWisePresentAbsentReport",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + $('#ddlMenuType').val() + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#monthName").text(thisMonth);
            drawDailyReportChart(data);
            //drawDailyReportChart(data);
            //$.each(data.d, function (i, item) {

            //    //dataPointsPresent.push(item.PresentCount);
            //    dataPointsPresent.push(item.Present);
            //    dataPointsAbsent.push(item.Absent);
            //    dataPointsClassName.push(item.ClassName);
            //    dataTotalCount.push(item.Present);
            //    dataTotalCount.push(item.Absent);

            //});
            dataTotalCount.sort(function (a, b) { return a - b });
            var maxYvalue1 = dataTotalCount[dataTotalCount.length - 1];
            maxYvalue1 = maxYvalue1 + 10;
            maxYvalue1 = maxYvalue1.toString().substr(0, maxYvalue1.toString().length - 1);
            var maxYvalue = parseInt(maxYvalue1 * 10);

            var barChartData = {
                labels: dataPointsClassName,
                datasets: [{
                    label: LocalResources.Lbl_Present, //'Present',
                    backgroundColor: "#83A7D0",
                    yAxisID: "y-axis-1",
                    data: dataPointsPresent
                }, {
                        label: LocalResources.Lbl_Absent , //'Absent',
                    backgroundColor: "#D38483",
                    yAxisID: "y-axis-2",
                    data: dataPointsAbsent
                }]

            };

            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    onClick: function (cc) {
                        console.log(cc);
                    },
                    responsive: true,
                    title: {
                        display: true,
                        text: "Class wise attendance report for " + $("#txtDatePicker").val()
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: true
                    },
                    scales: {
                        yAxes: [{
                            type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: true,
                            position: "left",
                            id: "y-axis-1",

                            ticks: {
                                beginAtZero: true,
                                steps: 10,
                                stepValue: 5,
                                max: maxYvalue
                            }
                        }, {
                            type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                            display: false,
                            position: "right",
                            id: "y-axis-2",
                            gridLines: {
                                drawOnChartArea: false
                            },
                            ticks: {
                                beginAtZero: true,
                                steps: 10,
                                stepValue: 5,
                                max: maxYvalue
                            }
                        }],
                    }
                }
            });

            $("#trigo").show();
            $("#fullReport").hide();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function LoadStudentAttendanceReport() {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    var SelectedType = 1;
   
    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
        url: "../Default.aspx/GetStudentWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassRoom").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
            DrawTeacherReportGrid(data.d);
            InitDataTableReport('dataTableAttendance', $("#ddlClassRoom option:selected").text() + ' - ' + thisMonth + ' ' + $('#datepicker').datepicker('getDate').getFullYear());
          
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}


function LoadDefaultReport() {
    var d = new Date();

    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = d.getFullYear() + '-' +
        (month < 10 ? '0' : '') + month + '-' +
        (day < 10 ? '0' : '') + day;
    // var inputDate = $("#datepicker").val();
    var SelectedType = 1;
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetClassWisePresentAbsentReportOnload",
        data: '{ SelectedDate: "' + output + '" , SelectedType: "' + SelectedType + '", MenuType: "' + $('#ddlMenuType').val() + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d.length > 0) {
                drawDailyReportChart(data);

                //$.each(data.d, function (i, item) {

                //    //dataPointsPresent.push(item.PresentCount);
                //    dataPointsPresent.push(item.Present);
                //    dataPointsAbsent.push(item.Absent);
                //    dataPointsClassName.push(item.ClassName);
                //    dataTotalCount.push(item.Present);
                //    dataTotalCount.push(item.Absent);

                //});
                dataTotalCount.sort(function (a, b) { return a - b });
                var maxYvalue1 = dataTotalCount[dataTotalCount.length - 1];
                maxYvalue1 = maxYvalue1 + 10;
                maxYvalue1 = maxYvalue1.toString().substr(0, maxYvalue1.toString().length - 1);
                var maxYvalue = parseInt(maxYvalue1 * 10);

                var barChartData = {
                    labels: dataPointsClassName,
                    datasets: [{
                        label: LocalResources.Lbl_Present, //'Present',
                        backgroundColor: "#83A7D0",
                        yAxisID: "y-axis-1",
                        data: dataPointsPresent
                    }, {
                            label: LocalResources.Lbl_Absent,  //'Absent',
                        backgroundColor: "#D38483",
                        yAxisID: "y-axis-2",
                        data: dataPointsAbsent
                    }]

                };

                var ctx = document.getElementById("canvas").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        onClick: function (cc) {
                            console.log(cc);
                        },
                        responsive: true,
                        title: {
                            display: true,
                            text: "Class wise attendance report for " + $("#txtDatePicker").val()
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: true
                        },
                        scales: {
                            yAxes: [{
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: true,
                                position: "left",
                                id: "y-axis-1",

                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }, {
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: false,
                                position: "right",
                                id: "y-axis-2",
                                gridLines: {
                                    drawOnChartArea: false
                                },
                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }],
                        }
                    }
                });

                $("#trigo").show();
                $("#fullReport").hide();
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function drawDailyReportChart(data) {
    
    $('#Dailyleftsec').show();
    $('#attendance-chart').show();
    $('#DailySummary').show();
    $('#leftsumarry').show();
    $('#grid').show();
    $('#YearlyGraphHeading').hide();
    $('#Monthlygraph').hide();
    $('#attendance-chart3').hide();
    $('#attendance-chart2').hide();
    $('#attendance-chart4').hide();

    var presentData = [];
    var absentData = [];
    var totalPresentCount = 0;
    var totalAbsentCount = 0;
    var totalBoysCount = 0;
    var totalGirlsCount = 0;
    var absentGirlsCount = 0;
    var absentBoysCount = 0;
    categoryData = [];

    debugger;
    for (var i = 0; i < data.d.length; i++) {
        presentData.push(data.d[i].Present);
        absentData.push(data.d[i].Absent);
        totalPresentCount = data.d[i].Present + totalPresentCount;
        totalAbsentCount = data.d[i].Absent + totalAbsentCount;
        totalBoysCount = data.d[i].TotalBoys + totalBoysCount;
        totalGirlsCount = data.d[i].TotalGirls + totalGirlsCount;
        absentGirlsCount = data.d[i].AbsentGirls + absentGirlsCount;
        absentBoysCount = data.d[i].AbsentBoys + absentBoysCount;
        switch ($('#ddlMenuType').val()) {
            case "1":
                break;
            case "2":
                categoryData.push(data.d[i].ClassName);
                break;
            default:
                categoryData.push(data.d[i].AttendanceDate);
                break;
        }
        
    }

    $("#presentCount").text(totalPresentCount);
    $("#absentCount").text(totalAbsentCount);
    $("#absentBoysCount").text(absentBoysCount + '/' + totalBoysCount);
    $("#absentGirlsCount").text(absentGirlsCount + '/' + totalGirlsCount);


    Highcharts.chart('attendance-chart', {
        chart: {
            type: 'column',
            marginBottom: 80
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categoryData
        },
        yAxis: {
            min: 0,
            title: {
                text: LocalResources.Lbl_TotalStudents  //'Total Students'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
            align: 'center',
            x: -30,
            verticalAlign: 'bottom',
            y: 15,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            // pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            pointFormat: 'Percentage {series.name}: {point.percentage:.1f}% <br/> {series.name}: {point.y}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                }
            }
        },
        series: [{
            name: LocalResources.Lbl_Absent,   //'Absent',
            data: absentData,
            pointWidth: 50,
            color: '#a00101'
        }, {
                name: LocalResources.Lbl_Present,  //Present',
            data: presentData,
            pointWidth: 50,
            color: '#01a051'
        }],
        destroy: true
    });
}

