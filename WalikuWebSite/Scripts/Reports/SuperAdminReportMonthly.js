﻿
var thisMonth = "";
$("#divLoader").show(); 

var menutypevalue = 0;
$(document).ready(function () { 

    debugger;
    if (window.location.pathname == '/Report-schoolsummary' || window.location.pathname == '/Report-schoolsummary.aspx') {
        menutypevalue = 1;
        document.getElementById("listview_printout").style.display = "none";
    }
    else if (window.location.pathname == '/Report-CompareClasses' || window.location.pathname == '/Report-CompareClasses.aspx') {
        menutypevalue = 2;
        document.getElementById("listview_printout").style.display = "none";
        //listview_printout
    }
    else if (window.location.pathname == '/Reports-IndClassSummary' || window.location.pathname == '/Reports-IndClassSummary.aspx') {
        menutypevalue = 3;
    }
    else if (window.location.pathname == '/Report-IndChildSummary' || window.location.pathname == '/Report-IndChildSummary.aspx') {
        menutypevalue = 4;
    }
    else {
        menutypevalue = 5;
        document.getElementById("schoolsearch").style.display = "block";
        document.getElementById("schoolsearch1").style.display = "block";
        document.getElementById("classsearch").style.display = "none";

    }


    //LoadMonthlyAttendanceRateSummary();
    //LoadReport();
    //LoadStudentAttendanceReport();    
    //LoadReasonsChart();
    //LoadDaysStudentAbsent();    




    //LoadMonthlyAttendanceRateSummary();
    LoadReport();
    //LoadStudentAttendanceReport();
    // LoadReasonsChart();
    //LoadReasonSummary();
    //LoadDaysStudentAbsent();
    //LoadHealthReasonsChart();
    //LoadNonHealthReasonsChart();
   
    //LoadConsecutiveDaysAbsentMonthly();

    if (menutypevalue != 3) {
        document.getElementById("AddedDeleted").style.display = "block";
        OnLoadGetTrackStudentsMonthly();
    }
    else {
        document.getElementById("AddedDeleted").style.display = "none";

    }

    $('#StudentsAddedorDeleted').click(function () {

        debugger;
        GetTrackStudentsMonthly();


    });


    $('#clickPopup').click(function () {
        $('#ViewOtherCharts').modal('show');
    });


    //LoadMonthlyReasonReportRate();       
    //LoadReasonCountHealthForAdmin();
    //LoadReasonCountNonHealthForAdmin();
  //  $('.portlet .nav-tabs a').on('shown.bs.tab', function (event) {
       // initDataTable();
    //    var table = $('#dataTableAttendance').DataTable({
    //        scrollY: "300px",
    //        scrollX: true,
    //        scrollCollapse: true,
    //        paging: false,
    //        destroy: true,
    //        fixedColumns: {
    //            leftColumns: 2
    //        }
    //    });
    });

   
//})


//function initDataTable() {
//    $('#dataTableAttendance').DataTable({
//      //  data: dataSet,
//        columns: [
//            { title: "Name" },
//            { title: "Position" },
//            { title: "Office" },
//            { title: "Extn." },
//            { title: "Start date" },
//            { title: "Salary" }
//        ]
//    });

//}

var classData = [];

var tdClass = { "H": "present", "K": "absent", "NK": "sick_leave", "C": "unknown_leave", "L": "holyday" };

function LoadReport() {
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 3;
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];
      
    thisMonth = getMonthName(inputDate);
    var Eng_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var Ind_months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var dummymonths = [];
    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
    }
    else {
        for (var j = 0; j < Eng_months.length; j++) {
            if (thisMonth == Eng_months[j]) {
                thisMonth = Ind_months[j];
            }
        }
    }
    $("#monthName").text(thisMonth);

    GetHolidayList(inputDate, $("#ddlClassSearch").val(), 1);

    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetClassWisePresentAbsentReportForAdmin",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '" , MenuType: "' + menutypevalue + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
           // $("#monthName").text(thisMonth);

            if (data.d.length > 0) {

                if (menutypevalue != 5 && menutypevalue != 2) {
                    drawMonthlyReportChart(data);
                    //$("#divLoader").hide();
                }
                else {
                    drawMonthlyReportChartBar(data);
                    //$("#divLoader").hide();
                }
                //$.each(data.d, function (i, item) {

                //    //dataPointsPresent.push(item.PresentCount);
                //    dataPointsPresent.push(item.Present);
                //    dataPointsAbsent.push(item.Absent);
                //    dataPointsClassName.push(item.ClassName);
                //    dataTotalCount.push(item.Present);
                //    dataTotalCount.push(item.Absent);

                //});
                dataTotalCount.sort(function (a, b) { return a - b });
                var maxYvalue1 = dataTotalCount[dataTotalCount.length - 1];
                maxYvalue1 = maxYvalue1 + 10;
                maxYvalue1 = maxYvalue1.toString().substr(0, maxYvalue1.toString().length - 1);
                var maxYvalue = parseInt(maxYvalue1 * 10);

                var barChartData = {
                    labels: dataPointsClassName,
                    datasets: [{
                        label: LocalResources.Lbl_Present, //'Present',
                        backgroundColor: "#83A7D0",
                        yAxisID: "y-axis-1",
                        data: dataPointsPresent
                    }, {
                            label: LocalResources.Lbl_Absent,  //'Absent',
                        backgroundColor: "#D38483",
                        yAxisID: "y-axis-2",
                        data: dataPointsAbsent
                    }]

                };

                var ctx = document.getElementById("canvas").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        onClick: function (cc) {
                            console.log(cc);
                        },
                        responsive: true,
                        title: {
                            display: true,
                            text: "Class wise attendance report for " + $("#txtDatePicker").val()
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: true
                        },
                        scales: {
                            yAxes: [{
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: true,
                                position: "left",
                                id: "y-axis-1",

                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }, {
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: false,
                                position: "right",
                                id: "y-axis-2",
                                gridLines: {
                                    drawOnChartArea: false
                                },
                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }],
                        }
                    }
                });

                $("#trigo").show();
                $("#fullReport").hide();
            }
            //$("#divLoader").hide(); 
        },
        failure: function (response) {
            $("#divLoader").hide();
            document.getElementById("hideAll").style.display = "block";

            alert(response.d);
        }
    });

    LoadMonthlyAttendanceRateSummary();
    LoadStudentAttendanceReport();
    LoadReasonSummary();
    LoadHealthReasonsChart();
    LoadNonHealthReasonsChart();

    LoadConsecutiveDaysAbsentMonthly();
}

function LoadMonthlyReasonReportRate() {
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyReasonReportRate",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.d.length; i++) {
                $("#Health").text(data.d[i].Abstsmratesickness);
                $("#NonHealth").text(data.d[i].Abstsmratenonhealth);
                $("#Unknown").text(data.d[i].AbstsmrateUnknown);
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function LoadMonthlyAttendanceRateSummary() {
    //var inputDate = $("#datepicker").val();
    ////var SelectedType = SelectedType;
    //var SelectedType = 3;
    //dataTotalCount = [];
    //$.ajax({
    //    type: "POST",
    //    url: "../Default.aspx/GetMonthlyAttendanceRateSummaryForSA",
    //    data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (data) {
            
    //        for (var i = 0; i < data.d.length; i++) {
    //            $("#MonthlyAttendance").text(data.d[i].AttendanceRate + ' %');
    //            $("#AbsenteesimRate").text(data.d[i].AbsenteesimRate + ' %');
    //            $("#BoysAbsenteeRate").text(data.d[i].BoysAbsenteeRate + ' %');
    //            $("#GirlsAbsenteeRate").text(data.d[i].GirlsAbsenteeRate + ' %');
    //            $("#Health").text(data.d[i].HealthAbsenteeRate + ' %');
    //            $("#NonHealth").text(data.d[i].NonHealthAbsenteeRate + ' %');
    //            $("#Unknown").text(data.d[i].UnKnownAbsenteeRate + ' %');
    //        }
    //    },
    //    failure: function (response) {
    //        alert(response.d);
    //    }
    //});


    var inputDate = $("#datepicker").val();
    var SelectedType = 1;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyAbsentismForReports",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '", SelectedDate: "' + inputDate + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d.length > 0) {
                DrawYearlyAbsentReasonChart(data);
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadStudentAttendanceReport() {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 3;
    if (menutypevalue == 5) {
        $.ajax({
            type: "POST",
            //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
            url: "../Default.aspx/GetAdminStaffSummaryMonthPrintout",
            //data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + $('#ddlMenuType').val() + '"   }',
            data: '{ClassId: "' + $("#ddlSchoolSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',

            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var slNo = 0;
                $('#total').text(data.d.length);
                if (data.d.length > 0) {
                    DrawTeacherReportGridStaff(data.d);
                    var title = '';
                    let lang = localStorage.getItem('loglang');
                    if (lang == "en-us") {
                        title = 'Staff Summary';
                    }
                    else {
                        title = 'Ringkasan Absen Staff';
                    }

                    InitDataTableReportForMonthly('dataTableAttendance', title + ' - ' + thisMonth + ' ' + $('#datepicker').datepicker('getDate').getFullYear());
                    LoadMonthlyStaffAttendanceRateSummaryForLabel(data.d);

                }

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
            url: "../Default.aspx/GetStudentWisePresentAbsentReport",
            //data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + $('#ddlMenuType').val() + '"   }',
            data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',

            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var slNo = 0;
                $('#total').text(data.d.length);
                if (data.d.length > 0) {
                    for (var i = 0; i < data.d.length; i++) {
                        if (data.d[i].Gender == "Male") {
                            data.d[i].Gender = "Boy"
                        }
                        else {
                            data.d[i].Gender = "Girl"
                        }
                    }
                    DrawTeacherReportGrid(data.d);
                    var title = '';
                    let lang = localStorage.getItem('loglang');
                    if (lang == "en-us") {
                        title = 'Individual Class Summary';
                    }
                    else {
                        title = 'Ringkasan Kelas Individu';
                    }
                    InitDataTableReportForMonthly('dataTableAttendance', title + ' - ' + thisMonth + ' ' + $('#datepicker').datepicker('getDate').getFullYear());
                    LoadMonthlyAttendanceRateSummaryForLabel(data.d);

                }

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

}




function LoadDaysStudentAbsent() {
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyStudentAbsent",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '" , MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.d.length; i++) {
                $("#StudentCount1").text(data.d[i].StudentCount1);
                $("#StudentCount2").text(data.d[i].StudentCount2);
                $("#Unknown").text(data.d[i].AbstsmrateUnknown);
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function LoadReasonsChart() {
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 3;
    dataTotalCount = [];
    var classValue = $("#ddlClassRoom").val() == undefined ? 0 : $("#ddlClassRoom").val();
    
    $.ajax({
        type: "POST",
        //url: "../Default.aspx/GetMonthlyReasonCountForSA",
        url: "../Default.aspx/GetMonthlyReasonCountNew",

        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '" , MenuType: "' + menutypevalue + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            

            var top3HealthReson = "";
            var top3NonHealthReson = "";
            var healthData = [];
            var nonHealthData = [];
            var healthCount = 0;
            var nonHealthCount = 0;

            if (data.d.Top3Reason) {
                for (var i = 0; i < data.d.Top3Reason.length; i++) {
                    if (data.d.Top3Reason[i].ReasonCode == "Health") {

                        if (healthCount < 3) {
                            top3HealthReson += '<span class="badge badge-success">' + data.d.Top3Reason[i].Description + '<span class="badge badge-warning">' + data.d.Top3Reason[i].ReasonCount + '</span></span>';
                        }
                        healthCount++;

                    } else if (data.d.Top3Reason[i].ReasonCode == "Non-Health") {

                        if (nonHealthCount < 3) {
                            top3NonHealthReson += '<span class="badge badge-success">' + data.d.Top3Reason[i].Description + '<span class="badge badge-warning">' + data.d.Top3Reason[i].ReasonCount + '</span></span>';
                        }
                        nonHealthCount++;

                    }
                }
            }

            if (data.d.PieChart) {
                for (var i = 0; i < data.d.PieChart.length; i++) {
                    if (data.d.PieChart[i].ReasonCode == "Health") {

                        healthData.push(data.d.PieChart[i]);

                    } else if (data.d.PieChart[i].ReasonCode == "Non-Health") {

                        nonHealthData.push(data.d.PieChart[i]);
                    }
                }
            }

            drawReasonChartforHealth(healthData);
            drawReasonChartforNonHealth(nonHealthData);

            $('#dvHealthReasonCount').html(top3HealthReson);
            $('#dvNonHealthReasonCount').html(top3NonHealthReson);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function LoadReasonCountHealthForAdmin() {

    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyReasonCount1",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            drawReasonChartforHealth(data);

            var top3HealthReson = "";
            for (var i = 0; i < data.d.length; i++) {
                top3HealthReson += '<span class="badge badge-success">' + data.d[i].Description + '<span class="badge badge-warning">' + data.d[i].ReasonCount + '</span></span>';
                //  $("#sickness1Name").text(data.d[i].Description);
                //   $("#sickness1count").text(data.d[i].Reasoncount);
                // $("#Unknown").text(data.d[i].AbstsmrateUnknown);
            }
            $('#dvHealthReasonCount').html(top3HealthReson);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function drawReasonChartforHealth(data) {
    var ReasonName = [];
    var ReasonCount = [];



    for (var i = 0; i < data.length; i++) {
        var ReasonObj = new Object();
        ReasonObj.name = data[i].Description;
        ReasonObj.y = data[i].ReasonCount;
        ReasonCount.push(ReasonObj);
      
    }
    Highcharts.chart('burden-healthabsence', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: LocalResources.Lbl_AbsentDueToHealth //'Student absent days due to health'
        },
        tooltip: {
            //pointFormat: '{series.name}: {point.y}'
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' + '<br/>' + '{series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: LocalResources.Lbl_TotalCount,    //'Total Count',
            colorByPoint: true,
            data: ReasonCount
        }]
    });

}

function drawReasonChartforNonHealth(data) {
    
    var ReasonName = [];
    var ReasonCount = [];



    for (var i = 0; i < data.length; i++) {
        var ReasonObj = new Object();
        ReasonObj.name = data[i].Description;
        ReasonObj.y = data[i].ReasonCount;
        ReasonCount.push(ReasonObj);
       
    }

    Highcharts.chart('burden-non-healthabsence', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: LocalResources.Lbl_AbsentDueToNonHealth  // 'Student absent days due to non health'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>' + '<br/>' + '{series.name}: {point.y}'
            // pointFormat: '{series.name}: {point.y}'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: LocalResources.Lbl_TotalCount,    // 'Total Count',
            colorByPoint: true,
            data: ReasonCount
        }]
    });

}



function LoadReasonCountNonHealthForAdmin() {
    
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 3;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyReasonCount2",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            drawReasonChartforNonHealth(data);
            var top3NonHealthReson = "";
            for (var j = 0; j < data.d.length; j++) {
                top3NonHealthReson += '<span class="badge badge-success">' + data.d[j].Description + '<span class="badge badge-warning">' + data.d[j].ReasonCount + '</span></span>';
                //  $("#sickness1Name").text(data.d[i].Description);
                //   $("#sickness1count").text(data.d[i].Reasoncount);
                // $("#Unknown").text(data.d[i].AbstsmrateUnknown);
            }
            $('#dvNonHealthReasonCount').html(top3NonHealthReson);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function drawMonthlyReportChart(data) {
    var presentData = [];
    var absentData = [];
    var ClassName = [];
    


    for (var i = 0; i < data.d.length; i++) {
        presentData.push(parseFloat(data.d[i].PresentPercent.toFixed(1)));
        absentData.push(parseFloat(data.d[i].AbsentPercent.toFixed(1)));
        //ClassName.push(data.d[i].ClassName);
        switch (menutypevalue) {

            case 2:
                ClassName.push(data.d[i].ClassName);
                break;
            default:
                ClassName.push(data.d[i].AttendanceMonthName);
                break;
        }
    }

    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
    }
    else {
        var Eng_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var Ind_months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
        var dummymonths = [];
        for (var i = 0; i < ClassName.length; i++) {
            for (var j = 0; j < Eng_months.length; j++) {
                if (ClassName[i] == Eng_months[j]) {
                    dummymonths.push(Ind_months[j]);
                }
            }
        }
        ClassName = dummymonths;
        console.log(dummymonths);
    }

    
    $('#grid').show();
    $('#MonthlyLeftSummary').show();
    $('#YearlyGraphHeading').hide();
    $('#attendance-chart').hide();
    $('#attendance-chart3').hide();
    $('#attendance-chart4').hide();
    $('#MonthlyGraphHeading').show();
    $('#burden-non-healthabsence').show();
    $('#burden-healthabsence').show();
    $('#attendance-chart2').show();
    $('#Monthlygraph').show();
    $('#Monthlyleftsec').show();
    $('#MonthlySummary').show();
    $('#MonthlySummary1').show();
    $('#MonthlySummary2').show();
    $('#MonthlySummary3').show();
    $('#MonthlySummary5').show();
    $('#MonthlySummary4').show();
    $('#leftsumarry').show();


    Highcharts.chart('attendance-chart2', {
        title: {
            text: ''
        },
        xAxis: {
            categories: ClassName
        },
        yAxis: {
            title: {
                text: GlobalResources.Lbl_Percentage    //  'Percentage'
            },
            min: 0,
            max: 100
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';
                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y.toFixed(1) + '%';
                });
                return s;
            },
            shared: true
        },
        plotOptions: {
           
            series: {
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                }
            }
        },
        series: [{
            name: LocalResources.Lbl_AttendanceRate, // + ' %',   //'Present %',
            color: '#01a051',
            data: presentData,
        }, {
                name: LocalResources.Lbl_AbsenteeismRate,  // + ' %',    //'Absent %',
            color: '#ff0716',
            data: absentData,
        }]
    });

    
}


function LoadReasonSummary() {
    var inputDate = $("#datepicker").val();
    debugger;
    if (menutypevalue == 2 || menutypevalue == 5) {
        //document.getElementById("hidecmpclass").style.display = "none";
        //document.getElementById("hidecmpclass1").style.display = "none";
        document.getElementById("hidecmpclass2").style.display = "none";
        document.getElementById("hidecmpclass3").style.display = "none";

    }
    else {
        document.getElementById("hidecmpclass3").style.display = "block";

    }
    var SelectedType = 3;
    dataTotalCount = [];
    var classValue = $("#ddlClassSearch").val() == undefined ? 0 : $("#ddlClassSearch").val();
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetReasonSummary",
        data: '{ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            for (var i = 0; i < data.d.length; i++) {
                $('.reason-summary').show();
                //$("#AttendanceRate").text(data.d[i].ThreeDaysAttendanceRate + ' %');
                //$("#AbsenteesimRate").text(data.d[i].ThreeDaysAbsenteeismRate + ' %');
                if (menutypevalue == 1 || menutypevalue == 3)
                    $('.viewlistlink').show();
                else
                    $('.viewlistlink').hide();
                $('.threedays').show();
                $('oneortwodays').show();
                if (menutypevalue != 2) {
                    //var reasons = jQuery.parseJSON(data.d[i].TopThreeReasons);
                    if (menutypevalue == 3)
                        var reasons = jQuery.parseJSON(data.d[i].TopThreeReasons);
                    else
                        var reasons = data.d;
                    var index = 1;
                    reasons.sort((a, b) => parseFloat(b.Reasoncount) - parseFloat(a.Reasoncount));

                    
                    $.each(reasons, function (ind, reason) {
                        var count = reason.Reasoncount == undefined ? 0 : reason.Reasoncount;
                        var elementID = "#HealthReason" + index;
                        $(elementID).text(reason.Description + '(' + count + ')');
                        index++;
                    });
                    if (reasons.length == 2) {
                        document.getElementById('HealthReason3').style.display = "none";
                    }
                    if (reasons.length == 1) {
                        document.getElementById('HealthReason2').style.display = "none";
                        document.getElementById('HealthReason3').style.display = "none";
                    }
                }
                else {
                    $('.threedays').hide();
                    $('oneortwodays').hide();
                    $('.mostcommon').hide();
                }


                // $("#Unknown").text(data.d[i].UnKnownAbsenteeRate + ' %');
            }
            $("#divLoader").hide();
            document.getElementById("hideAll").style.display = "block";
        },
        failure: function (response) {
            alert(response.d);
            $("#divLoader").hide();
            document.getElementById("hideAll").style.display = "block";
        }
    });
}


function LoadHealthReasonsChart() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    dataTotalCount = [];
    var classValue = $("#ddlClassRoom").val() == undefined ? 0 : $("#ddlClassRoom").val();
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetYearlyHealthReason",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            if (data.d != '') {

                var obj = JSON.parse(data.d);
                var monthData = [];
                var reasonData = [];
                var valueData = [];
                var seriesData = [];
                var dataobj = {
                    name: null,
                    data: [],
                    visible: false
                };

                $.each(obj, function (i, item) {
                    var keyCount = 0;
                    var tempData = [];
                    for (var key in item) {
                        if (key == 'AttendanceMonthName') {
                            monthData.push(item[key]);
                        }

                        if (keyCount > 3) {
                            if (i == 0) {
                                reasonData.push(key);
                            }
                            tempData.push(item[key] == null ? 0 : item[key]);
                        }

                        keyCount++;
                    }
                    valueData.push(tempData);
                });
            }

            for (var j = 0; j < reasonData.length; j++) {

                dataobj = { name: null, data: [], visible: false };

                dataobj.name = reasonData[j];

                if (j < 3) {
                    dataobj.visible = true;
                }

                for (var k = 0; k < valueData.length; k++) {
                    var temp = valueData[k][j];
                    dataobj.data.push(temp);
                }

                seriesData.push(dataobj);

            }

            drawReasonChartforHealth(monthData, seriesData);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function drawReasonChartforHealth(data1, data2) {
    var sortingArr = ["Fever", "Injury", "Headache/ Stiff Neck", "Breathing Difficulty", "Cold/ Cough",
        "Toothache", "Nausea/Vomiting", "Abdominal Pain", "Diarrhea ", "Menstruation", "Other"];
    Object.keys(data2).sort((a, b) => {
        return sortingArr.indexOf(data2[a].name) - sortingArr.indexOf(data2[b].name);
    })
    console.log(sortingArr);
    console.log(data2);

    var dummyassign = [];

    for (var j = 0; j < sortingArr.length; j++) {
        for (var i = 0; i < data2.length; i++) {
            if (data2[i].name == sortingArr[j]) {
                let obj = {
                    name: data2[i].name,
                    data: data2[i].data,
                    visible: true
                }
                //if (j < 3) {
                //    obj.visible = true;
                //}
                //else {
                //    obj.visible = false;
                //}
                dummyassign.push(obj);
            }
        }
    }

    data2 = dummyassign;

    console.log(dummyassign);

    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
    }
    else {
        var Eng_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var Ind_months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
        var dummymonths = [];
        for (var i = 0; i < data1.length; i++) {
            for (var j = 0; j < Eng_months.length; j++) {
                if (data1[i] == Eng_months[j]) {
                    dummymonths.push(Ind_months[j]);
                }
            }
        }
        data1 = dummymonths;
        console.log(dummymonths);
    }

    $('#absence-prevalence-reason').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            categories: data1
        },
        yAxis: {
            title: {
                text: LocalResources.Lbl_TotalStudents  //'Total Students'
            },
            allowDecimals: false

        },
        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical'
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';

                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '';
                });
                return s;
            },
            shared: true
        },
        series: data2
    });
}


function LoadNonHealthReasonsChart() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    dataTotalCount = [];
    var classValue = $("#ddlClassRoom").val() == undefined ? 0 : $("#ddlClassRoom").val();
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetYearlyNonHealthReason",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //   
            if (data.d != '') {

                var obj = JSON.parse(data.d);
                var monthData = [];
                var reasonData = [];
                var valueData = [];
                var seriesData = [];
                var dataobj = {
                    name: null,
                    data: [],
                    visible: false
                };

                $.each(obj, function (i, item) {
                    var keyCount = 0;
                    var tempData = [];
                    for (var key in item) {
                        if (key == 'AttendanceMonthName') {
                            monthData.push(item[key]);
                        }

                        if (keyCount > 3) {
                            if (i == 0) {
                                reasonData.push(key);
                            }
                            tempData.push(item[key] == null ? 0 : item[key]);
                        }

                        keyCount++;
                    }
                    valueData.push(tempData);
                });
            }

            for (var j = 0; j < reasonData.length; j++) {

                dataobj = { name: null, data: [], visible: false };

                dataobj.name = reasonData[j];

                if (j < 3) {
                    dataobj.visible = true;
                }

                for (var k = 0; k < valueData.length; k++) {
                    var temp = valueData[k][j];
                    dataobj.data.push(temp);
                }

                seriesData.push(dataobj);

            }

            drawReasonChartforNonHealth(monthData, seriesData);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function drawReasonChartforNonHealth(data1, data2) {

    var sortingArr = ["Hunger/ Food Shortage", "Behavioral Issue", "Does not Like School", "Disability", "Violence",
        "Household Duties", "Farming/ Outdoor work", "Family/ Community Event", "Bad Weather",
        "Transportation Issue", "Other"];
    Object.keys(data2).sort((a, b) => {
        return sortingArr.indexOf(data2[a].name) - sortingArr.indexOf(data2[b].name);
    })
    console.log(sortingArr);
    console.log(data2);

    var dummyassign1 = [];

    for (var j = 0; j < sortingArr.length; j++) {
        for (var i = 0; i < data2.length; i++) {
            if (data2[i].name == sortingArr[j]) {
                let obj = {
                    name: data2[i].name,
                    data: data2[i].data,
                    visible : true
                }
                //if (j < 3) {
                //    obj.visible = true;
                //}
                //else {
                //    obj.visible = false;
                //}
                dummyassign1.push(obj);
            }
        }
    }

    data2 = dummyassign1;
    console.log(dummyassign1);

    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
    }
    else {
        var Eng_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var Ind_months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
        var dummymonths = [];
        for (var i = 0; i < data1.length; i++) {
            for (var j = 0; j < Eng_months.length; j++) {
                if (data1[i] == Eng_months[j]) {
                    dummymonths.push(Ind_months[j]);
                }
            }
        }
        data1 = dummymonths;
        console.log(dummymonths);
    }

    $('#absence-nonhealth-reason').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            categories: data1
        },
        yAxis: {
            title: {
                text: LocalResources.Lbl_TotalStudents  //'Total Students'
            },
            allowDecimals: false

        },
        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical'
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';

                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '';
                });
                return s;
            },
            shared: true
        },
        series: data2
    });
}

function OnLoadGetTrackStudentsMonthly() {
    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetTrackStudentsMonthly",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + $("#datepicker").val() + '" , SelectedType: "' + 3 + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //$('#ViewStudentsList').modal('show');
            debugger;
            $("#Lbl_Students_count").text(data.d.length);

        }

    });
}
function GetTrackStudentsMonthly() {
    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetTrackStudentsMonthly",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + $("#datepicker").val() + '" , SelectedType: "' + 3 + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#ViewStudentsList').modal('show');
            debugger;
            $("#Lbl_Students_count").text(data.d.length);

            var SerialNo = 0;
            //$('#dataTableList tbody').empty();
            //var oTable = $('#dataTableList').dataTable();
            //oTable.fnClearTable();
            //oTable.fnDestroy();
            //yearsList = data.d;
            $.each(data.d, function (i, item) {
                var tdHTML = "<td><span>" + item.NISN + "</span></td >"
                //    "<div class='col-md-2w-120'><div class= 'image-view' style='width: 80px; height: 80px;'>";
                //if (item.Image == "")
                //    tdHTML += "<img src='images/user.png' id='viewProfilePhoto' class='img-responsive img-border' alt='preview></div>";
                //else
                //    tdHTML += "<img src='images/" + item.Image + "' id='viewProfilePhoto' class='img-responsive img-border' alt='preview></div>";
                tdHTML += "<td><span>" + item.Name + "</span></td>";
                tdHTML += "</div></td >";
                tdHTML += "<td><span>" + item.ClassroomName + "</span></td >";
                let lang = localStorage.getItem('loglang');
                if (lang == "en-us") {
                    tdHTML += "<td><span>" + item.Status + "</span></td >";
                }
                else {
                    //item.Status =  item.Status == 'Added' ? 'Ditambahkan' : item.Status == 'Deleted' ? 'Dihapus' : ''
                    //tdHTML += "<td><span>" + item.Status + "</span></td >";
                    //if (item.Status == "Added") {
                    //    item.Status = 'Ditambahkan';
                    //    tdHTML += "<td><span>" + item.Status + "</span></td >";
                    //}
                    //else {
                    //    item.Status = 'Dihapus';
                    //    tdHTML += "<td><span>" + item.Status + "</span></td >";
                    //}
                    console.log(item.Status == "Added");

                    if (item.Status == "Added")
                        tdHTML += "<td><span> Ditambahkan </span></td >";
                    else
                        tdHTML += "<td><span> Dihapus </span></td >";

                }
                console.log(item.Reason);
                if (item.Reason == null) {
                    tdHTML += "<td><span>" + "-" + "</span></td >";
                }
                else
                tdHTML += "<td><span>" + item.Reason + "</span></td >";
                //if (item.IsActive)
                //    tdHTML += "<td><span>Added</span></td >";
                //else
                //    tdHTML += "<td><span>Deleted</span></td >";
                $('<tr id="tr' + item.ID + '" >').html(tdHTML).appendTo('#dataTableList tbody');

            });
            InitializeDataTable('dataTableList');
            //$('#ViewStudentsList').modal('show');
        }

    });
}


function LoadMonthlyAttendanceRateSummaryForLabel(data) {
    var inputDate = $("#datepicker").val();
    dataTotalCount = [];
    var classValue = $("#ddlClassSearch").val() == undefined ? 0 : $("#ddlClassSearch").val();
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetReasonSummaryForGrid",
        data: '{ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + 3 + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            document.getElementById("hdnassgmnts").style.display = "none";
            document.getElementById("hdnunknownfrchild").style.display = "inline-block";
            document.getElementById("hdnunknownfrstaff").style.display = "none";

            let lang = localStorage.getItem('loglang');
            if (lang == "en-us") {
                $("#lgdlbl").text("Legends");
                $("#sumlbl").text("Summary");
                $("#legend1").text("Total Students" + " - " + data.d[0].TotalStudents);
                $("#legend2").text("Male" + " - " + data.d[0].TotalBoys);
                $("#legend3").text("Female" + " - " + data.d[0].TotalGirls);
                $("#legend4").text("Total Absenteeism Rate" + " - " + data.d[0].AbsenteesimRate.toFixed(1) + "%");
                $("#legend5").text("Absenteeism due to Health" + " - " + data.d[0].HealthAbsenteeRate.toFixed(1) + "%");
                $("#legend6").text("Absenteeism due to Social" + " - " + data.d[0].NonHealthAbsenteeRate.toFixed(1) + "%");
                $("#legend7").text("Absenteeism due to Unknown Reasons" + " - " + data.d[0].UnKnownAbsenteeRate.toFixed(1) + "%");

            }
            else {
                $("#lgdlbl").text("Legends");
                $("#sumlbl").text("Ringkasan");
                $("#legend1").text("Jumlah Siswa" + " - " + data.d[0].TotalStudents);
                $("#legend2").text("Laki-Laki" + " - " + data.d[0].TotalBoys);
                $("#legend3").text("Perempuan" + " - " + data.d[0].TotalGirls);
                $("#legend4").text("Tingkat Absensi" + " - " + data.d[0].AbsenteesimRate.toFixed(1) + "%");
                $("#legend5").text("Sakit" + " - " + data.d[0].HealthAbsenteeRate.toFixed(1) + "%");
                $("#legend6").text("Izin" + " - " + data.d[0].NonHealthAbsenteeRate.toFixed(1) + "%");
                $("#legend7").text("Alpa" + " - " + data.d[0].UnKnownAbsenteeRate.toFixed(1) + "%");
            }



            //$.each(data.d, function (ind, s) {
            //    row += "<tr><td>Total Students</td><td>" + s.TotalStudents + "</td><td>&nbsp;</td><td>&nbsp;</td>" +
            //        "<td>Total Absenteeism Rate</td><td>" + s.AbsenteesimRate + "% </td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>P: Present</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
            //    for (var i = 0; i < TotalDays - 4; i++) {
            //        row += "<td>&nbsp;</td>";
            //    }
            //    row += "</tr > "
            //    row += "<tr><td>Male</td><td>" + s.TotalBoys + "</td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>Absenteeism due to sickness</td><td>" + s.HealthAbsenteeRate + "% </td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>A: Absent</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
            //    for (var i = 0; i < TotalDays - 4; i++) {
            //        row += "<td>&nbsp;</td>";
            //    }
            //    row += "</tr > "
            //    row += "<tr><td>Female</td><td>" + s.TotalGirls + "</td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>Absenteeism due to Social reasons</td><td>" + s.NonHealthAbsenteeRate + "% </td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>S: Absence due to social reason</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
            //    for (var i = 0; i < TotalDays - 4; i++) {
            //        row += "<td>&nbsp;</td>";
            //    }
            //    row += "</tr > "
            //    row += "<tr><td>Total Effective Days</td><td>" + TotalDays + "</td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>Absenteeism due to sickness</td><td>" + s.UnKnownAbsenteeRate + "% </td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>U: Unknown Reasons</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
            //    for (var i = 0; i < TotalDays - 4; i++) {
            //        row += "<td>&nbsp;</td>";
            //    }
            //    row += "</tr > "
            //    row += "<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>"
            //        + "<td>O: Holiday</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>";
            //    for (var i = 0; i < TotalDays - 4; i++) {
            //        row += "<td>&nbsp;</td>";
            //    }
            //    row += "</tr > "
            //});
            //$('#tblhead').html(header);
            //$('#tblBody').html(row);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function LoadMonthlyStaffAttendanceRateSummaryForLabel() {
    var inputDate = $("#datepicker").val();

    $.ajax({
        type: "POST",
        url: "../Default.aspx/LoadMonthlyStaffAttendanceRateSummaryForLabel",
        data: '{SelectedDate: "' + inputDate + '" , SchoolId: "' + $("#ddlSchoolSearch").val() + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);

            document.getElementById("hdnassgmnts").style.display = "inline-block";
            document.getElementById("hdnunknownfrchild").style.display = "none";
            document.getElementById("hdnunknownfrstaff").style.display = "inline-block";

            let lang = localStorage.getItem('loglang');
            if (lang == "en-us") {
                $("#legend1").text("Total No. of Staff	" + " - " + data.d[0].TotalStudents);
                $("#legend2").text("Male" + " - " + data.d[0].TotalBoys);
                $("#legend3").text("Female" + " - " + data.d[0].TotalGirls);
                $("#legend4").text("Total Absenteeism Rate" + " - " + data.d[0].AttendanceRate.toFixed(1) + "%");
                $("#legend5").text("Absenteeism due to Health" + " - " + data.d[0].HealthAbsenteeRate.toFixed(1) + "%");
                $("#legend6").text("Absenteeism due to Social" + " - " + data.d[0].NonHealthAbsenteeRate.toFixed(1) + "%");
                $("#legend7").text("Absenteeism due to Unknown Reasons" + " - " + data.d[0].UnKnownAbsenteeRate.toFixed(1) + "%");
                $("#legend8").text("Absenteeism due to Assignments" + " - " + data.d[0].AssignmentsAbsenteeRate.toFixed(1) + "%");
            }
            else {
                $("#lgdlbl").text("Legends");
                $("#sumlbl").text("Ringkasan");
                $("#legend1").text("Jumlah Staf" + " - " + data.d[0].TotalStudents);
                $("#legend2").text("Pria" + " - " + data.d[0].TotalBoys);
                $("#legend3").text("Wanita" + " - " + data.d[0].TotalGirls);
                $("#legend4").text("Tingkat Absensi" + " - " + data.d[0].AttendanceRate.toFixed(1) + "%");
                $("#legend5").text("Sakit" + " - " + data.d[0].HealthAbsenteeRate.toFixed(1) + "%");
                $("#legend6").text("Izin" + " - " + data.d[0].NonHealthAbsenteeRate.toFixed(1) + "%");
                $("#legend7").text("Tanpa Berita" + " - " + data.d[0].UnKnownAbsenteeRate.toFixed(1) + "%");
                $("#legend8").text("Tugas Luar" + " - " + data.d[0].AssignmentsAbsenteeRate.toFixed(1) + "%");


            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function drawMonthlyReportChartBar(data) {
    var presentData = [];
    var absentData = [];
    var categoryData = [];



    for (var i = 0; i < data.d.length; i++) {
        presentData.push(parseFloat(data.d[i].PresentPercent.toFixed(1)));
        absentData.push(parseFloat(data.d[i].AbsentPercent.toFixed(1)));
        switch (menutypevalue) {
            case 2:
                categoryData.push(data.d[i].ClassName);
                break;
            default:
                categoryData.push(data.d[i].AttendanceDate == null ? data.d[i].AttendanceMonthName : data.d[i].AttendanceDate);
                break;
        }

    }


    $('#grid').show();
    $('#MonthlyLeftSummary').show();
    $('#YearlyGraphHeading').hide();
    $('#attendance-chart').hide();
    $('#attendance-chart3').hide();
    $('#attendance-chart4').hide();
    $('#MonthlyGraphHeading').show();
    $('#burden-non-healthabsence').show();
    $('#burden-healthabsence').show();
    $('#attendance-chart2').show();
    $('#Monthlygraph').show();
    $('#Monthlyleftsec').show();
    $('#MonthlySummary').show();
    $('#MonthlySummary1').show();
    $('#MonthlySummary2').show();
    $('#MonthlySummary3').show();
    $('#MonthlySummary5').show();
    $('#MonthlySummary4').show();
    $('#leftsumarry').show();
    if (categoryData.length > 0) {
        debugger;
        if (menutypevalue == 2) {
            Highcharts.chart('attendance-chart2', {
                chart: {
                    type: 'column',
                    marginBottom: 80
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categoryData,
                    labels: {
                        style: {
                            width: '50px',
                        },
                        step: 1
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: LocalResources.Lbl_Percentage //'Total Students'
                    },

                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    },
                    max: 100
                },
                legend: {
                    align: 'center',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false,
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    // mval = point.y/point.stackTotal,
                    pointFormat: '{series.name}: {point.percentage:.1f}% '
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    },
                    series: {
                        events: {
                            legendItemClick: function () {
                                return false;
                            }
                        }
                    }

                },
                series: [{
                    name: LocalResources.Lbl_AbsenteeismRate,   //'Student Absent',
                    data: absentData,
                    pointWidth: 50,
                    color: '#a00101'
                }, {
                    name: LocalResources.Lbl_AttendanceRate,  //'Student present',
                    data: presentData,
                    pointWidth: 50,
                    color: '#01a051'
                }]
            });
        }
        else {
            Highcharts.chart('attendance-chart2', {
                chart: {
                    type: 'column',
                    marginBottom: 80
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categoryData
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: LocalResources.Lbl_Percentage //'Total Students'
                    },

                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    },
                    max: 100
                },
                legend: {
                    align: 'center',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false,
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    // mval = point.y/point.stackTotal,
                    pointFormat: '{series.name}: {point.percentage:.1f}% '
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    },
                    series: {
                        events: {
                            legendItemClick: function () {
                                return false;
                            }
                        }
                    }

                },
                series: [{
                    name: LocalResources.Lbl_AbsenteeismRate,   //'Student Absent',
                    data: absentData,
                    pointWidth: 50,
                    color: '#a00101'
                }, {
                    name: LocalResources.Lbl_AttendanceRate,  //'Student present',
                    data: presentData,
                    pointWidth: 50,
                    color: '#01a051'
                }]
            });

        }
    }
   
}

function DrawYearlyAbsentReasonChart(data) {

    var healthData = [];
    var nonHealthData = [];
    var unknownData = [];
    var AttendanceMonthData = [];

    for (var i = 0; i < data.d.length; i++) {
        healthData.push(parseFloat(data.d[i].Health.toFixed(1)));
        nonHealthData.push(parseFloat(data.d[i].NonHealth.toFixed(1)));
        unknownData.push(parseFloat(data.d[i].UnKnown.toFixed(1)));
        AttendanceMonthData.push(data.d[i].AttendanceMonthName);

    }

    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
    }
    else {
        var Eng_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var Ind_months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
        var dummymonths = [];
        for (var i = 0; i < AttendanceMonthData.length; i++) {
            for (var j = 0; j < Eng_months.length; j++) {
                if (AttendanceMonthData[i] == Eng_months[j]) {
                    dummymonths.push(Ind_months[j]);
                }
            }
        }
        AttendanceMonthData = dummymonths;
        console.log(dummymonths);
    }

    $('#absenteeism-rate-reason').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            categories: AttendanceMonthData
        },
        yAxis: {
            labels: {
                formatter: function () {
                    return this.value;
                }
            },
            title: {
                text: LocalResources.Lbl_Percentage
            },
            min: 0,
            max: 100
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';
                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '%';
                });
                return s;
            },
            shared: true
        },
        series: [{
            name: LocalResources.Lbl_Health,    // 'Health',
            data: healthData
        }, {
            name: LocalResources.Lbl_Social_monthly,    //'Non-Health',
            data: nonHealthData
        }, {
            name: LocalResources.Lbl_Unknown,    //'Unknown',
            data: unknownData
        }],
    });
}


function LoadConsecutiveDaysAbsentMonthly() {
    var inputDate = $("#datepicker").val();
    var classValue = $("#ddlClassSearch").val() == undefined ? 0 : $("#ddlClassSearch").val();

    if (menutypevalue == 1) {
        classValue = 0;
    }

    $.ajax({
        type: "POST",
        url: "../Default.aspx/LoadConsecutiveDaysAbsentMonthly",
        data: '{SelectedDate: "' + inputDate + '" , SchoolId: "' + $("#ddlSchoolSearch").val() + '", ClassId: "' + classValue + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $("#AttendanceRate").text(data.d[0].LChildId);
            $("#AbsenteesimRate").text(data.d[0].GChildId);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}
