﻿
////https://canvasjs.com/javascript-charts/multiple-axis-column-chart/

//if (FormLoad) {
//    LoadDefaultReport();
//    FormLoad = false;
//}
//else {
$("#divLoader").show(); 
var menutypevalue = 0; 
if (window.location.pathname == '/Report-schoolsummary' || window.location.pathname == '/Report-schoolsummary.aspx') {
    menutypevalue = 1;
}
else if (window.location.pathname == '/Report-CompareClasses' || window.location.pathname == '/Report-CompareClasses.aspx') {
    menutypevalue = 2;
}
else if (window.location.pathname == '/Reports-IndClassSummary' || window.location.pathname == '/Reports-IndClassSummary.aspx') {
    menutypevalue = 3;
}
else if (window.location.pathname == '/Report-IndChildSummary' || window.location.pathname == '/Report-IndChildSummary.aspx') {
    menutypevalue = 4;

    let utype = localStorage.getItem('Usertypefrimport');

    if (utype == 4) {
        document.getElementById("schoolsearch").style.display = "block";
        document.getElementById("schoolsearch1").style.display = "block";

    }
    else {
        document.getElementById("schoolsearch").style.display = "none";
        document.getElementById("schoolsearch1").style.display = "none";
    }
}
else {
    menutypevalue = 5;
}

setTimeout(function () {
    LoadReport();
}, 500)



//    LoadStudentAttendanceReport();
//    LoadDailyAttendanceRateSummary();

//}

$("#selmonth").change(function () {

    LoadAbsentDetails();
});

$("#ddlStudentSearch").change(function () {

    LoadReport();
});
$("#ddlClassSearch").change(function (e) {
    console.log(e);
 //LoadReport();
      
});



var thisMonth = "";

var categoryData = [];

var tdClass = { "H": "present", "K": "absent", "NK": "sick_leave", "C": "unknown_leave", "L": "holyday" };


function LoadReport() {
    debugger;
    if ($('#ddlStudentSearch').val() == null) {
        $("#divLoader").hide();
        document.getElementById("hideAll").style.display = "block";
        document.getElementById("showSmileyIfValues").style.display = "none";

    }
    else {
        document.getElementById("showSmileyIfValues").style.display = "block";

    }
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetDashboardForChild",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue  + '", ChildID: "' + $('#ddlStudentSearch').val() + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {      
            drawStudentReportBars(data);//drawDailyReportChart(data);
            LoadReasonSummary();
            LoadAbsentDetails();
            LoadAcademicMonthDetails();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function LoadReasonSummary() {
    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetReasonSummaryForChild",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + $("#datepicker").val() + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '", ChildID: "' + $('#ddlStudentSearch').val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            debugger;
            if (data.d.length > 0) {
                document.getElementById("hide_reason").style.display = "none";

                for (var i = 0; i < data.d.length; i++) {
                    if (data.d[i].Description == "Unknown" || data.d[i].Description == "UnKnown") {
                        data.d.splice(i, 1); 
                        i--;
                    }
                }

                $.each(data.d, function (ind, r) {
                    debugger;
                    document.getElementById("hide_reason").style.display = "block";
                    var reasonID = ind + 1;
                    let lang = localStorage.getItem('loglang');
                    if (lang == "en-us") {
                        if (r.Count > 1)
                            $('#Reason' + reasonID).text(r.Description + ' - ' + r.Count + ' Days');
                        else
                            $('#Reason' + reasonID).text(r.Description + ' - ' + r.Count + ' Day');

                    }
                    else {
                        $('#Reason' + reasonID).text(r.Description + ' - ' + r.Count + ' Hari');
                    }
                    //$('#AbsentDays' + reasonID).text(r.Reasoncount + ' Days');

                    //$(elementID).text(reason.Description + ' - ' + count);
                    if (data.d.length == 0) {
                        document.getElementById('Reason1').style.display = "none";
                        document.getElementById('Reason2').style.display = "none";
                        document.getElementById('Reason3').style.display = "none";
                        //document.getElementById('AbsentDays3').style.display = "none";
                    }
                    if (data.d.length == 1) {
                        document.getElementById('Reason2').style.display = "none";
                        document.getElementById('Reason3').style.display = "none";
                        //document.getElementById('AbsentDays3').style.display = "none";
                    }
                    if (data.d.length == 2) {
                        document.getElementById('Reason1').style.display = "block";
                        document.getElementById('Reason2').style.display = "block";
                        document.getElementById('Reason3').style.display = "none";
                        //document.getElementById('AbsentDays3').style.display = "none";
                    }
                    if (data.d.length == 3) {
                        document.getElementById('Reason1').style.display = "block";
                        document.getElementById('Reason2').style.display = "block";
                        document.getElementById('Reason3').style.display = "block";
                        //document.getElementById('AbsentDays3').style.display = "none";
                    }
                });
            }
            else {
                document.getElementById("hide_reason").style.display = "none";                

            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}
function drawStudentReportBars(data) {
    debugger;
    $.each(StudentsForSchool, function (ind, s) {
        if (s.ID == parseInt($('#ddlStudentSearch').val())) {
            let lang = localStorage.getItem('loglang');
            if (lang == "en-us") {
                console.log(s);
                //console.log(moment().diff(moment(s.Date_of_Birth, 'YYYYMMDD'), 'years');
                var age = 0;
                if (s.Date_of_Birth != null) {

                    var aa = moment(s.Date_of_Birth).format('YYYY-MM-DD')
                    age = moment().diff(moment(aa, 'YYYY-MM-DD'), 'years');
                }
                else {

                }

                $('.childsummary_profilename').text('Name: ' + s.Name);
                $('.childsummary_age').text(age + ' ' +  ' years old');
                $('.childsummary_nisn').text('NISN: ' + s.NISN);
                if (s.ImageBase != '')
                    $('.childsummary_profilephoto img').prop('src', 'Photos/' + '9b801ecd-44b8-437f-b7b9-c5af410260d9.jpg')//+ s.ImageBase);
            }
            else {
                var age = 0;
                if (s.Date_of_Birth != null) {

                    var aa = moment(s.Date_of_Birth).format('YYYY-MM-DD')
                    age = moment().diff(moment(aa, 'YYYY-MM-DD'), 'years');
                    }
                    else
                    {
                        
                    }
                $('.childsummary_profilename').text('Nama: ' + s.Name);
                $('.childsummary_age').text(age + ' ' + 'tahun');
                $('.childsummary_nisn').text('NISN: ' + s.NISN);
               
                if (s.ImageBase != '')
                    $('.childsummary_profilephoto img').prop('src', 'Photos/' + '9b801ecd-44b8-437f-b7b9-c5af410260d9.jpg')//+ s.ImageBase);

            }
        }
    });
    $.each(data.d, function (ind, dashboard) {
        debugger;
        var nhh_value = dashboard.Health + dashboard.NonHealth;
        var totalDays = dashboard.Present + dashboard.Absent + dashboard.UnKnown;
        var greenW = (dashboard.Present * 100) / totalDays;
        var yellowW = (nhh_value * 100) / totalDays;
        var unknownW = (dashboard.UnKnown * 100) / totalDays;
       
        $('.childsummary_bars_green span').text(dashboard.Present);
        $('.childsummary_bars_green').width(greenW + '%');
        $('.childsummary_bars_yellow span').text(nhh_value);
        $('.childsummary_bars_yellow').width(yellowW + '%');
        $('.childsummary_bars_red span').text(dashboard.UnKnown);
        $('.childsummary_bars_red').width(unknownW + '%');
        let lang = localStorage.getItem('loglang');
        if (lang == "en-us") {

            if (dashboard.PresentPercent >= 90) {
                document.getElementById('showGreen').style.display = "block";
                document.getElementById('showYellow').style.display = "none";
                document.getElementById('showRed').style.display = "none";

            }
            else if (dashboard.PresentPercent >= 80 && dashboard.PresentPercent < 90) {
                document.getElementById('showGreen').style.display = "none";
                document.getElementById('showYellow').style.display = "block";
                document.getElementById('showRed').style.display = "none";
            }
            else {
                document.getElementById('showGreen').style.display = "none";
                document.getElementById('showYellow').style.display = "none";
                document.getElementById('showRed').style.display = "block";
            }

            $('#attendance-percent').text(dashboard.PresentPercent.toFixed(1) + '% Attendance');
            $('#TotalDays_Att_child').text('In total school days ' + dashboard.TotalDays);
            $('#MonthlyAbsence').text(dashboard.MonthlyAbsence + ' days');
        }
        else {
            if (dashboard.PresentPercent >= 90) {
                document.getElementById('showGreen').style.display = "block";
                document.getElementById('showYellow').style.display = "none";
                document.getElementById('showRed').style.display = "none";

            }
            else if (dashboard.PresentPercent >= 80 && dashboard.PresentPercent < 90) {
                document.getElementById('showGreen').style.display = "none";
                document.getElementById('showYellow').style.display = "block";
                document.getElementById('showRed').style.display = "none";
            }
            else {
                document.getElementById('showGreen').style.display = "none";
                document.getElementById('showYellow').style.display = "none";
                document.getElementById('showRed').style.display = "block";
            }

            $('#attendance-percent').text(dashboard.PresentPercent.toFixed(1) + '% Kehadiran');
            $('#TotalDays_Att_child').text('dalam total' + ' ' + dashboard.TotalDays + ' ' + 'hari sekolah'  );
            $('#MonthlyAbsence').text(dashboard.MonthlyAbsence + ' hari');

        }
    });
    $("#divLoader").hide(); 
    document.getElementById("hideAll").style.display = "block";
}

function LoadAbsentDetails() {
    debugger;
    var inputDate = $("#datepicker").val();
    var selmonth = $("#selmonth").val();
    var aa = inputDate.split('/');
    var selyear = aa[2];
    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/LoadAbsentDetailsForChild",
        data: '{SelMonth: "' + selmonth + '",SelYear: "' + selyear + '", ChildID: "' + $('#ddlStudentSearch').val() + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            var SerialNo = 0;
            $('#dataTable tbody').empty();
            var oTable = $('#dataTable').dataTable();
            oTable.fnClearTable();
            oTable.fnDestroy();
            yearsList = data.d;
            $.each(data.d, function (i, item) {

                    $('<tr id="tr' + item.ID + '" >').html(
                        "<td> " + (++SerialNo) + "</td >" +
                        "<td><span>" + moment(item.AbsentDate).format('MM/DD/YYYY') + "</td >" +
                        "<td><span>" + item.Code + "</span></td >" +
                        "<td><span>" + item.Notes + "</td >" ).appendTo('#dataTable tbody');
                
                

            });
           
            InitializeDataTable('dataTable');
        },
        failure: function (response) {
            alert(response.d);
        }

    });
}

function LoadAcademicMonthDetails() {

    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetAcademicMonthDetails",
        data: '{  SchoolId: "' + $("#ddlSchoolSearch").val() + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata;
            var categoryData = [];
            var Eng_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
            var Ind_months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
            var dummymonths = [];

            var aa = Eng_months.indexOf(data.d[0].StartMonth);
            var bb = Eng_months.indexOf(data.d[0].EndMonth);

         
           
                for (var j = 0; j < Eng_months.length; j++) {
                    if (j >= aa && j <= bb) {
                        var obj = {
                            "Name": Eng_months[j],
                            "Value" : j + 1
                        }
                        dummymonths.push(obj);
                    }
                
            }
            console.log(dummymonths);


            $.each(dummymonths, function (i, item) {
                appenddata += "<option value = '" + item.Value + "'>" + item.Name + " </option>";
            });
            $('#selmonth').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}