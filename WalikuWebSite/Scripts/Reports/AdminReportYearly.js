﻿

////https://canvasjs.com/javascript-charts/multiple-axis-column-chart/

$(document).ready(function () {
    $("#divLoader").show();
    var menutypevalue = 0;
    debugger;
    var reasontypevalue = '';
    var changeYdetector = false;

    if (window.location.pathname == '/Report-schoolsummary' || window.location.pathname == '/Report-schoolsummary.aspx') {
        menutypevalue = 1;
    }
    else if (window.location.pathname == '/Report-CompareClasses' || window.location.pathname == '/Report-CompareClasses.aspx') {
        menutypevalue = 2;
    }
    else if (window.location.pathname == '/Reports-IndClassSummary' || window.location.pathname == '/Reports-IndClassSummary.aspx') {
        menutypevalue = 3;
    }
    else if (window.location.pathname == '/Report-IndChildSummary' || window.location.pathname == '/Report-IndChildSummary.aspx') {
        menutypevalue = 4;
    }
    else {
        menutypevalue = 5;
    }
    LoadAcademicYear();
    //LoadReport();
   // LoadStudentAttendanceReport();
   
   // LoadHealthReasonsChart();
   // LoadNonHealthReasonsChart();

    //$('.portlet .nav-tabs a').on('shown.bs.tab', function (event) {
    //    initDataTable();
    //});
    
    $("#ddlyear").change(function () {
       
        LoadReport();
    });
    $("#ddlGender").change(function () {
        if ($("#ddlGender").val() != "All")
            LoadGenderReport();
        else
            LoadReport();
    });

    $("#sev-chronicPopup").click(function () {
        debugger;
        reasontypevalue = 'Severely Chronically Absent';
        LoadStudentAttendanceReport(reasontypevalue);
        $('#Viewsev-chronicPopup').modal('show');
    });
    $("#chronicPopup").click(function () {
        reasontypevalue = 'Chronically Absent';
        LoadStudentAttendanceReport(reasontypevalue);
        $('#ViewchronicPopup').modal('show');
    });

    //$("#datepicker").on("changeDate", function (e) {
    //    var aa = localStorage.getItem('DpSelType');
    //    debugger;
      
    //    if (aa == 4 && !changeYdetector) {
    //        console.log('Monthly' + e);
    //        LoadReport();
    //        changeYdetector = true;
    //    }

    //});


});


//function initDataTable() {
//    $('#dataTableAttendance').DataTable({
//      //  data: dataSet,
//        columns: [
//            { title: "Name" },
//            { title: "Position" },
//            { title: "Office" },
//            { title: "Extn." },
//            { title: "Start date" },
//            { title: "Salary" }
//        ]
//    });

//}

var classData = [];

var tdClass = { "H": "present", "K": "absent", "NK": "sick_leave", "C": "unknown_leave", "L": "holyday" };

function LoadReport() {
    debugger;
    var academicyear = '';
    if (menutypevalue == "2") {
        academicyear = $("#ddlyear").val();
    }
    else {
        academicyear = '';
    }


    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    localStorage.setItem('DpSelType', SelectedType);
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];

    if (academicyear !== '') {
        $.ajax({
            type: "POST",
             url: "Default.aspx/GetClassWisePresentAbsentReportForAdminAcademicYear",
            // url: "../Default.aspx/GetClassWisePresentAbsentReportForAdmin",
            data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '", SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '", AcademicYear: "' + academicyear + '"   }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {

                    drawYearlyReportChart(data);
                    $("#divLoader").hide(); 
                }
                $("#divLoader").hide(); 
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    else {
        $.ajax({
            type: "POST",
            // url: "Default.aspx/GetClassWisePresentAbsentReportForAdminCompareClasses",
            url: "../Default.aspx/GetClassWisePresentAbsentReportForAdmin",
            data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '", SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    drawYearlyReportChart(data);
                    $("#divLoader").hide(); 

                }
                $("#divLoader").hide(); 
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
    LoadMonthlyAttendanceRateSummary();
    LoadYearlyTopReasonDetails();
    OnLoadStudentAttendanceReport('Severely Chronically Absent');
    OnLoadStudentAttendanceReport1('Chronically Absent');
}

function LoadGenderReport() {
    $.ajax({
        type: "POST",
        url: "Default.aspx/GetClassWiseGenderReportForAdminAcademicYear",
        // url: "../Default.aspx/GetClassWisePresentAbsentReportForAdmin",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",GenderSelectedType: "' + $("#ddlGender").val() + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {

                drawYearlyGenderReportChart(data);

            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function LoadMonthlyAttendanceRateSummary() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetReasonSummary",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SchoolID: "' + $("#ddlSchoolSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            var TopReason = '';
            var UnknownReasonCount = 0;
            for (var i = 0; i < data.d.length; i++) {
                debugger;
                if (data.d[i].Code != 'UnKnown' && TopReason == '') {
                    TopReason = data.d[i].Description;
                }
                if (data.d[i].Code == 'UnKnown') {
                    UnknownReasonCount = data.d[i].ReasonCount;
                }
                $("#AttendanceRate").text(data.d[i].AttendanceRate.toFixed(1) + ' %') ;
                $("#AbsenteesimRate").text(data.d[i].AbsenteesimRate.toFixed(1) + ' %');
                //let lang = localStorage.getItem('loglang');
                //if (lang == "en-us") {
                //    $("#Health").text('OverAll Top Reason for Absence is ' + TopReason);
                //}
                //else {
                //    $("#Health").text('Alasan Utama Absensi Adalah ' + TopReason);
                //}
               
                //$("#NonHealth").text(data.d[i].Abstsmratenonhealth);
                $("#Unknown").text(UnknownReasonCount);

            }
        },
        failure: function (response) {
            
            alert(response.d);
        }
    });
}


function LoadYearlyTopReasonDetails() {
    var inputDate = $("#datepicker").val();
    if (menutypevalue == 2) {
        document.getElementById("hidecmpclass").style.display = "none";
    }
    var SelectedType = 4;
    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetYearlyTopReasonDetails",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SchoolID: "' + $("#ddlSchoolSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            debugger;
            console.log(data);
            //let arrvalue = JSON.parse(data.d[0].TopThreeReasons);
            var arrvalue = data.d;
            var index = 1;
            debugger;
            let lang = localStorage.getItem('loglang');
            if (lang == "en-us") {
                $("#Health").text('Top 5 Absence Reasons ');
            }
            else {
                $("#Health").text('5 Alasan Utama Ketidakhadiran ');

            }
            arrvalue.sort((a, b) => parseFloat(b.Reasoncount) - parseFloat(a.Reasoncount));
            $.each(arrvalue, function (ind, reason) {
                var count = reason.Reasoncount == undefined ? 0 : reason.Reasoncount;
                var elementID = "#Health" + index;
                $(elementID).text(reason.Description + '(' + count + ')' );
                index++;
            });
          
            //let arr = [];
            //for (var i = 0; i < arrvalue.length; i++) {
            //    if (i < 5) {
            //        //var newLine = "\r\n"
            //        var obj = arrvalue[i].Description + '(' + arrvalue[i].Reasoncount + ')' + '\n';
            //        //obj += newLine;
            //        arr.push(obj);
            //    }
            //}

            //let lang = localStorage.getItem('loglang');
            //debugger;
            //if (lang == "en-us") {
            //    $("#Health").text('Top 5 Reason for Absence is ' + arr);
            //}
            //else {
            //    $("#Health").text('Alasan Utama Absensi Adalah ' + arr);
            //}

           
        },
        failure: function (response) {
            $("#divLoader").hide(); 
            document.getElementById("hideAll").style.display = "block";
            alert(response.d);
        }
    });
}

function LoadStudentAttendanceReport(reasontypevalue) {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    debugger;
    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
        url: "../Default.aspx/GetYearlychronicList",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '", ReasonType: "' + reasontypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
            if (data.d.length > 0) {
                if (reasontypevalue == 'Severely Chronically Absent') {
                    DrawTeacherYearlyGrid(data.d);
                    var title = '';
                    let lang = localStorage.getItem('loglang');
                    if (lang == "en-us") {
                        title = 'Severe Chronic Absentees';
                    }
                    else {
                        title = 'Absen Yang Sangat Kronis';
                    }
                    InitDataTableReport('dataTableAttendance', title + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());

                   // InitDataTableReport('dataTableAttendance', $("#ddlClassSearch option:selected").text() + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
                    // InitDataTableReport('dataTableAttendance1', $("#ddlClassSearch option:selected").text() + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
                }
                if (reasontypevalue == 'Chronically Absent') {
                    DrawTeacherYearlyGrid1(data.d);
                    var title = '';
                    let lang = localStorage.getItem('loglang');
                    if (lang == "en-us") {
                        title = 'Chronic Absentees';
                    }
                    else {
                        title = 'Absen Kronis';
                    }
                    InitDataTableReport('dataTableAttendance1', title + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
                    // InitDataTableReport('dataTableAttendance1', $("#ddlClassSearch option:selected").text() + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
                }
            }

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function OnLoadStudentAttendanceReport(reasontypevalue) {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    debugger;
    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
        url: "../Default.aspx/GetYearlychronicList",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '", ReasonType: "' + reasontypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
           // if (data.d.length > 0) {
                // DrawTeacherYearlyGrid(data.d);
                // InitDataTableReport('dataTableAttendance', $("#ddlClassSearch option:selected").text() + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
                // InitDataTableReport('dataTableAttendance1', $("#ddlClassSearch option:selected").text() + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
                $("#count-sevchronic").text(data.d.length);
                //$("#count-chronic").text(countdata1.length);

            //}

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function OnLoadStudentAttendanceReport1(reasontypevalue) {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    debugger;
    $.ajax({
        type: "POST",
        //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
        url: "../Default.aspx/GetYearlychronicList",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '", ReasonType: "' + reasontypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
            // if (data.d.length > 0) {
            // DrawTeacherYearlyGrid(data.d);
            // InitDataTableReport('dataTableAttendance', $("#ddlClassSearch option:selected").text() + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
            // InitDataTableReport('dataTableAttendance1', $("#ddlClassSearch option:selected").text() + ' - ' + $('#datepicker').datepicker('getDate').getFullYear());
            $("#count-chronic").text(data.d.length);
            //$("#count-chronic").text(countdata1.length);

            //}

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadHealthReasonsChart() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    dataTotalCount = [];
    var classValue = $("#ddlClassRoom").val() == undefined ? 0 : $("#ddlClassRoom").val();
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetYearlyHealthReason",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            if (data.d != '') {

                var obj = JSON.parse(data.d);
                var monthData = [];
                var reasonData = [];
                var valueData = [];
                var seriesData = [];
                var dataobj = {
                    name: null,
                    data: [],
                    visible: false
                };

                $.each(obj, function (i, item) {
                    var keyCount = 0;
                    var tempData = [];
                    for (var key in item) {
                        if (key == 'AttendanceMonthName') {
                            monthData.push(item[key]);
                        }

                        if (keyCount > 3) {
                            if (i == 0) {
                                reasonData.push(key);
                            }
                            tempData.push(item[key] == null ? 0 : item[key]);
                        }
                        
                        keyCount++;
                    }
                    valueData.push(tempData);
                });
            }

            for (var j = 0; j < reasonData.length; j++) {

                dataobj = { name: null, data: [], visible: false };

                dataobj.name = reasonData[j];

                if (j < 3) {
                    dataobj.visible = true;
                }

                for (var k = 0; k < valueData.length; k++) {
                    var temp = valueData[k][j];
                    dataobj.data.push(temp);
                }

                seriesData.push(dataobj);

            }

            drawReasonChartforHealth(monthData, seriesData);                        

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function drawReasonChartforHealth(data1, data2) {


    $('#absence-prevalence-reason').highcharts({
        title: {
            text: ''
        },
        xAxis: {            
            categories: data1
        },
        yAxis: {
            title: {
                text: LocalResources.Lbl_TotalStudents  //'Total Students'
            }
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';

                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '';
                });
                return s;
            },
            shared: true
        },
        series: data2
    });
}


function LoadNonHealthReasonsChart() {
    var inputDate = $("#datepicker").val();
    var SelectedType = 4;
    dataTotalCount = [];
    var classValue = $("#ddlClassRoom").val() == undefined ? 0 : $("#ddlClassRoom").val();
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetYearlyNonHealthReason",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + classValue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
         //   
            if (data.d != '') {

                var obj = JSON.parse(data.d);
                var monthData = [];
                var reasonData = [];
                var valueData = [];
                var seriesData = [];
                var dataobj = {
                    name: null,
                    data: [],
                    visible: false
                };

                $.each(obj, function (i, item) {
                    var keyCount = 0;
                    var tempData = [];
                    for (var key in item) {
                        if (key == 'AttendanceMonthName') {
                            monthData.push(item[key]);
                        }

                        if (keyCount > 3) {
                            if (i == 0) {
                                reasonData.push(key);
                            }
                            tempData.push(item[key] == null ? 0 : item[key]);
                        }

                        keyCount++;
                    }
                    valueData.push(tempData);
                });
            }

            for (var j = 0; j < reasonData.length; j++) {

                dataobj = { name: null, data: [], visible: false };

                dataobj.name = reasonData[j];

                if (j < 3) {
                    dataobj.visible = true;
                }

                for (var k = 0; k < valueData.length; k++) {
                    var temp = valueData[k][j];
                    dataobj.data.push(temp);
                }

                seriesData.push(dataobj);

            }
            
            drawReasonChartforNonHealth(monthData, seriesData);

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function drawReasonChartforNonHealth(data1, data2) {


    $('#absence-nonhealth-reason').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            categories: data1
        },
        yAxis: {
            title: {
                text: LocalResources.Lbl_TotalStudents  //'Total Students'
            }
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';

                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '';
                });
                return s;
            },
            shared: true
        },
        series: data2
    });
}


function DrawYearlyAbsentReasonChart(data) {

    var healthData = [];
    var nonHealthData = [];
    var unknownData = [];
    var AttendanceMonthData = []; 

    for (var i = 0; i < data.d.length; i++) {
        healthData.push(data.d[i].HealthAbsenteeRate);
        nonHealthData.push(data.d[i].NonHealthAbsenteeRate);
        unknownData.push(data.d[i].UnKnownAbsenteeRate);
        AttendanceMonthData.push(data.d[i].AttendanceMonthName);

    }

    $('#absenteeism-rate-reason').highcharts({
        title: {
            text: ''
        },
        xAxis: {
            categories: AttendanceMonthData
        },
        yAxis: {
            labels: {
                formatter: function () {
                    return this.value; // + "%";
                }
            },
            title: {
                text: LocalResources.Lbl_Percentage
            },
            min: 0,
            max: 100

        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';
                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '';
                });
                return s;
            },
            shared: true
        },
        series: [{
            name: LocalResources.Lbl_Health,    // 'Health',
            data: healthData
        }, {
            name: LocalResources.Lbl_NonHealth,    //'Non-Health',
            data: nonHealthData
        }, {
            name: LocalResources.Lbl_Unknown,    //'Unknown',
            data: unknownData
        }],
    });
}


function DrawTeacherYearlyGrid(data) {
    debugger;
    //$("#count-sevchronic").text(data.length);
    
    var header = '<tr>' +
        '<th style="min-width:70px; text-align: center;">' + GlobalResources.Lbl_SerialNo + '</th>' +
        ' <th style="min-width:160px;">' + LocalResources.Lbl_Nisn + '</th>' +
        ' <th style="min-width:160px;">' + LocalResources.Lbl_StudentName + '</th>' +
        ' <th style="min-width:160px;">' + LocalResources.Lbl_ClassName + '</th>' +
        '<th style="min-width:100px;">' + LocalResources.Lbl_AbsentDays + '</th></tr>';
        //' <th>' + LocalResources.Lbl_AbsenceType+'</th></tr>';


    var slNo = 0;
    var row = "";
    var row1 = "";
    $('#tblhead').empty();
    $('#tblBody').html('');
   
    var countdata = [];
    var countdata1 = [];
    row = "";
    row1 = "";
    for (var i = 0; i < data.length; i++) {


        var tempData = data[i];
        //if (tempData.ReasonType == 'Severely Chronically Absent') {
            row += '<tr>';
            row += ' <td>' + (++slNo) + '</td>' +
                ' <td>' + tempData.NISN + '</td>' +
                ' <td>' + tempData.Name + '</td>' +
                ' <td>' + tempData.ClassName + '</td>' +
                ' <td>' + tempData.AbsentDays + '</td>';
                //' <td>' + LocalResources[tempData.ReasonType] + '</td>';

            row += '</tr>';
        //    countdata.push(row);
        //}
        //if (tempData.ReasonType == 'Chronically Absent') {
        //    row1 += '<tr>';
        //    row1 += ' <td>' + (++slNo) + '</td>' +
        //        ' <td>' + tempData.NISN + '</td>' +
        //        ' <td>' + tempData.Name + '</td>' +
        //        ' <td>' + tempData.ClassName + '</td>' +
        //        ' <td>' + tempData.AbsentDays + '</td>';
        //        //' <td>' + LocalResources[tempData.ReasonType] + '</td>';

        //    row1 += '</tr>';
        //    countdata1.push(row1);
        //}

    }
  
    $('#tblhead').html(header);
    $('#tblBody').html(row);
    //$('#tblhead1').html(header);
    //$('#tblBody1').html(row1);

}


function DrawTeacherYearlyGrid1(data) {
    debugger;
    //$("#count-sevchronic").text(data.length);

    var header = '<tr>' +
        '<th style="min-width:70px; text-align: center;">' + GlobalResources.Lbl_SerialNo + '</th>' +
        ' <th style="min-width:160px;">' + LocalResources.Lbl_Nisn + '</th>' +
        ' <th style="min-width:160px;">' + LocalResources.Lbl_StudentName + '</th>' +
        ' <th style="min-width:160px;">' + LocalResources.Lbl_ClassName + '</th>' +
        '<th style="min-width:100px;">' + LocalResources.Lbl_AbsentDays + '</th></tr>';
    //' <th>' + LocalResources.Lbl_AbsenceType+'</th></tr>';


    var slNo = 0;
    var row = "";
    var row1 = "";
    $('#tblhead1').empty();
    $('#tblBody1').html('');

    var countdata = [];
    var countdata1 = [];
    row = "";
    row1 = "";
    for (var i = 0; i < data.length; i++) {


        var tempData = data[i];
        //if (tempData.ReasonType == 'Severely Chronically Absent') {
        row += '<tr>';
        row += ' <td>' + (++slNo) + '</td>' +
            ' <td>' + tempData.NISN + '</td>' +
            ' <td>' + tempData.Name + '</td>' +
            ' <td>' + tempData.ClassName + '</td>' +
            ' <td>' + tempData.AbsentDays + '</td>';
        //' <td>' + LocalResources[tempData.ReasonType] + '</td>';

        row += '</tr>';
        //    countdata.push(row);
        //}
        //if (tempData.ReasonType == 'Chronically Absent') {
        //    row1 += '<tr>';
        //    row1 += ' <td>' + (++slNo) + '</td>' +
        //        ' <td>' + tempData.NISN + '</td>' +
        //        ' <td>' + tempData.Name + '</td>' +
        //        ' <td>' + tempData.ClassName + '</td>' +
        //        ' <td>' + tempData.AbsentDays + '</td>';
        //        //' <td>' + LocalResources[tempData.ReasonType] + '</td>';

        //    row1 += '</tr>';
        //    countdata1.push(row1);
        //}

    }

    $('#tblhead1').html(header);
    $('#tblBody1').html(row);
    //$('#tblhead1').html(header);
    //$('#tblBody1').html(row1);

}


function drawYearlyReportChart(data) {
    debugger;
    //alert(3);
    var presentData = [];
    var absentData = [];
    var seriesData = []; 
    var yearNames = [];
    var yearCount = 0;
    var categoryNames = [];
    var currentYearID = 0;
    debugger;
    
        
    if (menutypevalue != 2) {
        for (var i = 0; i < data.d.length; i++) {
            if (!data.d[i].IsHistory)
                categoryNames.push(data.d[i].AttendanceMonthName);

            if (i == 0) {
                currentYearID = data.d[i].AYID;
                yearNames.push(data.d[i].AYName);
                // presentData.push(data.d[i].PresentPercent);
                absentData.push(parseFloat(data.d[i].AbsentPercent.toFixed(1)));
            }
            else if (currentYearID != data.d[i].AYID) {
                seriesData.push({data: absentData });
                currentYearID = data.d[i].AYID;
                yearNames.push(data.d[i].AYName);
                absentData = [];
                // presentData.push(data.d[i].PresentPercent);
                absentData.push(parseFloat(data.d[i].AbsentPercent.toFixed(1)));
            }
            else {
                // presentData.push(data.d[i].PresentPercent);
                absentData.push(parseFloat(data.d[i].AbsentPercent.toFixed(1)));
            }
        }
        seriesData.push({ name: yearNames[yearCount++], data: absentData });
        }
    else {
        for (var i = 0; i < data.d.length; i++) {
            absentData.push(parseFloat(data.d[i].AbsentPercent.toFixed(1)));
            presentData.push(parseFloat(data.d[i].PresentPercent.toFixed(1)));
            categoryNames.push(data.d[i].ClassName);
        }
        seriesData.push({ name: LocalResources.Lbl_Rpt_SchoolAbsentStudents, data: absentData, color: '#a00101' });

        seriesData.push({ name: LocalResources.Lbl_Rpt_SchoolPresentStudents, data: presentData, color: '#01a051' });
        //seriesData.push({ name: LocalResources.Lbl_AbsentStudents, data: absentData });

        }

    if (menutypevalue != 2) {
        let lang = localStorage.getItem('loglang');
        if (lang == "en-us") {
        }
        else {
            var Eng_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var Ind_months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
            var dummymonths = [];
            for (var i = 0; i < categoryNames.length; i++) {
                for (var j = 0; j < Eng_months.length; j++) {
                    if (categoryNames[i] == Eng_months[j]) {
                        dummymonths.push(Ind_months[j]);
                    }
                }
            }
            categoryNames = dummymonths;
            console.log(dummymonths);
        }
    }
    
    
    $('#absenteeism-rate').highcharts({
        chart: {
            type: menutypevalue != 2 ? 'line' : 'column',
            height : 250
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: categoryNames
        },
        
        yAxis: {
            labels: {
                formatter: function () {
                    return this.value; // + "%";
                }
            },
            title: {
                text: LocalResources.Lbl_Percentage
            },
            min: 0,
            max: 100
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                },
                colors: [
                    '#01a051', '#a00101'
                ]
            //    series: {
            //    colors: ['#01a051','#a00101']
            //}
            },
        },
        tooltip: {
            formatter: function () {
                var s = '<b>' + this.x + '</b>';

                $.each(this.points, function () {
                    s += '<br/>' + this.series.name + ': ' +
                        this.y + '%';
                });

                return s;
            },
            shared: true
        },
        series: seriesData
    });

}

function drawYearlyGenderReportChart(data) {
    debugger;
    console.log(data);
    var presentData = [];
    var absentData = [];
    var seriesData = [];
    var yearNames = [];
    var yearCount = 0;
    var categoryNames = [];
    var currentYearID = 0;
    for (var i = 0; i < data.d.length; i++) {
        if (data.d[i].TotalBoys > 0) {
            absentData.push(data.d[i].TotalBoys);

            categoryNames.push(data.d[i].ClassRoomName);
        }
    }

    if ($("#ddlGender").val() == 'M') {
        seriesData.push({ name: 'Boys', data: absentData });
    }
    else {
        seriesData.push({ name: 'Girls', data: absentData });

    }

$('#absenteeism-rate').highcharts({
    chart: {
        type: 'column'
    },
    title: {
        text: ''
    },
    xAxis: {
        categories: categoryNames
    },

    yAxis: {
        labels: {
            formatter: function () {
                return this.value;
            }
        },
        title: {
            text: ''
        }
    },
    plotOptions: {
        column: {
            stacking: 'normal',
            dataLabels: {
                enabled: true
            }
        }
    },
    tooltip: {
        formatter: function () {
            var s = '<b>' + this.x + '</b>';

            $.each(this.points, function () {
                s += '<br/>' + this.series.name + ': ' +
                    this.y + '';
            });

            return s;
        },
        shared: true
    },
    series: seriesData
});

}


function LoadAcademicYear() {
    debugger;
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetCompareClassAcademicYear",
        data: '{MenuType: "' + menutypevalue + '" , SchoolId: "' + $("#ddlSchoolSearch").val() + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            LoadAcademicSuccess(data);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}
function LoadAcademicSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.AYName + "'>" + item.AYName + " </option>";
    });
    $('#ddlyear').html(appenddata);
   
    LoadReport();
}



