﻿
////https://canvasjs.com/javascript-charts/multiple-axis-column-chart/

//if (FormLoad) {
//    LoadDefaultReport();
//    FormLoad = false;
//}
//else {
//    LoadReport();
//    LoadStudentAttendanceReport();
//    LoadDailyAttendanceRateSummary();

//}

$("#divLoader").show(); 
var thisMonth = "";
var menutypevalue = 0;



$(document).ready(function () {

    debugger;
    if (window.location.pathname == '/Report-schoolsummary' || window.location.pathname == '/Report-schoolsummary.aspx') {
        menutypevalue = 1;
        document.getElementById("schoolsearch").style.display = "block";

    }
    else if (window.location.pathname == '/Report-CompareClasses' || window.location.pathname == '/Report-CompareClasses.aspx') {
        menutypevalue = 2;
        document.getElementById("schoolsearch").style.display = "block";

    }
    else if (window.location.pathname == '/Reports-IndClassSummary' || window.location.pathname == '/Reports-IndClassSummary.aspx') {
        menutypevalue = 3;
        document.getElementById("schoolsearch").style.display = "block";

    }
    else if (window.location.pathname == '/Report-IndChildSummary' || window.location.pathname == '/Report-IndChildSummary.aspx') {
        menutypevalue = 4;
        document.getElementById("schoolsearch").style.display = "block";


    }
    else {
        menutypevalue = 5;
        document.getElementById("schoolsearch").style.display = "block";
        document.getElementById("schoolsearch1").style.display = "block";
    }
    LoadReport();
    //LoadStudentAttendanceReport();
    //LoadDailyAttendanceRateSummary();
    //LoadReasonSummary();

    

});



var classData = [];

var tdClass = { "H": "present", "K": "absent", "NK": "sick_leave", "C": "unknown_leave", "L": "holyday" };


function LoadReport() {
   // 
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 1;
    dataPointsPresent = [];
    dataPointsAbsent = [];
    dataPointsClassName = [];
    dataTotalCount = [];    

    thisMonth = getMonthName(inputDate);

    var Eng_months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var Ind_months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
    var dummymonths = [];
    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
    }
    else {
        for (var j = 0; j < Eng_months.length; j++) {
            if (thisMonth == Eng_months[j]) {
                thisMonth = Ind_months[j];
            }
        }
    }
    $("#monthName").text(thisMonth); 

    GetHolidayList(inputDate, $("#ddlClassSearch").val(), 1);

    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetClassWisePresentAbsentReport",
        url: "../Default.aspx/GetClassWisePresentAbsentReportForAdmin",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                // $("#monthName").text(thisMonth);      
                drawDailyReportChart(data);//drawDailyReportChart(data);
                $("#divLoader").hide();
                document.getElementById("hideAll").style.display = "block";
                dataTotalCount.sort(function (a, b) { return a - b });
                var maxYvalue1 = dataTotalCount[dataTotalCount.length - 1];
                maxYvalue1 = maxYvalue1 + 10;
                maxYvalue1 = maxYvalue1.toString().substr(0, maxYvalue1.toString().length - 1);
                var maxYvalue = parseInt(maxYvalue1 * 10);

                var barChartData = {
                    labels: dataPointsClassName,
                    datasets: [{
                        label: LocalResources.Lbl_Present, //'Present',
                        backgroundColor: "#83A7D0",
                        yAxisID: "y-axis-1",
                        data: dataPointsPresent
                    }, {
                        label: LocalResources.Lbl_Absent,  //'Absent',
                        backgroundColor: "#D38483",
                        yAxisID: "y-axis-2",
                        data: dataPointsAbsent
                    }]

                };

                var ctx = document.getElementById("canvas").getContext("2d");
                window.myBar = new Chart(ctx, {
                    type: 'bar',
                    data: barChartData,
                    options: {
                        onClick: function (cc) {
                            console.log(cc);
                        },
                        responsive: true,
                        title: {
                            display: true,
                            text: "Class wise attendance report for " + $("#txtDatePicker").val()
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: true
                        },
                        scales: {
                            yAxes: [{
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: true,
                                position: "left",
                                id: "y-axis-1",

                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }, {
                                type: "linear", // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
                                display: false,
                                position: "right",
                                id: "y-axis-2",
                                gridLines: {
                                    drawOnChartArea: false
                                },
                                ticks: {
                                    beginAtZero: true,
                                    steps: 10,
                                    stepValue: 5,
                                    max: maxYvalue
                                }
                            }],
                        }
                    }
                });

                $("#trigo").show();
                $("#fullReport").hide();
            }

            else {
                $("#divLoader").hide();
                document.getElementById("hideAll").style.display = "block";
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
      LoadStudentAttendanceReport();
    LoadDailyAttendanceRateSummary();
    LoadReasonSummary();
}



function LoadStudentAttendanceReport() {
    AttendanceList = [];
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 1;

    
    //$('#dataTableAttendance').dataTable({
    //    "bDestroy": true,
    //    scrollCollapse: true,
    //    scroller: true,
    //    deferRender: true,
    //}).fnDestroy();
    //$('#dataTableAttendance tbody').empty();
    $.ajax({
        type: "POST",
      //  url: "Default.aspx/GetStudentWisePresentAbsentReport",
        url: "../Default.aspx/GetStudentWisePresentAbsentReport",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            
            var slNo = 0;
            $('#total').text(data.d.length);
          
            DrawTeacherReportGrid(data.d);

            InitDataTableReportForMonthly('dataTableAttendance', $("#ddlClassSearch option:selected").text() + ' - ' + thisMonth + ' ' + $('#datepicker').datepicker('getDate').getFullYear());
            //$('#dataTableAttendance').DataTable({
            //    //scrollY: "300px",
            //    scrollX: true,
            //    scrollCollapse: true,
            //    paging: false,
            //    destroy: true,               
            //    dom: 'Bfrtip',
            //    buttons: [
            //        'copyHtml5',
            //        'excelHtml5',
            //        'csvHtml5',
            //        'pdfHtml5'
            //    ]
            //    //  });
            //});

            //$.each(data.d, function (i, item) {

            //    AttendanceList[item.ID] = item;
            //    $('<tr id="tr' + item.ID + '" geoLoc=' + item.GeoLocation + '>').html(

            //        "<td>" + (++slNo) + "</td>" +
            //        "<td>" + item.Name + "</td>" +
            //        "<td>" + item.Gender + "</td>" +
            //        "<td>" + + item.AttendanceType + + "</td>").appendTo('#dataTableAttendance tbody');


            //});
            //$('#dataTableAttendance').DataTable({
            //    initComplete: function () {
            //        this.api().columns().every(function () {
            //            var column = this;
            //            var select = $('<select><option value=""></option></select>')
            //                .appendTo($(column.footer()).empty())
            //                .on('change', function () {
            //                    var val = $.fn.dataTable.util.escapeRegex(
            //                        $(this).val()
            //                    );
            //                    //to select and search from grid  
            //                    column
            //                        .search(val ? '^' + val + '$' : '', true, false)
            //                        .draw();
            //                });

            //            column.data().unique().sort().each(function (d, j) {
            //                select.append('<option value="' + d + '">' + d + '</option>')
            //            });
            //        });
            //    }
            //});


            //  });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}


function LoadDailyAttendanceRateSummary() {
    var inputDate = $("#datepicker").val();
    //var SelectedType = SelectedType;
    var SelectedType = 1;
    var classvalue = 0;
    if (menutypevalue == 3)
        classvalue = $("#ddlClassSearch").val();
    else
        classvalue = 0;

    dataTotalCount = [];
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetMonthlyAttendanceRateSummaryForAdmin",
        data: '{SchoolId: "' + $("#ddlSchoolSearch").val() + '",ClassId: "' + classvalue + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            for (var i = 0; i < data.d.length; i++) {
                $("#AttendanceRate").text(data.d[i].Present);
                $("#AbsenteesimRate").text(data.d[i].Absent);
                $("#Unknown").text(data.d[i].UnKnown);
                $("#BoysAbsenteeRate").text(data.d[i].BoysAbsenteeRate + ' %');
                $("#BoysAttendanceRate").text(data.d[i].BoysAttendanceRate + ' %');
                $("#GirlsAbsenteeRate").text(data.d[i].GirlsAbsenteeRate + ' %');
                $("#GirlsAttendanceRate").text(data.d[i].GirlsAttendanceRate + ' %');
            }
            //$("#divLoader").hide(); 

        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function drawDailyReportChart(data) {
    $('#Dailyleftsec').show();
    $('#attendance-chart').show();
    $('#DailySummary').show();
    $('#leftsumarry').show();
    $('#grid').show();
    $('#YearlyGraphHeading').hide();
    $('#Monthlygraph').hide();
    $('#attendance-chart3').hide();
    $('#attendance-chart2').hide();
    $('#attendance-chart4').hide();

    var presentData = [];
    var absentData = [];

    categoryData = [];

    for (var i = 0; i < data.d.length; i++) {
        presentData.push(data.d[i].PresentPercent);
        absentData.push(data.d[i].AbsentPercent);
        switch (menutypevalue) {
            case 2:
                categoryData.push(data.d[i].ClassName);
                break;
            default:
                categoryData.push(data.d[i].AttendanceDate == null ? data.d[i].AttendanceMonthName : data.d[i].AttendanceDate);
                break;
        }
    }

    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
    }
    else {
        if (data.d.length > 0) {
            if (data.d[0].AttendanceDate == null) {

            }
            else {
                var Eng_months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                var Ind_months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'];
                var dummymonths = [];
                for (var i = 0; i < categoryData.length; i++) {
                    for (var j = 0; j < Eng_months.length; j++) {
                        var k = categoryData[i].split(" ");

                        if (k[1] == Eng_months[j]) {
                            dummymonths.push(k[0] + ' ' + Ind_months[j] + ' ' + k[2]);
                        }
                    }
                }
                categoryData = dummymonths;
            }
        }
    }

    if (categoryData.length > 0) {
        if (menutypevalue == 2) {
            Highcharts.chart('attendance-chart', {
                chart: {
                    type: 'column',
                    marginBottom: 80
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categoryData,
                    labels: {
                        style: {
                            width: '50px',
                        },
                        step: 1
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: LocalResources.Lbl_Percentage //'Total Students'
                    },
                    stackLabels: {
                        enabled: false,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    },

                    max: 100,
                    tickInterval: 25
                },
                legend: {
                    align: 'center',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false,
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    // mval = point.y/point.stackTotal,
                    pointFormat: '{series.name}: {point.percentage:.1f}%'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    },
                    series: {
                        events: {
                            legendItemClick: function () {
                                return false;
                            }
                        }
                    }
                },
                series: [{
                    name: LocalResources.Lbl_AbsenteeismRate,   //'Student Absent',
                    data: absentData,
                    pointWidth: 50,
                    color: '#a00101'
                }, {
                    name: LocalResources.Lbl_AttendanceRate,  //'Student present',
                    data: presentData,
                    pointWidth: 50,
                    color: '#01a051'
                }]
            });
        }
        else {
            Highcharts.chart('attendance-chart', {
                chart: {
                    type: 'column',
                    marginBottom: 80
                },
                title: {
                    text: ''
                },
                xAxis: {
                    categories: categoryData
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: LocalResources.Lbl_Percentage //'Total Students'
                    },
                    stackLabels: {
                        enabled: false,
                        style: {
                            fontWeight: 'bold',
                            color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                        }
                    },

                    max: 100,
                    tickInterval: 25
                },
                legend: {
                    align: 'center',
                    x: -30,
                    verticalAlign: 'bottom',
                    y: 15,
                    floating: true,
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false,
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    // mval = point.y/point.stackTotal,
                    pointFormat: '{series.name}: {point.percentage:.1f}%'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true,
                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                        }
                    },
                    series: {
                        events: {
                            legendItemClick: function () {
                                return false;
                            }
                        }
                    }
                },
                series: [{
                    name: LocalResources.Lbl_AbsenteeismRate,   //'Student Absent',
                    data: absentData,
                    pointWidth: 50,
                    color: '#a00101'
                }, {
                    name: LocalResources.Lbl_AttendanceRate,  //'Student present',
                    data: presentData,
                    pointWidth: 50,
                    color: '#01a051'
                }]
            });
        }
    }





    //Highcharts.chart('attendance-chart', {
    //    chart: {
    //        type: 'column'
    //    },
    //    title: {
    //        text: ''
    //    },
    //    xAxis: {
    //        categories: classData
    //    },
    //    yAxis: {
    //        min: 0,
    //        title: {
    //            text:  LocalResources.Lbl_TotalStudents  //'Total Students'
    //        },
    //        stackLabels: {
    //            enabled: true,
    //            style: {
    //                fontWeight: 'bold',
    //                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
    //            }
    //        }
    //    },
    //    legend: {
    //        align: 'right',
    //        x: -30,
    //        verticalAlign: 'top',
    //        y: 25,
    //        floating: true,
    //        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
    //        borderColor: '#CCC',
    //        borderWidth: 1,
    //        shadow: false
    //    },
    //    tooltip: {
    //        headerFormat: '<b>{point.x}</b><br/>',
    //        // pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    //        pointFormat: 'Percentage {series.name}: {point.percentage:.1f}% <br/> {series.name}: {point.y}'
    //    },
    //    plotOptions: {
    //        column: {
    //            stacking: 'normal',
    //            dataLabels: {
    //                enabled: true,
    //                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
    //            }
    //        }
    //    },
    //    series: [{
    //        name: 'Absent',
    //        data: absentData,
    //        pointWidth: 50,
    //        color: '#a00101'
    //    }, {
    //        name: 'Present',
    //        data: presentData,
    //        pointWidth: 50,
    //        color: '#01a051'
    //    }],
    //    destroy: true
    //});
}

function LoadReasonSummary() {
    var inputDate = $("#datepicker").val();
    if (menutypevalue == 2) {
        document.getElementById("hidecmpclass").style.display = "none";
    }
    //$("#hidecmpclass").style.display = "none";
    if (window.location.pathname == '/Report-StaffSummary' || window.location.pathname == '/Report-StaffSummary.aspx')
        var SelectedType = 3;
    else
        var SelectedType = 1;

    dataTotalCount = [];
    //alert('hi');
    $.ajax({
        type: "POST",
        url: "../Default.aspx/GetReasonSummary",
        data: '{ClassId: "' + $("#ddlClassSearch").val() + '", SchoolID: "' + $("#ddlSchoolSearch").val() + '", SelectedDate: "' + inputDate + '" , SelectedType: "' + SelectedType + '", MenuType: "' + menutypevalue + '"   }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            var TopReason = '';
            var TopReasonCount = 0;
            var UnknownReasonCount = 0;
            console.log(data.d);
            debugger;
            if (data.d.length > 0) {
                for (var i = 0; i < data.d.length; i++) {

                    if (data.d[i].Code != 'UnKnown' && TopReason == '') {
                        TopReason = data.d[i].Description;
                        TopReasonCount = '(' + data.d[i].Reasoncount + ')';
                        //$("#Health").text(TopReason + TopReasonCount);

                    }
                    if (data.d[i].Code == 'UnKnown') {
                        UnknownReasonCount = data.d[i].ReasonCount;
                    }


                    var aa = data.d.sort(function (a, b) { return b.Reasoncount - a.Reasoncount; });

                    if (aa.length > 1) {
                        if (aa[0].Reasoncount == aa[1].Reasoncount) {
                            let lang = localStorage.getItem('loglang');                        
                            if (lang == "en-us") {
                                $("#Health").text("Multiple");
                            }
                            else {
                                $("#Health").text("Banyak");

                            }
                           
                            document.getElementById("NotEquals").style.display = "none";
                            document.getElementById("Equals").style.display = "block";

                        }
                        else if (aa[0].Reasoncount > aa[1].Reasoncount) {
                            $("#Health1").text(TopReason + TopReasonCount);
                            document.getElementById("NotEquals").style.display = "block";
                            document.getElementById("Equals").style.display = "none";
                        }
                    }
                    else {
                        $("#Health1").text(TopReason + TopReasonCount);
                        document.getElementById("NotEquals").style.display = "block";
                        document.getElementById("Equals").style.display = "none";
                    }
                    //let lang = localStorage.getItem('loglang');
                    //if (lang == "en-us") {
                    //    if (TopReason !== 'UnKnown')
                    //    $("#Health").text(TopReason + ' ( ' + TopReasonCount + ' ) ');
                    //}
                    //else {
                    //    if (TopReason !== 'UnKnown')
                    //    $("#Health").text(TopReason + ' ( ' + TopReasonCount + ' ) ');
                    //}
                    //$("#NonHealth").text(data.d[i].Abstsmratenonhealth); 'Count : ' + TopReasonCount + ' ' + 
                    //$("#Unknown").text(UnknownReasonCount);

                }
            }
            else {
                $("#Health").text("");
                $("#Health1").text("");
                document.getElementById("NotEquals").style.display = "block";
                document.getElementById("Equals").style.display = "none";
            }
            //$("#divLoader").hide();

        },
        failure: function (response) {
            $("#divLoader").hide();
            alert(response.d);
        }
    });
}
