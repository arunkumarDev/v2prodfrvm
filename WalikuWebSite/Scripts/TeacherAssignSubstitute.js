﻿$(document).ready(function () {
  
    initEvents();
    ClearControl();
    LoadNotifications();
});


function initEvents() {

    $("#btnAddAssignSubstitute").click(function () {
        ClearControl();
        $('#txtHeader').text(Lbl_AddAssignSubstitute);
        $('#AssignSubstituteModel').modal('show');
        
    });
    LoadSchool();
    LoadTeachers();  
   
    $("#btnAssignSubstitute").click(function () {
        SubmitForm();
    });
}

function LoadTeacherAssignSubstitute() {
    assignSubstituteList = [];
    $('#dataTableAssignSubstitute').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
        "autoWidth": false
    }).fnDestroy();
    $('#dataTableAssignSubstitute tbody').empty();
    $.ajax({
        type: "POST",
        url: "TeacherAssignSubstitute.aspx/GetTeacherAssignSubstituteList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#total').text(data.d.length);
            var SerialNo = 0;
            $.each(data.d, function (i, item) {
                assignSubstituteList[item.ID] = item;
                $('<tr id="tr' + item.ID + '" >').html(
                    "<td>" + (++SerialNo) + "</td>" +
                    "<td> " + moment(item.AssignFromDate).format('MM/DD/YYYY')  + "</td >" +
                    "<td > " + moment(item.AssignToDate).format('MM/DD/YYYY')  + "</td >" +
                    //"<td style='width: 150px;'>" + item.SchoolName + "</td>" +
                    "<td> " + item.ClassName + "</td >" +
                    "<td > " + item.Name + "</td >" +
                    "<td><a href='javascript:void(0);' data-toggle='modal' onclick='return EditAssignSubstituteEvent(" + item.ID + ");'><i class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                    "<a href='javascript:void(0);' onclick='return DeleteTeacherAssignSubstitute(" + item.ID + ");'><i class='fa fa-trash site-color mr-10' aria-hidden='true'></i></a>" +
                    //"<a href='javascript:void(0);' onclick='return ViewProfile(" + item.ID + ");'><i class='fa fa-eye site-color' aria-hidden='true'></i></a>" +

                    "</td > ").appendTo('#dataTableAssignSubstitute tbody');


            });

            InitializeDataTable('dataTableAssignSubstitute');

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}  

function ClearControl() {
    FormatSQLDate($("#dtStartDate").val());
    FormatSQLDate($("#dtEndDate").val());
    /*$("#ddlSchool").val(0);
    $("#ddlClassRoom").val(0);
    $("#ddlTeacherName").val(0);*/
    $("#hdnUserID").val('0');
}

function EditAssignSubstituteEvent(ID) {
    $('#txtHeader').text(Lbl_EditAssignSubstitute);
    OpenModalPopup(ID);
    return false;
}

function DeleteTeacherAssignSubstitute(ID) {
    if (!confirm("Are you sure want to delete?")) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "TeacherAssignSubstitute.aspx/DeleteTeacherAssignSubstituteByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window.location.href = "TeacherAssignSubstitute.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function OpenModalPopup(ctrl) {
    var assignSubstituteData = assignSubstituteList[ctrl];
    ClearControl();

    if (!IsNullOrEmpty(assignSubstituteData)) {
        $("#hdnUserID").val($.trim(assignSubstituteData.ID));
        $("#dtStartDate").val($.trim(FormatDate(assignSubstituteData.AssignFromDate)));
        $("#dtEndDate").val($.trim(FormatDate(assignSubstituteData.AssignToDate)));
        $("#ddlSchool").val($.trim(assignSubstituteData.SchoolID));
        $("#ddlClassRoom").val($.trim(assignSubstituteData.ClassRoomID));
        $("#ddlTeacherName").val($.trim(assignSubstituteData.SubstituteTeacherID));
  
        $('#AssignSubstituteModel').modal('show');

    }

}


function LoadSchoolSuccess(data) {
    var appenddata;// = "<option value=''>--Select School--</option>";
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
    });
    $('#ddlSchool').html(appenddata);

    LoadClassRoom($('#ddlSchool').val());
    LoadTeacherAssignSubstitute();
}

function LoadClassRoomSuccess(data) {
    var appenddata;//= "<option value=''>--Select Informer--</option>";
    $.each(data.d, function (i, item) {

        appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";
    });

    $('#ddlClassRoom').html(appenddata);

}

function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{ PageIndex: "' + 1 + '" }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadClassRoom(SchoolId) {
    AjaxCall("ChildrenManagement.aspx/GetAllClassRoomForMe", '{SchoolID: "' + SchoolId + '"  }', "POST", LoadClassRoomSuccess, FailureCallBack);
}

function LoadTeachers() {

    $.ajax({
        type: "POST",
        url: "ClassTeacherManagement.aspx/GetMyTeacherList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--" + Lbl_SelectSubstituteTeacher + "--</option>";
            $.each(data.d, function (i, item) {
                if (item.UserType == 2) {
                    appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";
                }

            });
            $('#ddlTeacherName').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function FailureCallBack(data) {
    alert(data.d);
}

$('#ddlSchool').on('change', function () {

    LoadClassRoom($(this).val());
});

var assignSubstituteList = [];

function ValidateUser() {

    if ($.trim($("#ddlSchool").val()) === "") {
        IsInputValid("#ddlSchool", false);
        return false;
    }
    if ($.trim($("#ddlClassRoom").val()) === "") {
        IsInputValid("#ddlClassRoom", false);
        return false;
    }
    if ($.trim($("#ddlTeacherName").val()) === "") {
        IsInputValid("#ddlTeacherName", false);
        return false;
    }
    
    return true;
}

function SubmitForm() {
     if (!ValidateUser()) {
        return false;
    }

    var formData = new FormData();
    formData.append('ID', $("#hdnUserID").val());
    formData.append('AssignFromDate', FormatSQLDate($("#dtStartDate").val()));
    formData.append('AssignToDate', FormatSQLDate($("#dtEndDate").val()));
    formData.append('SchoolID', $("#ddlSchool").val());
    formData.append('ClassRoomID', $("#ddlClassRoom").val());
    formData.append('SubstituteTeacherID', $("#ddlTeacherName").val());
    
    $.ajax({
        url: "TeacherAssignSubstitute.ashx",
        data: formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            if (data == 'Success')
                window.location.href = "TeacherAssignSubstitute.aspx";
            else
                alert(data);
        },
        failure: function (response) {
            alert(response.d);
        }

    }); 

        
}







