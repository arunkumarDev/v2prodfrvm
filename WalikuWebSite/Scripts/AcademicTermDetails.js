﻿$(document).ready(function () {
    LoadSchool();
    LoadAcademicTerm();
   // InitDataTable();

    $('#btnAdd').click(function () {
        AddTerm();
    })
});


function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadAcademicTerm() {
    AjaxCall("AcademicTermDetails.aspx/GetAllAcademicTermList", '{SchoolID: "' + $('#ddlSchool').val() + '"  }', "POST", AcademicListSuccess, FailureCallBack);
}

function DeleteTerm(ID) {
    if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
        return;
    }
    AjaxCall("AcademicTermDetails.aspx/DeleteAcademicTerm", '{ID: "' + ID + '"  }', "POST", SuccessFunction, FailureCallBack);
}

function AddTerm() {
    var StartDate = ""
    var obj = new Object();
    obj.SchoolID = parseInt($('#ddlSchool').val());
    obj.StartDate = StartDate
    obj.StartDate = FormatSQLDate($("#dtStartDate").val());// $('#dtStartDate').val().toString('yyyy-MM-dd');
    obj.EndDate = FormatSQLDate($("#dtEndDate").val());// $('#dtEndDate').val().toString('yyyy-MM-dd');

    AjaxCall("AcademicTermDetails.aspx/ManageAcademicTerm", JSON.stringify(obj), "POST", SuccessFunction, FailureCallBack);
}


function SuccessFunction(data) {
    if (data.d.Message == 'Success') {
        LoadAcademicTerm();
    }
    else {
        alert(data.d.Message);
    }

}

function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
    });
    $('#ddlSchool').html(appenddata);

}

function AcademicListSuccess(data) {
    var SerialNo = 0;
    $('#dataTable tbody').empty();   
    var oTable = $('#dataTable').dataTable();
    oTable.fnClearTable();
    oTable.fnDestroy();
  
    $.each(data.d, function (i, item) {
        $('<tr id="tr' + item.ID + '" >').html(
            "<td> " + (++SerialNo) + "</td >" +
            "<td><span style='display:none'>" + FormatDate(item.Start_Date, 3) +"</span>" + FormatDate(item.Start_Date,1) + "</td >" +
            "<td><span style='display:none'>" + FormatDate(item.Start_Date, 3) +"</span>" + FormatDate(item.End_Date,1) + "</td >" +
            "<td> " + item.SchoolName + "</td >" +
            "<td> " + (item.IsCurrentTerm == true ? Lbl_Active:'') + "</td >" +          
            "<td><a href='javascript:;' onclick='return DeleteTerm(" + item.ID + ");' data-toggle='modal'>	<i class= 'fa fa-trash site-color mr-10' aria-hidden='true'></i></a></td> " +         
            "</td > ").appendTo('#dataTable tbody');
           
    });
    InitializeDataTable('dataTable');

}



function FailureCallBack() {

}