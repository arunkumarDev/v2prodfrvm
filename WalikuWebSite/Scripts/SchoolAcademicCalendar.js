﻿$(document).ready(function () {

    let school = localStorage.getItem('Usertypefrimport');
    if (school == 1) {
        document.getElementById("schoolsearch").style.display = "none";

    }
    else {
        document.getElementById("schoolsearch").style.display = "block";
    }
    initEvents();
    ClearControl();
    LoadSchoolFrDD();
    LoadNotifications();
});

function initEvents() {

    $("#btnAddSchoolAcademicCalendar").click(function () {
        ClearControl();
        $('#txtHeader').text(Lbl_AddCalendarEvent);
        $('#SchoolAcademicCalendarModel').modal('show');
        var calendarDate = FormatSQLDate($("#dtCalendarDate").val());
        
    });
    LoadSchool();
    LoadCalendarEventType(); 
   
    $("#btnSaveCalendar").click(function () {
        SubmitForm();
    });

    $("#ddlSchoolSearch1").on("change", function () {
        var SelectedVal = $(this).val();

        LoadSchoolAcademicCalendar();

    });
}

function LoadSchoolAcademicCalendar() {
    academicCalendarList = [];
    $('#dataTableUser').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
    $('#dataTableUser tbody').empty();
    $.ajax({
        type: "POST",
        data: '{ID: "' + $("#ddlSchoolSearch1").val() + '"}',
        url: "SchoolAcademicCalendar.aspx/GetSchoolAcademicCalendarList",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#total').text(data.d.length);
            var SerialNo = 0;
            $.each(data.d, function (i, item) {
                academicCalendarList[item.ID] = item;
                $('<tr id="tr' + item.ID + '" >').html(
                    "<td>" + (++SerialNo) + "</td>" +
                    "<td>" + item.Name + "</td>" +
                    "<td> " + item.EventType + "</td >" +
                    "<td> " + moment(item.CalendarDate).format('MM/DD/YYYY')  + "</td >" +
                    "<td> " + moment(item.CalendarToDate).format('MM/DD/YYYY') + "</td >" +
                    "<td><a href='javascript:void(0);' data-toggle='modal' onclick='return EditCalendarEvent(" + item.ID + ");'><i class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                    "<a href='javascript:void(0);' onclick='return DeleteSchoolAcademicCalendar(" + item.ID + ");'><i class='fa fa-trash site-color mr-10' aria-hidden='true'></i></a>" +
                    //"<a href='javascript:void(0);' onclick='return ViewProfile(" + item.ID + ");'><i class='fa fa-eye site-color' aria-hidden='true'></i></a>" +

                    "</td > ").appendTo('#dataTableUser tbody');


            });

            InitializeCalendarDataTable('dataTableUser');

        },
        failure: function (response) {
            alert(response.d);
        }
    });

} 

function ClearControl() {
    $("#txtName").val('');
    FormatSQLDate($("#dtCalendarDate").val());
    FormatSQLDate($("#dtCalendarToDate").val());
    $("#hdnUserID").val('0');
}

function EditCalendarEvent(ID) {
    $('#txtHeader').text(Lbl_EditCalendarEvent);
    OpenModalPopup(ID);
    return false;
}

function DeleteSchoolAcademicCalendar(ID) {
    if (!confirm("Are you sure want to delete?")) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "SchoolAcademicCalendar.aspx/DeleteAcademicCalendarByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            window.location.href = "SchoolAcademicCalendar.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function OpenModalPopup(ctrl) {
    var academicCalendarData = academicCalendarList[ctrl];
    ClearControl();

    if (!IsNullOrEmpty(academicCalendarData)) {
        $("#hdnUserID").val($.trim(academicCalendarData.ID));
        $("#txtName").val($.trim(academicCalendarData.Name));
        $("#dtCalendarDate").val($.trim(FormatDate(academicCalendarData.CalendarDate)));
        $("#dtCalendarToDate").val($.trim(FormatDate(academicCalendarData.CalendarToDate)));
        $("#ddlSchoolSearch").val($.trim(academicCalendarData.SchoolId));
        $("#ddlCalendarEventType").val($.trim(academicCalendarData.CalendarTypeId));
  
        $('#SchoolAcademicCalendarModel').modal('show');

    }

}

function LoadCalendarEventTypeSuccess(data) {
    $('#ddlCalendarEventType').find('option').remove().end();
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.EventType + " </option>";
    });
    $('#ddlCalendarEventType').html(appenddata);
    debugger;
}

function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

    });
    $('#ddlSchoolSearch').html(appenddata);
    $('#ddlSchoolSearch1').html(appenddata);
    LoadSchoolAcademicCalendar();
}

function LoadCalendarEventType() {
    AjaxCall("SchoolAcademicCalendar.aspx/GetCalendarEventType", '{PageIndex: "' + 1 + '"  }', "POST", LoadCalendarEventTypeSuccess, FailureCallBack);
}

function LoadSchool() {
    AjaxCall("SchoolAcademicCalendar.aspx/GetMySchoolList", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadSchoolFrDD() {
    AjaxCall("SchoolAcademicCalendar.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function FailureCallBack(data) {
    alert(data.d);
}

var academicCalendarList = [];

function ValidateUser() {
    var Eventname = $.trim($("#txtName").val());

    if (Eventname === '') {

        IsInputValid("#txtName", false);
        return false;
    }
    if ($.trim($("#ddlCalendarEventType").val()) === "") {
        IsInputValid("#ddlCalendarEventType", false);
        return false;
    }
    
    return true;
}

function SubmitForm() {
     if (!ValidateUser()) {
        return false;
    }
   
    var a = FormatSQLDate($("#dtCalendarDate").val());
    var aa = a.split("-");
    var StartDate = aa[0] + '-' + aa[2] + '-' + aa[1];

    var b = FormatSQLDate($("#dtCalendarToDate").val());
    var bb = b.split("-");
    var EndDate = bb[0] + '-' + bb[2] + '-' + bb[1];

    var formData = new FormData();
    formData.append('ID', $("#hdnUserID").val());
    formData.append('Name', $("#txtName").val());
    formData.append('CalendarTypeId', $("#ddlCalendarEventType").val());
    formData.append('SchoolId', $("#ddlSchoolSearch").val());
    formData.append('CalendarDate', StartDate);
    formData.append('CalendarToDate', EndDate);
   // formData.append('CalendarDate', FormatSQLDate($("#dtCalendarDate").val()));
   // formData.append('CalendarToDate', FormatSQLDate($("#dtCalendarToDate").val()));

    var startDate = document.getElementById("dtCalendarDate").value;
    var endDate = document.getElementById("dtCalendarToDate").value;

    if ((Date.parse(startDate) > Date.parse(endDate))) {
        alert("End date should be greater than Start date");
        return false;
    }
 
   
    $.ajax({
        url: "SchoolAcademicCalendar.ashx",
        data: formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            if (data == 'Success')
                window.location.href = "SchoolAcademicCalendar.aspx";
            else
                alert(data);
        },
        failure: function (response) {
            alert(response.d);
        }

    }); 

        
}







