﻿$(document).ready(function () {
   

    $('#btnFilter').click(function () {
        AddFilter();
    })
});


function AddFilter() {
    
    $.ajax({
        type: "POST",
        url: "Report-TrackStudents.aspx/GetTrackStudents",
        data: '{Date: "' + $('#datepicker').val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            //alert("Added Succesfully");
            //window.location.href = "Report-TrackStudents.aspx";
            console.log(data);
            GetDataForTable(data)
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function GetDataForTable(data) {
    var SerialNo = 0;
    $('#dataTable tbody').empty();
    var oTable = $('#dataTable').dataTable();
    oTable.fnClearTable();
    oTable.fnDestroy();
   
    $.each(data.d, function (i, item) {   
            $('<tr id="tr' + item.ID + '" >').html(
                "<td> " + (++SerialNo) + "</td >" +
                "<td><span>" + item.ID + "</span></td >" +
                "<td><span>" + item.Name + "</td >" +
                "<td><span>" + item.Status + "</td >"  ).appendTo('#dataTable tbody');
       

    });
    InitializeDataTable('dataTable');
}