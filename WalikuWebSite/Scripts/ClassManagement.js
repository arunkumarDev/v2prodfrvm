﻿$(window).load(function () {
    InitEvents();
    LoadSchool();
      
    LoadNotifications();
});

function InitEvents() {    
    $('#btnAddClass').click(function () {
        ClearControl();
        let lang = localStorage.getItem('loglang');
        if (lang == "en-us") {
            $('#txtHeader').text("Add Class");
        }
        else {
            $('#txtHeader').text("Tambahkan Kelas");
        }
        $('#ClassManagementModel').modal('show');
    })
    
}

var classid = 0;
$("#btnSaveClass").bind("click", function () {
    if (!ValidateClass()) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "ClassManagement.aspx/ManageClass",
        data: '{ID: "' + $("#hdnClassID").val() + '" ,ClassName: "' + $("#txtClassName").val() + '",SchoolId: "' + $("#ddlSchool").val() + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadClasses();
            ////ClearControl();
            if (data.d.Message == 'Success') {
                window.location.href = "ClassManagement.aspx";
            }
            else {
                let lang = localStorage.getItem('loglang');
                if (lang == "en-us") {
                    alert(data.d.Message);
                }
                else {
                    if (data.d.Message == 'Class name already exists') {
                        var aa = "Nama kelas sudah ada yang sama";
                        alert(aa);
                    }
                    else {
                        alert(data.d.Message);
                    }

                }
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
});


function ValidateClass() {

    if ($.trim($("#txtClassName").val()) === "") {
        IsInputValid("#txtClassName", false);
        return false;
    }

    if ($.trim($("#ddlSchool").val()) === "") {
        IsInputValid("#ddlSchool", false);
        return false;
    }

    return true;
}

$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });
});

function LoadClasses() {
    ClassList = [];
    $('#dataTable').dataTable({
        "bDestroy": true
    }).fnDestroy();
    $('#dataTable tbody').empty();


    $.ajax({
        type: "POST",
       // url: "ClassManagement.aspx/GetAllClasses",
       // data: '{PageIndex: "' + 1 + '"  }',
        url: "ChildrenManagement.aspx/GetAllClassRoomForMe",
        data: '{SchoolID: "' + $("#ddlSchool").val() + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var cnt = 0;
            $.each(data.d, function (i, item) {
                ClassList[item.ID] = item;

                $('<tr id="tr' + item.ID + '" schoolId=' + item.SchoolId + ' classId =' + item.ID + '>').html("<td >" + (++cnt) + "</td>" +
                    //  '<input type="hidden" id="hdn_schoolId' + item.ID + '" value=' + item.SchoolId+ '>' +
                    "<td> " + item.ClassName + "</td >" +
                 //   "<td> " + item.SchoolName + "</td >" +
                    "<td><a href='javascript:;' data-toggle='modal' onclick='return OpenModalPopup(" + item.ID + ");'><i class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                    "<a href='javascript:;' data-toggle='modal' onclick='return DeleteClass(" + item.ID + ");'><i class='fa fa-trash site-color mr-10' aria-hidden='true'></i></a>" +
                    "</td > ").appendTo('#dataTable tbody');
            });
            InitializeDataTable('dataTable');
          
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function ClearControl() {
    $("#txtClassName").val('');
    $("#hdnClassID").val('0');   
    
}
function OpenModalPopup(ctrl) {
   
    ClearControl();
    var ClassData = ClassList[ctrl];
   // alert(ClassData.SchoolId)
    if (ClassData != undefined) {
        $("#hdnClassID").val(ClassData.ID); // $("#hdnClassID").val($('#tr' + ctrl + ' td').eq(0).text());
        $("#txtClassName").val(ClassData.ClassName);
        $("#ddlSchool").val(ClassData.SchoolId);
        $('#ClassManagementModel').modal('show');
        let lang = localStorage.getItem('loglang');
        if (lang == "en-us") {
            $('#txtHeader').text("Edit Class");
        }
        else {
            $('#txtHeader').text("Edit Kelas");
        }

    }

    $("#ClassManagementModel").modal('show');


}
function DeleteClass(ID) {
    $('#DelReasonPopup').modal('show');    
    classid = ID;

    //if (!confirm("Are you sure want to delete?")) {
    //    return;
    //}
    //$.ajax({
    //    type: "POST",
    //    url: "ClassManagement.aspx/DeleteClassByID",
    //    data: '{ID: "' + ID + '"}',
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (data) {
    //        ////LoadClasses();
    //        window.location.href = "ClassManagement.aspx";
    //    },
    //    failure: function (response) {
    //        alert(response.d);
    //    }
    //});
}

function LoadSchool() {

    $.ajax({
        type: "POST",
     //   url: "ChildrenManagement.aspx/GetAllSchool",
        url: "SchoolManagement.aspx/GetMySchools",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata;
            $.each(data.d, function (i, item) {
               // if(typeof a.c === 'undefined'
                appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

            });
            $('#ddlSchool').html(appenddata);
            LoadClasses();  
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}


$("#btndelChild").bind("click", function () {
    debugger;
     $.ajax({
        type: "POST",
         url: "ClassManagement.aspx/DeleteClassByID",
         data: '{ID: "' + classid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadClasses();
            window.location.href = "ClassManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
})
