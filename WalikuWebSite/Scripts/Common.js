﻿
var aa = [];
function validEmail(email) {
    var reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (reg.test(email)) {
        return true;
    }
    else {
        return false;
    }
} 

function validPhoneNumber(phoneNumber) {
   // var PhoneNoPattern = /^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/;
    var PhoneNoPattern = /^[+]?\d+$/;
    if ($.trim(phoneNumber).match(PhoneNoPattern)) {
        return true;
    }
    else {
        return false;
    }
}
window.onload = function () {
    $('input').parent().addClass('is-filled');
    $('select').parent().addClass('is-filled');
   
};
function unreadClick(currElem) {
    
    $(currElem).removeClass('unread').addClass('read');
    var notificationCount = localStorage.getItem('notificationCount');
    var getNotifytext = localStorage.getItem('NotifyText');
    if (getNotifytext != null && getNotifytext.length > 0) {
        var aa1 = getNotifytext.includes(currElem.textContent);
        if (aa1) {

        }
        else {
            getNotifytext = getNotifytext.concat("," + currElem.textContent);
            localStorage.setItem('NotifyText', getNotifytext);
            notificationCount--;
            localStorage.setItem('notificationCount', notificationCount);
        }
    }
    else {
        aa.push(currElem.textContent)
        localStorage.setItem('NotifyText', aa);
        notificationCount--;
        localStorage.setItem('notificationCount', notificationCount);
        //SubmitNotificationDetails();

    }
    //NotificationSuccess();
    if (notificationCount > 0)
        $('.notification-count').text(notificationCount);
    else
        $('.notification-count').hide();

    
}
function LoadNotifications() {
    var Count = 0;
    var inputDate = moment().format('MM/DD/YYYY'); //"10/13/2020"; //moment().format('MM/DD/YYYY');
    if (localStorage.getItem('NotifyDate') == null)
        localStorage.setItem('NotifyDate', inputDate);
    else {
     
        if (localStorage.getItem('NotifyDate') == inputDate) {

        }
        else {
            localStorage.removeItem('NotifyDate');
            localStorage.removeItem('notificationCount');
            localStorage.removeItem('NotifyText');

        }
    }
    //    $("#datepicker").val();
    //if (inputDate == undefined) {
    //    inputDate = moment().format('MM/DD/YYYY'); 
    //}
    $.ajax({
        type: "POST",
        url: "Default.aspx/LoadNotifications",
        data: '{ SelectedDate: "' + inputDate + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            debugger;
            var arrayOfWeekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]

            var dateObj = new Date(inputDate);
            var MonthDate = dateObj.getDate();
            var weekdayNumber = dateObj.getDay();
            var weekdayName = arrayOfWeekdays[weekdayNumber]
       
            var lastday = function (y, m) {
                return new Date(y, m, 0).getDate();
            }
            var LastdayofMonth = lastday(dateObj.getFullYear(), dateObj.getMonth() + 1);
            console.log(LastdayofMonth);

            var FmonthDate = 1;
            var SmonthDate = 2;
            var TmonthDate = 3;
            var FomonthDate = 4;
            var FimonthDate = 5;
            var SimonthDate = 6;
            var SemonthDate = 7;
            var notificationCount = 0;
            for (var i = 0; i < data.d.length; i++) {
                $('.notification-ul').empty();
                
                   
                var schoolName = '';
                var notifications = '';
                if (data.d[i].GetSchoolName) {
                    schoolName = data.d[i].GetSchoolName;
                }
                debugger;
                let lang = localStorage.getItem('loglang');
                //if (!data.d[i].IsStaffAttendanceTaken) {
                   

                if (lang == "en-us") {
                    notificationCount++;
                        if (localStorage.getItem('NotifyText') != null) {
                            var cc = localStorage.getItem('NotifyText');
                            var dd = cc.includes('Please Take Teacher Attendance!');
                            if (dd) {
                                notifications += '<li class="read" style="height: 50px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Please Take Teacher Attendance!' +
                                    '</span></li>';
                                //<span class=" notification-time" >Today</span>
                            }
                            else {
                                notifications += '<li class="unread" style="height: 50px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Please Take Teacher Attendance!' +
                                    '</span></li>';
                            }
                        }
                        else {
                            notifications += '<li class="unread" style="height: 50px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Please Take Teacher Attendance!' +
                                '</span></li>';
                        }
                    }
                    else {

                        if (localStorage.getItem('NotifyText') != null) {
                            var cc = localStorage.getItem('NotifyText');
                            var dd = cc.includes('Mohon Mengambil Absen Guru!');
                            if (dd) {
                                notifications += '<li class="read" style="height: 50px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Mohon Mengambil Absen Guru!' +
                                    '</span></li>';
                            }
                            else {
                                notifications += '<li class="unread" style="height: 50px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Mohon Mengambil Absen Guru!' +
                                    '</span></li>';
                            }
                        }
                        else {
                            notifications += '<li class="unread" style="height: 50px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Mohon Mengambil Absen Guru!' +
                                '</span></li>';
                            //<span class=" notification-time" >Hari Ini</span>
                        }

                    }
                //}
               

               
               
                if (lang == "en-us") {
                    notificationCount++;
                    if (localStorage.getItem('NotifyText') != null) {
                        var cc = localStorage.getItem('NotifyText');
                        var dd = cc.includes('The number of classes that synced attendance today is');
                        if (dd) {
                            notifications += '<li class="read" style="height: 50px !important"  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> The number of classes that synced attendance today is '
                                + data.d[i].CountOfClassesSynched + '</span></li>';
                        }
                        else {
                            notifications += '<li class="unread" style="height: 50px !important; "  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> The number of classes that synced attendance today is '
                                + data.d[i].CountOfClassesSynched + '</span></li>';
                            //<span class=" notification-time" >Today</span>
                        }
                    }
                    else {
                        notifications += '<li class="unread" style="height: 50px !important; "  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> The number of classes that synced attendance today is '
                            + data.d[i].CountOfClassesSynched + '</span></li>';
                            //<span class=" notification-time" >Today</span>
                    }
              
                }
                else {
                    notificationCount++;
                    if (localStorage.getItem('NotifyText') != null) {
                        var cc = localStorage.getItem('NotifyText');
                        var dd = cc.includes('Total Kelas yang disinkronkan dengan kehadiran hari ini adalah');
                        if (dd) {
                            notifications += '<li class="read" style="height: 50px !important"  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Total Kelas yang disinkronkan dengan kehadiran hari ini adalah '
                                + data.d[i].CountOfClassesSynched + '</span></li>';
                        }
                        else {
                            notifications += '<li class="unread" style="height: 50px !important"  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Total Kelas yang disinkronkan dengan kehadiran hari ini adalah '
                                + data.d[i].CountOfClassesSynched + '</span></li>';
                        }
                    }
                    else {
                        notifications += '<li class="unread" style="height: 50px !important"  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Total Kelas yang disinkronkan dengan kehadiran hari ini adalah '
                            + data.d[i].CountOfClassesSynched + '</span></li>';
                    //<span class=" notification-time" >Hari Ini</span>
                    }
                   
                }

                if (weekdayName == 'Friday' || weekdayName == 'Monday') {
                    notificationCount = notificationCount + 2;

                    if (lang == "en-us") {
                       
                        if (localStorage.getItem('NotifyText') != null) {
                            var cc = localStorage.getItem('NotifyText');
                            var dd = cc.includes('Review of Academic Calendar');
                            if (dd) {
                                notifications += '<li class="read" style="height: 45px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review of Academic Calendar' +
                                    '</span></li>';
                                //+
                                //'<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review the weekly report and inform teachers if half or more absences are unknown.' +
                                //'</span></li>';
                            }
                            else {
                                notifications += '<li class="unread" style="height: 45px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review of Academic Calendar' +
                                    '</span></li>';
                            }

                            var dd1 = cc.includes('Review the weekly report and inform teachers if half or more absences are unknown.');
                            if (dd1) {
                                notifications += '<li class="read" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review the weekly report and inform teachers if half or more absences are unknown.' +
                                '</span></li>';
                            }
                            else {
                                notifications += '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review the weekly report and inform teachers if half or more absences are unknown.' +
                                    '</span></li>';
                            }

                        }
                        else {
                            notifications += '<li class="unread" style="height: 45px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review of Academic Calendar' +
                                '</span></li>' +
                                '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review the weekly report and inform teachers if half or more absences are unknown.' +
                                    '</span></li>';
                        }
                    }
                    else {
                        if (localStorage.getItem('NotifyText') != null) {
                            var cc = localStorage.getItem('NotifyText');
                            var dd = cc.includes('Tinjaulah kalender akademik untuk update hari libur seperlunya');
                            if (dd) {
                                notifications += '<li class="read" style="height: 45px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjaulah kalender akademik untuk update hari libur seperlunya' +
                                '</span></li>';
                             
                            }
                            else {
                                notifications += '<li class="unread" style="height: 45px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjaulah kalender akademik untuk update hari libur seperlunya' +
                                '</span></li>';
                            }
                        
                            var dd1 = cc.includes('Tinjau laporan mingguan dan beri tahu guru jika setengah atau lebih absen tidak diketahui.');
                        if (dd1) {
                            notifications += '<li class="read" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjau laporan mingguan dan beri tahu guru jika setengah atau lebih absen tidak diketahui.' +
                                '</span></li>';
                        }
                        else {
                            notifications += '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjau laporan mingguan dan beri tahu guru jika setengah atau lebih absen tidak diketahui.' +
                                '</span></li>';
                            }
                        }
                        else {
                            notifications += '<li class="unread" style="height: 45px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjaulah kalender akademik untuk update hari libur seperlunya' +
                                '</span></li>' +
                                '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjau laporan mingguan dan beri tahu guru jika setengah atau lebih absen tidak diketahui.' +
                                '</span></li>';
                        }

                    }
                }


                if ((LastdayofMonth == MonthDate || FmonthDate == MonthDate || SmonthDate == MonthDate || TmonthDate == MonthDate || FomonthDate == MonthDate
                    || FimonthDate == MonthDate || SimonthDate == MonthDate || SemonthDate == MonthDate)) {
                    notificationCount = notificationCount + 3;
                    //LoadMonthlyCount(inputDate);
                    var TotCount = data.d[0].Transfercount + data.d[0].Dropcount + data.d[0].Diedcount + data.d[0].Othercount ;

                    if (lang == "en-us") {
                        if (localStorage.getItem('NotifyText') != null) {
                            var cc = localStorage.getItem('NotifyText');
                            var dd = cc.includes('Review the monthly and yearly report and meet with class teachers and principal about high risk, chron');
                            if (dd) {
                                notifications += '<li class="read" style="height: 100px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review the monthly and yearly report and meet with class teachers and principal about high risk, chron' +
                                    '</span></li>';
                                //+
                                //'<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Submit monthly class absence reports to the district education office' +
                                //'</span></li>' +
                                //'<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message">' + TotCount + ' Student Deleted - ' + data.d[0].Transfercount + ' transferred,' + data.d[0].Dropcount + ' drop - out, ' + data.d[0].Diedcount + ' died, ' + data.d[0].Othercount + ' Other reasons in ' + data.d[0].NotifyMonthName + ' ' + data.d[0].NotifyYearName + '</span ></li > ';
                            }
                            else {
                                notifications += '<li class="unread" style="height: 100px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review the monthly and yearly report and meet with class teachers and principal about high risk, chron' +
                                    '</span></li>';
                            }
                            var dd1 = cc.includes('Submit monthly class absence reports to the district education office');
                            if (dd1) {
                                notifications +=  '<li class="read" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Submit monthly class absence reports to the district education office' +
                                '</span></li>'
                            }
                            else {
                                notifications += '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Submit monthly class absence reports to the district education office' +
                                '</span></li>'
                            }
                            var dd2 = cc.includes('Student Deleted');
                            if (dd2) {
                                notifications +=  '<li class="read" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message">' + TotCount + ' Student Deleted - ' + data.d[0].Transfercount + ' transferred,' + data.d[0].Dropcount + ' drop - out, ' + data.d[0].Diedcount + ' died, ' + data.d[0].Othercount + ' Other reasons in ' + data.d[0].NotifyMonthName + ' ' + data.d[0].NotifyYearName + '</span ></li > ';
                            }
                            else {
                                notifications += '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message">' + TotCount + ' Student Deleted - ' + data.d[0].Transfercount + ' transferred,' + data.d[0].Dropcount + ' drop - out, ' + data.d[0].Diedcount + ' died, ' + data.d[0].Othercount + ' Other reasons in ' + data.d[0].NotifyMonthName + ' ' + data.d[0].NotifyYearName + '</span ></li > ';
                            }

                        }
                        else {
                            notifications += '<li class="unread" style="height: 100px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Review the monthly and yearly report and meet with class teachers and principal about high risk, chron' +
                                '</span></li>'+
                                '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Submit monthly class absence reports to the district education office' +
                                '</span></li>' +
                                '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message">' + TotCount + ' Student Deleted - ' + data.d[0].Transfercount + ' transferred,' + data.d[0].Dropcount + ' drop - out, ' + data.d[0].Diedcount + ' died, ' + data.d[0].Othercount + ' Other reasons in ' + data.d[0].NotifyMonthName + ' ' + data.d[0].NotifyYearName + '</span ></li > ';

                        }
                    }
                    else {

                        if (localStorage.getItem('NotifyText') != null) {
                            var cc = localStorage.getItem('NotifyText');
                            var dd = cc.includes('Review the monthly and yearly report and meet with class teachers and principal about high risk, chron');
                            if (dd) {
                                notifications += '<li class="read" style="height: 100px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjau laporan bulanan dan tahunan dan temui guru kelas dan kepala sekolah tentang siswa yang absen secara kronis' +
                                    '</span></li>';
                            }
                            else {
                                notifications += '<li class="unread" style="height: 100px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjau laporan bulanan dan tahunan dan temui guru kelas dan kepala sekolah tentang siswa yang absen secara kronis' +
                                    '</span></li>';
                            }
                            var dd1 = cc.includes('Submit monthly class absence reports to the district education office');
                            if (dd1) {
                                notifications += '<li class="read" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Kirim laporan absensi kelas bulanan ke kantor dinas pendidikan ' +
                                    '</span></li>';
                            }
                            else {
                                notifications +=   '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Kirim laporan absensi kelas bulanan ke kantor dinas pendidikan ' +
                                    '</span></li>';
                            }
                        }
                        else {
                            notifications += '<li class="unread" style="height: 100px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Tinjau laporan bulanan dan tahunan dan temui guru kelas dan kepala sekolah tentang siswa yang absen secara kronis' +
                                '</span></li>' +
                                '<li class="unread" style="height: 80px !important" onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Kirim laporan absensi kelas bulanan ke kantor dinas pendidikan ' +
                                '</span></li>';
                        }
                      
                    }
                }

                console.log(notifications);
                $('.notification-ul').append(notifications);
               
                $('.school-name').text(schoolName);

            }
            if (localStorage.getItem('notificationCount') != null) {
                debugger;
                notificationCount = localStorage.getItem('notificationCount');
                if (notificationCount == 0) {
                    notificationCount = '';
                    document.getElementById("notify-count").style.display = "none";

                }
                else {
                    $('.notification-count').text(notificationCount);
                    document.getElementById("notify-count").style.display = "block";
                }
            }
            else {
                debugger;
                localStorage.setItem('notificationCount', notificationCount);
                $('.notification-count').text(notificationCount);

            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

function SubmitNotificationDetails() {
    var notificationCount = localStorage.getItem('notificationCount');
    var getNotifytext = localStorage.getItem('NotifyText');
    $.ajax({
        type: "POST",
        url: "Default.aspx/ManageNotificationDetails",
        data: '{ SelectedDate: "' + inputDate + '",NotifyCount: "' + notificationCount + '",NotifyText: "' + getNotifytext + '", }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

//function NotificationSuccess()
//{
//    var classsynced = 0;
//    var notifications = '';
//    let lang = localStorage.getItem('loglang');
//        if (lang == "en-us") {
            
//            if (localStorage.getItem('NotifyText') != null) {
//                var cc = localStorage.getItem('NotifyText');
//                var dd = cc.includes('The number of classes that synced attendance today is');
//                if (dd) {
//                    notifications += '<li class="read" style="height: 50px !important"  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> The number of classes that synced attendance today is '
//                        + classsynced + '</span></li>';
//                }
//                else {
//                    notifications += '<li class="unread" style="height: 50px !important;font-weight: bold; "  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> The number of classes that synced attendance today is '
//                        + classsynced + '</span></li>';
//                    //<span class=" notification-time" >Today</span>
//                }
//            }
//            else {
//                notifications += '<li class="unread" style="height: 50px !important;font-weight: bold; "  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> The number of classes that synced attendance today is '
//                    + data.d[i].classsynced + '</span></li>';
//                //<span class=" notification-time" >Today</span>
//            }

//        }
//        else {
           
//            notifications += '<li class="unread" style="height: 50px !important"  onclick="return unreadClick(this);"><span class=" col-md-12 notification-message"> Total Kelas yang disinkronkan dengan kehadiran hari ini adalah '
//                + classsynced + '</span></li>';
//            //<span class=" notification-time" >Hari Ini</span>
//        }

      
//        console.log(notifications);
//        $('.notification-ul').append(notifications);

    
   
//}

function LoadMonthlyCount(inputDate) {

    $.ajax({
        type: "POST",
        url: "Default.aspx/LoadMonthlyCount",
        data: '{ SelectedDate: "' + inputDate + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            Count = data.d[0].Transfercount;
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


$('.divNotifications').click(function () {
    debugger;
   
    $('.notification-panel').slideToggle();
});

$('.sidebar-menuitem').click(function (event) {
    
    var currentScope = $(event.currentTarget).parent('li');
    var tagA = $(event.currentTarget);
    if (tagA.hasClass('collapsed')) {
        //Below piece is to close all other menu items
        //$('.sub-menu').slideUp();
        //$('.m-menu_item_inner').find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
        //$('.sidebar-menuitem .expanded').removeClass('expanded').addClass('collapsed');
        // Add class to expand
        tagA.removeClass('collapsed');
        tagA.addClass('expanded');
        // show the sub menu items
        currentScope.find('.sub-menu').slideDown();

        //change glyphicon to down
        tagA.find('.m-menu_item_inner').find('i').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
    }
    else if (tagA.hasClass('expanded')) {

        //Close all submenus and change all glyphicon to right
        $('.sub-menu').slideUp();
        $('.m-menu_item_inner').find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
        $('.sidebar-menuitem .expanded').removeClass('expanded').addClass('collapsed');

        tagA.removeClass('expanded');
        tagA.addClass('collapsed');

      
    }
    else {
        //Close all submenus and change all glyphicon to right
        //$('.sub-menu').slideUp();
        //$('.m-menu_item_inner').find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');
        //$('.sidebar-menuitem .expanded').removeClass('expanded').addClass('collapsed');

        tagA.addClass('expanded');

        currentScope.find('.sub-menu').slideDown();
        tagA.find('.m-menu_item_inner').find('i').removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
    }
});
function IsInputValid(InputCtrl, Isvalid) {
    debugger;
    if (!Isvalid)
        $(InputCtrl).parent().addClass("invalid");
    else
        $(InputCtrl).parent().removeClass("invalid");

    if (InputCtrl.id == "txtNisn")
        $(InputCtrl).parent().removeClass("invalid");

}
function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
}
function IsNullOrEmpty(str) {
    if (typeof(str) === 'undefined' || $.trim(str) == 0)
        return true;
    else
        return false;
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isPhoneNumber(evt, ctrl) { 
    var charCode = (evt.which) ? evt.which : event.keyCode;   
    if (charCode == 43 || (charCode >= 48 && charCode <= 57)) 
        return true;
    
    return false;
}


//function GetSundayHolidayDate(selectedDate, type) {

//    var holidayList = [];
//    var d = new Date(selectedDate);    

//    if (type == 0) {
//        if (d.getDay() == 0) {
//            holidayList.push(d);
//        }
//    }
//    else if (type == 1) {
//        var getTot = daysInMonth(d.getMonth(), d.getFullYear());
//        for (var i = 1; i <= getTot; i++) {    //looping through days in month
//            var newDate = new Date(d.getFullYear(), d.getMonth(), i)
//            if (newDate.getDay() == 0) {   //if Sunday
//                holidayList.push(i);
//            }
//        }

//    }
//    else if (type == 2) {
//        var date1 = new Date(d.getFullYear(), d.getMonth(), d.getDate() – d.getDay() + 7);
//        holidayList.push(date1);
//    }

//}

//function daysInMonth(month, year) {
//    return new Date(year, month, 0).getDate();
//}


function GetCalendarStartAndEndDate(selectedDate, type) {
    if (type == 1) {
        var current = new Date(selectedDate);  
        return [current.getDate(), current.getDate()];
    }
    else if (type == 3) {     //Monthly
        var current = new Date(selectedDate);  
        var lastDay = new Date(current.getFullYear(), current.getMonth() + 1, 0);
        return [1, lastDay.getDate()];
    }
    else if (type == 2) { // Week
        var current = new Date(selectedDate);     // get current date 
        var weekstart = current.getDate() - current.getDay() //+1;    
        var weekend = weekstart + 6;       // end day is the first day + 6 
        var sunday = new Date(current.setDate(weekstart));
        var saturday = new Date(current.setDate(weekend));
        //  console.log(sunday.getDate(), saturday.getDate())
        var startDate = sunday.getDate();
        var endDate = saturday.getDate();
        return [startDate, endDate];
    }
    else {
        return [0, 0];
    }
}

function FormatSQLDate(date) {
  //  var nowDate = new Date(parseInt(date.substr(6))).toISOString();
  // date = new Date(nowDate);
    //date = new Date(date);
    // return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
    var dt = date.split('/');
    return dt[2] + '-' + dt[1] + '-' + dt[0];
}


function setViewProfileMap(latlng, ElementID) {
    if (IsNullOrEmpty(latlng)) {
        latlng = "48.857/2.352";
    }
   
   // if (latlng != "") {
        var myLatlng = latlng.split("/");
        var latlng = new google.maps.LatLng(myLatlng[0], myLatlng[1]);
        var mapOptions = {
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: latlng
        };

        var map = new google.maps.Map(document.getElementById(ElementID), mapOptions);

        var infowindow = new google.maps.InfoWindow();
       var  markers = new google.maps.Marker({
            position: new google.maps.LatLng(myLatlng[0], myLatlng[1]),
            map: map,          
            draggable: true
        });
   // }
}


function AjaxCall(Url, Data, Method, CallBackSuccessFn, CallBackErrorFn)
{
    $.ajax({
        type: Method,
        url: Url,
        data: Data,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        headers: {
            'Lang': CurrentCulture
        },
        success: function (data) {
            CallBackSuccessFn(data)

        },
        failure: function (response) {
            CallBackErrorFn(response);
        }
    });

}

function setCookie(name, value, days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}


function FormatDate(date, type = 1) {
    if (type == 1) {
       // console.log(parseInt(date.substr(6)))
        var nowDate = new Date(parseInt(date.substr(6))).toISOString();        
        date = new Date(nowDate);
        return str_pad(date.getDate()) + '/' + str_pad(date.getMonth() + 1) + '/' + date.getFullYear();
    }

   else if (type == 2) {
        date = new Date(date);
        return str_pad(date.getDate()) + '/' + str_pad(date.getMonth() + 1) + '/' + date.getFullYear();
    }
    else if (type == 3) {
        return date;        
    }
}

function str_pad(n) {
    return n < 10 ? '0' + n : '' + n;
}

//function DestroyDataTable(id) {
//    var oTable = $('#' + id).dataTable();
//    oTable.fnClearTable();
//    oTable.fnDestroy();
//}

function DataTableDestroy(ID) {
    $('#'+ID).dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
}

function InitializeDataTable(ID) {
    var zeroRecText = "";
    var zeroFilterText = "";
    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        zeroRecText = "No matching records found";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json";

    }
    else {
        zeroRecText = "Tidak ada data ditemukan";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/Indonesian.json"

    }

    console.log(zeroFilterText);

    $('#' + ID).dataTable({
        destroy: true,
        "order": [],
        language: {
            url: zeroFilterText
        },
       
        initComplete: function () {
            this.api().columns().every(function () {
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        //to select and search from grid  
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
}

function InitializeCalendarDataTable(ID) {


    //"oLanguage": {
    //    "sLengthMenu": GlobalResources.Lbl_Page_Showing + " _MENU_ " + GlobalResources.Lbl_Page_Entries,
    //        "sInfo": GlobalResources.Lbl_Page_Showing + ' ' + GlobalResources.Lbl_Page + "  _PAGE_ " + GlobalResources.Lbl_Page_Of + " _PAGES_",
    //            "infoEmpty": GlobalResources.Lbl_No_Record,
    //                "sSearch": GlobalResources.Lbl_Search,
    //                    "sEmptyTable": GlobalResources.Lbl_No_Data,
    //                        "zeroRecords": "Nothing found - sorry",
    //                            //  "infoFiltered": "(filtered from _MAX_ total records)",
    //                            "oPaginate": {
    //        "sFirst": GlobalResources.Lbl_Page_First, // This is the link to the first page
    //            "sPrevious": GlobalResources.Lbl_Page_Previous, // This is the link to the previous page
    //                "sNext": GlobalResources.Lbl_Page_Next, // This is the link to the next page
    //                    "sLast": GlobalResources.Lbl_Page_Last // This is the link to the last page
    //    }

    //},

    var zeroRecText = "";
    var zeroFilterText = "";
    let lang = localStorage.getItem('loglang');
    if (lang == "en-us") {
        zeroRecText = "No matching records found";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/English.json";

    }
    else {
        zeroRecText = "Tidak ada data ditemukan";
        zeroFilterText = "//cdn.datatables.net/plug-ins/1.10.22/i18n/Indonesian.json"

    }

    console.log(zeroFilterText);

    $('#' + ID).dataTable({
        dom: 'Bfrtip',
        "stripeClasses": ['tr.odd'],
        destroy: true,
        "order": [],
        buttons: [
            {
                extend: 'excelHtml5',
                title: 'Calendar List'
            },
            {
                extend: 'pdfHtml5',
                title: 'Calendar List',
                orientation: 'landscape',//orientamento stampa
                pageSize: 'A4', //formato stampa
                alignment: "center", //non serve a nnt ma gli dice di stampare giustificando centralmente
                titleAttr: 'PDF',   //titolo bottone    
                exportOptions: {
                    stripNewlines: false
                }
            }
        ],
        language: {
            url: zeroFilterText
        },
       

        initComplete: function () {
            this.api().columns().every(function () {
                debugger;
                var column = this;
                var select = $('<select><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
                        //to select and search from grid  
                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + d + '">' + d + '</option>')
                });
            });
        }
    });
}

var FnGooglMaps = {
    // Sets the map on all markers in the array.
    setMapOnAll:function (map, markers) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    },
    // Removes the markers from the map, but keeps them in the array.
    clearMarkers: function(markers) {
        this.setMapOnAll(null, markers);
    }
};

var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

function getMonthName(inputValue) {
    var now = parseInt(inputValue.toString().substring(0, 2)) - 1;
    return thisMonth = months[now];
}




	