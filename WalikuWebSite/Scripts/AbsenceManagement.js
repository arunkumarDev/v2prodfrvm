﻿$(document).ready(function () {
    InitEvents();
    LoadSchool();
    GetReasonData();   
});

var ReasonMasterList = [];
var ReasonTypeList = [];
var AbsenceInformerList = [];
var ClassRoomList = [];

var AbsentStudList = [];


var SelectedID = 0;
var FormLoad = true;

//function GetServerDateFormat(strDate) {
//   // var date = strDate.split("/"); //.toString('yyyy-MM-dd');
//   // return date[2] + "-" + date[1] + "-" + date[0];
//   // return  strDate.toString('yyyy-MM-dd');
//   return FormatSQLDate(strDate);
//}

function LoadData() {
    var startDate = FormatSQLDate($("#dtStartDate").val());
    var endDate = FormatSQLDate($("#dtEndDate").val());
    GetAbsenceList(0, $('#ddlClassRoom').val(), startDate, endDate);  // $('#ddlClassRoom').val()
}

function InitEvents() {


    $("#ddlAbsenceReason").on("change", function () {
        var SelectedVal = $(this).val();
        ShowHideReason($(this).val())
      //  ShowHideReason($(this).val())

        var AbsenceReason = ReasonTypeList[SelectedVal].Code.toLowerCase();
       
        if (AbsenceReason == "non-health")    
            $('#lblReason').text(LocalResources.Lbl_AbsenceReasonNonHealth);
        else if (AbsenceReason == "health")
            $('#lblReason').text(LocalResources.Lbl_HealthReason);

    });

   

    $("#ddlReason").on("change", function () {
        var SelectedVal = $(this).val();
        var reason = GetSelectedReason(SelectedVal).toLowerCase();
        var AbsenceReason = ReasonTypeList[$('#ddlAbsenceReason').val()].Code.toLowerCase();
        if (reason == 'other' || reason == 'lainnya') {
            $('#dvOtherReason').css('display', 'block');
            if (AbsenceReason == "health")
                $('#lblOtherReason').text(LocalResources.Lbl_OtherHealthIssues);
            else
                $('#lblOtherReason').text(LocalResources.Lbl_OtherNonHealthIssues);
        }
        else {
            $('#dvOtherReason').css('display', 'none');
        }

    });

    $('#ddlSchool').on('change', function () {
       
        LoadClassRoom($(this).val());
    });

   
    $('input[type=radio][name=rdoAttendance]').change(function () {
        if (this.value == 1) {
            //alert("present");
            $('#dvAbsenceReason').css('display', 'none');
            $('#dvHealthReason').css('display', 'none');
            $('#dvInformer').css('display', 'none');
            $('#dvPermission').css('display', 'none');
        }
        else if (this.value == 0) {
            // alert("Absent");
            $('#dvAbsenceReason').css('display', 'block');
            $('#dvHealthReason').css('display', 'block');
            $('#dvInformer').css('display', 'block');
            $('#dvPermission').css('display', 'block');
        }
    });

    $("#btnSearch").bind("click", function () {
        LoadData()
    });

    $("#btnSave").bind("click", function () {
        var rdoAttendance = $("input[name='rdoAttendance']:checked").val();
        var param = new Object();
        param.AttendanceMasterID = SelectedID; 
        param.ChildID = AbsentStudList[SelectedID].ID;
        param.TeacherID = 0;
        param.ReasonType = $("#ddlAbsenceReason").val();
        param.ReasonID = $("#ddlReason").val();
        param.InformationProvider = $("#ddlInformer").val();       
        param.Notes = $("#txtOtherReason").val();
        param.HasPermission = $('#chkPermission').prop("checked");
        param.IsPresent = rdoAttendance;
        param.CreatedOn = AbsentStudList[SelectedID].AttendanceDate;
        param.ClassroomID = AbsentStudList[SelectedID].ClassroomID;
    //    var data: '{ChildID: "' + param.ID + '",ReasonType: "' + $("#ddlReasonType").val() + '",Description: "' + $("#txtDescription").val() + '" ,DescriptionIndian: "' + $("#txtDescriptionIndian").val() + '"}',

        $.ajax({
            url: "AbsenceManagement.aspx/ManageAbsence",
            data: JSON.stringify(param),        
            contentType: "application/json; charset=utf-8",
            type: 'POST',
            dataType: "json",
            headers: {
                'Lang': CurrentCulture
            },
            success: function (data) {

                if (!IsNullOrEmpty(data.d)) {
                    data.d.Message == 'Success';
                   // window.location.href = "AbsenceManagement.aspx";
                    LoadData();
                    $('#editabsence').modal('hide');
                }
                else {
                    alert(response.d.Message);
                }

            },
            failure: function (response) {
                alert(response.d);
            }
        });
    });

}

function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{ PageIndex: "' + 1 + '" }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadClassRoom(SchoolId) {
  //  AjaxCall(AppUrls.WalikuWebApiUri + "api/Children/GetClassBySchool/" + SchoolId, null, "GET", LoadClassRoomSuccess, FailureCallBack);
    AjaxCall("ChildrenManagement.aspx/GetAllClassRoomForMe", '{SchoolID: "' + SchoolId + '"  }', "POST", LoadClassRoomSuccess, FailureCallBack);
}

function GetReasonData() {
    AjaxCall(AppUrls.WalikuWebApiUri + "api/Reason/GetData/", null, "GET", GetReasonDataSuccess, FailureCallBack);
}

function GetAbsenceList(UserId, ClassRoomId, startDate, endDate) {
    var params = new Object()
    params.UserId = 0;
    params.ClassRoomId = ClassRoomId;
    params.StartDate = startDate;
    params.EndDate = endDate,
    params.Type = 2

   // var params = '{ "UserId": ' + UserId + ', "ClassRoomId": ' + ClassRoomId + ',"StartDate":"' + startDate + '" ,  "EndDate": "' + EndDate +'"}';

    AjaxCall(AppUrls.WalikuWebApiUri + "api/Children/GetAllAbsenceChildrenByClass", JSON.stringify(params), "POST", GetAbsenceListResponse, FailureCallBack);
}


function GetAbsenceListResponse(data) {

    var html = "";
    $('#totalAbsence').text(data.length);
    DataTableDestroy('dataTable');
    $('#dataTable tbody').empty();

    $.each(data, function (i, item) {
        //  console.log(ReasonTypeList[item.ReasonID]);
        if (item.AbsentDays > 0) {
         //   AbsentStudList[item.ID] = item;
            AbsentStudList[item.AttendanceMasterID] = item;
            var UserImage = $.trim(item.Image) !== "" ? ('/Photos/' + item.Image) : 'images/user.png';

            html += '<tr><td class="list_content">' +
                '<div class="red">' +
                '<div class="col-md-1 text-center">' +
                '<figure class="list-pic">' +
                '<img src=' + UserImage + ' alt="user image" class="img-responsive img-border">' +
                ' </figure>' +
                //  ' <span class="badge">UID123</span>' +
                ' </div>' +
                '<div class="col-md-10 mt-5">' +
                '<div class="row mb-5 text-left">' +
                ' <div class="col-md-12">' +
                ' <strong>' + item.Name + ' ' + item.Surname + '</strong>' +
                //' <div class="att-count red">' +
                //'<i class="fa fa-calendar-times-o" aria-hidden="true"></i><span class="pl-5">No. of absent days : <strong>' + item.AbsentDays + '</strong></span>' +
                //'</div>' +
                ' <div class="att-count red">' +
                '<i class="fa fa-calendar-times-o" aria-hidden="true"></i><span class="pl-5">Date : <strong>' + FormatDate(item.AttendanceDate,2) + '</strong></span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                ' <div class="row dark-font info-icon mt-15">' +
                ' <div class="col-md-3">' +
                '<i class="fa fa-phone site-color" aria-hidden="true"></i><span>' + item.Phone + '</span>' +
                '</div>' +
                '<div class="col-md-3">' +
                '<img src="images/icons/parent.png" class="mr-10"><span>' + item.ParentName + '</span>' +
                '</div > ' +
                ' <div class="col-md-3">' +
                ' <img src="images/icons/school.png" class="mr-10"><span class="info-content">' + item.ClassName + '</span>' +
                ' </div>' +
                ' <div class="col-md-3">' +
                ' <i class="fa fa-map-marker site-color" aria-hidden="true"></i>' +
                ' <span class="info-content">' + item.Address + '</span>' +
                '</div>' +
                ' </div>' +
                '<div class="row dark-font info-icon mt-10">' +
                ' <div class="btm-content">' +
                ' <div class="col-md-5 pl-0">' +
                '<span> ' + LocalResources.Lbl_ReasonForAbsense + ' : <strong>' + (!IsNullOrEmpty(item.AbsenceReason) ? (GetReasonDescriptionByID(item.ReasonType) + ' - ' + GetSelectedReason(item.ReasonID)) : '') + '</strong></span > ' +
                '</div>' +
                ' <div class="col-md-7">' +
                ' <span>' + LocalResources.Lbl_AbsenceInformer+' : <strong>' + GetInformationProvider(item.InformationProvider) + '</strong></span>' +
                '</div>' +
                ' </div>' +
                '</div>' +
                ' </div>' +
                ' <div class="col-md-1 pull-right text-right act-icon mt-5">';
            if (item.ElapsedTime < 144)
                html += ' <a href="#" data-toggle="modal"  onclick="return OpenModalPopup(' + item.AttendanceMasterID + ');"><i class="fa fa-pencil-square-o site-color mr-10" aria-hidden="true"></i></a>';
            html += ' </div>' +
                '</div></td></tr>';
        }
    });

    $('#dataTable tbody').html(html);
    InitializeDataTable('dataTable');
}

function LoadSchoolSuccess(data) {
    var appenddata;// = "<option value=''>--Select School--</option>";
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
    });
    $('#ddlSchool').html(appenddata);
    LoadClassRoom($('#ddlSchool').val());
}

function LoadClassRoomSuccess(data) {
    var appenddata;//= "<option value=''>--Select Informer--</option>";
    $.each(data.d, function (i, item) {
      
        appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";
    });
  
    $('#ddlClassRoom').html(appenddata);
    if (FormLoad) {
        LoadData();
        FormLoad = false;
    }
}

function LoadClassList() {

}

function GetReasonDataSuccess(data) {
    var appenddata= "<option value=''>--Select Absence Reason--</option>";
    $.each(data.ReasonTypeList, function (i, item) {
        ReasonTypeList[item.ID] = item;
        appenddata += "<option value = '" + item.ID + "'>" + item.Description + " </option>";
    });
    ReasonMasterList = data.ReasonMasterList;
    AbsenceInformerList = data.AbsenceInformerList;

    $('#ddlAbsenceReason').html(appenddata);

    LoadHealthReason($('#ddlAbsenceReason').val())
    LoadAbsenceInformer(data.AbsenceInformerList);

    ShowHideReason($('#ddlAbsenceReason').val());
}

function LoadHealthReason(ReasonTypeID) {
  //  var appenddata;
    var appenddata = "<option value=''>--Select Reason--</option>";
    $.each(ReasonMasterList, function (i, item) {
        if (item.ReasonType == ReasonTypeID) {
            appenddata += "<option value = '" + item.ID + "'>" + item.Description + "</option>";
        }
    });
   // $('#ddlReason').html(appenddata);
    $('#ddlReason')
        .find('option')
        .remove()
        .end()
        .append(appenddata) ;
}

function ShowHideReason(ReasonType) {
    // console.log(ReasonType, ReasonTypeList)
    if (!IsNullOrEmpty(ReasonTypeList[ReasonType])) {
        var reason = ReasonTypeList[ReasonType].Code;

        if (reason == 'UnKnown') {
            $('#dvHealthReason').css('display', 'none');
            $('#dvInformer').css('display', 'none');
            $('#dvPermission').css('display', 'none');
        }
        else if (reason == 'Health') {
            LoadHealthReason(ReasonType);
            $('#dvHealthReason').css('display', 'block');
            $('#dvInformer').css('display', 'block');
            $('#dvPermission').css('display', 'none');
        }
        else if (reason == 'Non-Health') {
            LoadHealthReason(ReasonType);
            $('#dvHealthReason').css('display', 'block');
            $('#dvInformer').css('display', 'block');
            $('#dvPermission').css('display', 'block');

        }
    }
}
function LoadAbsenceInformer(data) {
    var appenddata;// = "<option value=''>--Select Informer--</option>";
    $.each(data, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.Description + " </option>";
    });
    // $('#ddlReason').html(appenddata);
    $('#ddlInformer')
        .find('option')
        .remove()
        .end()
        .append(appenddata);
}

function GetSelectedReason(ReasonId)
{
    if (ReasonId > 0) {
        // $.each(ReasonMasterList, function (i, item) {
        for (var i = 0; i < ReasonMasterList.length; i++) {
            var item = ReasonMasterList[i];
            if (item.ID == ReasonId)
                return item.Description;//.toLowerCase();
        }
    }
}
function OpenModalPopup(ID) {
    ClearControl();
    SelectedID = ID;
    var stud = AbsentStudList[ID];
    GetAbsenteeReasonDetails(stud)
    SetSelectedValue(stud)
    $('#editabsence').modal('show');
   // return false;
}

function SetSelectedValue(stud) {   
    $('#rdoAbsent').prop('checked', true);
}

function GetAbsenteeReasonDetails(child) {
   // console.log(attendance)
   // alert(1)
    var param = new Object();
    param.ChildID = child.ID;
    //param.AttendanceMasterID = child.AttendanceMasterID;
    param.ClassroomID = child.ClassroomID;
    param.CreatedOn = child.AttendanceDate;

    AjaxCall(AppUrls.WalikuWebApiUri + "api/Attendance/GetAbsenceDetails", JSON.stringify(param), "POST", AbsenteeReasonResponse, FailureCallBack);
}

function AbsenteeReasonResponse(data) {
    try {
        if (!IsNullOrEmpty(data)) {
            $('#ddlAbsenceReason').val(data.ReasonType);
            if (data.ReasonType == 3) {

                $('#dvOtherReason').css('display', 'none');
                $('#dvHealthReason').css('display', 'none');
                $('#dvInformer').css('display', 'none');
                $('#dvPermission').css('display', 'none');
                return;
            }
            else {
                $('#dvOtherReason').css('display', 'block');
                $('#dvHealthReason').css('display', 'block');
                $('#dvInformer').css('display', 'block');
                $('#dvPermission').css('display', 'block');
            }
                       
            LoadHealthReason(parseInt($('#ddlAbsenceReason').val()))

            $('#ddlReason').val(data.ReasonID); //ReasonID
            if (!IsNullOrEmpty(data.Notes)) {
                $('#txtOtherReason').val(data.Notes);
                $('#dvOtherReason').css('display', 'block');
                
            }
            else
                $('#dvOtherReason').css('display', 'none');

            $('#chkPermission').prop("checked", data.HasPermission); 
            $('#ddlInformer').val(data.InformationProvider);
            
            
        }

        
    }
    catch (ex) {

    }
  // console.log(data)
}

function GetInformationProvider(informerID) {

    for (var i = 0; i < AbsenceInformerList.length; i++) {
        var item = AbsenceInformerList[i];
        if (item.ID == informerID) {            
            return item.Description;           
        }
    }

        return "";
  
}

function GetReasonDescriptionByID(ReasonTypeID) {
    if (!IsNullOrEmpty(ReasonTypeList[ReasonTypeID])) {
        return ReasonTypeList[ReasonTypeID].Description;
    }
    return '';
}

function ClearControl() {
    $('#txtOtherReason').val('');
    $('#chkPermission').prop("checked",false);
}

function FailureCallBack(data) {
    alert(data.d);
}

