﻿$(document).ready(function () {

    initEvents();
    $("#dtCalendarDate").on("changeDate", function () {
        LoadTeacherAttendanceList();
    });
});

function initEvents() {
    
    LoadTeacherAttendanceList();
   // getStaffAttendanceDateDetails();
    //LoadReasonType();
    LoadNotifications();
   // GetAttendanceList()
}



function SuccessFunction()
{
    alert('success');
    $("#divLoader").hide(); 
    LoadTeacherAttendanceList();
   // location.reload();
   // getStaffAttendanceDateDetails();

    //let lang = localStorage.getItem('loglang');
    //if (lang == "en-us") {
    //    localStorage.setItem('NotifyText', 'Please Take Teacher Attendance!');
    //    var aa = localStorage.getItem('notificationCount');
    //    var bb = aa - 1;
    //    localStorage.setItem('notificationCount', bb);
    //}
    //else {
    //    localStorage.setItem('NotifyText', 'Mohon Mengambil Absen Guru!');
    //    var aa = localStorage.getItem('notificationCount');
    //    var bb = aa - 1;
    //    localStorage.setItem('notificationCount', bb);
    //}
}

function FailureCallBack() {
    alert('failed');
}

var TeachersAttendanceList = [];
function checkboxClick(curElem, userID,ID) {
        TeachersAttendanceList.forEach(function (val, ind) {
            if (val.UserID == userID) {
                val.IsPresent = curElem.checked;
                val.ID = ID;
                if (val.IsPresent) {
                    var dd = document.getElementById("dd_" + val.UserID);
                    dd.selectedIndex = 0;
                    val.ReasonTypeID = 0;
                    val.ReasonId = 0;
                }
                else {
                    val.ReasonId = 0;
                    val.ReasonTypeID = 0;
                }
            }
            
        });
}

function ddlchange(curElem, userID, ReasonId) {
    debugger;
    TeachersAttendanceList.forEach(function (val, ind) {
        if (val.UserID == userID) {  
            if (curElem.value == "0" && val.ReasonTypeID == "0") {
                val.ReasonId = 0;
            }
            val.ReasonTypeID = curElem.value;
            
        }
    });
}



$('#btnSaveStaffAttendance').click(function () {
    debugger;
    $("#divLoader").show(); 
    var StaffList = [];
    var inputDate = moment($("#dtCalendarDate").val()).format('MM-DD-YYYY'); 

   
  
    $.each(TeachersAttendanceList, function (ind, val) {
      
        var obj = new Object();
        obj.Id = val.ID == null || val.ID == undefined ? 0 : val.ID;
        obj.IsPresent = val.IsPresent;
        obj.UserID = val.UserID;
        obj.SchoolID = val.SchoolID;
        obj.ReasonTypeID = val.ReasonTypeID == "0" ? val.ReasonId : parseInt(val.ReasonTypeID);
        obj.AttendanceDate = inputDate;
       // obj.ReasonTypeID = $(".reasontype").val();
       // obj.ReasonTypeID = val.reasontype;
        StaffList.push(obj);
    });

    AjaxCall("TeacherAttendance.aspx/SaveStaffAttendance", JSON.stringify({ 'StaffList': StaffList }), "POST", SuccessFunction, FailureCallBack);
});

function LoadReasonType() {

    $.ajax({
        type: "POST",
        url: "ReasonManagement.aspx/GetTeacherAttendanceReasonTypes", 
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            let lang = localStorage.getItem('loglang');
            if (lang == "en-us") {
                var appenddata = "<option value='0'>--Select Reason--</option>";
            }
            else {
                var appenddata = "<option value='0'>--Pilih Alasan--</option>";

            }
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.Description + " </option>";

            });
            $('.reasontype').html(appenddata);
            localStorage.setItem('Reasontypes', JSON.stringify(data.d));
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadTeacherAttendanceList() {

    teacherAttendanceList = [];
    $('#dataTableTeacherAttendance').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
    $('#dataTableTeacherAttendance tbody').empty();
    var inputDate = moment($("#dtCalendarDate").val()).format('MM-DD-YYYY'); 
    var Schoolid = localStorage.getItem('StaffSchoolId');

    $.ajax({
        type: "POST",
        url: "TeacherAttendance.aspx/GetTeacherAttendanceList1",
        data: '{SelectedDate: "' + inputDate + '" ,SchoolId: "' + Schoolid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            LoadReasonType();
            $('#total').text(data.d.length);
            var SerialNo = 0;
            TeachersAttendanceList = data.d;
            $.each(data.d, function (i, item) {
                teacherAttendanceList[item.UserID] = item;
                let lang = localStorage.getItem('loglang');
                if (lang == "en-us") { 
                }
                else {
                    if (item.UserType == 'Teacher')
                    item.UserType = 'Guru';
                }

                if (item.IsPresent == false && item.ID > 0) {

                    $('<tr id="tr' + item.UserID + '" >').html(
                        "<td style='width: 70px;'>" + (++SerialNo) + "</td>" +
                        "<td style='width: 200px;'>" + item.UserID + "</td>" +
                        "<td style='width: 250px;'> " + item.UserName + "</td >" +
                        "<td style='width: 250px;'> " + item.UserType + "</td >" +
                        "<td> <label class='switch'><input type = 'checkbox'  id='chk_" + item.UserID + "' onClick='return checkboxClick(this," + item.UserID + "," + item.ID + ");' >" +
                        "<span class='slider round'></span></label ></td >" +
                        "<td style='width: 230px;'> <label ><select style='width: 230px;' id='dd_" + item.UserID + "' class='form-control reasontype' onChange='return ddlchange(this," + item.UserID + "," + item.ReasonId + ");' ></select></label ></td >" +
                        "</td > ").appendTo('#dataTableTeacherAttendance tbody');
                    setTimeout(function () {
                      //  $("#chk_" + item.UserID).click();
                        $("#dd_" + item.UserID).change();
                      
                        var dd = document.getElementById("dd_" + item.UserID);
                        for (var i = 0; i < dd.options.length; i++) {
                            if (dd.options[i].value == item.ReasonId.toString()) {
                                dd.selectedIndex = i;
                            }
                        }
                    }, 500);
                   
                }
                else if (item.IsPresent == true) {

                    $('<tr id="tr' + item.UserID + '" >').html(
                        "<td style='width: 70px;'>" + (++SerialNo) + "</td>" +
                        "<td style='width: 200px;'>" + item.UserID + "</td>" +
                        "<td style='width: 250px;'> " + item.UserName + "</td >" +
                        "<td style='width: 250px;'> " + item.UserType + "</td >" +
                        "<td> <label class='switch'><input type = 'checkbox'  id='chk_" + item.UserID + "' onClick='return checkboxClick(this," + item.UserID + "," + item.ID + ");' >" +
                        "<span class='slider round'></span></label ></td >" +
                        "<td style='width: 230px;'> <label ><select style='width: 230px;' id='dd_" + item.UserID + "' class='form-control reasontype' onChange='return ddlchange(this," + item.UserID + "," + item.ReasonId + ");' ></select></label ></td >" +
                        "</td > ").appendTo('#dataTableTeacherAttendance tbody');
                    setTimeout(function () {
                        $("#chk_" + item.UserID).click();
                       // $("#dd_" + item.UserID).change();
                        var dd = document.getElementById("dd_" + item.UserID);
                        for (var i = 0; i < dd.options.length; i++) {
                            if (dd.options[i].value == item.ReasonId.toString()) {
                                dd.selectedIndex = i;
                            }
                        }
                    }, 500);
                }
                else {

                    $('<tr id="tr' + item.UserID + '" >').html(
                        "<td style='width: 70px;'>" + (++SerialNo) + "</td>" +
                        "<td style='width: 200px;'>" + item.UserID + "</td>" +
                        "<td style='width: 250px;'> " + item.UserName + "</td >" +
                        "<td style='width: 250px;'> " + item.UserType + "</td >" +
                        "<td> <label class='switch'><input type = 'checkbox' id='chk_" + item.UserID + "' onClick='return checkboxClick(this," + item.UserID + "," + item.ID + ");' >" +
                        "<span class='slider round'></span></label ></td >" +
                        "<td style='width: 230px;'> <label ><select style='width: 230px;' id='dd_" + item.UserID + "' class='form-control reasontype' onChange='return ddlchange(this," + item.UserID + ");' ></select></label ></td >" +
                        "</td > ").appendTo('#dataTableTeacherAttendance tbody');
                }


            });
            
           // LoadNotifications();
            //InitializeDataTable('dataTableTeacherAttendance');
            $("#divLoader").hide(); 
        },
        failure: function (response) {
            alert(response.d);
        }
    });
} 

function GetAttendanceList() {
    //$.ajax({
    //    url: "TeacherAttendance.aspx/GetTeacherAttendanceList",
    //    method: "POST",
    //    contentType: 'application/json'
    //}).done(function (data) {

    //    var test = [];
    //    test = data;
    //    $('#dataTableTeacherAttendance').dataTable({
    //        lengthChange: false,
    //        pageLength: 10,
    //        deferRender: true,
    //        dom: 'Bfrtip',
    //        order: [[1, 'asc']],
    //        "aaData": data,
    //        "columns": [
                
    //            {
    //                data: "StaffID",
    //                width: "50%"
    //            },
    //            {
    //                data: "UserName",
    //                width: "30%"
    //            },
    //        ]
    //    });
    //});

    var url = "TeacherAttendance.aspx/GetTeacherAttendanceList";
    table = $('#dataTableTeacherAttendance').DataTable({
        //"processing": true,
        "ajax": {
            "type": "POST",
            "url": url,
            "datatype": "json",
            "dataSrc": function (json) {
                return JSON.parse(json);
            }
        },
        "columns": [
            //{ "data": null, defaultContent: '' },
            { "data": 'UserID' },
            { "data": 'UserName' }
        ],
        
        order: [1, 'asc'],
        scrollY: "400px",
        scrollX: true,
        paging: false,
    });
}


function getStaffAttendanceDateDetails() {
    var inputDate = moment($("#dtCalendarDate").val()).format('MM-DD-YYYY'); 
    var Schoolid = localStorage.getItem('StaffSchoolId');
    $.ajax({
        type: "POST",
        url: "TeacherAttendance.aspx/GetStaffAttendanceDateDetails",
        data: '{SelectedDate: "' + inputDate + '" ,SchoolId: "' + Schoolid + '"}',

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);


            debugger;
            //if (data.d[0].StaffAttendanceTaken > 0)
            //    //IsEnabled = false;
            //    document.getElementById('btnSaveStaffAttendance').setAttribute("disabled", "disabled");
            //else
            //    document.getElementById('btnSaveStaffAttendance').removeAttribute("disabled");
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}