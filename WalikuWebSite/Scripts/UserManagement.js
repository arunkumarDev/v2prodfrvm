﻿$(document).ready(function () {
   

    initEvents();
    LoadSchool();
    LoadUserType();
    LoadNotifications();
  //  LoadUsers();   
    LoadMap();
    ClearControl();
    $("#toggle").change(function () {

        // Check the checkbox state
        if ($(this).is(':checked')) {
            // Changing type attribute
            $("#txtLoginPassword").attr("type", "text");

            // Change the Text
            $("#toggleText").text("Hide");
        } else {
            // Changing type attribute
            $("#txtLoginPassword").attr("type", "password");

            // Change the Text
            $("#toggleText").text("Show");
        }

    });
});

function initEvents() {
    $("#btnAddUser").click(function () {
        ClearControl();
        $('#txtHeader').text(Lbl_AddUser);
        $('#UserManagementModel').modal('show');
        setDefaultMapLocation();
    });
    $("#btnLocationSearch").click(function () {
        //alert("The paragraph was clicked.");
        var address = $("#txtLocation").val();
        searchMapLocation(address);
    });
    LoadReasonForDelete();
    $("#btnSaveUser").click(function () {       
        SubmitForm();
    });

   

}

function LoadReasonForDeleteSuccess(data) {
    $('#ddlDeleteReason').find('option').remove().end();
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.Reason + " </option>";
    });
    $('#ddlDeleteReason').html(appenddata);

}

function LoadReasonForDelete() {
    AjaxCall("UserManagement.aspx/GetAllReasonForDeleteTeacher", '{PageIndex: "' + 1 + '"  }', "POST", LoadReasonForDeleteSuccess, FailureCallBack);
}
function FailureCallBack(data) {
    alert(data.d);
}

var userList = [];
var userType = [];
var userid = 0;
    function ValidateUser() {
        var Username = $.trim($("#txtName").val());
        
        if (Username === '') {

            IsInputValid("#txtName", false);
            return false;
        }
        if ($.trim($("#ddlUserType").val()) === "") {
            IsInputValid("#ddlUserType", false);
            return false;
        }
        if ($.trim($("#txtLoginUserName").val()) === "") {
            IsInputValid("#txtLoginUserName", false);
            return false;
        }
        if ($.trim($("#txtLoginPassword").val()) === "") {
            IsInputValid("#txtLoginPassword", false);
            return false;
        }

        if ($.trim($("#txtPhone").val()) === "") {
            IsInputValid("#txtPhone", false);
            return false;
        }

        if (!validPhoneNumber($.trim($("#txtPhone").val()))) {
            IsInputValid("#txtPhone", false);
            return false;
        }


        var emailId = $.trim($("#txtEmail").val());
        if (emailId !== "" && !validEmail(emailId)) {
            IsInputValid("#txtEmail", false);
            alert("Invalid Email");
            return false;
        }

        if ($.trim($("#ddlGender").val()) === "") {
            IsInputValid("#ddlGender", false);
            return false;
        }
        if ($.trim($("#txtAddress").val()) === "") {
            IsInputValid("#txtAddress", false);
            return false;
        }

        //if ($.trim($("#ddlUserType").val()) === "2") {
        //    if ($.trim($("#txtNik").val()) === "") {
        //        IsInputValid("#txtNik", false);
        //        return false;
        //    }
        //}
        
        return true;
    }


    function SubmitForm() {
        if (!ValidateUser()) {
            return false;
        }
        console.log($("#hdnUserID").val());
        if ($("#hdnUserID").val() > 0) {
            var formData = new FormData();
            var file = $('#inImage')[0].files[0];
            formData.append('fileData', $('#inImage')[0].files[0]);

            formData.append('hdnImage', $("#hdnImage").val());
            formData.append('ID', $("#hdnUserID").val());
            formData.append('UserName', $("#txtLoginUserName").val());
            formData.append('Email', $("#txtEmail").val());
            if ($("#txtLoginPassword").val() !== "")
                formData.append('Password', $("#txtLoginPassword").val());

            formData.append('Name', $('#txtName').val());
            formData.append('Surname', $("#txtSurname").val());
            formData.append('Image', 'NA');
            formData.append('UserType', $("#ddlUserType").val());
            formData.append('Address', $("#txtAddress").val());
            formData.append('Phone', $("#txtPhone").val().toString());
            formData.append('Note', $("#txtNote").val());
            formData.append('GeoLocation', $("#txtGeoLocation").val());
            formData.append('Gender', $("#ddlGender").val());
            formData.append('NIK', $("#txtNik").val());
            formData.append('DeleteReasonID', $("#ddlDeleteReason").val());

            $.ajax({
                url: "UserManagement.ashx",
                data: formData,
                enctype: 'multipart/form-data',
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    //alert(data);
                    //LoadUsers();
                    //ClearControl();
                    if (data == 'Success') {
                        //  window.location.href = "UserManagement.aspx";
                        alert("Success");
                        LoadUsers();
                        $('#UserManagementModel').modal('hide');
                    }
                    else
                        alert("Failed");
                },
                failure: function (response) {
                    alert(response.d);
                }

            });
        }
        else {
            $.ajax({
                type: "POST",
                url: "UserManagement.aspx/GetUserExistscount",
                data: '{Name: "' + $('#txtLoginUserName').val() + '"  }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    if (data.d[0].UserCount > 0) {
                        alert('User already exist')
                    }
                    else {


                        var formData = new FormData();
                        var file = $('#inImage')[0].files[0];
                        formData.append('fileData', $('#inImage')[0].files[0]);

                        formData.append('hdnImage', $("#hdnImage").val());
                        formData.append('ID', $("#hdnUserID").val());
                        formData.append('UserName', $("#txtLoginUserName").val());
                        formData.append('Email', $("#txtEmail").val());
                        if ($("#txtLoginPassword").val() !== "")
                            formData.append('Password', $("#txtLoginPassword").val());

                        formData.append('Name', $('#txtName').val());
                        formData.append('Surname', $("#txtSurname").val());
                        formData.append('Image', 'NA');
                        formData.append('UserType', $("#ddlUserType").val());
                        formData.append('Address', $("#txtAddress").val());
                        formData.append('Phone', $("#txtPhone").val().toString());
                        formData.append('Note', $("#txtNote").val());
                        formData.append('GeoLocation', $("#txtGeoLocation").val());
                        formData.append('Gender', $("#ddlGender").val());
                        formData.append('NIK', $("#txtNik").val());
                        formData.append('DeleteReasonID', $("#ddlDeleteReason").val());

                        $.ajax({
                            url: "UserManagement.ashx",
                            data: formData,
                            enctype: 'multipart/form-data',
                            processData: false,
                            contentType: false,
                            type: 'POST',
                            success: function (data) {
                                //alert(data);
                                //LoadUsers();
                                //ClearControl();
                                if (data == 'Success') {
                                    //  window.location.href = "UserManagement.aspx";
                                    alert("Success");
                                    LoadUsers();
                                    $('#UserManagementModel').modal('hide');
                                }
                                else
                                    alert("Failed");
                            },
                            failure: function (response) {
                                alert(response.d);
                            }

                        });
                    }

                },
                failure: function (response) {
                    alert(response.d);
                }

            });
        }
   
}

function LoadUsers() {
    userList = [];
    $('#dataTableUser').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
    $('#dataTableUser tbody').empty();
    $.ajax({
        type: "POST",
       // url: "UserManagement.aspx/GetAllUsers",
      //  data: '{PageIndex: "' + 1 + '"  }',
        url: "UserManagement.aspx/GetMyUserList",
        //  data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $('#total').text(data.d.length);
            var SerialNo = 0;
            $.each(data.d, function (i, item) {
                userList[item.ID] = item;
                var UserImage = $.trim(item.Image) != "" ? ('/Photos/' + item.Image) : 'images/user.png';
                //$('<tr id="tr' + item.ID + '">').html(
                //    "<td>" + (++SerialNo) + "</td>" +
                //    "<td>"+
                //        "<div class=\"text-center\">" +
                //        "<figure class=\"list-pic\">" +
                //      "<img src=" + UserImage + " alt=\"user\" class=\"img-responsive img-border\">" +

                //    "</figure>" +
                //    "</div>"+
                //    "</td> " + 
                //    "<td>" + item.UserName + "</td>" +
                //    "<td  data-userTypeID =" + item.UserType + ">" + (userType[item.UserType] ? userType[item.UserType].UserType : '') + "</td>" +
                //    "<td>" + item.Phone + "</td>" +
                //    "<td data-GeoLocation =" + item.GeoLocation + ">" + item.Address + "</td>" +
                //    "<td><a href='javascript:void(0);' data-toggle='modal' onclick='return EditUser(" + item.ID + ");'><i class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                //    "<a href='javascript:void(0);' onclick='return DeleteUser(" + item.ID + ");'><i class='fa fa-trash site-color mr-10' aria-hidden='true'></i></a>" +
                //    "<a href='javascript:void(0);' onclick='return ViewProfile(" + item.ID + ");'><i class='fa fa-eye site-color' aria-hidden='true'></i></a>" +

                //    "</td > ").appendTo('#dataTableUser tbody');

                $('<tr id="tr' + item.ID + '" >').html(
                    // "<td>" + item.ID + "</td>" +
                    //"<td>" +
                    //"<div class=\"text-center\">" +
                    //" <figure class=\"list-pic\">" +
                    //"<img src=" + UserImage + " alt='user image' class='img-responsive img-border'>" +
                    //"</figure>" +
                    //" </div></td>" +

                    "<td> " + (++SerialNo) + "</td >" +
                    "<td>" +
                    "<div class=\"text-center\">" +
                    "<figure class=\"list-pic\">" +
                    "<img src=" + UserImage + " alt=\"user\" class=\"img-responsive img-border\">" +

                    "</figure>" +
                    "</div>" +
                    "</td> " +
                    "<td> " + item.Name + "</td >" +
                    "<td>" + (userType[item.UserType] ? userType[item.UserType].UserType : '') + "</td>" +
                    "<td>" + item.Phone + "</td>" +
                    "<td>" + item.Address + "</td>" +
                    //  "<td>&nbsp;</td>" +
                    
                    "<td><a href='javascript:void(0);' data-toggle='modal' onclick='return EditUser(" + item.ID + ");'><i class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                    "<a href='javascript:void(0);' onclick='return DeleteUser(" + item.ID + ");'><i class='fa fa-trash site-color mr-10' aria-hidden='true'></i></a>" +
                    "<a href='javascript:void(0);' onclick='return ViewProfile(" + item.ID + ");'><i class='fa fa-eye site-color' aria-hidden='true'></i></a>" +

                    "</td > ").appendTo('#dataTableUser tbody');
            });

            InitializeDataTable('dataTableUser');
         
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
$("#inImage").change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //alert(e.target.result);
            $('#imgUser').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function LoadUserType() {

    $.ajax({
        type: "POST",
        url: "UserManagement.aspx/GetAllUserTypes",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata;
            $.each(data.d, function (i, item) {
                userType[item.ID] = item;
                appenddata += "<option value = '" + item.ID + "'>" + item.UserType + " </option>";

            });
            $('#ddlUserType').html(appenddata);
            LoadUsers();   
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function ClearControl() {
   // $("#MainContent_hdnPath").val('');
    $("#txtEmail").val('');
    $("#txtLoginPassword").val(''); $("#txtName").val(''); $("#txtLoginUserName").val(''); $("#txtSurname").val(''); $("#ddlUserType").val(1); $("#txtAddress").val(''); $("#txtPhone").val(''); $("#txtNote").val('');
    $("#txtNik").val(''); $("#ddlDeleteReason").val(0);
    $("#hdnUserID").val('0');
}

function EditUser(ID) {   
    $('#txtHeader').text(Lbl_EditUser);
    OpenModalPopup(ID);
    return false;
}

function OpenModalPopup(ctrl) {
    var userData = userList[ctrl];
    ClearControl();
  
    if (!IsNullOrEmpty(userData)) {
        $("#hdnUserID").val($.trim(userData.ID));
        $("#txtEmail").val($.trim(userData.Email));
        $("#txtName").val($.trim(userData.Name));
        $("#txtLoginUserName").val($.trim(userData.UserName));
        $("#txtLoginPassword").val($.trim(userData.Password));

        $("#txtSurname").val($.trim(userData.Surname));
        // $("#txtHeader").val($.trim("Edit User"));
        $('#txtHeader').text(Lbl_EditUser);


        if ($.trim(userData.Image) != "")
            $("#imgUser").attr("src", $.trim("Photos/" + userData.Image));
        else
            $("#imgPhoto").attr("src", $.trim("images/user.png"));

        $("#hdnImage").val($.trim(userData.Image));
        $("#ddlUserType").val($.trim(userData.UserType));
        $("#txtAddress").val($.trim(userData.Address));
        $("#txtPhone").val($.trim(userData.Phone));
        $("#txtNote").val($.trim(userData.Note));
        //alert($("#hdnImage").val());
        $("#txtGeoLocation").val($.trim(userData.GeoLocation));
        $("#ddlGender").val($.trim(userData.Gender));
        $("#txtNik").val($.trim(userData.NIK));
        userData.DeleteReasonID === 0 ? $("#ddlDeleteReason").val("8") : $("#ddlDeleteReason").val(userData.DeleteReasonID);
     //   changeLanguageByButtonClick();

        $('#UserManagementModel').modal('show');

        setTimeout(function () {
            setLocation($("#txtGeoLocation").val());
        }, 1000);
    }
 
}
function DeleteUser(ID) {
    $('#DelReasonPopup').modal('show');
    console.log($("#ddlDeleteReason").val());
    userid = ID;

    //if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
    //    return;
    //}
    //$.ajax({
    //    type: "POST",
    //    url: "UserManagement.aspx/DeleteUserByID",
    //    data: '{ID: "' + ID + '"}',
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (data) {
    //        ////LoadUsers();
    //        window.location.href = "UserManagement.aspx";
    //    },
    //    failure: function (response) {
    //        alert(response.d);
    //    }
    //});
}

function checkEmptyImage(v) {
    if (("" + v).length === 0)
        return "emptyuser.jpg";
    else
        return v;
}


function LoadSchool() {
    $.ajax({
        type: "POST",
        url: "SchoolManagement.aspx/GetMySchools",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata;
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

            });
            $('#ddlSchool').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });
});

function ViewProfile(ID) {
    var data = userList[ID];
    $('#viewName').html(data.Name + ' ' + data.Surname);
    $('#viewEmail').html(data.Email);
    $('#viewPhone').html(data.Phone);
    $('#viewAddress').html(data.Address);
    $('#viewNotes').html(data.Note);

    if ($.trim(data.Image) !== "")
        $("#viewProfilePhoto").attr("src", $.trim("Photos/" + data.Image));
    else
        $("#viewProfilePhoto").attr("src", $.trim("images/user.png"));

    setViewProfileMap(data.GeoLocation, 'viewprofileMap');
    $('#viewprofile').modal('show');
}


$("#btndelChild").bind("click", function () {

    $.ajax({
        type: "POST",
        url: "UserManagement.aspx/DeleteUserByID",
        data: '{DelID: "' + $("#ddlDeleteReason").val() + '",ID: "' + userid + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadChildren();
            window.location.href = "UserManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
})