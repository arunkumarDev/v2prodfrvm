﻿$(document).ready(function () {
  
    LoadSchool();
    LoadNotifications();
    // InitDataTable();
    $('#btnOpenAdd').click(function () {
        currentYear = 0;
        document.getElementById('add').style.display = "block";
        document.getElementById('update').style.display = "none";

        document.getElementById("txtYearName").value = "";
        document.getElementById("dtStartDate").value = moment().format("MM/DD/YYYY");
        document.getElementById("dtEndDate").value = moment().format("MM/DD/YYYY");

        $('#editTerm').modal('show');
    });


    $('#btnAdd').click(function () {
        debugger;
        AddTerm();
        //if (currentYear == 0)
        //    AddTerm();
        //else
        //    EditTerm();
        $('#editTerm').modal('hide');
    })
    $('#btnUpdate').click(function () {
        debugger;
        EditTerm();
        //if (currentYear == 0)
        //    AddTerm();
        //else
        //    EditTerm();
        $('#editTerm').modal('hide');
    })

    $('#ddlSchool').on('change', function () {
        LoadAcademicTerm();
    });



});

var yearsList = [];
var currentYear = 0;

function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadAcademicTerm() {
    AjaxCall("AcademicYears.aspx/GetAllAcademicYearsList", '{SchoolID: "' + $('#ddlSchool').val() + '"  }', "POST", AcademicYearsListSuccess, FailureCallBack);
}

function EditTermShow(ID) {
    currentYear = ID;
    document.getElementById('add').style.display = "none";
    document.getElementById('update').style.display = "block";
        $.each(yearsList, function (ind, val) {
            if (val.ID == ID) {
                $('#txtYearName').val(val.Name);
                $("#dtStartDate").val(moment(val.Start_Date).format('MM/DD/YYYY'));
                $("#dtEndDate").val(moment(val.End_Date).format('MM/DD/YYYY'));
                $('#chkIsCurrentTerm').prop("checked", val.IsCurrentTerm);
                $('#ddlSchool1').val(val.SchoolId);
            }
        });

   
    $('#editTerm').modal('show');
}
function EditTerm(ID) {
    debugger;
    var isCurrent = false;
    if ($('#chkIsCurrentTerm').prop("checked"))
        $.each(yearsList, function (ind, val) {
            if (val.IsCurrentTerm) {
                isCurrent = true;
            }

        });

    //if (!isCurrent) {
        var StartDate = ""
        var obj = new Object();
        obj.ID = currentYear;
        obj.Name = $('#txtYearName').val();
        obj.StartDate = moment($("#dtStartDate").val()).format('YYYY-MM-DD'); //FormatSQLDate($("#dtStartDate").val());// $('#dtStartDate').val().toString('yyyy-MM-dd');
        obj.EndDate = moment($("#dtEndDate").val()).format('YYYY-MM-DD'); //FormatSQLDate($("#dtEndDate").val());// $('#dtEndDate').val().toString('yyyy-MM-dd');
        obj.IsCurrentTerm = $('#chkIsCurrentTerm').prop("checked");
        obj.SchoolID = $('#ddlSchool1').val();
      
        AjaxCall("AcademicYears.aspx/ManageAcademicYear", JSON.stringify(obj), "POST", SuccessFunction, FailureCallBack);
    //}
    //else {
    //    confirm('Cannot Set this as Current Term. Please uncheck Current Term Checkbox to proceed!');
    //}
   
}
function DeleteTerm(ID) {
    var isCurrent = false;
    $.each(yearsList, function (ind, elem) {
        if (elem.ID == ID && elem.IsCurrentTerm) {
            isCurrent = true;
                return;
            }
    });
    if (isCurrent)
        confirm('Cannot Delete a Current Term.');
    else {
        if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
            return;
        }
        AjaxCall("AcademicYears.aspx/DeleteAcademicYear", '{ID: "' + ID + '"  }', "POST", SuccessFunction, FailureCallBack);
    }
}

function AddTerm() {
    if ($('#chkIsCurrentTerm').prop("checked"))
        //$.each(yearsList, function (ind, elem) {
        //    if (elem.IsCurrentTerm) {
        //        confirm('Current Term already exists. Please uncheck the current term checkbox!');
        //        return;
        //    }
        //});
    var StartDate = ""
    var obj = new Object();
    obj.ID = 0;
    obj.Name = $('#txtYearName').val();
    obj.StartDate = StartDate
    obj.StartDate = FormatSQLDate($("#dtStartDate").val());// $('#dtStartDate').val().toString('yyyy-MM-dd');
    obj.EndDate = FormatSQLDate($("#dtEndDate").val());// $('#dtEndDate').val().toString('yyyy-MM-dd');
    obj.IsCurrentTerm = $('#chkIsCurrentTerm').prop("checked");
    obj.SchoolID = $('#ddlSchool1').val();

    var a = obj.StartDate;
    var aa = a.split("-");
    obj.StartDate = aa[0] + '-' + aa[2] + '-' + aa[1];
    var b = obj.EndDate;
    var bb = b.split("-");
    obj.EndDate = bb[0] + '-' + bb[2] + '-' + bb[1]; 


    AjaxCall("AcademicYears.aspx/ManageAcademicYear", JSON.stringify(obj), "POST", SuccessFunction, FailureCallBack);
}


function SuccessFunction(data) {
    if (data.d.Message == 'Success') {
        LoadAcademicTerm();
    }
    else {
        alert(data.d.Message);
    }

}

function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
    });
    $('#ddlSchool').html(appenddata);
    $('#ddlSchool1').html(appenddata);   
    LoadAcademicTerm();
}

function AcademicYearsListSuccess(data) {
    var SerialNo = 0;
    $('#dataTable tbody').empty();
    var oTable = $('#dataTable').dataTable();
    oTable.fnClearTable();
    oTable.fnDestroy();
    yearsList = data.d;
    $.each(data.d, function (i, item) {
        if (item.usertype == 4) {
            document.getElementById("schoolsearch").style.display = "block";

            $('<tr id="tr' + item.ID + '" >').html(
                "<td> " + (++SerialNo) + "</td >" +
                "<td><span>" + item.Name + "</span></td >" +
                "<td><span style='display:none'>" + moment(item.Start_Date).format('MM/DD/YYYY') + "</span>" + moment(item.Start_Date).format('MM/DD/YYYY') + "</td >" +
                "<td><span style='display:none'>" + moment(item.End_Date).format('MM/DD/YYYY') + "</span>" + moment(item.End_Date).format('MM/DD/YYYY') + "</td >" +
                "<td> <label class='switch'><input type = 'checkbox' " + (item.IsCurrentTerm == true ? 'checked disabled >' : ' disabled >') +
                "<span class='slider round'></span></label ></td >" +
                "<td><a href='javascript:;' onclick='return EditTermShow(" + item.ID + ");'><i class= 'fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                " <a href='javascript:;'  onclick='return DeleteTerm(" + item.ID + ");' data-toggle='modal'>	<i class= 'fa fa-trash site-color mr-10' aria-hidden='true'></i></a>" +
                "</td > ").appendTo('#dataTable tbody');
        }
        else {
            document.getElementById("btnOpenAdd").style.display = "none";
            //document.getElementById("statusid").style.display = "none";
            document.getElementById("actionid").style.display = "none";
            document.getElementById("schoolsearch").style.display = "none";

            $('<tr id="tr' + item.ID + '" >').html(
                "<td> " + (++SerialNo) + "</td >" +
                "<td><span>" + item.Name + "</span></td >" +
                "<td><span style='display:none'>" + moment(item.Start_Date).format('MM/DD/YYYY') + "</span>" + moment(item.Start_Date).format('MM/DD/YYYY') + "</td >" +
                "<td><span style='display:none'>" + moment(item.End_Date).format('MM/DD/YYYY') + "</span>" + moment(item.End_Date).format('MM/DD/YYYY') + "</td >" +
                "<td > <label class='switch'><input type = 'checkbox' " + (item.IsCurrentTerm == true ? 'checked disabled >' : ' disabled >') +
                "<span class='slider round'></span></label >" +
                "<td style='display:none'><a href='javascript:;' onclick='return EditTermShow(" + item.ID + ");'><i class= 'fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                "</td > ").appendTo('#dataTable tbody');
        }

    });
    InitializeDataTable('dataTable');

}



function FailureCallBack() {

}