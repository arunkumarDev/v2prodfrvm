﻿$(window).load(function () {   
    LoadSchool();
  //  LoadClassRoom();       
  //  InitEvents();
   
});

$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
            return false;
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
            return false;
        }
        else {
            IsInputValid(this, true);
        }
    });

    InitEvents();
});

function InitEvents() {
    $("#ddlSchoolSearch").change(function () {
        var selectedValue = parseInt($(this).val());
        if (selectedValue > 0) {
            GetAllHoliday(selectedValue);
        }
    });
}

function ValidateForm() {
    var FieldValue = $.trim($('#inputReason').val());
    if (IsNullOrEmpty(FieldValue)) {
        IsInputValid('#inputReason', false);
        return false;
    }
    else {
        $('#inputReason').parent().removeClass("invalid");
    }
    return true;
}


$("#btnSaveHoliday").bind("click", function () {

    if (!ValidateForm()) {
        return;
    }

    var inputDate = FormatSQLDate($("#datepicker").val()) ; //$("#datepicker").val().toString('d-M-yyyy');
    var schoolid = $("#ddlSchoolSearch").val();
  //  var schoolid2 = $("#ddlSchool").val();
    $.ajax({
        url: "AcademicManagement.aspx/ManageHoliday",
        data: '{ID: "' + $("#hdnHolidayID").val() + '",Reason: "' + $("#inputReason").val() + '",SchoolID: "' + $("#ddlSchoolSearch").val() + '",HolidayDate: "' + inputDate + '" }',
        contentType: "application/json; charset=utf-8",
        type: 'POST',
        dataType: "json",
        success: function (data) {
            ClearControl();
            var selectedValue = parseInt($('#ddlSchoolSearch').val());
            if (selectedValue > 0) {
                GetAllHoliday(selectedValue);
            }
            //window.location.href = "AcademicManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
    //$.ajax({
    //    type: "POST",
    //    url: "ChildrenManagement.aspx/ManageChild",
    //    data: '{ID: "' + $("#hdnChildID").val() + '",Name: "' + $("#txtChildName").val() + '",Surname: "' + $("#txtSurName").val() + '",ParentName: "' + $("#txtParentName").val() + '",CommunityWorkerID: "' + $("#ddlCommunityWorker").val() + '",Email:"' + $("#txtEmail").val() + '",Image: "' + $("#txtEmail").val() + '", SchoolID: "' + $("#ddlSchool").val() + '",ClassroomID: "' + $("#ddlClassRoom").val() + '",Address: "' + $("#txtAddress").val() + '",Phone: "' + $("#txtPhone").val() + '",Note: "' + $("#txtNote").val() + '" }',
    //    contentType: "application/json; charset=utf-8",
    //    dataType: "json",
    //    success: function (data) {
    //        ClearControl();
    //    },
    //    failure: function (response) {
    //        alert(response.d);
    //    }
    //});
});


function LoadSchool() {

    $.ajax({
        type: "POST",
        url: "SchoolManagement.aspx/GetMySchools",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata;
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

            });
        
            $('#ddlSchoolSearch').html(appenddata);
            GetAllHoliday(parseInt($('#ddlSchoolSearch').val()));

        },
        failure: function (response) {
            alert(response.d);
        }
    });

}




function GetAllHoliday(SchoolID) {
    HolidayList = [];
    $('#dataTable').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
    $('#dataTable tbody').empty();

    $.ajax({
        type: "POST",
        url: "AcademicManagement.aspx/GetAllHoliday",
        data: '{SchoolID: "' + SchoolID + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
           // console.log(data.d)

            $('#total').text(data.d.length);
           
            $.each(data.d, function (i, item) {
              
                HolidayList[item.ID] = item;
                var date = item.HolidayDate;
             //   var nowDate = new Date(parseInt(date.substr(6))).toISOString();

           //     date = new Date(nowDate);
               // var HolidayDate = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() ;
               
                $('<tr id="tr' + item.ID + '">').html(
                    "<td>" + (++slNo) + "</td>" +
                    "<td>" +  FormatDate(item.HolidayDate) + "</td>" +
                    "<td>" + item.Reason + "</td>" +
                    "<td><a onclick='return DeleteHoliday(" + item.ID + ");' class='fa fa-trash site-color mr-10' aria-hidden='true'></a></td>").appendTo('#dataTable tbody');


            });
            $('#dataTableUser').DataTable({
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                //to select and search from grid  
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });


            //  });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function DeleteHoliday(ID) {
    if (!confirm("Are you sure want to delete?")) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "AcademicManagement.aspx/DeleteHolidayByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadClasses();
            window.location.href = "AcademicManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}



function ClearControl() {
    $("#inputReason").val('');
}

