﻿$(window).load(function () {
    LoadSchools();
    LoadAdminUsers();
    LoadNotifications();
});

var schoolList = [];


$("#btnSaveSchool").bind("click", function () {
    if (!ValidateSchool()) {
        return;
    }


    var formData = new FormData();
    var file = $('#txtImage')[0].files[0];
    formData.append('fileData', $('#txtImage')[0].files[0]);

    formData.append('hdnImage', $("#hdnImage").val());
    formData.append('ID', $("#hdnSchoolID").val());
    formData.append('SchoolName', $("#txtSchoolName").val());
    formData.append('Email', $("#txtEmail").val());
    formData.append('Image', $("#txtImage").val());
    formData.append('City', $('#txtCity').val());
    formData.append('Address', $("#txtAddress").val());
    formData.append('Country', $("#txtCountry").val());
    formData.append('Phone1', $("#txtPhone1").val());
    formData.append('Phone2', $("#txtPhone2").val());
    formData.append('Note', $("#txtNote").val());
    formData.append('GeoLocation', $("#txtSearchResult").val());
    formData.append('AdminId', $("#ddlAdmin").val());
    $.ajax({
        url: "SchoolManagement.ashx",
        data: formData,
        enctype: 'multipart/form-data',
        processData: false,
        contentType: false,
        type: 'POST',
        success: function (data) {
            //LoadChildren();
            //ClearControl();
            window.location.href = "SchoolManagement.aspx";
        }
    });

   
});

function ValidateSchool() {

    if ($.trim($("#txtSchoolName").val()) === "") {
        IsInputValid("#txtSchoolName", false);
        return false;
    }

    var emailId = $.trim($("#txtEmail").val());
    if (emailId === "") {
        IsInputValid("#txtEmail", false);
        return false;
    }
    if (emailId !== "" && !validEmail(emailId)) {
        IsInputValid("#txtEmail", false);
        alert("Invalid Email");
        return false;
    }

    if ($.trim($("#txtPhone1").val()) === "") {
        IsInputValid("#txtPhone1", false);
        return false;
    }


    var PhoneNo = $.trim($("#txtPhone1").val());
    if (!validPhoneNumber(PhoneNo)) {
        IsInputValid("#txtPhone1", false);
        return false;
    }

    PhoneNo = $.trim($("#txtPhone2").val());
    if (PhoneNo != '' && !validPhoneNumber(PhoneNo)) {
        IsInputValid("#txtPhone2", false);
        return false;
    }


    if ($.trim($("#txtCity").val()) === "") {
        IsInputValid("#txtCity", false);
        return false;
    }
    if ($.trim($("#txtCountry").val()) === "") {
        IsInputValid("#txtCountry", false);
        return false;
    }
    if ($.trim($("#txtAddress").val()) === "") {
        IsInputValid("#txtAddress", false);
        return false;
    }

    return true;
}

$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });
});

function LoadSchools() {
    schoolList = [];
    $('#dataTable').dataTable({
        "bDestroy": true
    }).fnDestroy();
    $('#dataTable tbody').empty();
    $.ajax({
        type: "POST",
       // url: "SchoolManagement.aspx/GetAllSchools",
        url: "SchoolManagement.aspx/GetMySchools",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $.each(data.d, function (i, item) {
                schoolList[item.ID] = item;
                $('<tr id="tr' + item.ID + '">').html("<td>" + (++slNo) + "</td>" +
                    "<td> " + item.SchoolName + "</td >" +                   
                    "<td>" + item.Phone1 + '  ' + item.Phone2 +"</td>" +                  
                    "<td>" + item.Address + "</td>" +                 
                    "<td><a href='javascript:;' data-toggle='modal' onclick='return OpenModalPopup(" + item.ID + ");'><i class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
                    "<a href='javascript:;' data-toggle='modal' onclick='return DeleteSchool(" + item.ID + ");'><i class='fa fa-trash site-color mr-10' aria-hidden='true'></i></a>" +
                    "</td > ").appendTo('#dataTable tbody');

            });
            InitializeDataTable('dataTable');
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function ClearControl() {
    $("#txtSchoolName").val('');
    $("#txtEmail").val(''); $("#txtImage").val('');
    $("#txtCity").val(''); $("#txtAddress").val(''); $("#txtCountry").val('');
    $("#txtPhone1").val(''); $("#Phone2").val(''); $("#txtNote").val('');
    $("#txtSearchResult").val('');
    $("#hdnSchoolID").val('0');
    $("#imgPhoto").attr('src', "images/user.png");
}
function OpenModalPopup(ctrl) {    

    var schoolData = schoolList[ctrl];
    if (schoolData != undefined) {
        $('#txtHeader').text(Lbl_EditSchool);

        $("#hdnSchoolID").val(schoolData.ID);
        $("#txtSchoolName").val($.trim(schoolData.SchoolName));
        $("#txtEmail").val($.trim(schoolData.Email));
       // $("#txtImage").val($('#tr' + ctrl + ' td').eq(3).text().trim());
        $("#txtCity").val($.trim(schoolData.City));
        $("#txtAddress").val($.trim(schoolData.Address));
        $("#txtCountry").val($.trim(schoolData.Country));
        $("#txtPhone1").val($.trim(schoolData.Phone1));
        $("#txtPhone2").val($.trim(schoolData.Phone2));
        $("#txtNote").val($.trim(schoolData.Note));
        $("#txtSearchResult").val($.trim(schoolData.GeoLocation));

        if ($.trim(schoolData.Image) != "")
            $("#imgPhoto").attr("src", $.trim("Photos/" + schoolData.Image));
        else
            $("#imgPhoto").attr("src", $.trim("images/user.png"));

        $("#hdnImage").val($.trim(schoolData.Image));
        $('#txtHeader').text(Lbl_EditSchool);

        $('#SchoolManagementModel').modal('show');
        setLocation($("#txtSearchResult").val());

        GetAdminMapping($("#hdnSchoolID").val());
    }
}
function DeleteSchool(ID) {
    if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "SchoolManagement.aspx/DeleteSchoolByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadSchools();
            window.location.href = "SchoolManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

$("#txtImage").change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //alert(e.target.result);
            $('#imgPhoto').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function LoadAdminUsers() {
    var usertype = 1;
        $.ajax({
            type: "POST",
            url: "SchoolManagement.aspx/GetUserByType",
            data: '{usertype: "' + usertype + '"}',
            //url: AppUrls.WalikuWebApiUri + "api/Common/GetUserByType/1",        
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {              
                var appenddata;
                debugger;
                $.each(data.d, function (i, item) {
                    appenddata += "<option value = '" + item.Id + "'>" + item.Name + " </option>";

                });

                $('#ddlAdmin').html(appenddata);
            },
            failure: function (response) {
                alert(response.d);
            }
        });
}


function GetAdminMapping(SchoolId) {
    $.ajax({
        type: "POST",
        url: "SchoolManagement.aspx/GetSchoolAdminDetails",
        data: '{SchoolId: "' + SchoolId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata;
            $.each(data.d, function (i, item) {
                if (data.d.length > 0) {
                    $('#ddlAdmin').val(data.d[0].UserId);
                  
                }

            });
        }
    });
}

$("#btnAddSchool").bind("click", function () {
    $('#txtHeader').text(Lbl_AddSchool);
    AddSchool();
});

function AddSchool() {
    ClearControl();
    $('#SchoolManagementModel').modal('show');
    setDefaultMapLocation();
    
}
