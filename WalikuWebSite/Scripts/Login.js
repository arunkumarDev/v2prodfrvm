﻿var language = {
    "en-us": {
        "Lbl_SelectLanguage": "Select Language", "Lbl_UerName": "UserName", "Lbl_Password": "Password", "Lbl_RememberMe": "Remember Me","Lbl_Login":"Login"},
    "id-ID": {
        "Lbl_SelectLanguage": "Pilih bahasa", "Lbl_UerName": "Nama pengguna", "Lbl_Password": "Kata sandi", "Lbl_RememberMe": "Ingat saya", "Lbl_Login": "Masuk"
    }
};



function ValidateForm() {
    var FieldValue = $.trim($('#txtUserName').val());
    if (IsNullOrEmpty(FieldValue)) {
        IsInputValid('#txtUserName', false);
        return false;
    }
    else {
        $('#txtUserName').parent().removeClass("invalid");
    }

    FieldValue = $.trim($('#txtPassword').val());
    if (IsNullOrEmpty(FieldValue)) {
        IsInputValid('#txtPassword', false);
        return false;
    }
    else {
        $('#txtPassword').parent().removeClass("invalid");
    }

    return true;
}



$(document).ready(function () {
    //localStorage.removeItem('NotifyText');
    //localStorage.removeItem('NotifyDate');
    //localStorage.removeItem('notificationCount');

    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    })

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val()))  {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    })

    var lang;
    $("#ddlLanguage").change(function () {
        var selectedValue = $(this).val();
        lang = language[selectedValue];
        ChangeLangulage(lang);
    });
})

function ChangeLangulage(lang) {
    $('#Lbl_SelectLanguage').text(lang.Lbl_SelectLanguage);
    $('#Lbl_UerName').text(lang.Lbl_UerName);
    $('#Lbl_Password').text(lang.Lbl_Password);
    $('#Lbl_RememberMe').text(lang.Lbl_RememberMe);
    $('#btnSignIn').text(lang.Lbl_Login);
}

$("#btnSignIn").bind("click", function () {
    if (!ValidateForm()) {
        return;
    }
    submitLogin();
});

$('.input-validate').keypress(function (e) {
    if (e.which == 13) {
        if (!ValidateForm()) {
            return;
        }
        submitLogin();
        e.preventDefault();
        e.stopPropagation();
        return false;    
    }
});


function submitLogin() {
    localStorage.setItem('loglang', $("#ddlLanguage").val());
    $.ajax({
        type: "POST",

        url: "/Login.aspx/ValidateUser",
        data: '{UserName: "' + $("#txtUserName").val() + '" , Password: "' + $("#txtPassword").val() + '", Language: "' + $("#ddlLanguage").val() + '" , RememberMe: ' + $("#chkRemember").prop('checked') + '}',

        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.d) {
               
                GetuserType();
               // window.location = "Report-schoolsummary";
            }
            else {
                alert("Invalid UserName/password");
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function GetuserType() {
    $.ajax({
        type: "POST",
        url: "/Login.aspx/GetuserType",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            console.log(data);
            if (data.d) {
                if (data.d != 2)
                  window.location = "Report-schoolsummary";
            }
            else {
                alert("Invalid UserName/password");
            }
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}