﻿$(document).ready(function () {
    InitEvents();
    LoadSchooAdmin();
    LoadSchool();
    LoadAdmin();
    LoadNotifications();
});

function InitEvents() {
    $('#btnAssignAdmin').click(function () {
        $('#txtHeader').text(Title_AssignAdmin);
        $('#Assign-admin').modal('show');
    });
   
}

$("#btnSaveAdmin").bind("click", function () {
    if (!ValidateAdmin()) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "AssignAdmin.aspx/ManageAdmin",
        data: '{ID: "' + $("#hdnAdminID").val() + '",SchoolId: "' + $("#ddlSchool").val() + '",UserId: "' + $("#ddlAdmin").val() +'" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadSchools();
            ////ClearControl();
            window.location.href = "AssignAdmin.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
});

function ValidateAdmin() {

    if ($.trim($("#ddlSchool").val()) === "") {
        IsInputValid("#ddlSchool", false);
        return false;
    }

    if ($.trim($("#ddlAdmin").val()) === "") {
        IsInputValid("#ddlAdmin", false);
        return false;
    }

    return true;
}

$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });
});


function LoadSchooAdmin() {
    SchooAdminList = [];
    $('#dataTableUser').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();
    $('#dataTableUser tbody').empty();
    $.ajax({
        type: "POST",
        url: "AssignAdmin.aspx/GetAllSchoolAdmin",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var slNo = 0;
            $('#total').text(data.d.length);
            $.each(data.d, function (i, item) {
                SchooAdminList[item.ID] = item;
                $('<tr id="tr' + item.ID + '" geoLoc=' + item.GeoLocation + '>').html(

                    "<td>" + (++slNo) + "</td>" +
                    "<td>" + item.SchoolName + "</td>" +
                    "<td>" + item.UserName + "</td>" +
                    "<td><a onclick='return OpenModalPopup(" + item.ID + ");' data-toggle='modal' class='fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></a><a onclick='return DeleteSchoolAdmin(" + item.ID + ");' class='fa fa-trash site-color mr-10' aria-hidden='true'></a></td>").appendTo('#dataTableUser tbody');


            });

            InitializeDataTable('dataTableUser');

            //  });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function OpenModalPopup(ctrl) {
    var SchooAdminData = SchooAdminList[ctrl];
    //$("#hdnUserID").val($('#tr' + ctrl + ' td').eq(0).text());
    //$("#txtEmail").val($('#tr' + ctrl + ' td').eq(1).text().trim());
    //$("#txtName").val($('#tr' + ctrl + ' td').eq(2).text().trim());
    //$("#txtSurname").val($('#tr' + ctrl + ' td').eq(3).text().trim());
    //$("#imgUser").attr("src", $('#tr' + ctrl + ' td').eq(4).attr("data-userImage"));
    //$("#hdnImage").val($('#tr' + ctrl + ' td').eq(4).attr("data-image"));
    //$("#ddlUserType").val($('#tr' + ctrl + ' td').eq(5).attr("data-userTypeID"));
    //$("#txtAddress").val($('#tr' + ctrl + ' td').eq(6).text().trim());
    //$("#txtPhone").val($('#tr' + ctrl + ' td').eq(7).text().trim());
    //$("#txtNote").val($('#tr' + ctrl + ' td').eq(8).text().trim());
    ////alert($("#hdnImage").val());
    //$("#txtGeoLocation").val($('#tr' + ctrl + ' td').eq(12).attr("data-geolocation"));
    if (SchooAdminData != undefined) {
        $("#hdnAdminID").val(SchooAdminData.ID);
        $("#ddlSchool").val(SchooAdminData.SchoolId);
        $("#ddlAdmin").val(SchooAdminData.UserId);
        $('#txtHeader').text(Title_EditAdmin);

        $('#Assign-admin').modal('show');
    }


}


function ClearControl() {
    $("#hdnAdminID").val(0);
    $("#ddlSchool").val(1);
    $("#ddlAdmin").val('');
    // $("#txtDescriptionIndian").val('');
}

function DeleteSchoolAdmin(ID) {
    if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "AssignAdmin.aspx/DeleteSchoolAdminByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadClasses();
            window.location.href = "AssignAdmin.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}
function LoadSchool() {

    $.ajax({
        type: "POST",
        url: "AssignAdmin.aspx/GetSchoolListForUser",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--Select School--</option>";
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

            });
            $('#ddlSchool').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

function LoadAdmin() {

    $.ajax({
        type: "POST",
        url: "AssignAdmin.aspx/GetAllUser",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=''>--Select Admin--</option>";
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";

            });
            $('#ddlAdmin').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}

