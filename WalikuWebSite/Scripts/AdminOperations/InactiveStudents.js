﻿$(document).ready(function () {
    LoadStudentsList();
});

function LoadStudentsList() {
    $.ajax({
        type: "POST",
        url: "/InactiveStudents.aspx/LoadInactiveStudents",
        //  data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var rowsList = '';
            var SerialNo = 0;
            var result = data.d;
            $.each(result, function (ind, item) {
                var genderVal = item.Gender == null ? '' : item.Gender;
                rowsList += '<tr id="tr' + item.ID + '" >';
                var columnsList = "<td></td><td>" +
                    "<div class=\"text-center\">" +
                    " <figure class=\"list-pic\">";
                
                    //if (item.ImageBase != '') {
                    //var blobData = b64toBlob(item.ImageBase, 'image/gif');
                    //$('figure').append("image", blobData);
                columnsList += "<img src='/WalikuWebsite/Photos/" + item.ImageBase+"' alt='user image' class='img-responsive img-border'>";
                //}
                columnsList += "</figure>";
                columnsList += " </div></td>" +
                    "<td> " + item.Name + ' ' + item.Middlename + ' ' + item.Surname + "</td >" +
                    "<td>" + genderVal + "</td>" +
                    "<td>" + item.SchoolName + "</td>" +
                    "<td>" + item.ClassroomName + "</td>" +
                    "<td><a href='javascript:;'  onclick='return DeleteChild(" + item.ID + ");' data-toggle='modal'><i class= 'fa fa-trash site-color mr-10' aria-hidden='true'></i></a> " +
                    "</td > ";
                rowsList += columnsList + '</tr>'



            });
            $('#inactiveStudentsDataTable tbody').append(rowsList);
            InitializeDataTable('inactiveStudentsDataTable');
        },
        failure: function (resp) { }
    });
}

function DeleteChild(childId) {
    $.ajax({
        type: "POST",
        url: "/InactiveStudents.aspx/DeleteChildren",
        data: '{ID: "' + childId + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            $('#inactiveStudentsDataTable tbody').find('#tr' + childId).remove();
        },
        failure: function (resp) { }
    });
}