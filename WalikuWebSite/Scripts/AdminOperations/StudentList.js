﻿$(document).ready(function () {
    
    LoadSchool();
    InitEvents();
    
});

function LoadChildrenList() {
    var SerialNo = 0;
    
    $('#dataTable tbody').empty();
    var rowList = '';
    $.each(studentsList, function (i, item) {
        //  console.log(item.GeoLocation )
        studentsList[item.ID] = item;
        var UserImage = $.trim(item.Image) !== "" ? ('/Photos/' + item.Image) : 'images/user.png';
        $('#total').text(studentsList.length);
        $('<tr id="tr' + item.ID + '">').html("<td>" +
            "<div class=\"text-center\">" +
            " <figure class=\"list-pic\">" +
            "<img src=" + UserImage + " alt='user image' class='img-responsive img-border'>" +
            "</figure>" +
            " </div></td>" +

            "<td> " + (++SerialNo) + "</td >" +
            "<td> " + item.Name + ' ' + item.Middlename + ' ' + item.Surname + "</td >" +
            "<td>" + item.Phone + "</td>" +
            "<td data-GeoLocation =" + item.GeoLocation + ">" + item.Address + "</td>" +
            //  "<td>&nbsp;</td>" +
            "<td><a href='javascript:;' onclick='return EditChildren(" + item.ID + ");'><i class= 'fa fa-pencil-square-o site-color mr-10' aria-hidden='true'></i></a>" +
            "<a href='javascript:;'  onclick='return DeleteChild(" + item.ID + ");' data-toggle='modal'>	<i class= 'fa fa-trash site-color mr-10' aria-hidden='true'></i></a> " +
            "<a href='javascript:;' onclick='return ViewProfile(" + item.ID + ");' data-toggle='modal'>	<i class= 'fa fa-eye site-color' aria-hidden='true'></i></a> " +
            "</td > ").appendTo('#dataTable tbody');;
  
    });
    //$('#dataTableStudent tbody').append(rowList);
    InitializeDataTable('dataTable');
}
var studentsList = [];
var FormLoad = true;
var MySchoolList = [];
var MyClassList = [];


function InitEvents() {
    $("#ddlSchool").change(function () {
        LoadClassRoom(parseInt($(this.val())));
    });
    $("#ddlSchoolSearch").change(function () {
        LoadClassRoom(parseInt($(this).val()));
    });

    $("#btnSearchChildren").bind("click", function () {
        var table = $('#dataTable').DataTable();
        table.destroy();// table.clear().draw();
        LoadChildren($("#ddlSchoolSearch").val(), $("#ddlClassSearch").val());   // GetChildrenData(2,0);       
    });

    LoadGrade();
    LoadReasonForDelete();

    $("#btnAddStudent").bind("click", function () {
        ClearControl();
        AddChildren();

    });

    $("#ddlClassRoom").change(function () {
        GetTeacherMapping(parseInt($(this).val()));
    });


}


function ValidateUser() {
    var Studentname = $.trim($("#txtChildName").val());

    if (Studentname === '') {
        // $('#txtChildName').parent().addClass("invalid");  
        IsInputValid("#txtChildName", false);
        return false;
    }
    if ($.trim($("#txtParentName").val()) === "") {
        IsInputValid("#txtParentName", false);
        return false;
    }
    var emailId = $.trim($("#txtEmail").val());

    if (emailId !== "" && !validEmail(emailId)) {
        IsInputValid("#txtEmail", false);
        alert("Invalid Email");
        return false;
    }
    if ($.trim($("#ddlSchool").val()) === "") {
        IsInputValid("#ddlSchool", false);
        return false;
    }


    if ($.trim($("#ddlGender").val()) === "") {
        IsInputValid("#ddlGender", false);
        return false;
    }
    var PhoneNo = $.trim($("#txtPhone").val());

    if (PhoneNo != '' && !validPhoneNumber(PhoneNo)) {
        IsInputValid("#txtPhone", false);
        return false;
    }

    if ($.trim($("#txtAddress").val()) === "") {
        IsInputValid("#txtAddress", false);
        return false;
    }
    if ($.trim($("#ddlGrade").val()) !== "1") {
        if ($.trim($("#txtNisn").val()) === "") {
            IsInputValid("#txtNisn", false);
            return false;
        }
    }
    return true;
}

$("#ddlGrade").change(function () {
    if ($.trim($("#ddlGrade").val()) === "1") {
        if ($.trim($("#txtNisn").val()) === "") {
            IsInputValid("#txtNisn", true);
            return false;
        }
    }
    else {
        if ($.trim($("#txtNisn").val()) === "") {
            IsInputValid("#txtNisn", false);
            return false;
        }
    }
});


$(document).ready(function () {
    $(".input-validate").focusout(function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });

    $('.select-validate').on('change', function () {
        if (IsNullOrEmpty($(this).val())) {
            IsInputValid(this, false);
        }
        else {
            IsInputValid(this, true);
        }
    });
});


$("#btnSaveChild").bind("click", function () {
    if (!ValidateUser()) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "Views/StudentList.aspx/ManageChild",
        //  data: '{PageIndex: "' + 1 + '"  }',
        data:
            '{WalikuID:' + $("#hdnChildID").val()+ ',' +
            'Name:"' + $("#txtChildName").val() + '",' +
            'Surname:"' + $("#txtSurName").val() + '",' +
            'ParentName:"' + $("#txtParentName").val() + '",' +
            'CommunityWorkerID:' + $("#ddlCommunityWorker").val() + ',' +
            'Email:"' + $("#txtEmail").val() + '",' +
            'SchoolID:' + $("#ddlSchool").val() + ',' +
            'ClassroomID:' + $("#ddlClassRoom").val() + ',' +
            'Image:' + '""'+ ',' +
            'Phone:"' + $("#txtPhone").val() + '",' +
            'Note:"' + $("#txtNote").val() + '",' +
            'GradeID:' + $("#ddlGrade").val() + ',' +
            'DeleteReasonID:' + $("#ddlDeleteReason").val() + ',' +
            'Gender:"' + $("#ddlGender").val() + '",' +
            'Nisn:"' + $("#txtNisn").val() + '",' +
            'Address:"' + $("#txtAddress").val() + '",' +
            'Note_Indonesian:""}' ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            $('#ChildrenManagementModel').modal('hide');
            GetChildrenData(parseInt($("#ddlSchool").val()), parseInt($("#ddlClassSearch").val())); 
        },
        failure: function (response) { }
    });
   
});
$("#txtImage").change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            //alert(e.target.result);
            $('#imgPhoto').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function LoadChildren(SchoolId, ClassRoomID) {
    $('#dataTable').dataTable({
        "bDestroy": true,
        scrollCollapse: true,
        scroller: true,
        deferRender: true,
    }).fnDestroy();

    SchoolId = IsNullOrEmpty(SchoolId) ? 0 : SchoolId;
    ClassRoomID = IsNullOrEmpty(ClassRoomID) ? 0 : ClassRoomID;

    GetChildrenData(SchoolId, ClassRoomID);


    InitializeDataTable('dataTable');
}

function GetChildrenData(SchoolId, ClassRoomID) {
    $('#dataTable tbody').empty();
    studentsList = [];
    $.ajax({
        type: "POST",
        url: "Views/StudentList.aspx/GetAllChildrenForMe",
        //  data: '{PageIndex: "' + 1 + '"  }',
        data: '{SchoolID: "' + SchoolId + '" ,ClassRoomID: "' + ClassRoomID+ '",PageIndex: "1"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            studentsList = data.d;
            LoadChildrenList();
        },

        failure: function (response) {

            alert(response.d);
        }
    });
}


function OpenChildrenImageModel(childID) {
    $('#ChildrenImageModel').modal('show');
    $.ajax({
        type: "POST",
        url: "ChildrenManagement.aspx/GetChildImageByID",
        data: '{ChildID: "' + childID + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            $("#srcImage").attr('src', data.d);
        },
        failure: function (response) {

            alert(response.d);
        }
    });
}
//function LoadCommunityWorker() {

//    $.ajax({
//        type: "POST",
//        url: "ChildrenManagement.aspx/GetAllCommunityWorker",
//        data: '{PageIndex: "' + 1 + '"  }',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (data) {
//            var appenddata;
//            $.each(data.d, function (i, item) {
//                appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";

//            });
//            $('#ddlCommunityWorker').html(appenddata);
//        },
//        failure: function (response) {
//            alert(response.d);
//        }
//    });

//}


function LoadSchoolSuccess(data) {
    var appenddata;
    MySchoolList = data.d;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

    });
    $('#ddlSchoolSearch').html(appenddata);
    $('#ddlSchoolSearch').val(updatedFormValues['SchoolID']);
    LoadClassRoom(parseInt($('#ddlSchoolSearch').val()));

}

function LoadGradeSuccess(data) {
    $('#ddlGrade').find('option').remove().end();
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";
    });
    $('#ddlGrade').html(appenddata);

}

function LoadReasonForDeleteSuccess(data) {
    $('#ddlDeleteReason').find('option').remove().end();
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.Reason + " </option>";
    });
    $('#ddlDeleteReason').html(appenddata);

}

function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST", LoadSchoolSuccess, FailureCallBack);
}

function LoadClassRoom(SchoolId) {
    AjaxCall("ChildrenManagement.aspx/GetAllClassRoomForMe", '{SchoolID: "' + SchoolId + '"  }', "POST", LoadClassRoomSuccess, FailureCallBack);
}

function GetTeacherMapping(ClassRoomID) {
    AjaxCall("ChildrenManagement.aspx/GetTeacherClassMapping", '{ClassRoomID: "' + ClassRoomID + '"  }', "POST", TeacherMappingSuccess, FailureCallBack);
}

function LoadGrade() {
    AjaxCall("ChildrenManagement.aspx/GetAllGrade", '{PageIndex: "' + 1 + '"  }', "POST", LoadGradeSuccess, FailureCallBack);
}

function LoadReasonForDelete() {
    AjaxCall("ChildrenManagement.aspx/GetAllReasonForDelete", '{PageIndex: "' + 1 + '"  }', "POST", LoadReasonForDeleteSuccess, FailureCallBack);
}

function LoadClassRoomSuccess(data) {
    $('#ddlClassSearch')
        .find('option')
        .remove()
        .end();

    MyClassList = data.d;
    var appenddata;
    $.each(data.d, function (i, item) {
        appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";

    });
    $("#ddlClassSearch").val(updatedFormValues['ClassroomID']);
    $('#ddlClassSearch').html(appenddata);
    if (FormLoad) {
        FormLoad = false;
        setTimeout(function () {
            LoadChildren($("#ddlSchoolSearch").val(), $("#ddlClassSearch").val());
        }, 500)

    }
}

function ClearControl() {
    $("#hdnChildID").val('0');
    $("#txtChildName").val('');
    $("#txtChildMiddlename").val('');
    $("#txtSurName").val('');
    $("#txtParentName").val('');
    $("#ddlCommunityWorker").val(0);
    $("#txtEmail").val('');
    $("#txtImage").val('');
    $("#ddlSchool").val(0);
    $("#ddlClassRoom").val(0);
    $("#txtPhone").val('');
    $("#txtAddress").val('');
    $("#txtNote").val('');
    $("#imgPhoto").attr('src', "images/user.png");
    $("#txtNisn").val('');
    $("#ddlGrade").val(0);
    $("#ddlDeleteReason").val(0);

}


function TeacherMappingSuccess(data) {
    $.each(data.d, function (i, item) {
        $('#ddlCommunityWorker').html("<option value = '" + item.ID + "'>" + item.Name + " </option>");
    });

    $("#ddlCommunityWorker").val($("#ddlCommunityWorker option:first").val());
}


function AddChildren() {
    ClearControl();
    $('#txtHeader').text(Lbl_AddStudent);
    SetSelectedSchool(parseInt($('#ddlSchoolSearch').val()));
    SetSelectedClass(parseInt($('#ddlClassSearch').val()));
    GetTeacherMapping(parseInt($('#ddlClassSearch').val()));
    $("#ddlGrade").val("1");
    $("#ddlDeleteReason").val("7");
    $('#ChildrenManagementModel').modal('show');
    setDefaultMapLocation();
}
function EditChildren(ID) {
    ClearControl();
    $('#txtHeader').text(Lbl_EditStudent);
    var studData = studentsList[ID];
    SetSelectedSchool(parseInt(studData.SchoolID));
    SetSelectedClass(parseInt(studData.ClassroomID));
    GetTeacherMapping(parseInt(studData.ClassroomID));
    OpenModalPopup(studData);
}

function SetSelectedSchool(val) {
    $('#ddlSchool').find('option').remove().end();
    var appenddata;
    $.each(MySchoolList, function (i, item) {
        if (val == item.ID) {
            $('#ddlSchool').html("<option value = '" + item.ID + "'>" + item.SchoolName + " </option>");
            return;
        }
    });
}

function SetSelectedClass(val) {
    $('#ddlClassRoom').find('option').remove().end();
    var appenddata;
    $.each(MyClassList, function (i, item) {
        //if (val == item.ID) {
        //    $('#ddlClassRoom').html("<option value = '" + item.ID + "'>" + item.ClassName + " </option>");
        //    return;
        //}
        $('#ddlClassRoom').append("<option value = '" + item.ID + "'>" + item.ClassName + " </option>");
    });
    $('#ddlClassRoom').val(val);
}

function OpenModalPopup(studData) {

    if (studData !== undefined) {

        //  $("#hdnChildID").val($('#tr' + ctrl + ' td').eq(0).text());
        $("#hdnChildID").val(studData.ID);
        $("#txtChildName").val(studData.Name);
        $("#txtChildMiddlename").val($.trim(studData.Middlename));
        $("#txtSurName").val($.trim(studData.Surname));
        $("#txtParentName").val($.trim(studData.ParentName));
        $("#ddlCommunityWorker").val(studData.CommunityWorkerID);
        $("#txtEmail").val($.trim(studData.Email));

        if ($.trim(studData.Image) !== "")
            $("#imgPhoto").attr("src", $.trim("Photos/" + studData.Image));
        else
            $("#imgPhoto").attr("src", $.trim("images/user.png"));

        $("#hdnImage").val($.trim(studData.Image));
        $("#ddlSchool").val(studData.SchoolID);

        $("#ddlClassRoom").val(studData.ClassroomID);
        $("#ddlGender").val(studData.Gender);
        $("#txtPhone").val($.trim(studData.Phone));
        $("#txtAddress").val($.trim(studData.Address));
        $("#txtNote").val($.trim(studData.Note));

        $("#txtSearchResult").val($.trim(studData.GeoLocation));

        $('#txtHeader').text(Lbl_EditStudent);

        $("#txtNote").parent().addClass('is-filled');

        $("#txtNisn").val($.trim(studData.NISN));
        studData.GradeID === 0 ? $("#ddlGrade").val("1") : $("#ddlGrade").val(studData.GradeID);
        studData.DeleteReasonID === 0 ? $("#ddlDeleteReason").val("7") : $("#ddlDeleteReason").val(studData.DeleteReasonID);

        $('#ChildrenManagementModel').modal('show');

        //setLocation($("#txtSearchResult").val());
    }
}

function DeleteChild(ID) {
    if (!confirm(GlobalResources.AlrtDeleteConfirm)) {
        return;
    }
    $.ajax({
        type: "POST",
        url: "ChildrenManagement.aspx/DeleteChildByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            ////LoadChildren();
            window.location.href = "ChildrenManagement.aspx";
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}


function checkEmptyImage(v) {
    if (("" + v).length === 0)
        return "emptyuser.jpg";
    else
        return v;
}

function FailureCallBack(data) {
    alert(data.d);
}

function ViewProfile(ID) {
    GetChildDetails(ID);
    $('#viewprofile').modal('show');
}

function GetChildDetails(ID) {
    //alert(ID)
    AjaxCall("ChildrenManagement.aspx/GetChildDetailsByID", '{ID: "' + ID + '"  }', "POST", GetChildDetailsSuccess, FailureCallBack);
}

function GetChildDetailsSuccess(data) {
    data = data.d;
    $('#viewStudName').html(data.Name + ' ' + data.Surname);
    $('#viewGender').html(data.Gender);
    $('#viewSchoolName').html(data.SchoolName);
    $('#viewClassName').html(data.ClassroomName);
    $('#viewTeacherName').html(data.TeacherName);
    $('#viewParentName').html(data.ParentName);
    $('#viewPhone').html(data.Phone);
    $('#viewAddress').html(data.Address);
    $('#viewNotes').html(data.Note);
    $('#viewNisn').html(data.NISN);
    var studData = studentsList[data.ID];
    //  $('#AttendancePercent').html(studentsList[data.ID].AttendancePercent);

    $('#viewAttendancePercent').html("<div class='progress att-percent'><div class='progress-bar red' role='progressbar' style='width: " + studData.AttendancePercent + "%;' aria-valuenow=" + studData.AttendancePercent + " aria-valuemin='0' aria-valuemax='100'>" + studData.AttendancePercent.toFixed(2) + "%</div> </div>");

    if ($.trim(data.Image) !== "")
        $("#viewProfilePhoto").attr("src", $.trim("Photos/" + data.Image));
    else
        $("#viewProfilePhoto").attr("src", $.trim("images/user.png"));

    setViewProfileMap(data.GeoLocation, 'viewprofileMap');

}



function FailureCallBack() {

}


