﻿$(document).ready(function () {
    LoadStudentForm();
    LoadSchool();

    $("#ddlGrade").change(function () {
        if ($.trim($("#ddlGrade").val()) === "1") {
            if ($.trim($("#txtNisn").val()) === "") {
                IsInputValid("#txtNisn", true);
                return false;
            }
        }
        else {
            if ($.trim($("#txtNisn").val()) === "") {
                IsInputValid("#txtNisn", false);
                return false;
            }
        }
    });
    $("#ddlSchool").change(function () {
        LoadClassRoom(parseInt(this.value));
    });
});

function LoadSchool() {
    AjaxCall("SchoolManagement.aspx/GetMySchools", '{PageIndex: "' + 1 + '"  }', "POST",
        function (data) {
            $('#ddlSchool').find('option').remove().end();
            var appenddata;
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";
            });
            $('#ddlSchool').html(appenddata);
            LoadClassRoom(data.d[0].ID);
        },
        function (response) {
            alert(resposnse);
        });
}
function LoadGrade() {
    AjaxCall("Views/StudentList.aspx/GetAllGrade", '{PageIndex: "' + 1 + '"  }', "POST", function (data) {
        $('#ddlGrade').find('option').remove().end();
        var appenddata;
        $.each(data.d, function (i, item) {
            appenddata += "<option value = '" + item.ID + "'>" + item.Name + " </option>";
        });
        $('#ddlGrade').html(appenddata);
    },
        function (response) {
            alert(resposnse);
        });
}
function LoadClassRoom(SchoolId) {
    AjaxCall("ChildrenManagement.aspx/GetAllClassRoomForMe", '{SchoolID: "' + SchoolId + '"  }', "POST", function (data) {
        $('#ddlClass').find('option').remove().end();
        var appenddata;
        $.each(data.d, function (i, item) {
            appenddata += "<option value = '" + item.ID + "'>" + item.ClassName + " </option>";
        });
        $('#ddlClass').html(appenddata);
        LoadGrade();
    },
        function (response) {
            alert(resposnse);
        });
}
function LoadStudentForm() {
    
    var fieldsList = '';
    $.each(selectedFields, function (ind, field) {
        $.each(masterConfig, function (i, config) {
            if (field == config.FieldID) {
                fieldsList += '<div class="col-md-6 bmd-form-group">' +
                    '<div class="col-md-8"' +
                    '<label class="bmd-label-floating" >' + config.FieldName + '</label >';
                if (config.FieldID == 'SchoolID') {
                    fieldsList += '<select id="ddlSchool"  class="form-control select-validate">'+
                                    '</select></div></div>';
                }
                else if (config.FieldID == 'ClassroomID') {
                    fieldsList += '<select id="ddlClass"  class="form-control select-validate">'+
                                    '</select></div></div>';
                }
                else if (config.FieldID == 'Grade') {
                    fieldsList += '<select id="ddlGrade"  class="form-control select-validate">'+
                                     '</select></div></div>';
                }
                else
                    fieldsList += '<input type="text" id="txt' + config.FieldID + '" class="form-control studenFormFields">' +
                        '</div></div>';
            }
        });
    });
    $('#studentFormGroup').append(fieldsList);
}