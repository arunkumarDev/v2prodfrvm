﻿$(document).ready(function () {
    $('.loaderDiv').show();
    LoadConfig();
    LoadProgressBar();
});
var selectedFields = [];
var masterConfig = [];
var updatedFormValues = {
    WalikuID: 0, CommunityWorkerID: 0, SchoolID: 43, ClassroomID:1382, Gender: 0, Nisn: 0, GradeID: 0, DeleteReasonID:0,
    FirstName: null, Lastname: null, ParentName: null, Email: null, Address: null, Phone: null, Note: null, Gender: null, Image: null, Note_Indonesian: null
};
var currentPage = 0;
function checkboxClick(curElem) {
    if (curElem.checked)
        selectedFields.push(curElem.value);
    else {
        var newFields = [];
        selectedFields.forEach(function (val, ind) {
            if (val != curElem.value)
                newFields.push(val);
        });
        selectedFields = newFields;
    }
}
$('#btnSave').click(function (event) {
    
    if (currentPage == 0 && selectedFields.length == 0) {
        masterConfig.forEach(function (val, ind) {
            if (selectedFields.length > 3) // && val.IsMandatory
                break;
            else
                selectedFields.push(val.FieldID);
        });
    }
    currentPage = 1;
    if ($('.studenFormFields').length > 0) {
        $('.studenFormFields').each(function (i, v) {
            if (v.value != undefined && v.value != '')
                updatedFormValues[v.id.split('txt')[1]] = v.value == undefined ? '' : v.value;
        });
        saveStudent();
        currentPage = 2
    }
    
    LoadConfig();
    LoadProgressBar();
});
function fetchStudentList() {
    $.ajax({
        type: "POST",
        url: "Views/StudentList.aspx/GetAllChildrenForMe",
        //  data: '{PageIndex: "' + 1 + '"  }',
        data: '{SchoolID: "' + updatedFormValues['SchoolID'] + '" ,ClassRoomID: "' + updatedFormValues['ClassroomID'] + '",PageIndex: "1"}',      
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        async: false,
        success: function (data) {
            var url = "Views/StudentList.aspx";
            $('.loaderDiv').hide();
            $("#configDataGroup").load(url, function (responseTxt, statusTxt, xhr) {
                if (statusTxt == "success") {
                    $('.panel-footer').hide();
                }
                if (statusTxt == "error")
                    alert("Error: " + xhr.status + ": " + xhr.statusText);
            });
            $('.loaderDiv').hide();
        },

        failure: function (response) {
            $('.loaderDiv').hide();
            alert(response.d);
        }
    });
}
function saveStudent() {
    $('.loaderDiv').show();
    if (updatedFormValues['Nisn'] != 0) {

        $.ajax({
            type: "POST",
            url: "Views/StudentList.aspx/ManageChild",
            //  data: '{PageIndex: "' + 1 + '"  }',
            data:
                '{WalikuID:' + 0 + ',' +
                'Name:"' + updatedFormValues['FirstName'] + '",' +
                'Surname:"' + updatedFormValues['LastName'] + '",' +
                'ParentName:"' + updatedFormValues['ParentName'] + '",' +
                'CommunityWorkerID:' + updatedFormValues['CommunityWorkerID'] + ',' +
                'Email:"' + updatedFormValues['Email'] + '",' +
                'SchoolID:' + updatedFormValues['SchoolID'] + ',' +
                'ClassroomID:' + updatedFormValues['ClassroomID'] + ',' +
                'Image:' + updatedFormValues['Image'] + ',' +
                'Phone:"' + updatedFormValues['Phone'] + '",' +
                'Note:"' + updatedFormValues['Note'] + '",' +
                'GradeID:' + updatedFormValues['GradeID'] + ',' +
                'DeleteReasonID:' + updatedFormValues['DeleteReasonID'] + ',' +
                'Gender:"' + updatedFormValues['Gender'] + '",' +
                'Nisn:"' + updatedFormValues['Nisn'] + '",' +
                'Address:"' + updatedFormValues['Address'] + '",'+
                'Note_Indonesian:"' + updatedFormValues['Note_Indonesian'] + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                $('.loaderDiv').hide();
            },
            failure: function (response) { $('.loaderDiv').hide();}
        });
    }
}
function LoadProgressBar() {
    $('#rdoStudentsList').attr('checked', false);
    $('#rdoStudentForm').attr('checked', false);
    $('#rdoStudentConfig').attr('checked', false);
    if (updatedFormValues['WalikuID'] != 0) {
        setStudenListProgressStyle();
        $('#rdoStudentsList').prop("checked", true);

    }
    else if (selectedFields.length > 0) {
        setStudentFormPogressStyle();
        $('#rdoStudentForm').prop("checked", true);
    }
    else {
        $('.studentconfigProgress').css('padding-bottom', '30px');
        $('#rdoStudentConfig').prop("checked", true);
        setStudentConfigProgressStyle();
    }
}
function setStudentConfigProgressStyle() {
    
    $('.studentConfigBG').css("background-color", "#D6D1D0");
    $('.studentFormBG').css("background-color", "#D6D1D0");
    $('.studentListBG').css("background-color", "#D6D1D0");
}
function setStudentFormPogressStyle() {
    $('.studentConfigBG').css("background-color", "#00ca9d");
    $('.studentFormBG').css("background-color", "#D6D1D0");
    $('.studentListBG').css("background-color", "#D6D1D0");
}
function setStudenListProgressStyle() {
    $('.studentConfigBG').css("background-color", "#00ca9d");
    $('.studentFormBG').css("background-color", "#00ca9d");
    $('.studentListBG').css("background-color", "transparent");
}
$('#rdoStudentForm').click(function (event) {
    $('.loaderDiv').show();
    if (selectedFields.length == 0) {
        masterConfig.forEach(function (val, ind) {
            if (val.IsMandatory)
                selectedFields.push(val.FieldID);
            else
                break;
        });
    }
    currentPage = 1;
    setStudentFormPogressStyle();
    LoadConfig();
    $('.panel-footer').show();
});
$('#rdoStudentConfig').click(function (event) {
    $('.loaderDiv').show();
    currentPage = 0;
    setStudentConfigProgressStyle();
    LoadConfig();
    $('.panel-footer').show();
});
$('#rdoStudentsList').click(function (event) {
    $('.loaderDiv').show();
    currentPage = 2;
    setStudenListProgressStyle();
    LoadConfig();
    $('.panel-footer').hide();
});

function LoadConfig() {
    if (currentPage == 2) {

        fetchStudentList();
    }
    else {
        if (selectedFields.length == 0) {
            $.ajax({
                type: "POST",
                url: "AdminOperations.aspx/LoadStudentConfigurationData",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('.loaderDiv').hide();
                    var checkboxList = '';
                    masterConfig = data.d;
                    $.each(masterConfig, function (ind, val) {
                        if (val.IsMandatory) {
                            selectedFields.push(val.FieldID);
                            checkboxList += '<div class="md-checkbox row col-md-6 "><label> <input checked="true" disabled value="' + val.FieldID + '" onClick="checkboxClick(this)" type="checkbox" id="chk_' + val.FieldID + '"></input>' +
                                '<span class="md-checkbox-material">' + val.FieldName + '</span></label></div>';
                        }
                        else {
                            checkboxList += '<div class="md-checkbox row col-md-6 "><label> <input value="' + val.FieldID + '" onClick="checkboxClick(this)" type="checkbox" id="chk_' + val.FieldID + '"></input>' +
                                '<span class="md-checkbox-material">' + val.FieldName + '</span></label></div>';
                        }
                    });
                    $('#configDataGroup').append(checkboxList);
                }
            });
        }
        else {
            switch (currentPage) {
                case 0:
                    $('#configDataGroup').empty();
                    $('.loaderDiv').hide();
                    if (masterConfig.length > 0) {
                        var checkboxList = '';
                        $.each(masterConfig, function (ind, val) {
                            if (val.IsMandatory) {
                                checkboxList += '<div class="md-checkbox row col-md-6 "><label> <input checked="true" disabled value="' + val.FieldID + '" onClick="checkboxClick(this)" type="checkbox" id="chk_' + val.FieldID + '"></input>' +
                                    '<span class="md-checkbox-material">' + val.FieldName + '</span></label></div>';
                            }
                            else {
                                checkboxList += '<div class="md-checkbox row col-md-6 "><label> <input value="' + val.FieldID + '" onClick="checkboxClick(this)" type="checkbox" id="chk_' + val.FieldID + '"></input>' +
                                    '<span class="md-checkbox-material">' + val.FieldName + '</span></label></div>';
                            }
                        });
                        $('#configDataGroup').append(checkboxList);
                    }
                    break;
                case 1:
                    var url = 'Views/StudentForm.aspx?selectedFields=' + selectedFields;
                    currentPage = 1;
                    $("#configDataGroup").load(url, function (responseTxt, statusTxt, xhr) {
                        if (statusTxt == "success")
                            $('.loaderDiv').hide();
                            //   alert("External content loaded successfully!");
                        if (statusTxt == "error")
                                alert("Error: " + xhr.status + ": " + xhr.statusText);
                    });
                    break;
                default:
                    break;
            }
        }
    }
}
