﻿
$(window).load(function () {
    LoadSchool();
    LoadNotifications();
});

$("#btnSearch").bind("click", function () {
    LoadFeedbackPerSchool();
});

function LoadSchool() {

    $.ajax({
        type: "POST",
        url: "Feedback.aspx/GetAllSchool",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var appenddata = "<option value=0>--School--</option>";
            $.each(data.d, function (i, item) {
                appenddata += "<option value = '" + item.ID + "'>" + item.SchoolName + " </option>";

            });
            $('#ddlSchool').html(appenddata);
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}


function LoadFeedbackPerSchool() { 
    var inputDate1 = FormatSQLDate($("#datepicker1").val());    // $("#datepicker1").val().toString('d-M-yyyy');
    var inputDate2 = FormatSQLDate($("#datepicker2").val());    // $("#datepicker2").val().toString('d-M-yyyy');
    var SchoolId = $("#ddlSchool").val();

    var obj = new Object();
    obj.SchoolID = parseInt($('#ddlSchool').val());
    obj.StartDate = inputDate1
    obj.EndDate = inputDate2

    $.ajax({
        type: "POST",
       // url: AppUrls.WalikuWebApiUri + "api/feedback/GetFeedBackList?",
      //  data: 'SchoolId=' + $("#ddlSchool").val() + '&StartDate=' + inputDate1 + '&Enddate=' + inputDate2,
       

        url: "Feedback.aspx/GetFeedBackList",
        data: JSON.stringify(obj),
      
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            var serialNo = 0;
            $('#dataTable tbody').empty();
            DataTableDestroy('dataTable');
            $.each(data.d, function (i, item) {

                $('<tr id="tr' + item.ID + '">').html("<td>" + (++serialNo) + "</td>" +
                    "<td>" + item.Name + "</td >" +
                    "<td>" + item.Content + "</td >" +
                    "<td>" + FormatDate(item.CreatedOn,1) + "</td >").appendTo('#dataTable tbody');
            });

            InitializeDataTable('dataTable');
            
        },
        failure: function (response) {
            alert(response.d);
        }
    });
} 