﻿$(window).load(function () {
    LoadReasons();
});
$("#btnSaveReason").bind("click", function () {

    $.ajax({
        type: "POST",
        url: "SickChildReasonManagement.aspx/ManageReason",
        data: '{ID: "' + $("#hdnReasonID").val() + '" ,Title: "' + $("#txtTitle").val() + '" ,Content: "' + btoa($("#txtContent").val()) + '" , ContentIndian: "' + btoa($("#txtContentIndian").val()) + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            location.reload();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
});


function LoadReasons() {
    $('#dataTable').dataTable({
        "bDestroy": true,
        "scrollX": true
    }).fnDestroy();
    $('#dataTable tbody').empty();
    
    
    $.ajax({
        type: "POST",
        url: "SickChildReasonManagement.aspx/GetAllReason",
        data: '{PageIndex: "' + 1 + '"  }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (i, item) {

                $('<tr id="tr' + item.ID + '">').html("<td>" + item.ID + "</td>" +
                    "<td> " + item.Title + "</td >" +
                    "<td><div style='width:600px'> " + item.Content + "<div></td >" +
                    "<td><div style='width:600px'> " + item.ContentIndian + "<div></td >" +
                    "<td><a onclick='return OpenModalPopup(" + item.ID + ");' class='glyphicon glyphicon-edit'></a>&nbsp;&nbsp; <a onclick='return DeleteReason(" + item.ID + ");' class='glyphicon glyphicon-remove'></a></td>").appendTo('#dataTable tbody');
            });
            $('#dataTable').DataTable({
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var select = $('<select><option value=""></option></select>')
                            .appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );
                                //to select and search from grid  
                                column
                                    .search(val ? '^' + val + '$' : '', true, false)
                                    .draw();
                            });

                        column.data().unique().sort().each(function (d, j) {
                            select.append('<option value="' + d + '">' + d + '</option>')
                        });
                    });
                }
            });
        },
        failure: function (response) {
            alert(response.d);
        }
    });

}
function ClearControl() {
    $("#txtTitle").val('');
    $("#txtContent").summernote('code', '');
    $("#txtContentIndian").summernote('code', '');
    $("#hdnReasonID").val('0');
}
function OpenModalPopup(ctrl) {
    $("#hdnReasonID").val($('#tr' + ctrl + ' td').eq(0).text());
    $("#txtTitle").val($('#tr' + ctrl + ' td').eq(1).text().trim());
    $("#txtContent").summernote('code', $('#tr' + ctrl + ' td').eq(2).html().trim());
    $("#txtContentIndian").summernote('code', $('#tr' + ctrl + ' td').eq(3).html().trim());
    $('#SickChildReasonManagementModel').modal('show');
}
function DeleteReason(ID) {
    if (!confirm("Are you sure want to delete?")) {
        return;
    }

    $.ajax({
        type: "POST",
        url: "SickChildReasonManagement.aspx/DeleteReasonByID",
        data: '{ID: "' + ID + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            location.reload();
        },
        failure: function (response) {
            alert(response.d);
        }
    });
}

// Quando la pagina è pronta
$.ready(function () {
    $("#txtContent").summernote({
        height: 300
    }); 
    $("#txtContentIndian").summernote({
        height: 300
    }); 
});