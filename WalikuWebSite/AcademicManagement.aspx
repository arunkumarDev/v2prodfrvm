﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="AcademicManagement.aspx.cs" Inherits="WalikuWebSite.AcademicManagement" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">

    <style type="text/css">

    .fa-icons {
    position: absolute;
    top: 7px;
    right: 25px;
}
</style>

    <!-- Modal -->
    <main class="l-main">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"><%=Resources.Resource.Title_HolidayManagement %></h1>
                </div>
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="user-details">
                            <div class="user_data">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                         <input type="hidden" id="hdnHolidayID" value="0" />
                                            <div class="col-sm-3 col-form-label mt-10"><%=Resources.Resource.Lbl_Date %></div>
                                            <div class="col-sm-9">                                              
                     
                                                <input type="text" class="form-control date-icon has Datepicker" id="datepicker">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <div class="col-sm-3 col-form-label  mt-10">
                                                <%=Resources.Resource.Lbl_Reason %>
                                            </div>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control input-validate" id="inputReason" placeholder="Reason">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-sm-4 col-form-label  mt-10">
                                                <%=Resources.Resource.Lbl_SchoolName %>
                                            </div>
                                            <div class="col-sm-8">
                                                <select id="ddlSchoolSearch" class="form-control">
                                                </select>
                                                <i class="fa fa-angle-down fa-icons text-18"></i>
                                            </div>
                                           
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <button id="btnSaveHoliday" type="button" class="btn btn-primary mr-15" title=""> <%=Resources.Resource.Lbl_Add %> </button>
                                            <!-- <a href="javascript:;" class="btn btn-primary" class="add_details" autocomplete="false" type="button"> Add </button> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-striped table-bordered data-table mb-0" role="grid">
                                <thead>
                                    <tr>
                                        <th><%=Resources.Resource.Lbl_SerialNo %></th>
                                        <th><%=Resources.Resource.Lbl_Date %></th>
                                        <th><%=Resources.Resource.Lbl_HolidayType %></th>
                                        <th><%=Resources.Resource.Lbl_Action %></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="Scripts/bootstrap-datepicker.js"></script>
     <script>document.write("<script type='text/javascript' src='Scripts/AcademicManagement.js?v=" + JsVerion + "'><\/script>");</script>
    <script>

       $('#datepicker').datepicker({
           autoclose: true,
           format: 'dd/mm/yyyy'
        });
        var date = new Date(); //'2018-01-01'
        $('#datepicker').datepicker('setDate', date);

     
    </script>
</asp:Content>


