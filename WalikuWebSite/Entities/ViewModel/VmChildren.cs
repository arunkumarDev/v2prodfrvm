﻿using System;


public class VmChildren
{

    public int ID { get; set; }

    public string Name { get; set; }

    public string Middlename { get; set; }

    public string Surname { get; set; }

    public string ParentName { get; set; }

    public string Email { get; set; }

    public string Image { get; set; }

    public string ImageBase { get; set; }

    public int SchoolID { get; set; }

    public string SchoolName { get; set; }

    public int ClassroomID { get; set; }

    public string ClassroomName { get; set; }

    public string Address { get; set; }

    public string Phone { get; set; }

    public string Note { get; set; }

    public string GeoLocation { get; set; }

    public string Gender { get; set; }

    public Single AttendancePercent { get; set; }

    public string TeacherName { get; set; }

    public string NISN { get; set; }

    public Nullable<System.DateTime> Date_of_Birth { get; set; }

}