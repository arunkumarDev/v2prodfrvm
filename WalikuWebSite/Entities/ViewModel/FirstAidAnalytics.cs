﻿using System;

public class VmFirstAidAnalytics
{
    public int ID { get; set; }

    public int VisitorId { get; set; }

    public string SchoolName { get; set; }

    public DateTime VisitedOn { get; set; }

    public string Name { get; set; }

    public string Surname { get; set; }
}