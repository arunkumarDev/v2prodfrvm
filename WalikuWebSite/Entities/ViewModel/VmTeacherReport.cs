﻿public class VmChildrenReport
{
    public int ID { get; set; }

    public string Name { get; set; }

    public string Gender { get; set; }

    public int AttendanceDate { get; set; }

    public string Attendance { get; set; }

    public string AttendanceType { get; set; }

    public int ChildId { get; set; }

    public int AbsentDays { get; set; }

    public string ReasonType { get; set; }

    public string NISN { get; set; }

    public string ClassName { get; set; }

    public string TotalDays { get; set; }


}
