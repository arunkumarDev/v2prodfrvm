﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.Entities.DataModels
{
    public class NotificationDetails
    {
        public int Id { get; set; }
        public string NotifyText { get; set; }
        public int NotifyCount { get; set; }
        public int UserId { get; set; }
        public DateTime CreatedOn {get; set;}
        public int CreatedBy { get; set; }
    }
}