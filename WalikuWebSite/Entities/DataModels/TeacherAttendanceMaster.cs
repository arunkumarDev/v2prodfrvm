﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.Entities.DataModels
{
    public class TeacherAttendanceMaster
    {
        public int ID { get; set; }
        public int UserID { get; set; }

        public int ReasonTypeID { get; set; }
        public int SchoolID { get; set; }
        public DateTime AttendanceDate { get; set; }
        public bool IsPresent { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsSubmitted { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }

    }

  
}