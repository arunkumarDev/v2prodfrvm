﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WalikuWebSite.Entities.DataModels
{
    public class TrackStudents
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string createdon { get; set; }
        public int isactive { get; set; }
        public string Status { get; set; }
        public string FirstDay { get; set; }
        public string LastDay { get; set; }

    }
}