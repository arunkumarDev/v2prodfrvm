﻿using Aspose.Cells;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using WalikuWebSite.EntityFrameworkDAC;
using dac = WalikuWebSite.EntityFrameworkDAC;


namespace WalikuWebSite
{
    /// <summary>
    /// Summary description for ImportDataHandler
    /// </summary>
    public class ImportDataHandler : IHttpHandler
    {
        private bool isError = false;
        private bool isValidate = false;
        private bool isValidate1 = false;
        private bool isClassValidate = false;
        List<dac.ChildrenMaster> erroredList = new List<dac.ChildrenMaster>();
        List<dac.ChildrenMaster> erroredList1 = new List<dac.ChildrenMaster>();
        List<dac.ChildrenMaster> classNotMatchList = new List<dac.ChildrenMaster>();
        List<dac.ChildrenMaster> classEmptyList = new List<dac.ChildrenMaster>();
        private bool isNisnLengthexist = false;
        public void ProcessRequest(HttpContext context)
        {
            var lang = Common.CommonFunction.GetCurrentCulture();
            string strImageName = string.Empty;
            if (context.Request.Files.Count > 0)
            {
                HttpFileCollection files = context.Request.Files;
                for (int i = 0; i < files.Count; i++)
                {
                    try
                    {


                        HttpPostedFile file = files[i];
                        strImageName = file.FileName;
                        //string filePath = context.Server.MapPath("~/ImportExport/" + strImageName);
                        // filePath = "h:\\root\\home\\walikuwaliku-001\\www\\site2\\Photos\\" + strImageName;
                        //file.SaveAs(filePath);
                        var streamData = file.InputStream;

                        dac.ImportData importData = new dac.ImportData();

                        Workbook wb = new Workbook(streamData);
                        Worksheet ws = wb.Worksheets["Daftar guru-ENG"];
                        if (ws != null)
                        {
                            var teachers = GetTeachersList(ws,context);
                            if (teachers.Count > 0)
                                importData.ImportTeacherData(teachers);
                        }

                        ws = wb.Worksheets["Daftar guru-IND"];

                        if (ws != null)
                        {
                            var teachers = GetTeachersList(ws,context);
                            if (teachers.Count > 0)
                                importData.ImportTeacherData(teachers);
                        }

                        //var schid = context.Request.Form["schid"];

                         if (lang == "en")
                            ws = wb.Worksheets["Student Profile"];
                         else
                            ws = wb.Worksheets["Daftar Peserta Didik"];

                        


                        if (ws != null)
                        {
                            var students = GetStudentsList(ws,context);


                          

                            if (students.Count > 0 && erroredList.Count == 0)
                            {
                                foreach (var childrenMaster in students)
                                {
                                    if(childrenMaster.NISN.Length != 10)
                                    {
                                        isNisnLengthexist = true;
                                        isValidate = true;
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(childrenMaster.Name) || string.IsNullOrEmpty(childrenMaster.Gender) ||
                                        string.IsNullOrEmpty(childrenMaster.NISN) || string.IsNullOrWhiteSpace(childrenMaster.Date_of_Birth.ToString()) ||
                                        string.IsNullOrEmpty(childrenMaster.Current_Class))
                                        {
                                            //data.Add("<ul>");
                                            //data.Add("<li> Compulsory fields are : 'Name, Gender, NISN, Date of Birth, Father's Name, Current Class'</li>");

                                            //data.Add("</ul>");
                                            //data.ToArray();
                                            isValidate = true;
                                        }
                                    }
                                   

                                }
                                if (isValidate == false)
                                {
                                    importData.ImportStudentData(students);
                                }

                                //return true;

                            }

                            if(erroredList.Count >= 0 && classNotMatchList.Count == 0)
                            {
                                isValidate = true;
                            }


                            if (classNotMatchList.Count > 0 && erroredList.Count == 0)
                            {
                                isClassValidate = true;
                            }

                            //importData.ImportStudentData(students);
                        }
                       
                        if (isValidate1 == true && ws != null && isClassValidate == false)
                        {
                            context.Response.Write("Error3");
                        }
                        else
                        {
                            


                            if (isValidate == false && ws != null && isClassValidate == false)
                            {
                                if (isError)
                                    context.Response.Write("Error");
                                else
                                    context.Response.Write("Success");
                            }
                            else if (isNisnLengthexist == true)
                            {
                                context.Response.Write("Error2");

                            }
                            else if(isClassValidate)
                            {
                                context.Response.Write("ClassError");
                            }
                            else
                            {
                                if (ws != null)
                                {
                                    context.Response.Write("Error1");
                                }
                                else
                                    context.Response.Write("Error4");

                            }
                        }
                       
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }




                }


            }
        }


        public List<dac.ChildrenMaster> GetStudentsList(Worksheet ws, HttpContext context)
        {
            var myUser = SqlHelper.GetUserByID(Common.CommonFunction.GetUserIdFromCookieByID());
            var uType = 1;
            var uId = 0;
            if (myUser != null)
            {
                if (myUser.Tables[0].Rows.Count != 0)
                {


                    uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
                    uId = int.Parse(myUser.Tables[0].Rows[0]["ID"].ToString());
                }
            }
            
                List<dac.ChildrenMaster> studentsList = new List<dac.ChildrenMaster>();

            

            List<dac.ChildrenMaster> updateList = new List<dac.ChildrenMaster>();

            var schools =  SqlHelper.GetSchoolListForUser(Common.CommonFunction.GetUserIdFromCookieByID(), Common.CommonFunction.GetCurrentCulture());
                     
            var SchoolID = schools.FirstOrDefault().ID;

            var schid = Convert.ToInt32(context.Request.Form["schid"]);

            List<ChildrenMaster> existing = SqlHelper.GetAllChildrenInSchool(schid);

            var classes = uType == 4 ? SqlHelper.GetAllClassesForMe(schid, uId) : SqlHelper.GetAllClassesForMe(schid, uId);

            


            try
            {

                for (var rowId = 6; rowId < ws.Cells.Rows.Count; rowId++)
                {
                    dac.ChildrenMaster student = new dac.ChildrenMaster();
                    student.CreatedOn = DateTime.Now;
                    var isExisting = false;
                    var IsClassNotinSchool = true;
                    var IsClassEmpty = true;
                    var SNo = ws.Cells[rowId, 0];
                    var SNoVal = (SNo == null || SNo.Value == null) ? "0" : ws.Cells[rowId, 0].Value.ToString().Trim();
                    var SNoValStr = SNoVal == "" ? "0" : SNoVal;
                    if (SNoValStr != "0")
                    {
                        for (var colId = 0; colId <= 66; colId++)
                        {
                            var cell = ws.Cells[rowId, colId];
                            var cellVal = (cell == null || cell.Value == null) ? "" : ws.Cells[rowId, colId].Value.ToString().Trim();
                            var cellValStr = cellVal; //cellVal == "" ? "0" : cellVal;


                            if (colId == 1)
                                student.Name = cellValStr;
                            if (colId == 2)
                                student.Student_Registration_Number = cellValStr;
                            if (colId == 3)
                                student.Gender = cellValStr;
                            if (colId == 4)
                            {

                                var stroutput = Regex.Replace(cell.StringValue, @"\p{C}+", string.Empty);
                                
                              
                                if (cellValStr != "" && stroutput.Length == 10)
                                {
                                    existing.ForEach(eStudent =>
                                    {

                                        if (stroutput == eStudent.NISN)
                                        {
                                            isExisting = true;

                                        }
                                    });
                                }
                                else
                                {
                                    isExisting = false;
                                }
                                student.IsActive = true;
                                student.NISN = stroutput;
                            }
                            if (colId == 5)
                                student.Birthplace = cellValStr;
                            if (colId == 6)
                            {
                                if (ws.Cells[rowId, colId].Value == null)
                                    student.Date_of_Birth = null;
                                //student.Date_of_Birth = DateTime.Now;
                                else
                                    student.Date_of_Birth = Convert.ToDateTime(cellValStr);
                            }
                            if (colId == 7)
                                student.ID_Number = cellValStr;
                            if (colId == 8)
                                student.Religion = cellValStr;
                            if (colId == 9)
                                student.Address = cellValStr;
                            if (colId == 10)
                                student.RT = cellValStr;
                            if (colId == 11)
                                student.RW = cellValStr;
                            if (colId == 12)
                                student.Village = cellValStr;
                            if (colId == 13)
                                student.Village_Office = cellValStr;
                            if (colId == 14)
                                student.Sub_District = cellValStr;
                            if (colId == 15)
                                student.Postal_Code = cellValStr;
                            if (colId == 16)
                                student.Living_Situation = cellValStr;
                            if (colId == 17)
                                student.Transportation_Mode = cellValStr;
                            if (colId == 18)
                                student.Phone = cellValStr;
                            if (colId == 19)
                                student.Mobile_No = cellValStr;
                            if (colId == 20)
                                student.Email = cellValStr;
                            if (colId == 21)
                                student.Certificate_of_National_Examination_Result = cellValStr;
                            if (colId == 22)
                                student.Recipient_of_Social_Security_Card = cellValStr;
                            if (colId == 23)
                                student.Social_Security_No = cellValStr;
                            if (colId == 24)
                                student.F_Name = cellValStr;
                            if (colId == 25)
                                student.F_Birth_Year = cellValStr;
                            if (colId == 26)
                                student.F_Education_Level = cellValStr;
                            if (colId == 27)
                                student.F_Occupation = cellValStr;
                            if (colId == 28)
                                student.F_Income = cellValStr;
                            if (colId == 29)
                                student.F_ID_Number = cellValStr;
                            if (colId == 30)
                                student.M_Name = cellValStr;
                            if (colId == 31)
                                student.M_Birth_Year = cellValStr;
                            if (colId == 32)
                                student.M_Education_Level = cellValStr;
                            if (colId == 33)
                                student.M_Occupation = cellValStr;
                            if (colId == 34)
                                student.M_Income = cellValStr;
                            if (colId == 35)
                                student.M_ID_Number = cellValStr;
                            if (colId == 36)
                                student.G_Name = cellValStr;
                            if (colId == 37)
                                student.G_Birth_Year = cellValStr;
                            if (colId == 38)
                                student.G_Education_Level = cellValStr;
                            if (colId == 39)
                                student.G_Occupation = cellValStr;
                            if (colId == 40)
                                student.G_Income = cellValStr;
                            if (colId == 41)
                                student.G_ID_Number = cellValStr;
                            if (colId == 42)
                            {
                                classes.ForEach(c =>
                                {
                                    
                                    if (cellValStr != "" && c.ClassName == cellValStr )
                                    {
                                        IsClassNotinSchool = false;
                                        student.ClassroomID = c.ID;
                                        student.SchoolID = c.SchoolId;
                                    }
                                    else
                                    {
                                        IsClassEmpty = false;
                                    }
                                });
                                student.Current_Class = cellValStr;
                            }
                            if (colId == 43)
                                student.National_Examination_Participant_No = cellValStr;
                            if (colId == 44)
                                student.Certificate_Serial_No = cellValStr;
                            if (colId == 45)
                                student.Recipient_of_Indonesian_Smart_Card = cellValStr;
                            if (colId == 46)
                                student.Smart_Card_No = cellValStr;
                            if (colId == 47)
                                student.Name_on_Smart_Card = cellValStr;
                            if (colId == 48)
                                student.Family_Card_No = cellValStr;
                            if (colId == 49)
                                student.Birth_Certificate_No = cellValStr;
                            if (colId == 50)
                                student.Bank = cellValStr;
                            //if (colId == 51)
                            //    student.Bank_Account_Number = cellValStr;
                            //if (colId == 52)
                            //    student.Name_of_Account_Holder = cellValStr;
                            if (colId == 51)
                                student.Suitable_for_Smart_Indonesia_Program = cellValStr;
                            if (colId == 52)
                                student.Reason_for_Smart_Indonesia_Program = cellValStr;
                            if (colId == 53)
                                student.Special_Needs = cellValStr;
                            if (colId == 54)
                                student.Previous_School = cellValStr;
                            if (colId == 55)
                                student.Position_of_Child_among_Siblings = cellValStr;
                            if (colId == 56)
                                student.Latitude = cellValStr;
                            if (colId == 57)
                                student.Longitude = cellValStr;
                            if (colId == 58)
                                student.Family_Register_No = cellValStr;
                            if (colId == 59)
                                student.Weight = cellValStr;
                            if (colId == 60)
                                student.Height = cellValStr;
                            if (colId == 61)
                                student.Head_circumference = cellValStr;
                            if (colId == 62)
                                student.TimeTaken_Hrs = cellValStr;
                            if (colId == 63)
                                student.TimeTaken_Mins = cellValStr;
                            if (colId == 64)
                                student.dst_frm_houseto = cellValStr;
                            if (colId == 65)
                                student.No_of_Siblings_related_by_blood = cellValStr;
                            
                        }

                        if (IsClassNotinSchool == false)
                        {
                            classNotMatchList.Add(student);
                        }
                        else if (IsClassNotinSchool)
                        {
                            erroredList.Add(student);
                        }
                       
                        else if (isExisting)
                        {
                            //updateList.Add(student);
                            isValidate1 = true;
                        }
                        else
                        {

                            //if (!isExisting)
                            studentsList.Add(student);
                        }
                    }
                }
                if (erroredList.Count > 0)
                {
                    isError = true;

                   // WriteErroredRecordsForStudent(erroredList, context);
                }
                if(updateList.Count >0)
                {
                    foreach (var childrenMaster in updateList)
                    {
                        if (childrenMaster.NISN.Length != 10)
                        {
                            isNisnLengthexist = true;
                            isValidate = true;

                        }
                        else
                        {
                            isValidate1 = true;
                        }
                    }
                    if(isValidate1 == true)
                    {
                       
                    }
                    else
                    {
                        dac.ImportData importData = new dac.ImportData();
                        importData.UpdateStudentData(updateList);
                    }
                        
                }

                return studentsList;
            }
            catch(Exception e)
            {
                throw e;
            }
            
        }


        public List<dac.UserMaster> GetTeachersList(Worksheet ws, HttpContext context)
        {
            List<dac.UserMaster> teachersList = new List<dac.UserMaster>();
            List<dac.UserMaster> erroredList = new List<dac.UserMaster>();

            List<UserMaster> existing = SqlHelper.GetAllTeachers();


            for (var rowId = 5; rowId < ws.Cells.Rows.Count; rowId++)
            {
                dac.UserMaster teacher = new dac.UserMaster();
                teacher.CreatedOn = DateTime.Now;
                teacher.UserType = 2;
                teacher.IsActive = true;

                var isExisting = false;


                for (var colId = 1; colId <= 50; colId++)
                {
                    var cell = ws.Cells[rowId, colId];
                    var cellVal = (cell == null || cell.Value == null) ? "0" : ws.Cells[rowId, colId].Value.ToString().Trim();
                    var cellValStr = cellVal == "" ? "0" : cellVal;


                    if (colId == 1)
                    {
                        teacher.Name = cellValStr;
                        teacher.UserName = cellValStr.Replace(" ", "");
                        teacher.Password = cellValStr.Replace(" ", "");
                    }
                    if (colId == 2)
                        teacher.NUPTK = Convert.ToInt64(cellValStr);
                    if (colId == 3)
                        teacher.Gender = cellValStr;
                    if (colId == 4)
                        teacher.Place_of_Birth = cellValStr;
                    if (colId == 5)
                    {
                        if (ws.Cells[rowId, colId].Value == null)
                            teacher.Date_of_Birth = DateTime.Now;
                        else
                            teacher.Date_of_Birth = Convert.ToDateTime(cellValStr);
                    }
                    if (colId == 6)
                        teacher.NIP = Convert.ToInt64(cellValStr);
                    if (colId == 7)
                        teacher.Employee_Status = cellValStr;
                    if (colId == 8)
                        teacher.Type_of_Teacher = cellValStr;
                    if (colId == 9)
                        teacher.Religion = cellValStr;
                    if (colId == 10)
                        teacher.Address = cellValStr;
                    if (colId == 11)
                        teacher.Street = cellValStr;
                    if (colId == 12)
                        teacher.RT = cellValStr;
                    if (colId == 13)
                        teacher.RW = cellValStr;
                    if (colId == 14)
                        teacher.Hamlet_Village = cellValStr;
                    if (colId == 15)
                        teacher.Subdistrict = cellValStr;
                    if (colId == 16)
                        teacher.Postal_Code = cellValStr;
                    if (colId == 17)
                        teacher.Phone = cellValStr;
                    if (colId == 18)
                        teacher.Mobile_No = cellValStr;
                    if (colId == 19)
                        teacher.Email = cellValStr;
                    if (colId == 20)
                        teacher.Additional_Assignments = cellValStr;
                    if (colId == 21)
                        teacher.SK_CPNS = cellValStr;
                    if (colId == 22)
                    {
                        if (ws.Cells[rowId, colId].Value == null)
                            teacher.SK_Date = DateTime.Now;
                        else
                            teacher.SK_Date = Convert.ToDateTime(cellValStr);
                    }
                    if (colId == 23)
                        teacher.SK_Appointment = cellValStr;
                    if (colId == 24)
                    {
                        if (ws.Cells[rowId, colId].Value == null)
                            teacher.TMT_Appointment = DateTime.Now;
                        else
                            teacher.TMT_Appointment = Convert.ToDateTime(cellValStr);
                    }
                    
                    if (colId == 25)
                        teacher.Appointment_Agency = cellValStr;
                    if (colId == 26)
                        teacher.Rank_Group = cellValStr;
                    if (colId == 27)
                        teacher.Salary_Source = cellValStr;
                    if (colId == 28)
                        teacher.Biological_mothers_name = cellValStr;
                    if (colId == 29)
                        teacher.Marital_status = cellValStr;
                    if (colId == 30)
                        teacher.Husband___Wife_Name = cellValStr;
                    if (colId == 31)
                        teacher.Husband___Wife_NIP = cellValStr;
                    if (colId == 32)
                        teacher.Husband___Wife_Occupation = cellValStr;
                    if (colId == 33)
                    {
                        if (ws.Cells[rowId, colId].Value == null)
                            teacher.TMT_PNS = DateTime.Now;
                        else
                            teacher.TMT_PNS = Convert.ToDateTime(cellValStr);
                    }
                    if (colId == 34)
                        teacher.Already_a_Principals_License = cellValStr;
                    if (colId == 35)
                        teacher.Supervision_Training = cellValStr;
                    if (colId == 36)
                        teacher.Braille_expertise = cellValStr;
                    if (colId == 37)
                        teacher.Sign_Language_Expertise = cellValStr;
                    if (colId == 38)
                        teacher.NPWP = cellValStr;
                    if (colId == 39)
                        teacher.The_name_of_the_taxpayer = cellValStr;
                    if (colId == 40)
                        teacher.Citizenship = cellValStr;
                    if (colId == 41)
                        teacher.Bank = cellValStr;
                    if (colId == 42)
                        teacher.Bank_account_number = cellValStr;
                    if (colId == 43)
                        teacher.Account_Name = cellValStr;
                    if (colId == 44)
                    {
                        existing.ForEach(eTeacher =>
                        {
                            if (teacher.NUPTK == eTeacher.NUPTK || cellValStr == eTeacher.NIK)
                            {
                                isExisting = true;

                            }
                        });
                        teacher.NIK = cellValStr;
                    }
                    if (colId == 45)
                        teacher.No_KK = cellValStr;
                    if (colId == 46)
                        teacher.Karpeg = cellValStr;
                    if (colId == 47)
                        teacher.Karis_Karsu = cellValStr;
                    if (colId == 48)
                        teacher.Latitude = cellValStr;
                    if (colId == 49)
                        teacher.Longitude = cellValStr;
                    if (colId == 50)
                        teacher.NUKS = cellValStr;
                }

                
                if (!isExisting)
                    teachersList.Add(teacher);
                else
                    erroredList.Add(teacher);
                
            }
            if (erroredList.Count > 0)
            {
                isError = true;
                WriteErroredRecordsForTeacher(erroredList, context);
            }

            return teachersList;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        public void WriteErroredRecordsForTeacher(List<dac.UserMaster> ErroredData, HttpContext context)
        {
            var exportPath = context.Server.MapPath("~/ImportExport/Errors/TeacherImportFailed.xlsx");

            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets[0];

            ImportTableOptions imp = new ImportTableOptions();
            imp.InsertRows = true;

            imp.DateFormat = "dd/mm/yyyy";
            imp.ColumnIndexes = null;

            ws.Cells.ImportCustomObjects((ICollection)ErroredData, 1, 0, imp);

            //var cell = ws.Cells["A2"];
            //cell.PutValue("Fantastic");

            // Auto-fit all the columns
            wb.Worksheets[0].AutoFitColumns();
            //var currentPath = @"~/ImportExport/ExportedFiles/ChildrenExport.xlsx";
            //var exportPath = context.Server.MapPath(currentPath);
            // Save the Excel file
            //  wb.Save("ChildrenExport.xlsx",SaveFormat.Xlsx);
            wb.Save(exportPath);

        }

        public void WriteErroredRecordsForStudent(List<dac.ChildrenMaster> ErroredData, HttpContext context)
        {
            
            var exportPath = context.Server.MapPath("~/App_Data/ChildrenImportFailed.xlsx");

            Workbook wb = new Workbook();
            Worksheet ws = wb.Worksheets[0];

            ImportTableOptions imp = new ImportTableOptions();
            imp.InsertRows = true;

            imp.DateFormat = "dd/mm/yyyy";
            imp.ColumnIndexes = null;

            ws.Cells.ImportCustomObjects((ICollection)ErroredData, 1, 0, imp);
            wb.Worksheets[0].AutoFitColumns();
            wb.Save(exportPath);

        }
    }


}