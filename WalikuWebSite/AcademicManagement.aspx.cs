﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Waliku.GoogleTranslator;

namespace WalikuWebSite
{
    public partial class AcademicManagement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
            }

        }


        [WebMethod]
        public static void ManageHoliday(int ID, string Reason, int SchoolID, DateTime HolidayDate)
        {


            string CurrentCulture = Common.CommonFunction.GetCurrentCulture(), ToCulture = Common.CommonFunction.GetToCulture();
            Dictionary<string, string> Dict = GoogleTranslator.TranslateText(Reason.Trim(), CurrentCulture, ToCulture);

            SqlHelper.ManageHoliday(new AcademicCalenderMaster()
            {
                ID = ID,
                Reason = Dict["en"],
                HolidayDate = HolidayDate,
                SchoolID = SchoolID,
                IsActive = true,
                CreatedBy = Common.CommonFunction.GetUserIdFromCookieByID(),
                CreatedOn = DateTime.Now,
                Reason_Indonesian = Dict["id"]
            });
        }

        [WebMethod]
        public static List<AcademicCalenderMaster> GetAllHoliday(int SchoolID)
        {
            return SqlHelper.GetAllHoliday(SchoolID, Common.CommonFunction.GetCurrentCulture());


        }

        [WebMethod]
        public static void DeleteHolidayByID(int ID)
        {
            SqlHelper.DeleteHolidayByID(ID);
        }

    }
}