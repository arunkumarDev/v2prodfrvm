﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report-StaffSummary.aspx.cs" MasterPageFile="~/Site.Master" Inherits="WalikuWebSite.Report_StaffSummary" %>


  <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

      <script>
        var LocalResources = {
       "Lbl_StudentName":'<%=Resources.Resource.Lbl_StudentName %>',
       "Lbl_Gender":'<%=Resources.Resource.Lbl_Gender %>',
       "Lbl_Present":'<%=Resources.Resource.Lbl_Present %>',
        "Lbl_Absent":'<%=Resources.Resource.Lbl_Abscent %>',
        "Lbl_AbsentDueToHealth":'<%=Resources.Resource.Lbl_AbsentDueToHealth %>',
        "Lbl_AbsentDueToNonHealth":'<%=Resources.Resource.Lbl_AbsentDueToNonHealth %>',
        "Lbl_TotalStudents":'<%=Resources.Resource.Lbl_TotalStudents %>',
        "Lbl_AbsentDays": '<%=Resources.Resource.Lbl_AbsentDays %>',
        "Lbl_ChronicAbsentChildren":'<%=Resources.Resource.Lbl_ChronicAbsentChildren.ToUpper() %>',
        "Lbl_SevereChronicAbsentChildren":'<%=Resources.Resource.Lbl_SevereChronicAbsentChildren.ToUpper() %>',
        "Lbl_AbsentStudents":'<%=Resources.Resource.Lbl_AbsentStudents %>',
         "Lbl_PresentStudents": '<%=Resources.Resource.Lbl_PresentStudents %>',
         "Lbl_AttendanceRate": '<%=Resources.Resource.Lbl_AttendanceRate %>',
         "Lbl_AbsenteeismRate": '<%=Resources.Resource.Lbl_AbsenteeismRate %>',
        "Lbl_Health": '<%=Resources.Resource.Lbl_Health %>',
        "Lbl_NonHealth": '<%=Resources.Resource.Lbl_NonHealth %>',
        "Lbl_Unknown": '<%=Resources.Resource.Lbl_Unknown %>',            
        "Severely Chronically Absent": '<%=Resources.Resource.Lbl_SeverelyChronicallyAbsent %>',
        "Chronically Absent": '<%=Resources.Resource.Lbl_ChronicallyAbsent %>',
        "Lbl_ReasonType": '<%=Resources.Resource.Lbl_ReasonType %>',
        "Lbl_AbsenceType": '<%=Resources.Resource.Lbl_AbsenceType %>',
        "None": '<%=Resources.Resource.Lbl_None %>',
            "null": '<%=Resources.Resource.Lbl_None %>',
        "Lbl_Select": '<%=Resources.Resource.Lbl_Select %>',
        "Lbl_SelectClass": '<%=Resources.Resource.Lbl_SelectClass %>',
        "Lbl_TotalCount": '<%=Resources.Resource.Lbl_TotalCount %>',
        "H": '<%=Resources.Resource.Staff_Health %>',
        "NH": '<%=Resources.Resource.Staff_NonHealth %>',
            "UL": '<%=Resources.Resource.Staff_UnknownLeave %>',
        "A": '<%=Resources.Resource.Assignments %>',
        "P": '<%=Resources.Resource.Present %>',
            "Holiday": '<%=Resources.Resource.Holiday %>',
        "Lbl_Percentage": '<%=Resources.Resource.Lbl_Percentage %>' ,
            "Lbl_Staff": '<%=Resources.Resource.Lbl_Staff %>',
            "Lbl_Assignments": '<%=Resources.Resource.Lbl_Assignments %>',
         "Lbl_Print_UnknownLeave": '<%=Resources.Resource.Lbl_Print_UnknownLeave %>'

        }
        
        

    </script>

       <% if (uType == 2)  //(uType != 1)
        { %>
          <main class="l-main custom-material">

       <div class="content-wrapper content-wrapper--with-bg">
      <div class="row">
        <div class="col-sm-2">
          <div class="bmd-form-group">
            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectDate %></label>
           <input type="text" class="form-control date-icon hasDatepicker" id="datepicker">

            <i class="fa fa-calendar fa-icons"></i>
          </div>
        </div>
         <div class="col-sm-3">
          <div class="bmd-form-group is-filled">
            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ClassName %></label>
            <select id="ddlClassSearch" class="form-control">
                            
                        </select>
            <i class="fa fa-angle-down fa-icons text-18"></i>
          </div>
        </div>
           <%-- <div class="col-sm-3" id="divStudentFilter" style="display:none;">
          <div class="bmd-form-group is-filled">
            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_StudentName %></label>
            <select id="ddlStudentSearch" class="form-control">
                            
                        </select>
            <i class="fa fa-angle-down fa-icons text-18"></i>
          </div>
        </div>--%>

      <%--     <div class="col-sm-3">
          <div class="bmd-form-group is-filled">
            <label class="bmd-label-floating"><%=Resources.Resource.Menu_Type %></label>
            <select id="ddlMenuType" class="form-control">
                <option value="1">School Summary</option>            
                <option value="2">Compare Classes</option>  
                <option value="3">Individual Class Summary</option>  
                <option value="4">Individual Child Summary</option>  
                <option value="5">Staff Summary</option>  
                        </select>
            <i class="fa fa-angle-down fa-icons text-18"></i>
          </div>
        </div>--%>

        <div class="col-sm-3">
        </div>
        <div class="col-sm-10">
         <ul class="nav nav-pills">
              <li class="active panel-menu" id="DailyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Daily%></a></li>
              <li class="panel-menu" id="WeeklyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Weekly%></a></li>
              <li class="panel-menu" id="MonthlyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Monthly%></a></li>
                <li class="panel-menu" id="YearlyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Yearly%></a></li>
            </ul>
        </div>
    <%--  <div class="col-sm-2 mt-15">
          <button type="button" id="btnSearchReport" class="btn btn-primary" title=""> <%=Resources.Resource.Lbl_GenerateReport %> </button>
        </div>--%>
      </div>

      <div class="panel">
      <div id="ReportContainer" class="panel-body"></div>                
    </div>             

    <script src="Scripts/Reports/index.js?1"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/Reports/Common.js?v=" + JsVerion + "'><\/script>");</script>


    <script src="Scripts/bootstrap-datepicker.js"></script>

</div>
 </main>


    <script>
        var FormLoad = true;
  function noSunday(date) {
        var day = date.getDay();
        return [(day > 0), ''];
    };


        $('#datepicker').datepicker({
            autoclose: true,
             beforeShowDay: noSunday
        });
      
        var date = new Date();
        $('#datepicker').datepicker('setDate', date);

    </script>

     <% }

         else { %>

    <main class="l-main custom-material" id="hideAll">
        <div class="main-headactivename">
             <div id="divLoader" class="loading" align="center"> 
             <div class="loading_inner">
        <img src="Content/loading.gif" alt="Loader" /> 
              </div>
      </div>
       <%--     Staff Summary--%>
            <%=Resources.Resource.title_Rpt_StaffSum  %>
        </div>
       <div class="content-wrapper content-wrapper--with-bg">
      <div class="row">
        <div class="col-sm-2">
          <div class="bmd-form-group">
            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectDate %></label>
           <input type="text" class="form-control date-icon hasDatepicker" id="datepicker">

            <i class="fa fa-calendar fa-icons"></i>
          </div>
        </div>
          <div class="col-sm-3" id="schoolsearch1">
                    <div class="bmd-form-group is-filled" id="schoolsearch">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchoolSearch" class="form-control">
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
        <div class="col-sm-3">
          <div class="bmd-form-group is-filled" id="classsearch">
            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ClassName %></label>
            <select id="ddlClassSearch" class="form-control">
                            
                        </select>
            <i class="fa fa-angle-down fa-icons text-18"></i>
          </div>
        </div>
         <%-- <div class="col-sm-3" id="divStudentFilter" style="display:none;">
          <div class="bmd-form-group is-filled">
            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_StudentName %></label>
            <select id="ddlStudentSearch" class="form-control">
                            
                        </select>
            <i class="fa fa-angle-down fa-icons text-18"></i>
          </div>
        </div>--%>
         
        <div class="col-sm-12">  
         <ul class="nav nav-pills" style="text-align: center;">
              <%-- <li class="active panel-menu" id="DailyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Daily%></a></li>--%>
              <%--<li class="panel-menu" id="WeeklyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Weekly%></a></li>--%>
              <li class="panel-menu" id="MonthlyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Monthly%></a></li>
                <%--<li class="panel-menu" id="YearlyContainer"><a data-toggle="pill" href="#"><%=Resources.Resource.Lbl_Yearly%></a></li>--%>
           
         </ul>
        </div>
        
<%--      <div class="col-sm-2 mt-15">
          <button type="button" id="btnSearchReport1" class="btn btn-primary" title=""><%=Resources.Resource.Lbl_GenerateReport %></button>
        </div>--%>
      </div>
<div class="panel">
      <% if (HttpContext.Current.Session["divtest"] == "true")  //(uType != 1)
        { %>
     <div id="testdiv" runat="server">
       <% if (HttpContext.Current.Session["uType1"].ToString() == "2")  //(uType != 1)
        { %>
           <div class="col-sm-12" >  
        <select id="ddlyear" class="form-control">
            
                        </select>
               </div>
            <% } %>  
        </div>
     <% } %>  
      <div id="ReportContainer" class="panel-body">
          
      </div>                
    </div>
    <script src="Scripts/Reports/indexAdmin.js?3"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/Reports/Common.js?v=" + JsVerion + "'><\/script>");</script>
           <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
    <script src="Scripts/bootstrap-datepicker.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.id.min.js"></script>

</div>
 </main>

    <script>

        var userType = <%=uType %>;
        
        var FormLoad = true;
  function noSunday(date) {
        var day = date.getDay();
        return [(day > 0), ''];
    };


        $('#datepicker').datepicker({
            autoclose: true,
            beforeShowDay: noSunday,
            language: localStorage.getItem('loglang')

        });
      
        var date = new Date();
        $('#datepicker').datepicker('setDate', date);

    </script>

    <% } %>



      </asp:Content>