﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="TeacherAssignSubstitute.aspx.cs" Inherits="TeacherAssignSubstitute" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <link id="bs-css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/css/bootstrap-datepicker.css" rel="stylesheet">
  <%--  <link rel="Stylesheet" href=" " "text="" javascript""="" src=""https://code.jquery.com/jquery-1.12.3.js""> " target="_blank">https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" />  --%>

    <!-- Add User -->
   <div id="viewprofile" class="modal fade custom-modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><%=Resources.Resource.Title_UserProfile %></h4>
            </div>
            <div class="modal-body ">
                <form class="custom-material">
                    
                    <div class="col-md-10 right-content no-pad">
                        <div class="col-md-8">
                            <div class="row mt-20">
                                <div class="col-md-12">
                                    <strong class="pr-15 mb-10" style="display:block;" id="viewName"></strong>
                                    
                                </div>
                            </div>
                            
                            <div class="row mt-10">
                                <div class="col-md-12">
                                    <i class="fa fa-map-marker site-color pr-15" aria-hidden="true"></i><span class="info-content" id="viewAddress"></span>
                                </div>
                            </div>
                            <div class="row mt-10">
                                <div class="col-md-12">
                                    <i class="fa fa-comments site-color pr-15" aria-hidden="true"></i><span class="info-content" id="viewNotes"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="viewprofileMap" style="width: 100%; height:280px"></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<div id="AssignSubstituteModel" class="modal fade custom-modal" role="dialog"  data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg custom-material" >
        <div class="modal-content" >
           
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Lbl_AddAssignSubstitute %></h4>
            </div>
            <div class="modal-body custom-scroll mh-400">
                <form class="custom-material">
                       
                 <div class="row mb-10">
                <div class="col-sm-3">
                    <div class="bmd-form-group">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarFromDate %></label>
                        <input type="text" class="form-control date-icon hasDatepicker" id="dtStartDate">
                        <i class="fa fa-calendar fa-icons"></i>
                        <input type="hidden" id="hdnUserID" value="0" />
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="bmd-form-group">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_CalendarToDate %></label>
                        <input type="text" class="form-control date-icon hasDatepicker" id="dtEndDate">
                        <i class="fa fa-calendar fa-icons"></i>
                    </div>
                </div>
                <div class="col-sm-4" style="display:none;">
                    <div class="bmd-form-group is-filled">
                        <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SchoolName %></label>
                        <select id="ddlSchool" class="form-control select-validate">
                        </select>
                        <i class="fa fa-angle-down fa-icons text-18"></i>
                    </div>
                </div>
                     <div class="col-sm-4">
                             <div class="bmd-form-group is-filled">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_ClassName %></label>
                                <select id="ddlClassRoom" class="form-control select-validate">
                                </select>
                                <i class="fa fa-angle-down fa-icons text-18"></i>
                            </div>
                            </div>
               
               
            </div>
                        <div class="row mb-10">
                             
                             <div class="col-sm-4">
                             <div class="bmd-form-group is-filled">
                                <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectSubstituteTeacher %></label>
                                <select id="ddlTeacherName" class="form-control select-validate">
                                </select>
                                <i class="fa fa-angle-down fa-icons text-18"></i>
                            </div>
                            </div>
                             <div class="col-md-2 ">
                                <div class="bmd-form-group">
                                 <div class="col-md-12 text-right">
                     <button type="button" class="btn btn-primary mr-15" title="" id="btnAssignSubstitute"> <%=Resources.Resource.Lbl_Save %> </button>
                </div>
                                 </div>
                           </div>
                        </div>

                       
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                
            </div>
        </div>
    </div>
</div>


       <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
           

            <div class="row mb-10 mt-10">

                <div class="col-sm-4">
             <h1 class="page-header"><%=Resources.Resource.Title_AssignSubstitute %></h1>
        </div>
                <div class="col-md-8">
                    <button type="button" class="btn btn-primary pull-right" title="" data-toggle="modal" id="btnAddAssignSubstitute"><%=Resources.Resource.Lbl_AddAssignSubstitute %></button>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper sort-table">
                    <table id="dataTableAssignSubstitute" class="table table-hover mb-0 user-tbl" style="width:100%">
                        <thead>
                            <tr>
                                <th ><span><%=Resources.Resource.Lbl_SerialNo %></span></th>   
                                <th ><%=Resources.Resource.Lbl_CalendarFromDate %></th>
                                <th><%=Resources.Resource.Lbl_CalendarToDate %></th>
                                <%--<th style="width:150px;"><span><%=Resources.Resource.Lbl_SchoolName %></span></th>--%>
                                <th><%=Resources.Resource.Lbl_ClassName %></th>
                                <th><%=Resources.Resource.Lbl_TeacherName %></th>
                                <th><%=Resources.Resource.Lbl_Action%></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </main>
    <script src="Scripts/bootstrap-datepicker.js"></script>
    <script>document.write("<script type='text/javascript' src='Scripts/TeacherAssignSubstitute.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
        
    <script>
        var Lbl_AddAssignSubstitute = '<%=Resources.Resource.Lbl_AddAssignSubstitute %>'
        var Lbl_EditAssignSubstitute = '<%=Resources.Resource.Lbl_EditAssignSubstitute %>'
        var Lbl_SelectSubstituteTeacher = '<%=Resources.Resource.Lbl_SelectSubstituteTeacher %>'

        $('#dtStartDate').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
        var date = new Date(); //'2018-01-01'
        $('#dtStartDate').datepicker('setDate', date);

        $('#dtEndDate').datepicker({
            autoclose: true,
            format: 'dd/mm/yyyy'
        });
        var date = new Date()
        $('#dtEndDate').datepicker('setDate', date);

    </script>

</asp:Content>

