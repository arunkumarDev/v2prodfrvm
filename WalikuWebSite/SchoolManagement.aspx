﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="SchoolManagement.aspx.cs" Inherits="SchoolManagement" %>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
<div id="SchoolManagementModel" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg custom-material">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="txtHeader"><%=Resources.Resource.Lbl_AddSchool %></h4>
            </div>
            <div class="modal-body custom-scroll mh-500">
                <form class="custom-material">
                    <div class="col-md-3 w-190">
                        <div class="image-view">
                            <img src="images/user2.png" id="imgPhoto"  class="img-responsive img-border" alt="preview">
                            <input type="file" id="txtImage" name="txtImage" class="custom-file-input preview-icon">
                             <input type="text" id="hdnImage" name="hdnImage" style="display: none" />
                            <a href="javascript:;" class="reset-photo"><%=Resources.Resource.Lbl_UploadPhoto %></a>
                        </div>
                    </div>
                    <div class="col-md-9 right-content">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                     <input type="hidden" id="hdnSchoolID" value="0" />
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_SchoolName %></label>
                                    <input type="text" id="txtSchoolName" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group is-filled">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SelectAdmin %></label>
                                    <select id="ddlAdmin" class="form-control">
                                        
                                     </select>
                                    <i class="fa fa-angle-down fa-icons text-18"></i>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Email %></label>
                                    <input type="email" id="txtEmail" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Phone %> 1</label>
                                    <input type="text" id="txtPhone1" onkeypress="return isPhoneNumber(event,this)" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Phone %> 2</label>
                                    <input type="text" id="txtPhone2" onkeypress="return isPhoneNumber(event,this)"  class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_City %></label>
                                    <input type="text" id="txtCity" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Country %></label>
                                    <input type="text" id="txtCountry" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating required"><%=Resources.Resource.Lbl_Address %></label>
                                    <input type="text" id="txtAddress" class="form-control input-validate">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="bmd-form-group">
                                    <label class="bmd-label-floating"><%=Resources.Resource.Lbl_Note %></label>
                                    <textarea class="form-control mt-10" id="txtNote"></textarea>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group mcus-search">
                                            <input type="text" class="form-control" placeholder="<%=Resources.Resource.Lbl_Search %>" name="srch-term"
                                                 id="txtSearch">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary" type="button" id="btnLocationSearch"><i class="glyphicon glyphicon-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="bmd-form-group">
                                            <label class="bmd-label-floating"><%=Resources.Resource.Lbl_SearchResult %></label>
                                            <input type="text" id="txtSearchResult" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-8">
                              <%--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d26360909.888257876!2d-113.74875964478716!3d36.242299409623534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x54eab584e432360b%3A0x1c3bb99243deb742!2sUnited+States!5e0!3m2!1sen!2sin!4v1545830068358"
                                    width="100%" height="150" frameborder="0" style="border:0" allowfullscreen></iframe>--%>
                                  <div id="dvMap" style="width: 100%; height:150px"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-default mr-15" title="" data-dismiss="modal"> <%=Resources.Resource.Lbl_Cancel %> </button>
                    <button type="button" class="btn btn-primary" title=""  id="btnSaveSchool">  <%=Resources.Resource.Lbl_Save %> </button>
                </div>
            </div>
        </div>
    </div>
</div>

<main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="page-header"> <%=Resources.Resource.Title_SchoolManagement %></h1>
                </div>
            </div>
            <div class="row">
                <!-- <div class="col-md-3 mb-5">
                    <div class="search-form"><span class="bmd-form-group"><input type="text" placeholder="Search..."
                                class="form-control"></span>
                        <i class="fa fa-search fa-icons"></i>
                    </div>
                </div> -->
                <div class="col-md-12 mb-5 text-right">
                    <button type="button" id="btnAddSchool" class="btn btn-primary " title="" data-toggle="modal"><%=Resources.Resource.Lbl_AddSchool %></button>
                </div>
            </div>
            <div class="page-content">
                <div class="data-table-wrapper">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th style="width:40px;"><%=Resources.Resource.Lbl_SerialNo %></th>
                                            <th><%=Resources.Resource.Lbl_SchoolName %></th>
                                            <th><%=Resources.Resource.Lbl_Phone %></th>
                                            <th><%=Resources.Resource.Lbl_Address %></th>
                                            <th style="width:100px;"><%=Resources.Resource.Lbl_Action %></th>
                                        </tr>
                                    </thead>
                                   <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV6jv36xrNX23a9RWk-9ZsCSCOmSU-iys"></script>
        <script>document.write("<script type='text/javascript' src='Scripts/SchoolManagement.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>


  <script>

      var Lbl_AddSchool = '<%=Resources.Resource.Lbl_AddSchool %>'
       var Lbl_EditSchool = '<%=Resources.Resource.Lbl_EditSchool %>'

     $(document).ready(function () {
        //  $('#dataTable').DataTable();
             LoadMap();
        });

    var map;
    var marker;
      var geocoder;
      var markers = [];
     
    function LoadMap() {
	  geocoder = new google.maps.Geocoder();
	   var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
           // center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: latlng
        };
        map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

       google.maps.event.addListener(map, 'click', function (e) {
            $("#txtSearchResult").val(e.latLng.lat().toFixed(3) + "/" + e.latLng.lng().toFixed(3));              
       });
       // SetMarker(0);
        setDefaultMapLocation();
    };

          function setDefaultMapLocation() {
               searchMapLocation('Waikabubak, Indonesia');
              FnGooglMaps.clearMarkers(markers);
          }

      function setLocation(latlng) {
        //  map.clearOverlays();
              if (latlng != "") {
                  var myLatlng = latlng.split("/");
                   var data =  {                     	
                        "lat": myLatlng[0],	
                        "lng": myLatlng[1]	                      
                  }
					SetMapMarker(data);
              }
              else
                    setDefaultMapLocation();
          }
      function SetMapMarker(data) {
          FnGooglMaps.clearMarkers(markers);
        var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        marker = new google.maps.Marker({
            position: myLatlng,
            map: map
          //  title: data.title
        });
          markers.push(marker);  
        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow({
							//content: data.title 
        });   

        google.maps.event.trigger(map, "resize");
         map.panTo(marker.getPosition());
         map.setZoom(14);
	    // map.setCenter(marker.getPosition());   
    };
	
	
	$("#btnLocationSearch").click(function(){
		//alert("The paragraph was clicked.");
        var address = $("#txtSearch").val();
        var latlng = address.split("/");
        if (latlng.length == 2) {
            try {          
                setLocation(address);
                e.preventDefault();                
            }
            catch (e) {}
        }
        else {
            searchMapLocation(address);
        }

		 searchMapLocation(address);
	});
	
	function searchMapLocation(address)
    {	
      
		  getMapLatLang(address);
	}

        function getMapLatLang(address) {
            //alert('sd1');
            // Define address to center map to
            if (address == "") {
                address = 'Waikabubak, Indonesia';
            }
            geocoder.geocode({
                'address': address
            }, function (results, status) {

                if (status == google.maps.GeocoderStatus.OK) {
                    $("#txtSearchResult").val(results[0].geometry.location.lat().toFixed(3) + "/" + results[0].geometry.location.lng().toFixed(3));
                   
				  var data =  {
                        "title": address,	
                        "lat": results[0].geometry.location.lat().toFixed(3),	
                        "lng": results[0].geometry.location.lng().toFixed(3),	
                        "description": ''
                    }
					SetMapMarker(data);
                } else {
                  
                }
            });
      }

    

    </script>

    </asp:Content>

