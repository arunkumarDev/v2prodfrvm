﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using WalikuWebSite.DataAccess;
using WalikuWebSite.Entities.DataModels;

namespace WalikuWebSite
{
    public partial class CalendarConfigureWeekdays : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static List<ConfigureWeekdays> LoadConfigureWeekdays()
        {
            try
            {
                List<ConfigureWeekdays> configureWeekdayslist = ConfigureWeekdaysDataAccess.LoadConfigureWeekdays();
                return configureWeekdayslist;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        [WebMethod]
        public static List<SchoolMaster> GetMySchools()
        {
            return SqlHelper.GetSchoolListForUser(Common.CommonFunction.GetUserIdFromCookieByID(), Common.CommonFunction.GetCurrentCulture());
        }
    }
}