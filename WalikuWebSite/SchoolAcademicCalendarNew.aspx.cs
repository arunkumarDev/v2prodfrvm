﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using WalikuWebSite.DataAccess;
using System.Collections;

public partial class SchoolAcademicCalendarNew : System.Web.UI.Page
    {
    private static List<DateTime> calendarDate = new List<DateTime>();
    private static List<DateTime> calendarToDate = new List<DateTime>();
    private static List<string> calendarEventName = new List<string>();

    public object ASPXToPDF1 { get; private set; }

    protected void Page_Load(object sender, EventArgs e)
        {
        //HttpContext.Current.Session["SchoolID"] = 0;
            if (!Page.IsPostBack)
            {
                if (!Common.CommonFunction.IsUserLoggedIn())
                {
                    Response.Redirect("login");
                }
           //string schoolID = HttpContext.Current.Session["SchoolID"].ToString();
           // GetAcademicCalendarList(Convert.ToInt32(schoolID));
        }

        }

    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        //LabelAction.Text = "Date changed to :" + Calendar1.SelectedDate.ToShortDateString();
        //string schoolID = HttpContext.Current.Session["SchoolID"].ToString();
        //GetAcademicCalendarList(Convert.ToInt32(schoolID));
    }

    protected void Calendar1_VisibleMonthChanged(object sender, MonthChangedEventArgs e)
    {
        //var userId = document.getElementById("ddlSchoolSearch");
        //string strValue = Page.Request.Form["ddlSchoolSearch"].ToString();
        //string sValu = ddlSchoolSearch.Value.ToString();
        string schoolID = HttpContext.Current.Session["SchoolID"].ToString();
        //GetAcademicCalendarList(Convert.ToInt32(schoolID));
    }

    [WebMethod]
   // private static void GetAcademicCalendarList(int schoolID)
    public static List<SchoolAcademicCalendarMaster> GetSchoolAcademicCalendarList(int SchoolID)
    {
        var LangID = Common.CommonFunction.GetCurrentCulture();
        var uid = Common.CommonFunction.GetUserIdFromCookieByID();
        using (SqlConnection sqlCnn = new SqlConnection(Common.SqlQueries.CONNECTION_STRING))
        {
            DataSet ds = new DataSet();
            sqlCnn.Open();

            calendarDate = new List<DateTime>();
            calendarToDate = new List<DateTime>();
            calendarEventName = new List<string>();

            using (SqlCommand sqlCmd = new SqlCommand(Common.SqlQueries.GetSchoolAcademicCalendarList, sqlCnn))
            {
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.Parameters.Add(new SqlParameter("@UserID", SqlDbType.Int)).Value = uid;
                sqlCmd.Parameters.Add(new SqlParameter("@SchoolID", SqlDbType.Int)).Value = SchoolID;
                sqlCmd.Parameters.Add(new SqlParameter("@LangId", SqlDbType.Char)).Value = LangID;

                using (SqlDataAdapter adapter = new SqlDataAdapter(sqlCmd))
                {
                    adapter.Fill(ds, "SchoolAcademicCalendarList");
                }

                sqlCmd.Dispose();

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    //{
                    //    calendarDate.Add((DateTime)ds.Tables[0].Rows[i][6]);
                    //    calendarEventName.Add((string)ds.Tables[0].Rows[i][3]);
                    //    calendarToDate.Add((DateTime)ds.Tables[0].Rows[i][9]);
                    //}
                    List<SchoolAcademicCalendarMaster> lst = ds.Tables[0].ToList<SchoolAcademicCalendarMaster>();
                    return lst;
                }

            }
        }
        return new List<SchoolAcademicCalendarMaster>();
    }
    //[WebMethod]
    //public static string GetSchoolAcademicCalendarList(int ID)
    //{
    //    HttpContext.Current.Session["SchoolID"] = ID;
    //    GetAcademicCalendarList(ID);
       
    //    return "Success";
    //    //return AcademicCalendarDataAccess.GetSchoolAcademicCalendarList(uid, ID);
    //}
    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        //var id = HttpContext.Current.Session["SchoolID"].ToString();
        //GetAcademicCalendarList(Convert.ToInt32(id));
        Literal l = new Literal(); //Creating a literal  
        l.Visible = true;
        l.Text = "<br/>"; //for breaking the line in cell  
        e.Cell.Controls.Add(l); //adding in all cell  
        string strEventName = string.Empty;

        //if (IsEventDay(e.Day.Date, out strEventName))
        //{
        //    //e.Cell.BackColor = System.Drawing.Color.Pink;

        //    Label lblEventName = new Label();
        //    lblEventName.Visible = true;
        //    lblEventName.Text = strEventName;
        //    lblEventName.Font.Size = new FontUnit(FontSize.Small);
        //    string a = lblEventName.Text;
        //    e.Cell.Controls.Add(lblEventName);
        //    e.Day.IsSelectable = false;
        //    e.Cell.ToolTip = strEventName;
        //}

        for (int i = 0; i < calendarDate.Count; i++)
        {
            DateTime stratDate = calendarDate[i]; 
            DateTime endDate = calendarToDate[i];
            if (e.Day.Date >= stratDate && e.Day.Date <= endDate)
            {
                Label lblEventName = new Label();
                lblEventName.Visible = true;
                lblEventName.Text = calendarEventName[i];
                lblEventName.Font.Size = new FontUnit(FontSize.Small);
                string a = lblEventName.Text;
                e.Cell.Controls.Add(lblEventName);
                e.Day.IsSelectable = false;
                e.Cell.ToolTip = calendarEventName[i];
            }
        }


    }

    private bool IsEventDay(DateTime day, out string strEventName)
    {
        strEventName = string.Empty;
        for (int i = 0; i < calendarDate.Count; i++)
        {
            if (calendarDate[i] == day)
            {
                strEventName = calendarEventName[i];
                return true;
            }
        }
        return false;
    }
}
