﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.UI;
using WalikuWebSite.App_Start;

namespace WalikuWebSite
{
    public partial class SiteMaster : MasterPage
    {
        public string DefaultLanguage = "";
        public int uType = 0;
        public int UserID = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
              
                LoadEvent();
                if (UserID == 0)
                {
                    UserID = Common.CommonFunction.GetUserIdFromCookieByID();
                }

                var myUser = SqlHelper.GetUserByID(UserID);
                if (myUser.Tables[0].Rows.Count > 0)
                {
                    uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
                }

            }
            // Thread.CurrentThread.CurrentCulture = new CultureInfo("id-ID");

            SetLanguage(GetLanguageFromCookie());
        }

        private string GetLanguageFromCookie()
        {
           string Language = Common.CommonFunction.GetUserLanguageFromCookie();

            if (Language != "")
            {
                string[] str = Language.Split('-');
                DefaultLanguage = (str.Length == 2) ? DefaultLanguage = str[0] : "en";               
            }
            else
            {
                Language = "en-us";
            }
            Session["Language"] = Language;

            Session["CurrentCulture"] = DefaultLanguage;// Common.CommonFunction.GetCurrentCulture();
           
            return Language;
        }

        private void SetLanguage(string Language)
        {
            if (Language != null)
            {
                string Lang = Language.ToString();
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Lang);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(Lang);
                Page.Culture = Lang;
            }
            else
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-us");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-us");
                Page.Culture = "en-us";
            }
        }

        private void LoadEvent()
        {
            string userName = Common.CommonFunction.GetUserNameFromCookieByID();
            h5UserName.InnerText = userName;
            if (Common.CommonFunction.GetUserImageFromCookieByID().Trim() != "")
                userImage.Src = "/Photos/" + Common.CommonFunction.GetUserImageFromCookieByID();
            else
                userImage.Src = "/images/user.png";
            SetPageheader();

        }

        void btnLogout_Click(Object sender,
                          EventArgs e)
        {
            DoLogout();
        }

        private void DoLogout()
        {
            HttpCookie myCookie = new HttpCookie(Common.Constant.CookieName);
            myCookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(myCookie);
            HttpContext.Current.Response.Redirect("Login.aspx");
            Session["Language"] = null;
            Session["CurrentCulture"] = null;

            Session.Abandon();
        }
       

        private void SetPageheader()
        {
            // spnName.InnerText = HttpContext.Current.Request.Cookies[Common.Constant.CookieName][Common.Constant.CookieNameKey];

            string strPageName = Page.AppRelativeVirtualPath;
            switch (strPageName.ToLower())
            {
                case "~/usermanagement.aspx":
                   // spnHeaderName.InnerText = "User Management";
                    //liUserManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/default.aspx":
                  //  spnHeaderName.InnerText = "DashBoard";
                    liDashBoard.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/classmanagement.aspx":
                   // spnHeaderName.InnerText = "Class Management";
                    //liClassManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/classteachermanagement.aspx":
                  //  spnHeaderName.InnerText = "Assign Teacher";
                    //liClassTeacherManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/schoolmanagement.aspx":
                   // spnHeaderName.InnerText = "School Management";
                    liSchoolManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/firstaid.aspx":
                    //  spnHeaderName.InnerText = "First Aid";
                   // liFirstAid.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/reasonmanagement.aspx":
                   // spnHeaderName.InnerText = "Reason Management";
                   // liReasonManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/feedback.aspx":
                   // spnHeaderName.InnerText = "Feedback";
                    //liFeedback.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/assignadmin.aspx":
                    // spnHeaderName.InnerText = "AssignAdmin";
                   // liAssignAdmin.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/academicmanagement.aspx":
                    // spnHeaderName.InnerText = "AcademicManagement";
                   // liAcademicManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/academictermdetails.aspx":
                    // spnHeaderName.InnerText = "AssignAdmin";
                    liAcademicTermDetails.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;

                case "~/absencemanagement.aspx":
                    // spnHeaderName.InnerText = "AssignAdmin";
                    //liAbsenceManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                case "~/mapoverview.aspx":
                    // spnHeaderName.InnerText = "AssignAdmin";
                    //liMapsReportManagement.Attributes.Add("class", "m-menu_item has-submenu is-active");
                    break;
                default:
                    break;
            }

            // Nascondiamo le pagine che non possiamo vedere
            var myUser = SqlHelper.GetUserByID(Common.CommonFunction.GetUserIdFromCookieByID());
            if (myUser != null)
            {
                if (myUser.Tables[0].Rows.Count == 0)
                {
                    DoLogout();
                }

                var uType = int.Parse(myUser.Tables[0].Rows[0]["UserType"].ToString());
                
                switch (uType)
                {
                    // Super Admin
                    case 4:
                        //liClassTeacherManagement.Visible = false;
                        //liClassManagement.Visible = false;
                        liAcademicCalendar.Visible = true;
                        divassignadmin.Visible = true;
                        divconweekdays.Visible = false;
                        liReasonManagement.Visible = true;
                        liFirstAid.Visible = true;
                        liFeedback.Visible = true;
                        divaddschool.Visible = true;
                        liAdminOperations.Visible = true;
                        divholidays.Visible = false;
                        diveditclass.Visible = false;
                        //divTrackStudent.Visible = false;
                        divassignmbllogin.Visible = false;
                        liStaffAttendance.Visible = false;
                        break;
                    // Community Worker
                    case 3:
                        DeleteInactiveStudents.Visible = false;
                        //liClassTeacherManagement.Visible = false;
                        liSchoolManagement.Visible = false;
                        //liClassManagement.Visible = false;
                        //liReasonManagement.Visible = false;
                        //liFirstAid.Visible = false;
                        //liUserManagement.Visible = false;
                        //liMapsReportManagement.Visible = true;
                        //liFeedback.Visible = false;
                        liAcademicTermDetails.Visible = false;
                        liAcademicCalendar.Visible = false;
                        liStaffAttendance.Visible = false;
                        divassignadmin.Visible = false;
                        divconweekdays.Visible = false;
                        liReasonManagement.Visible = false;
                        liFirstAid.Visible = false;
                        liFeedback.Visible = false;
                        divaddschool.Visible = false;
                        diveditclass.Visible = false;
                        divholidays.Visible = false;
                        //divTrackStudent.Visible = false;
                        divassignmbllogin.Visible = false;
                        break;
                    // Teacher
                    case 2:
                        DeleteInactiveStudents.Visible = false;
                        //liClassTeacherManagement.Visible = false;
                        //liSchoolManagement.Visible = false;
                        //liClassManagement.Visible = false;
                        //liReasonManagement.Visible = false;
                        //liFirstAid.Visible = false;
                        //liUserManagement.Visible = false;
                        //liFeedback.Visible = false;
                        //liAcademicManagement.Visible = false;
                        //liAssignAdmin.Visible = false;
                        liAcademicTermDetails.Visible = false;
                        liAcademicCalendar.Visible = false;
                        liStaffAttendance.Visible = false;
                        divassignadmin.Visible = false;
                        divconweekdays.Visible = false;
                        liReasonManagement.Visible = false;
                        liFirstAid.Visible = false;
                        liFeedback.Visible = false;
                        divaddschool.Visible = false;
                        divholidays.Visible = false;
                        //divTrackStudent.Visible = false;
                        diveditclass.Visible = false;
                        divassignmbllogin.Visible = false;
                        break;
                    // Admin
                    case 1:
                        DeleteInactiveStudents.Visible = false;
                        liAdminOperations.Visible = false;
                        //liFeedback.Visible = false;
                        //liFirstAid.Visible = false;
                        //liAssignAdmin.Visible = false;
                        //liReasonManagement.Visible = false;
                        liAcademicTermDetails.Visible = true;
                        divassignadmin.Visible = false;
                        divconweekdays.Visible = false;
                        liReasonManagement.Visible = false;
                        liFirstAid.Visible = false;
                        liFeedback.Visible = false;
                        divaddschool.Visible = false;                       
                        divholidays.Visible = false;
                        //divTrackStudent.Visible = true;
                        diveditclass.Visible = true;
                        divassignmbllogin.Visible = true;
                        break;

                }
            }

        }
    }
}