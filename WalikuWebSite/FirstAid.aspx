﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="FirstAid.aspx.cs" Inherits="FirstAid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="Server">
    <!-- Modal -->
    <main class="l-main custom-material">
        <div class="content-wrapper content-wrapper--with-bg">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="page-header"><%=Resources.Resource.Title_FirstAid %></h1>
                </div>
                <div class="col-md-6 mb-5 text-right">
                    <button id="btnCounter" type="button" class="btn btn-primary" title=""><%=Resources.Resource.Lbl_ViewCounter %></button>
                </div>
            </div>
            <div class="row">
                <!-- <div class="col-md-3 mb-5">
                    <div class="search-form"><span class="bmd-form-group"><input type="text" placeholder="Search..."
                                class="form-control"></span>
                        <i class="fa fa-search fa-icons"></i>
                    </div>
                </div> -->
            </div>
            <div class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="nav nav-stacked" id="accordion1">

                            <%-- <li class="panel"> <a data-toggle="collapse" data-parent="#accordion1" href="#fa2" class="collapsed">1.
                                   <%=Resources.Resource.Lbl_BasicFirstAid %></a>
                                <ul id="fa2" class="collapse fa-content">
                                    <li>
                                        <iframe id="GeneralPrinciples" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen width="500" height="375";">
</iframe>
                                       </li>
                                </ul>
                            </li>--%>

                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa17" class="collapsed">1.
                                    <%=Resources.Resource.Lbl_Unresponsive %></a>
                                <ul id="fa17" class="collapse fa-content">
                                    <li>
                                        <iframe id="Unresponsive" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>

                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa1">2.
                                    <%=Resources.Resource.Lbl_AbdominalPain %></a>
                                <ul id="fa1" class="collapse in fa-content">
                                    <li>
                                        <iframe id="AbdominalPain" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>

                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa3" class="collapsed">3.
                                    <%=Resources.Resource.Lbl_Burn %></a>
                                <ul id="fa3" class="collapse fa-content">
                                    <li>
                                        <%--  <iframe id="Burns" src="https://research.google.com/pubs/archive/44678.pdf"  frameborder="0" 
                                            webkitallowfullscreen mozallowfullscreen allowfullscreen width="500" height="375";">
                                        </iframe>--%>

                                        <iframe id="Burns" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>

                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa4" class="collapsed">4.
                                    <%=Resources.Resource.Lbl_Chocking %></a>
                                <ul id="fa4" class="collapse fa-content">
                                    <li>
                                        <iframe id="Choking" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa5" class="collapsed">5.
                                    <%=Resources.Resource.Lbl_Cough %></a>
                                <ul id="fa5" class="collapse fa-content">
                                    <li>
                                        <iframe id="Cough" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa6" class="collapsed">6.
                                    <%=Resources.Resource.Lbl_Diarrhea %></a>
                                <ul id="fa6" class="collapse fa-content">
                                    <li>
                                        <iframe id="SickChild" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa7" class="collapsed">7.
                                    <%=Resources.Resource.Lbl_EyeComplaint %></a>
                                <ul id="fa7" class="collapse fa-content">
                                    <li>
                                        <iframe id="EyeComplaints" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa8" class="collapsed">8.
                                    <%=Resources.Resource.Lbl_Fainting %></a>
                                <ul id="fa8" class="collapse fa-content">
                                    <li>
                                        <iframe id="Fainting" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa9" class="collapsed">9.
                                    <%=Resources.Resource.Lbl_Fever %></a>
                                <ul id="fa9" class="collapse fa-content">
                                    <li>
                                        <iframe id="Fever" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa10" class="collapsed">10.
                                    <%=Resources.Resource.Lbl_HeadInjury %></a>
                                <ul id="fa10" class="collapse fa-content">
                                    <li>
                                        <iframe id="HeadInjury" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa11" class="collapsed">11.
                                    <%=Resources.Resource.Lbl_MenstrualCramps %></a>
                                <ul id="fa11" class="collapse fa-content">
                                    <li>
                                        <iframe id="MenstrualCramps" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa12" class="collapsed">12.
                                    <%=Resources.Resource.Lbl_RashAllergicReaction %></a>
                                <ul id="fa12" class="collapse fa-content">
                                    <li>
                                        <iframe id="RashAndAllergicReaction" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa13" class="collapsed">13.
                                    <%=Resources.Resource.Lbl_Seizure %></a>
                                <ul id="fa13" class="collapse fa-content">
                                    <li>
                                        <iframe id="Seizure" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <%-- <li class="panel"> <a data-toggle="collapse" data-parent="#accordion1" href="#fa14" class="collapsed">15.
                                    <%=Resources.Resource.Lbl_SkingInjuries %></a>
                                <ul id="fa14" class="collapse fa-content">
                                    <li>
                                        <iframe id="Bleeding" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen width="500" height="375";">
</iframe>
                                       </li>
                                </ul>
                            </li>--%>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa14" class="collapsed">14.
                                    <%=Resources.Resource.Lbl_Sprain %></a>
                                <ul id="fa14" class="collapse fa-content">
                                    <li>
                                        <iframe id="Sprains" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>
                            <li class="panel"><a data-toggle="collapse" data-parent="#accordion1" href="#fa15" class="collapsed">15.
                                    <%=Resources.Resource.Lbl_Vomiting %></a>
                                <ul id="fa15" class="collapse fa-content">
                                    <li>
                                        <iframe id="Vomitting" style="border: 1px solid #666CCC" title="PDF in an i-Frame"
                                            frameborder="1" scrolling="auto" height="1100" width="850"></iframe>
                                    </li>
                                </ul>
                            </li>


                        </ul>

                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Add User -->
    <div id="view-count" class="modal fade custom-modal" role="dialog" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><%=Resources.Resource.Lbl_ViewCounter %></h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="CounterTable" class="table table-hover mb-0 " role="grid">
                            <thead>
                                <tr>
                                    <th style="width: 60px;"><%=Resources.Resource.Lbl_SerialNo %></th>
                                    <th style="width: 250px;"><%=Resources.Resource.Lbl_UserName %></th>
                                    <th style="width: 300px"><%=Resources.Resource.Lbl_SchoolName %></th>
                                    <th style="width: 150px"><%=Resources.Resource.Lbl_Date %></th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>document.write("<script type='text/javascript' src='Scripts/FirstAid.js?v=" + JsVerion + "'><\/script>");</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

    <script>
        $(document).ready(function () {
            var Siteurl = location.protocol + '//' + location.hostname;

            var lang = localStorage.getItem('loglang');
            if (lang == "en-us") {
                $("#Burns").attr("src", "Pdf/Burn Flowchart-en.pdf");
                $("#AbdominalPain").attr("src", "Pdf/Abdominal Pain Flowchart-en.pdf");
                $("#Choking").attr("src", "Pdf/Choking Flowchart-en.pdf");
                $("#Cough").attr("src", "Pdf/Cough Flowchart-en.pdf");
                $("#SickChild").attr("src", "Pdf/Sick Child Management Flowchart-en.pdf");
                $("#EyeComplaints").attr("src", "Pdf/Eye Injuries Flowchart-en.pdf");
                $("#Fainting").attr("src", "Pdf/Fainting Flowchart-en.pdf");
                $("#Fever").attr("src", "Pdf/Fever Flowchart-en.pdf");
                $("#HeadInjury").attr("src", "Pdf/Head Injury Flowchart-en.pdf");
                $("#MenstrualCramps").attr("src", "Pdf/Menstrual Cramps Flowchart-en.pdf");
                $("#RashAndAllergicReaction").attr("src", "Pdf/Skin Injuries Flowchart-en.pdf");
                $("#Seizure").attr("src", "Pdf/Seizure Flowchart-en.pdf");
                $("#Sprains").attr("src", "Pdf/Sprain Flowchart-en.pdf");
                $("#Vomitting").attr("src", "Pdf/Vomiting Flowchart-en.pdf");
                $("#Unresponsive").attr("src", "Pdf/Unresponsive Flowchart-en.pdf");

            }
            else {
                  $("#Burns").attr("src", "Pdf/Burn Flowchart-Id.pdf");
                $("#AbdominalPain").attr("src", "Pdf/Abdominal Pain Flowchart-Id.pdf");
                $("#Choking").attr("src", "Pdf/Choking Flowchart-Id.pdf");
                $("#Cough").attr("src", "Pdf/Cough Flowchart-Id.pdf");
                $("#SickChild").attr("src", "Pdf/Sick Child Management-Flowchart-Id.pdf");
                $("#EyeComplaints").attr("src", "Pdf/Eye Injuries Flowchart-Id.pdf");
                $("#Fainting").attr("src", "Pdf/Fainting Flowchart-Id.pdf");
                $("#Fever").attr("src", "Pdf/Fever Flowchart-Id.pdf");
                $("#HeadInjury").attr("src", "Pdf/Head Injury Flowchart-Id.pdf");
                $("#MenstrualCramps").attr("src", "Pdf/Menstrual Cramps Flowchart-Id.pdf");
                $("#RashAndAllergicReaction").attr("src", "Pdf/Skin Injuries Flowchart-Id.pdf");
                $("#Seizure").attr("src", "Pdf/Seizure Flowchart-Id.pdf");
                $("#Sprains").attr("src", "Pdf/Sprain Flowchart-Id.pdf");
                $("#Vomitting").attr("src", "Pdf/Vomiting Flowchart-Id.pdf");
                $("#Unresponsive").attr("src", "Pdf/Unresponsive Flowchart-Id.pdf");
            }

            //Arun commented on Dec 7 2020

              <%--$("#AbdominalPain").attr("src", "//docs.google.com/gview?url=" + Siteurl+ "/Pdf-New/Abdominal Pain Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");

            // $("#GeneralPrinciples").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/GeneralPrinciples_<%=LangID%>.pdf &embedded=true");
            $("#Burns").attr("src", "Pdf/Burn Flowchart-NEW-FINAL - English (1)_en.pdf");
            
            //$("#Burns").attr("src", "//docs.google.com/gview?url=" + Siteurl + "/Pdf-New/Burn Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#Choking").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Choking Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#Cough").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Cough Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#SickChild").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Sick Child Management-FINAL - English_<%=LangID%>.pdf &embedded=true");

            //$("#Diarrhea").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Diarrhea_<%=LangID%>.pdf &embedded=true");

            $("#EyeComplaints").attr("src", "//docs.google.com/gview?url=" + Siteurl + "/Pdf-New/Eye Injuries Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#Fainting").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Fainting Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#Fever").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Fever Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#HeadInjury").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Head Injury Flowchart-NEW-FINAL - English_<%=LangID%>.pdf &embedded=true");
            $("#MenstrualCramps").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Menstrual Cramps Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#RashAndAllergicReaction").attr("src", "//docs.google.com/gview?url=" + Siteurl + "/Pdf-New/Skin Injuries Flowchart-NEW-MR - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#Seizure").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Seizure Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");

            // $("#Bleeding").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Bleeding_<%=LangID%>.pdf &embedded=true");

            $("#Sprains").attr("src", "//docs.google.com/gview?url=" + Siteurl + "/Pdf-New/Sprain Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
            $("#Vomitting").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Vomiting Flowchart-NEW-FINAL - English (1)_<%=LangID%>.pdf &embedded=true");
//console.log(location.hostname);
              $("#Unresponsive").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf-New/Unresponsive Flowchart-NEW-FINAL - Unresponsive - English (1)_<%=LangID%>.pdf &embedded=true");
           --%>

          <%--  $("#AbdominalPain").attr("src", "//docs.google.com/gview?url=" + Siteurl+ "/Pdf/AbdominalPain_<%=LangID%>.pdf &embedded=true");
            $("#GeneralPrinciples").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/GeneralPrinciples_<%=LangID%>.pdf &embedded=true");
            $("#Burns").attr("src", "//docs.google.com/gview?url=" + Siteurl + "/Pdf/Burns_<%=LangID%>.pdf &embedded=true");
            $("#Choking").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Choking_<%=LangID%>.pdf &embedded=true");
            $("#Cough").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Cough_<%=LangID%>.pdf &embedded=true");
            $("#Diarrhea").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Diarrhea_<%=LangID%>.pdf &embedded=true");
            $("#EyeComplaints").attr("src", "//docs.google.com/gview?url=" + Siteurl + "/Pdf/EyeComplaints_<%=LangID%>.pdf &embedded=true");
            $("#Fainting").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Fainting_<%=LangID%>.pdf &embedded=true");
            $("#Fever").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Fever_<%=LangID%>.pdf &embedded=true");
            $("#HeadInjury").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/HeadInjury_<%=LangID%>.pdf &embedded=true");
            $("#MenstrualCramps").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/MenstrualCramps_<%=LangID%>.pdf &embedded=true");
            $("#RashAndAllergicReaction").attr("src", "//docs.google.com/gview?url=" + Siteurl + "/Pdf/RashAndAllergicReaction_<%=LangID%>.pdf &embedded=true");
            $("#Seizure").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Seizure_<%=LangID%>.pdf &embedded=true");
            $("#Bleeding").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Bleeding_<%=LangID%>.pdf &embedded=true");
            $("#Sprains").attr("src", "//docs.google.com/gview?url="  + Siteurl +"/Pdf/Sprains_<%=LangID%>.pdf &embedded=true");
            $("#Vomitting").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Vomitting_<%=LangID%>.pdf &embedded=true");
//console.log(location.hostname);
              $("#Unresponsive").attr("src", "//docs.google.com/gview?url="  + Siteurl + "/Pdf/Unresponsive_<%=LangID%>.pdf &embedded=true");
           --%>
        })

    </script>

</asp:Content>

