﻿using System;

namespace WalikuScheduler.Entities
{
    public class Absentees
    {
        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int ChildId { get; set; }

        public int date_diff { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }

    }
}
