﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WalikuScheduler.DataAccess;
using WalikuScheduler.Entities;
using WalikuScheduler.Common;

namespace WalikuScheduler.TwilioActivities
{
    public class AbsenteesList
    {
        SqlDatabaseHelper SqlDb;

        public AbsenteesList()
        {
            SqlDb = new SqlDatabaseHelper();
        }

        public List<Absentees> GetData(DateTime FromDate, DateTime ToDate, int ElapsedDays)
        {
            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("@FromDate", FromDate);
                param.Add("@ToDate", ToDate);
                param.Add("@Days", ElapsedDays);

                DataSet ds = SqlDb.ExecuteProcedureAsyncAndGetData("Usp_Rpt_TwilioSchdular_AbsenteeList", param).Result;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    List<Absentees> lst =  ds.Tables[0].ToList<Absentees>();
                    ds.Dispose();
                    return lst;
                }
                ds.Dispose();
                return new List<Absentees>();
            }
            catch (Exception ex)
            {
                throw ex;               
            }
        }


    }
}
