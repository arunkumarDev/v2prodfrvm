﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WalikuScheduler.Entities;
using WalikuScheduler.TwilioActivities;

namespace WalikuScheduler.Controller
{    
    public class HomeController : ControllerBase
    {
        // GET: Home
        [Route("/")]
        [HttpGet("[action]")]
        public string Index()
        {
            return "API Connected";
        }

        [Route("[action]")]
        public void RunScheduler()
        {

            JobStorage.Current = new SqlServerStorage(AppConfig.ConnectionString);
            var storage = JobStorage.Current;

            /* Message triggering Stopped temporarily as per client request   */
          //  BackgroundJob.Enqueue(() => WalikuMessageOutJob());
            BackgroundJob.Enqueue(() => WalikuVoiceCallOutJob());

        }

        public static void WalikuMessageOutJob()
        {
            WalikuMessageOut walikuMessageOut = new WalikuMessageOut();
            RecurringJob.AddOrUpdate(() => walikuMessageOut.SendMessage(), "0 8 * * MON-SAT", TimeZoneInfo.Utc);
        }

        public static void WalikuVoiceCallOutJob()
        {
            WalikuVoiceCallOut walikuVoiceCallOut = new WalikuVoiceCallOut();
            RecurringJob.AddOrUpdate(() => walikuVoiceCallOut.MakeVoiceCall(), "0 8 * * MON-SAT", TimeZoneInfo.Utc);
        }

       
    }
}