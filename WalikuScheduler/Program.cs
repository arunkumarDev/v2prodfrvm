﻿using System;
using System.Collections.Generic;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using WalikuScheduler.TwilioActivities;

namespace WalikuScheduler
{
    public class Program
    {
        public static void Main(string[] args)
        {

          //  JobStorage.Current = new SqlServerStorage(@"Server=jdaf4vbdsu.database.windows.net; Database = Waliku_Dev; User Id=pharmadev;Password=f5SXGf!4r; MultipleActiveResultSets = true;");

           //  var storage = JobStorage.Current;

            //BackgroundJob.Enqueue(() => WalikuMessageOutJob());
            //BackgroundJob.Enqueue(() => WalikuVoiceCallOutJob());

            CreateWebHostBuilder(args).Build().Run();

        }

        //public static void WalikuMessageOutJob()
        //{
        //    WalikuMessageOut walikuMessageOut = new WalikuMessageOut();
        //    RecurringJob.AddOrUpdate(() => walikuMessageOut.SendMessage(), Cron.Minutely);
        //}

        //public static void WalikuVoiceCallOutJob()
        //{
        //    WalikuVoiceCallOut walikuVoiceCallOut = new WalikuVoiceCallOut();
        //    RecurringJob.AddOrUpdate(() => walikuVoiceCallOut.MakeVoiceCall(), Cron.Minutely);
        //}

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<AppConfig>();
    }
}
