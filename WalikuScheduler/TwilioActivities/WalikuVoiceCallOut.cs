﻿using System;
using System.Collections.Generic;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Studio.V1;
using Twilio.Rest.Studio.V1.Flow;
using Twilio.Types;
using WalikuScheduler.DataAccess;
using WalikuScheduler.Entities;

namespace WalikuScheduler.TwilioActivities
{ 
    public class WalikuVoiceCallOut
    {
        public string MakeVoiceCall()
        {
            string response = "FAILED";
            ErrorLog Err = new ErrorLog();
            try
            {
                var x = AppConfig.TwilioMessageAccountSid;

                string accountSid = AppConfig.TwilioVoiceAccountSid;    // "AC5be7fb4980318f7ee30a2a5493013f0a";
                string authToken = AppConfig.TwilioVoiceAuthToken;  //"13de42364cb255f5bb4785905b521623";

                AbsenteesList ObjAbsenteesList = new AbsenteesList();
                List<Absentees> AbsenteeList = ObjAbsenteesList.GetData(DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7),1);

                if (AbsenteeList.Count > 0)
                {
                    TwilioClient.Init(accountSid, authToken);
                  
                    foreach (Absentees Absentee in AbsenteeList)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(Absentee.Phone.Trim()))
                            {
                                continue;
                            }

                            //var call = CallResource.Create(
                            //    url: new Uri(AppConfig.TwilioVoiceUrl),
                            //    to: new PhoneNumber(Absentee.Phone),
                            //    from: new PhoneNumber(AppConfig.TwilioVoiceFromNumber)
                            //);

                            var call = ExecutionResource.Create(
                            to: new PhoneNumber(Absentee.Phone),
                            from: new PhoneNumber(AppConfig.TwilioVoiceFromNumber),
                            pathFlowSid: AppConfig.TwilioVoicePathSid
                          );
                        }
                        catch (Exception e)
                        {
                            Err.LogError(300, "WalikuMessageOut.SendMessage :: ChildID :: " + Absentee.ChildId, e.StackTrace);
                        }
                    }
                }

                response = "SUCCESS";
            }
            catch (Exception ex)
            {
                //TODO
                Err.LogError(301, "WalikuMessageOut.SendMessage", ex.StackTrace);
                string error = ex.Message;
            }

            return response;
        }
    }

    //public string MakeVoiceCall()
    //{
    //    string response = "FAILED";
    //    ErrorLog Err = new ErrorLog();

    //    try
    //    {
    //        string accountSid = Startup.TwilioVoiceAccountSid; //  "AC5be7fb4980318f7ee30a2a5493013f0a";
    //        string authToken = Startup.TwilioVoiceAuthToken;// "13de42364cb255f5bb4785905b521623";

    //        TwilioClient.Init(accountSid, authToken);

    //        var listofFlows = FlowResource.Fetch(pathSid: "FW55b6a2097ae48bb281fd64aee5cc2905");

    //        var call = CallResource.Create(
    //                             url: new Uri("http://demo.twilio.com/docs/voice.xml"),
    //                             to: new PhoneNumber("+14158537224"),
    //                             from: new PhoneNumber("+919962206454")
    //                         );





    //        response = "SUCCESS";
    //    }
    //    catch (Exception ex)
    //    {
    //        //TODO
    //        //Log error in Database
    //        string error = ex.Message;
    //    }

    //    return response;
    //}
}

