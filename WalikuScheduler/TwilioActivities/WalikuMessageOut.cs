﻿using System;
using System.Collections.Generic;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Rest.Studio.V1;
using Twilio.Types;
using WalikuScheduler.DataAccess;
using WalikuScheduler.Entities;
using Twilio.Rest.Studio.V1.Flow;

namespace WalikuScheduler.TwilioActivities
{
    public class WalikuMessageOut
    {
        public string SendMessage()
        {
                       
            string response = "FAILED";
            ErrorLog Err = new ErrorLog();           
            try
            {
                var x = AppConfig.TwilioMessageAccountSid;

                string accountSid = AppConfig.TwilioMessageAccountSid; // "AC5be7fb4980318f7ee30a2a5493013f0a";
                string authToken = AppConfig.TwilioMessageAuthToken; //"13de42364cb255f5bb4785905b521623";

                //var message = MessageResource.Create(
                //    to: new PhoneNumber("+14158537224"),
                //    from: new PhoneNumber("+919962206454"),
                //    body: "Test message from hCue");

                AbsenteesList ObjAbsenteesList = new AbsenteesList();
                List<Absentees> AbsenteeList = ObjAbsenteesList.GetData(DateTime.Now.AddDays(-7), DateTime.Now.AddDays(7), 3);

                if (AbsenteeList.Count > 0)
                {
                    TwilioClient.Init(accountSid, authToken);
                 
                //    var listofFlows = FlowResource.Fetch(pathSid: AppConfig.TwilioMessagePathSid);    //"FW55b6a2097ae48bb281fd64aee5cc2905"

                    foreach (Absentees Absentee in AbsenteeList)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(Absentee.Phone.Trim()))
                            {
                                continue;
                            }
                            //var message = MessageResource.Create(
                            //    body: "Test msg from Twilio",
                            //    from: new Twilio.Types.PhoneNumber(AppConfig.TwilioMessageFromNumber),
                            //    to: new Twilio.Types.PhoneNumber(Absentee.Phone)
                            //);

                            var message = ExecutionResource.Create(
                                 to: new Twilio.Types.PhoneNumber(Absentee.Phone),
                                 from: new Twilio.Types.PhoneNumber(AppConfig.TwilioMessageFromNumber),
                                 pathFlowSid: AppConfig.TwilioMessagePathSid
                              );
                            
                        }
                        catch (Exception e)
                        {
                            Err.LogError(200, "WalikuMessageOut.SendMessage :: ChildID :: " + Absentee.ChildId, e.StackTrace);
                        }
                    }
                }

                response = "SUCCESS";
            }
            catch (Exception ex)
            {
                //TODO
                Err.LogError(201,"WalikuMessageOut.SendMessage", ex.StackTrace);
                string error = ex.Message;                
            }

            return response;
        }
    }
}
