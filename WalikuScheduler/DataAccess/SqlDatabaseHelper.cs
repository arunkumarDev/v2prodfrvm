﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace WalikuScheduler.DataAccess
{
    public class SqlDatabaseHelper
    {
        
        string _connection = string.Empty;

        public SqlDatabaseHelper()
        {
            _connection = GetConnectionString();
        }

        private string GetConnectionString()
        {
            return AppConfig.ConnectionString;
        }    
        
        public async Task<DataSet> ExecuteProcedureAsyncAndGetData(string ProcName, Dictionary<string, object> Parameters)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection conn = new SqlConnection(_connection))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(ProcName, conn))
                    {                      
                        cmd.CommandType = CommandType.StoredProcedure;
                        foreach (KeyValuePair<string, object> item in Parameters)
                        {
                            cmd.Parameters.AddWithValue(item.Key, GetParameterValue(item.Value));
                        }
                        using (SqlDataAdapter sda = new SqlDataAdapter(cmd))
                        {                            
                            await Task.Run(() => sda.Fill(ds));
                            return ds;
                        }                        
                    }
                    
                }
                
            }
            catch (SqlException ex)
            {
                string s = string.Join("&", Parameters.Select((x) => x.Key + "=" + x.Value.ToString()));
                new ErrorLog().LogError(ex.Number, ProcName + " " + s, ex.Message);
                return ds;
            }
        }

        public async Task<bool> NonQueryAsyc(string query, Dictionary<string, object> parameters)
        {
            using (SqlConnection conn = new SqlConnection(_connection))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand(query, conn))
                {
                    try
                    {
                        cmd.CommandType = CommandType.Text; ;
                        foreach (KeyValuePair<string, object> item in parameters)
                        {                            
                            cmd.Parameters.AddWithValue(item.Key, GetParameterValue(item.Value));
                        }
                        dynamic result = await cmd.ExecuteNonQueryAsync();
                        return true;                    }
                    catch (SqlException ex)
                    {
                        throw;
                    }
                }

            }
        }

        private object GetParameterValue(object value)
        {
            return value != null ? value : DBNull.Value;
        }
    }
}
