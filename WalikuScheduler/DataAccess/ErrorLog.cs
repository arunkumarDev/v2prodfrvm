﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WalikuScheduler.DataAccess
{
    public class ErrorLog
    {
        SqlDatabaseHelper SqlDb;

        public ErrorLog()
        {
            SqlDb = new SqlDatabaseHelper();
        }

        public void LogError(int ErrorNumber,string Description, string ErrorMessage)
        {
            var x = LogErrorToDataBase(ErrorNumber,Description, ErrorMessage).Result;
        }

        private async Task<bool> LogErrorToDataBase(int ErrorNumber,string Description,string ErrorMessage)
        {
            if (ErrorMessage == null)
                return true; 

            try
            {
                Dictionary<string, object> param = new Dictionary<string, object>();

                string SQL = $"INSERT INTO Schedular_Log (Description,ErrorMessage,CreatedOn,ErrorNumber) VALUES (@Description, @ErrorMessage, @CreatedOn,@ErrorNumber)";
                param.Add("@Description", Description);
                param.Add("@ErrorMessage", ErrorMessage);
                param.Add("@CreatedOn", DateTime.Now);
                param.Add("@ErrorNumber", ErrorNumber);
                
                var result = await SqlDb.NonQueryAsyc(SQL, param);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}
