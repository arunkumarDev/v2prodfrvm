﻿using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WalikuScheduler.Controller;
using WalikuScheduler.Filters;

namespace WalikuScheduler
{
    public class AppConfig
    {
        public IConfigurationRoot Configuration
        {
            get;
            set;
        }
        public static string ConnectionString
        {
            get;
            private set;
        }
        public static dynamic TwilioMessageAccountSid  { get; private set; }
        public static dynamic TwilioMessageAuthToken { get; private set; }
        public static dynamic TwilioMessagePathSid { get; private set; }
        public static dynamic TwilioMessageFromNumber { get; private set; }

        public static dynamic TwilioVoiceAccountSid { get; private set; }
        public static dynamic TwilioVoiceAuthToken { get; private set; }
        public static dynamic TwilioVoicePathSid { get; private set; }
        public static dynamic TwilioVoiceFromNumber { get; private set; }
        public static dynamic TwilioVoiceUrl { get; private set; }



        public AppConfig(IHostingEnvironment env)
        {
            Configuration = new ConfigurationBuilder().SetBasePath(env.ContentRootPath).AddJsonFile("appSettings.json").Build();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            ConnectionString = Configuration["Data:ConnectionStrings:DefaultConnection"];
            //  services.AddHangfire(x => x.UseSqlServerStorage(@"Data Source=jdaf4vbdsu.database.windows.net;Database=Waliku_Dev; User Id=pharmadev; Password=f5SXGf!4r; MultipleActiveResultSets=true;"));
            services.AddHangfire(x => x.UseSqlServerStorage(ConnectionString));
            services.AddMvc();
            services.AddTransient<HomeController, HomeController>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var applicationLifetime = app.ApplicationServices.GetRequiredService<IApplicationLifetime>();

            TwilioMessageAccountSid = Configuration["Data:Twilio:MessagCredentials:AccountSid"];
            TwilioMessageAuthToken = Configuration["Data:Twilio:MessagCredentials:AuthToken"];
            TwilioMessagePathSid = Configuration["Data:Twilio:MessagCredentials:PathSid"];
            TwilioMessageFromNumber = Configuration["Data:Twilio:MessagCredentials:FromNumber"];

            TwilioVoiceAccountSid = Configuration["Data:Twilio:VoiceCredentials:AccountSid"];
            TwilioVoiceAuthToken = Configuration["Data:Twilio:VoiceCredentials:AuthToken"];
            TwilioVoicePathSid = Configuration["Data:Twilio:VoiceCredentials:PathSid"];
            TwilioVoiceFromNumber = Configuration["Data:Twilio:VoiceCredentials:FromNumber"];
            TwilioVoiceUrl = Configuration["Data:Twilio:VoiceCredentials:Url"];


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHangfireServer();
            // app.UseHangfireDashboard();

            var options = new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorizationFilter() }
            };
            app.UseHangfireDashboard("/hangfire", options);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }

       
    }

  
}
